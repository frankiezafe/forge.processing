// gestion de l'affichage d'un stock

char stock[];
char affichage[];
int sID;
float offsetx;

void setup() {
  
  size( 200, 200 );
  
  // on crée la liste à afficher, dans le bon ordre
  stock = new char[ 50 ];
  stock[ 0 ] = (char)83;
  stock[ 1 ] = (char)65;
  stock[ 2 ] = (char)76;
  stock[ 3 ] = (char)85;
  stock[ 4 ] = (char)84;
  stock[ 5 ] = (char)32;
  stock[ 6 ] = (char)67;
  stock[ 7 ] = (char)79;
  stock[ 8 ] = (char)82;
  stock[ 9 ] = (char)69;
  stock[ 10 ] = (char)78;
  stock[ 11 ] = (char)84;
  stock[ 12 ] = (char)73;
  stock[ 13 ] = (char)78;
  stock[ 14 ] = (char)32;
  stock[ 15 ] = (char)69;
  stock[ 16 ] = (char)84;
  stock[ 17 ] = (char)32;
  stock[ 18 ] = (char)77;
  stock[ 19 ] = (char)65;
  stock[ 20 ] = (char)88;
  stock[ 21 ] = (char)73;
  stock[ 22 ] = (char)77;
  stock[ 23 ] = (char)69;
  stock[ 24 ] = (char)44;
  stock[ 25 ] = (char)32;
  stock[ 26 ] = (char)86;
  stock[ 27 ] = (char)79;
  stock[ 28 ] = (char)73;
  stock[ 29 ] = (char)67;
  stock[ 30 ] = (char)73;
  stock[ 31 ] = (char)32;
  stock[ 32 ] = (char)67;
  stock[ 33 ] = (char)79;
  stock[ 34 ] = (char)77;
  stock[ 35 ] = (char)77;
  stock[ 36 ] = (char)69;
  stock[ 37 ] = (char)78;
  stock[ 38 ] = (char)84;
  stock[ 39 ] = (char)32;
  stock[ 40 ] = (char)79;
  stock[ 41 ] = (char)78;
  stock[ 42 ] = (char)32;
  stock[ 43 ] = (char)70;
  stock[ 44 ] = (char)65;
  stock[ 45 ] = (char)73;
  stock[ 46 ] = (char)84;
  stock[ 47 ] = (char)32;
  stock[ 48 ] = (char)32;
  stock[ 49 ] = (char)32;

  // on met l'index du stock à 0
  sID = 0;
  // on crée 5 blocs à afficher
  affichage = new char[ 10 ];
  for ( int i = 0; i < affichage.length; i++ ) {
    // et on remplit l'affichage avec les 5 premiers éléments du stock
    affichage[ i ] = stock[ getNext() ];
  }
  
  offsetx = 0;
  
}

// méthode qu'on utilise à chaque fois qu'on veut l'élément suivant du stock
// elle vérifie que 'sID' ne dépasse pas la capacité du stock
int getNext() {
  int tmp = sID;
  sID++;
  if ( sID >= stock.length ) {
    sID = 0;
  }
  return tmp;
}


void draw() {
  
  background( 10 );
  
  // défilement vers la gauche
  offsetx -= 0.6;
  
  // à chaque fois qu'on est inférieur à -12:
  if ( offsetx < -12 ) {
    // on vire le premier élément de l'affichage (le 0, donc)
    // le 1 devient 0
    // le 2 devient 1 
    // et ainsi de suite
    for ( int i = 0; i < affichage.length - 1; i++ ) {
      affichage[ i ] = affichage[ i + 1 ];
    }
    // on va chercher l'élément de stock suivant, qu'on met en queue d'affichage
    affichage[ affichage.length - 1 ] = stock[ getNext() ];
    // on remonte l'offset
    offsetx += 12;
  }
  
  // et on affiche
  fill( 255 );
  for ( int i = 0; i < affichage.length; i++ ) {
    text( affichage[ i ], offsetx + 15 + i * 12, 25 );
  } 
  
}

void keyPressed() {
  
  // bout de code pour générer le stock :)
  // println( "stock[ " + sID + " ] = (char)" + keyCode +";" );
  // sID++;

}
