class Segment {

  PVector p1;
  PVector p2;
  
  public Segment( PVector p1, PVector p2 ) {
    this.p1 = p1;
    this.p2 = p2;
  }
  
  public void draw( PVector rot ) {
    
    stroke( 255, 100 );
    line( p1.x, p1.y, p1.z, p2.x, p2.y, p2.z );
    noStroke();
    fill( 255 );
    pushMatrix();
    translate( p1.x, p1.y, p1.z );
    rotateZ( rot.z );
    rotateY( rot.y );
    rotateX( rot.x );
    ellipse( 0,0, 5,5 );
    popMatrix();
    pushMatrix();
    translate( p2.x, p2.y, p2.z );
    rotateZ( rot.z );
    rotateY( rot.y );
    rotateX( rot.x );
    ellipse( 0,0, 5,5 );
    popMatrix();
    
  }
  
}
