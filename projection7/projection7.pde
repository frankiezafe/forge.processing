import java.util.*; 

// view rotation
private PVector rot;
// inverted view rotation (to draw ellipse parallel to camera)
private PVector tor;

private Plane p;

private Segment[] lines;
private Icosahedron ico;

// used in plane to calculate U & V vector
public static final PVector UP = new PVector( 0,1,0 );

private PImage tex;

public void setup() {

  size( 800, 600, P3D );
  p = new Plane( 50, -50, 50 );
  rot = new PVector();
  tor = new PVector();
  
  lines = new Segment[ 20 ];
  for ( int i = 0; i < lines.length; i++ ) {
    lines[ i ] = new Segment( 
      new PVector( random( -300, 300 ), random( -300, 300 ), random( -300, 300 ) ),
      new PVector( random( -300, 300 ), random( -300, 300 ), random( -300, 300 ) )
    );
  }
  
  ico = new Icosahedron( 350 );
  
  tex = loadImage( "texture.jpg" );
  textureWrap( REPEAT ); 
  
}

public void draw() {

  ico.move( cos( frameCount * 0.02 ) * 300, 0, 0 );
  p.update();
  
  rot.x = ( mouseY - height * 0.5) / 100;
  rot.y = ( -mouseX + width * 0.5) / 100;
  tor.set( rot );
  tor.mult( -1 );
  
  p.viewrot = tor;
  ico.viewrot = tor;
  
  background( 10 );

  pushMatrix();
  translate( width * 0.5, height * 0.5, -width * 0.5 );  
  rotateX( rot.x );
  rotateY( rot.y );
  rotateZ( rot.z );
  
  // 3D axis
  strokeWeight( 3 );
  stroke( 255, 0, 0 );
  line( 0, 0, 0, 50, 0, 0 );
  stroke( 0, 255, 0 );
  line( 0, 0, 0, 0, 50, 0 );
  stroke( 0, 0, 255 );
  line( 0, 0, 0, 0, 0, 50 );
  strokeWeight( 1 );
  
  p.draw();
  ico.draw();
  
  UVPoint proj = null;
  for ( int i = 0; i < lines.length; i++ ) {
    lines[ i ].draw( tor );
    proj = p.intersect( lines[ i ] );
    noStroke();
    if ( proj != null )
      p.drawpt( proj.x, proj.y, proj.z, 10 );
  }
  
  ArrayList< UVPoint > collected = new ArrayList< UVPoint >();
  for ( int i = 0; i < ico.edges.length; i++ ) {
    UVPoint tmpuv = p.intersect( ico.edges[ i ] );
    if ( tmpuv != null )
      collected.add( tmpuv );
  }
  if ( collected.size() > 2 ) {
    UVPoint[] colpts = collected.toArray( new UVPoint[ collected.size() ] );
    UVPoint barycenter = sortUVpts( colpts );
    UVFace[] projfaces = new UVFace[ colpts.length ];
    for ( int i = 0; i < colpts.length; i++ ) {
      int j = ( i + 1 ) % colpts.length;
      projfaces[ i ] = new UVFace( colpts[ i ], colpts[ j ], barycenter );
      projfaces[ i ].filled = true;
      projfaces[ i ].setColor( 255, 0, 0, 100 );
    }
    beginShape( TRIANGLES );
    texture( tex );
    for ( int i = 0; i < projfaces.length; i++ ) {
      projfaces[ i ].printVertices();
    }
    endShape();
  }
  
  popMatrix();
  
  fill( 255 );
  text( frameRate, 10, 25 );
  
}


