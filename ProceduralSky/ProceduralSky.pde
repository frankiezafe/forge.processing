boolean pixel_projection = true;

void setup() {
  
  size( 600,600 );
  
}

void keyPressed() {
  pixel_projection = !pixel_projection;
}

void draw() {

  float focal = ( mouseX * 1.f / width ) * 45.f / 180 * PI;
  float a = PI * 0.5 + ( ( mouseY * 1.f / height ) - 0.5 ) * PI;
  //println( a + ", " + cos(a) + ", " + sin(a) );
    
  background(0);
  
  loadPixels();

  for ( int y = 0; y < height; ++y ) {
  for ( int x = 0; x < width; ++x ) {
    
    float relx = x * 1.f / width;
    float rely = y * 1.f / height;
    
    if ( pixel_projection ) {
      
      // centered coordinates
      float cx = ( relx - 0.5 ) * 2;
      float cy = ( rely - 0.5 ) * 2;
      cx -= tan(focal) * cx * cy;
      cy *= cos( a );
      if ( cx < -1 ) { continue; }
      if ( cx > 1 ) { continue; }
      //println( x + ", " + y + ", " + cx );
      int id = int( ( cx + 1 ) * 0.5 * width ) + int( ( cy + 1 ) * 0.5 * height ) * width;
      if ( id < 0 || id >= pixels.length ) {
        continue;
      }
      int ix = int( relx * width );
      int iy = int( rely * height );
      if ( ix % 16 == 0 || iy % 16 == 0 ) {
        pixels[ id ] = color( 255, 255, 255 );
      } else {
        pixels[ id ] = color( relx * 255, rely * 255, 0 );
      }
      
    } else {
    
      int id = x + y * width;
      float cx = ( relx - 0.5 ) * 2;
      float cy = ( rely - 0.5 ) * 2;
      cx -= tan(focal) * cx * cy;
      cy /= cos( a );
      pixels[ id ] = color( ( cx + 1 ) * 0.5 * 255, ( cy + 1 ) * 0.5 * 255, 0 );
    
    }
    
  }
  }
  
  updatePixels();

  text( a / PI * 180, 10, 25 );
  text( focal / PI * 180, 10, 40 );
  text( tan(focal), 10, 55 );

}