class Boid {

  public float pos;
  public float diff;
  public float dir;
  public float speed;
  public float max_speed;
  public float diff_damping;
  public float dir_damping;
  public int fill_color;
  
  public Boid() {
    pos = 0;
    diff = 1;
    dir = 1;
    speed = 1;
    max_speed = 10;
    diff_damping = 0.3;
    dir_damping = 0.3;
    fill_color = color( 255 );
  }
  
  public void update( float f ) {
    
    diff = ( f - pos ) * diff_damping;
    dir = diff * dir_damping + dir * ( 1-dir_damping );
    if ( dir > max_speed ) {
      dir = max_speed;
    } else if ( dir < -max_speed ) {
      dir = -max_speed;
    }
    pos += dir * speed;
    
  }
  
  public void draw( float y ) {
    noStroke();
    fill(fill_color);
    ellipse( pos, y, 5, 5 );
  }

}