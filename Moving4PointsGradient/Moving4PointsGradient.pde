PVector[] colors;

Boid bo_fast;
Boid bo_regular;
Boid bo_slow;
ValueTracker[] vt_boids;

void setup() {
  
  size( 600, 600 );

  colors = new PVector[4];
  colors[0] = new PVector( 1,0,0 );
  colors[1] = new PVector( 0,1,0 );
  colors[2] = new PVector( 1,1,0 );
  colors[3] = new PVector( 0,1,1 );
  
  bo_fast = new Boid();
  bo_regular = new Boid();
  bo_slow = new Boid();
  
  bo_fast.fill_color = color( 255,0,255 );
  bo_slow.fill_color = color( 0,255,255 );
  bo_fast.dir_damping = 0.3;
  bo_fast.diff_damping = 0.8;
  bo_fast.max_speed = 25;
  bo_slow.dir_damping = 0.07;
  bo_slow.diff_damping = 0.07;
  
  vt_boids = new ValueTracker[3];
  for ( int i = 0; i < 3; ++i ) {
    vt_boids[i] = new ValueTracker( 500 );
  }
  
  vt_boids[0].stroke_color =  bo_fast.fill_color;
  vt_boids[0].rectangle[1] -= 100;
  vt_boids[1].stroke_color =  bo_regular.fill_color;
  vt_boids[1].rectangle[1] -= 50;
  vt_boids[2].stroke_color =  bo_slow.fill_color;
  
}

void draw() {
  
  background(0);

  /*
  float[] couleurA = new float[] { 0, 0, 0 };
  float[] couleurB = new float[] { 0, 0, 0 };
  float[] pcolor = new float[ 3 ];
      
  loadPixels();
  int i = 0;
  for ( int y = 0; y < height; ++y ) {
    float ry = y * 1.f / ( height - 1 );
    float ryi = 1 - ry;
    for ( int c = 0; c < 3; ++c ) {
      couleurA[ c ] = 255 * ( colors[0].array()[ c ] * ryi + colors[3].array()[ c ] * ry );
      couleurB[ c ] = 255 * ( colors[1].array()[ c ] * ryi + colors[2].array()[ c ] * ry );
    }
    for ( int x = 0; x < width; ++x ) {
      float ratio = x * 1.f / ( width - 1 );
      float oitar = 1 - ratio;
      for ( int c = 0; c < 3; ++c ) {
        pcolor[ c ] = couleurA[ c ] * oitar + couleurB[ c ] * ratio;
      }
      pixels[ i ] = color( pcolor[ 0 ], pcolor[ 1 ], pcolor[ 2 ] );
      ++i;
    } 
  }
  updatePixels();
  */
  
  bo_fast.update( mouseX );
  bo_regular.update( mouseX );
  bo_slow.update( mouseX );
  
  vt_boids[0].add( bo_fast.pos );
  vt_boids[1].add( bo_regular.pos );
  vt_boids[2].add( bo_slow.pos );
  
  bo_fast.draw( height * 0.5 - 20 );
  bo_regular.draw( height * 0.5 );
  bo_slow.draw( height * 0.5 + 20 );
  
  stroke( 255,0,0 );
  line( mouseX, 0, mouseX, height );
  
  for ( int i = 0; i < 3; ++i ) {
    vt_boids[i].draw();
  }
  
}