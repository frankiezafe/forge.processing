class ValueTracker {

  public float[] values;
  public int stroke_color;
  
  private float[] minmax;
  private int index;
  private int capacity;
  private float[] rectangle;
  
  public ValueTracker( int capacity ) {
    this.capacity = capacity;
    values = new float[ this.capacity ];
    index = 0;
    minmax = new float[2];
    minmax[0] = 0;
    minmax[1] = 0;
    rectangle = new float[4];
    rectangle[0] = 10;
    rectangle[1] = height - 60;
    rectangle[2] = width - 20;
    rectangle[3] = 50;
    stroke_color = color( 255 );
  }
  
  private void shift_down() {
    for ( int i = 1; i < capacity; ++i ) {
      values[i-1] = values[i];
    }
  }
  
  public void add( float v ) {
    
    if ( index < capacity ) {
      values[index] = v;
      index++;
    } else {
      shift_down();
      values[index-1] = v;
    }
    
    if ( minmax[0] > v ) {
      minmax[0] = v;
    }
    if ( minmax[1] < v ) {
      minmax[1] = v;
    }
    
  }
  
  public void draw() {
  
    if ( minmax[0] == minmax[1] || index < 1 ) {
      return;
    }
    
    float range = minmax[1] - minmax[0];
    float gapx = rectangle[2] / ( capacity - 1 );
    
    noFill();
    stroke( stroke_color );
    float prevx = rectangle[0];
    float prevy = rectangle[1] + ( ( 1 - ( values[0] - minmax[0] ) / range ) * rectangle[3] );
    for ( int i = 1; i < index; ++i ) {
      float y = rectangle[1] + ( ( 1 - ( values[i] - minmax[0] ) / range ) * rectangle[3] );
      line(
        prevx, prevy,
        prevx + gapx, y
      );
      prevx += gapx;
      prevy = y;
    }
  
  }
  
}