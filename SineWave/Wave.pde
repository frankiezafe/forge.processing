class Wave {
  
  private float localdephas = 0;
  private float localdephas_speed = 0;
  private float[] yvalues;
  private float dx = 1;
  private float x = 0;
  private int period;
  
  public Wave( int definition ) {
    period = 1;
    yvalues = new float[ definition ];
    dx = PI * 2 / yvalues.length;
    x = 0;
    for (int i = 0; i < yvalues.length; i++) {
      yvalues[i] = sin(x);
      x += dx;
    }
  }
  
  public Wave( int definition, int period ) {
    this.period = abs(period);
    yvalues = new float[ definition ];
    dx = (PI * 2 / yvalues.length) * (float) period;
    x = 0;
    for (int i = 0; i < yvalues.length; i++) {
      yvalues[i] = sin(x);
      x += dx;
    }
  }
  
  public void setSpeed( float speed ) {
    localdephas_speed = speed;
  }
  
  public float getValue() {
    return getValue(0);
  }
  
  public float getValue( float dephas ) {
    float reldephas =  (localdephas + dephas) * yvalues.length;
    while (reldephas < 0) { reldephas += yvalues.length; }
    while (reldephas > yvalues.length ) { reldephas -= yvalues.length; }
    int pointbelow = (int) floor( reldephas );
    if ( pointbelow >= yvalues.length )
      pointbelow = 0;
    int pointabove = pointbelow + 1;
    if ( pointabove >= yvalues.length )
      pointabove = 0;
    // need to calculate the distance between the 2 points
    float pc = reldephas - pointbelow;
    return ( yvalues[pointbelow] * (1-pc) ) + ( yvalues[pointabove] * pc );
  }
  
  public void update() {
     localdephas += localdephas_speed;
  }
  
  public void render( float offsetx, float offsety, float w, float h ) {
        render( offsetx, offsety, w, h, 0 );
  }
  
  public void render( float offsetx, float offsety, float w, float h, float dephas ) {
    
    noFill();
    
    float prevx = 0;
    float h2 = h * 0.5f;
    
    // frame
    stroke(0, 0, 0);
    rect(offsetx, offsety, w, h);
    line( offsetx, offsety + h2, offsetx + w, offsety + h2 );
    
    // sine
    stroke(255, 0, 0);
    w /= PI * 2 * period;
    x = 0;
    
    // real current point:
    float reldephas =  (localdephas + dephas) * yvalues.length;
    while (reldephas < 0) { reldephas += yvalues.length; }
    while (reldephas > yvalues.length ) { reldephas -= yvalues.length; }
    // println( "relative dephas: " + (dephas / yvalues.length) );
    int pointbelow = (int) floor( reldephas );
    if ( pointbelow >= yvalues.length )
      pointbelow = 0;
    int pointabove = pointbelow + 1;
    if ( pointabove >= yvalues.length )
      pointabove = 0;
    // need to calculate the distance between the 2 points
    float pc = reldephas - pointbelow;
    float x = ( 1 - pc ) * dx;
    float currentyv =  ( yvalues[pointbelow] * (1-pc) ) + ( yvalues[pointabove] * pc );
    // float currentyv = getCurrentValue(dephas);
    
    int currenti = pointabove;
    int lasti = -1;
    
    for ( int i = 0; i <= yvalues.length; i++ ) {
      
      if ( i > 0 && i < yvalues.length ) {
        // drawing normal lines
        line( 
            offsetx + prevx * w,
            offsety + h2 + yvalues[lasti] * h2, 
            offsetx + x * w, 
            offsety + h2 + yvalues[currenti] * h2
            );
      } else if ( i == 0 ) {
        // drawing the end of the first line
        line(
            offsetx + prevx * w,
            offsety + h2 + currentyv * h2, 
            offsetx + x * w, 
            offsety + h2 + yvalues[currenti] * h2
            );
      } else if ( i == yvalues.length ) {
        // drawing the beginning of the last line
        x -= (1 - pc) * dx;
        float nexty =  ( yvalues[lasti] * ( 1 - pc ) ) + ( yvalues[currenti] * pc );
        line(
            offsetx + prevx * w,
            offsety + h2 + yvalues[lasti] * h2, 
            offsetx + x * w, 
            offsety + h2 + nexty * h2
            );
      }
      prevx = x;
      x += dx;
      
      lasti = currenti;
      currenti++;
      if (currenti >= yvalues.length)
        currenti = 0;
      
    }
    
    // drawing the actual value
    fill( 255, 0, 0 );
    noStroke();
    ellipse( offsetx, offsety + h2 + currentyv * h2, 5, 5 );
  
  }
  
}
