PImage src;
PGraphics tex;
float tex_size[];
float uv_scale[];
float uv_offset[];
PVector map_scale = new PVector(1,1,1);
ArrayList<PVector> verts;
ArrayList<PVector> uvs;
PShape dots;
PShape mesh;
float rotx = 0;
float rotz = 0;
int last_time = -1;
int bg_color = color(80);
float elapsed_time = 0;
PShader shad;
float color2float = 0.003921569;

PVector mouse_view = new PVector();
PVector mouse_scale = new PVector();
PVector last_mouse = new PVector();

String paths[] = { "heightmap.jpg", "terrain.htmlcd_Image18.png", "vtest.png" };
int current_path = 0;

void load_map( String path ) {
  
  src = loadImage( path );
  
  tex_size = new float[] { src.width * 2, src.height * 2 };
  tex = createGraphics( int(tex_size[0]), int(tex_size[1]) );
  tex.beginDraw();
  tex.background(0);
  tex.translate( src.width, src.height );
  tex.image( src, -src.width, 0 );
  tex.scale( -1, 1 );
  tex.image( src, -src.width, 0 );
  tex.scale( 1, -1 );
  tex.image( src, -src.width, 0 );
  tex.scale( -1, 1 );
  tex.image( src, -src.width, 0 );
  tex.endDraw();
  
  verts = new ArrayList<PVector>();
  uvs = new ArrayList<PVector>();
  
  src.loadPixels();
  float hw = src.width * 0.5;
  float hh = src.height * 0.5;
  for ( int y = 0; y < src.height; y++ ) {
    for ( int x = 0; x < src.width; x++ ) {
      PVector vert = new PVector( 
        (-hw + x) * map_scale.x, 
        (-hh + y) * map_scale.y, 
        0 );
      PVector uv = new PVector( 
        float(x) / src.width,
        float(y) / src.height
        );
      verts.add( vert );
      uvs.add( uv );
    }
  }

  dots = createShape();
  dots.beginShape(POINTS);
  dots.stroke( 255 );
  dots.strokeWeight( 1 );
  for ( int i = 0; i < verts.size(); ++i ) {
    PVector v = verts.get(i);
    PVector u = uvs.get(i);
    dots.vertex( v.x, v.y, v.z, u.x, u.y );
  }
  dots.endShape();
  
  mesh = createShape();
  mesh.beginShape(TRIANGLES);
  mesh.noStroke();
  mesh.fill(255);
  for ( int y = 1; y < src.height; y++ ) {
  for ( int x = 1; x < src.width; x++ ) {
    int id0 = (x-1) + (y-1) * src.width;
    int id1 = (x-0) + (y-1) * src.width;
    int id2 = (x-0) + (y-0) * src.width;
    int id3 = (x-1) + (y-0) * src.width;
    add_vertuv( id0 );
    add_vertuv( id1 );
    add_vertuv( id2 );    
    add_vertuv( id0 );
    add_vertuv( id2 );
    add_vertuv( id3 );
  }
  }
  mesh.endShape();
  
  uv_scale = new float[] {1,1};
  uv_offset = new float[] {0,0};
  
}

void add_vertuv( int id ) {
    PVector v = verts.get(id);
    PVector u = uvs.get(id);
    mesh.vertex( v.x, v.y, v.z, u.x, u.y );
}

void setup() {

  size(800, 600, P3D);
  textureWrap(REPEAT);
  
  shad = loadShader("shader/frag.glsl", "shader/vert.glsl");
  
  load_map( paths[current_path]);
  
}

void draw() {
  
  int now = millis();
  float delta_time = 0;
  if (last_time != -1) {
    delta_time = (now-last_time) * 0.001;
    elapsed_time += delta_time;
  }
  last_time = now;
  
  if ( mousePressed ) {
    float diffx = 20 * (mouseX - last_mouse.x) / width * 0.5;
    float diffy = 20 * (mouseY - last_mouse.y) / height * 0.5;
    uv_scale[0] = 1 + diffx;
    uv_scale[1] = 1 + diffy;
  } else {
    mouse_view.x = (mouseX-width*0.5) / (width*0.5);
    mouse_view.y = (mouseY-height*0.5) / (height*0.5);
    last_mouse.x = mouseX;
    last_mouse.y = mouseY;
  }
  
  colorMode(HSB, 360, 100, 100);
  int c0 = color( int( elapsed_time * 5 )%360, 80 + (1+sin(elapsed_time*2)) * 20, 80 );
  int c1 = color( int( 180 + elapsed_time * 5 )%360, 20, 100 );
  
  colorMode(RGB);
  float bg[] = {0.31372549,0.31372549,0.31372549,1};
  float rgb0[] = {red(c0)*color2float,green(c0)*color2float,blue(c0)*color2float,1};
  float rgb1[] = {red(c1)*color2float,green(c1)*color2float,blue(c1)*color2float,1};
  
  uv_offset[0] += 0.08 * delta_time;
  uv_offset[1] += -0.06 * delta_time;
  
  shad.set("uv_scale", uv_scale, 2);
  shad.set("uv_offset", uv_offset, 2);
  shad.set("hmap", tex);
  shad.set("hmap_size", tex_size, 2);
  shad.set("bg", bg, 4);
  shad.set("rgb0", rgb0, 4);
  shad.set("rgb1", rgb1, 4);
  shad.set("timer", elapsed_time);
  
  background(bg_color);
  
  shader(shad);
  pushMatrix();
  translate( width / 2, height / 2, 0 );
  rotx += ((PI * 0.25 * (1 + mouse_view.y)) - rotx) * 0.2;
  rotateX( rotx );
  rotz += 0.1 * pow( mouse_view.x, 3 );
  rotateZ( rotz );
  scale( 1.3, 1.3, 90 );
  //shape( dots );
  shape( mesh );
  popMatrix();
  resetShader();
  
  hint(DISABLE_DEPTH_TEST);
  fill(255);
  image( src, 10,10,100,100 );
  text( "height map", 10, 120 );
  image( tex, 10,125,100,100 );
  text( "generated texture", 10, 235 );
  text( "uv scale: " + display_float(uv_scale[0]) + ", " + display_float(uv_scale[1]), 10, 250 );
  text( "uv offset: " + display_float(uv_offset[0]) + ", " + display_float(uv_offset[1]), 10, 265 );
  text( "rgb 0: " + display_float(rgb0[0]) + ", " + display_float(rgb0[1]) + ", " + display_float(rgb0[2]), 10, 280 );
  text( "rgb 1: " + display_float(rgb1[0]) + ", " + display_float(rgb1[1]) + ", " + display_float(rgb1[2]), 10, 295 );
  text( "------------------", 10, 320 );
  text( "move mouse to orient", 10, 345 );
  text( "drag to change scale", 10, 360 );
  text( "press 'n' for next map", 10, 375 );
  hint(ENABLE_DEPTH_TEST);
  
}

void keyPressed() {
  if ( key == 'n' ) {
    current_path++;
    if ( current_path >= paths.length ) {
      current_path = 0;
    }
    load_map( paths[current_path]);
  }
}

String display_float( float f ) {
  return String.format("%.2f", f);
}
