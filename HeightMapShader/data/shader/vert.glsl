#define PROCESSING_COLOR_SHADER

uniform mat4 modelview;
uniform mat4 transform;

uniform vec2 uv_scale;
uniform vec2 uv_offset;
uniform sampler2D hmap;
uniform vec2 hmap_size;
uniform float timer;

attribute vec4 vertex;
attribute vec2 texCoord;

varying vec4 VERT;
varying vec2 UV;
varying vec2 C_UV;
varying vec4 wav;

void main() {
	
	vec2 wuv = uv_offset + texCoord;
	vec2 scale_diff = ( uv_scale - vec2(1.0,1.0) ) * -0.5;
	vec2 vuv = uv_offset + scale_diff + texCoord * uv_scale;
	
	float wave0 = sin((wuv.x*3 + timer)*2);
	float wave1 = sin((wuv.y*3.2 + timer)*1.8);
	float wave2 = pow(sin(((wuv.x+wuv.y)*5.47 + timer)*5),3);
	float wave3 = pow(sin(((wuv.y+wuv.y)*-5.98 + timer)*4.9),3);
	
	vec4 rgb = texture2D( hmap, vuv ); // * hmap_size );
	
	vec4 v = vertex;
	v += vec4( 0.0, 0.0, rgb.x, 0.0 );
	v += vec4( 0.0, 0.0, wave0 * 0.1, 0.0 );
	v += vec4( 0.0, 0.0, wave1 * 0.1, 0.0 );
	v += vec4( 0.0, 0.0, wave2 * 0.02, 0.0 );
	v += vec4( 0.0, 0.0, wave3 * 0.02, 0.0 );
	
	wav = vec4(wave0, wave1, wave2, wave3);
	
	gl_Position = transform * v;
	VERT = v;
	UV = vuv;
	C_UV = ( vec2(0.5,0.5) - texCoord ) * 2;
	
	
}