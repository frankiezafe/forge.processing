#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

//uniform sampler2D hmap;
//uniform vec2 hmap_size;

uniform vec4 bg;
uniform vec4 rgb0;
uniform vec4 rgb1;

varying vec4 VERT;
varying vec2 UV;
varying vec2 C_UV;
varying vec4 wav;

void main() {
	float d = pow( min( 1.0, ( pow(C_UV.x,2.0) + pow(C_UV.y,2.0) ) ), 3 );
	float h = (wav.x + wav.y) * -0.06;
	vec4 negc =vec4( h, h, h, 0.0 );
	gl_FragColor = mix( mix(rgb0,rgb1,VERT.z) - negc, bg, d);
	//gl_FragColor = texture2D( hmap, UV );
	
}