void setup () {
  size( 800, 600 );
}

void draw() {
  float mx = ( mouseX * 1.f / width );
  float my = ( mouseY * 1.f / height );
  background( mx * 255, 0, ( 1 - mx ) * 255 );
}