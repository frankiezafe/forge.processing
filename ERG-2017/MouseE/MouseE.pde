float w;

void setup () {
  size( 800, 600 );
  w = 0;
}

void draw() {
  if ( mousePressed ) {
    w = 255;
  } else if ( w > 0 ) {
    w--;
  }
  background( w );
}