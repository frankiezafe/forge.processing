float w;
int freq;
int val;

void setup () {
  size( 800, 600 );
  w = 0;
  freq = 0;
  val=0;
}

void draw() {
  
  freq = mouseX;
  val += freq;
  if ( val >= width * 2 ) {
    val = 0;
    if ( w == 0 ) {
      w = 255;
    } else {
      w = 0;
    }
    background( w );
  }
    
}