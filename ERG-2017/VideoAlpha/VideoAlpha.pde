import processing.video.*;
import de.bezier.guido.*;

Movie mov;
boolean new_frame;
PGraphics mov_layer;
int gridsize;

int red_key_in;
int red_key_mid;
int red_key_out;
int green_key_in;
int green_key_mid;
int green_key_out;
int blue_key_in;
int blue_key_mid;
int blue_key_out;
int threshold;

void setup() {
  size(800, 600);
  mov = new Movie(this, "imagine_the_fire.mp4");
  mov.loop();
  new_frame = false;
  mov_layer = null;
  gridsize = 20;
  
  setupUI();
}

void draw() {
  
  if ( new_frame ) {
    getUIvalues();
    new_frame = false;
    if ( mov_layer == null ) {
        mov_layer = createGraphics(mov.width, mov.height);
        println( mov_layer.width, mov_layer.height );
    }
    mov.loadPixels();
    mov_layer.beginDraw();
    mov_layer.loadPixels();
    int pxnum = mov.width * mov.height;
    for ( int i = 0; i < pxnum; ++i ) {
      
      int rgb = mov.pixels[ i ];
      int r = (0x00ff0000 & rgb) >> 16;
      int g = (0x0000ff00 & rgb) >> 8;
      int b = (0x000000ff & rgb);
      if ( 
        ( r >= red_key_in && r <= red_key_out ) || 
        ( g >= green_key_in && g <= green_key_out ) || 
        ( b >= blue_key_in && b <= blue_key_out )
        ) {
        
        mov_layer.pixels[ i ] = color( r,g,b,0 );
        continue;
        
      }
      
      if ( threshold == 0 ) {
        
        mov_layer.pixels[ i ] = color( r,g,b,255 );
        continue;
        
      }
      
      int rd = r - red_key_mid;
      if ( rd < 0 ) rd *= -1;
      if ( rd > threshold ) rd = -1;
      
      int gd = r - green_key_mid;
      if ( gd < 0 ) gd *= -1;
      if ( gd > threshold ) gd = -1;
      
      int bd = r - blue_key_mid;
      if ( bd < 0 ) bd *= -1;
      if ( bd > threshold ) bd = -1;
      
      int biggestd = rd;
      if ( gd > rd ) {
        biggestd = gd;
      }
      if ( bd > rd ) {
        biggestd = bd;
      }
      
      int a = 255;
      if ( biggestd != -1 ) {
        a = int( 255 * ( biggestd / threshold * 1.f ) );
      }
      mov_layer.pixels[ i ] = color( r,g,b,a );
      
    }
    mov_layer.updatePixels();
    mov_layer.endDraw();
  }
  
  background( 100 );
  noStroke();
  fill( 154 );
  int ynum = height / gridsize;
  int xnum = width / gridsize;
  for ( int y = 0; y < ynum; ++y ) {
  for ( int x = y % 2; x < xnum; x += 2 ) {
    rect( x * gridsize, y * gridsize, gridsize, gridsize );
  }
  }
  if ( mov_layer != null ) {
    float ratio = width * 1.f / mov_layer.width;
    int yoffset = int(height - mov_layer.height * ratio) / 2;
    image(mov_layer,0,yoffset, width, mov.height*ratio);
  }
  
  drawUI();
  
}

// Called every time a new frame is available to read
void movieEvent(Movie m) {
  m.read();
  new_frame = true;
}