public class Slider
{
    float x, y, width, height;
    float valueX = 0, value;
    boolean on;
    
    Slider ( float xx, float yy, float ww, float hh ) 
    {
        x = xx; 
        y = yy; 
        width = ww; 
        height = hh;
        
        valueX = x;
    
        Interactive.add( this );
    }
    
    void setValue( float value ) {
      this.value = value;
    }
     
    void mouseEntered ()
    {
        on = true;
    }
    
    void mouseExited ()
    {
        on = false;
    }
    
    void mouseDragged ( float mx, float my )
    {
        valueX = mx - height/2;
        
        if ( valueX < x ) valueX = x;
        if ( valueX > x+width-height ) valueX = x+width-height;
        
        value = map( valueX, x, x+width-height, 0, 1 );
        
        Interactive.send( this, "valueChanged", value );
    }
    
    public void draw ()
    {
        noStroke();
        
        fill( 0 );
        rect( x, y, width, height );
        
        fill( on ? 200 : 120 );
        rect( valueX, y, height, height );
    }
}

public class MultiSlider
{
    float x,y,width,height;
    int bg_rgb;
    int handler_rgb;
    int over_rgb;
    boolean on = false;
    
    SliderHandle left, right, activeHandle;
    
    float values[];
    
    MultiSlider ( float xx, float yy, float ww, float hh )
    {
        this.x = xx; this.y = yy; this.width = ww; this.height = hh;
        
        left  = new SliderHandle( x, y, height, height );
        right = new SliderHandle( x+width-height, y, height, height );
        
        values = new float[]{0,1};
    }
    
    void setValues( float min, float max ) {
      values[0] = min;
      values[1] = max;
      left  = new SliderHandle( x + values[0] * width, y, height, height );
      right = new SliderHandle( x + values[1] * width, y, height, height );
    }
    
    void rgb( int r, int g, int b ) {
      over_rgb = color( r,g,b );
      handler_rgb = color( r * 0.8,g * 0.8,b * 0.8 );
      bg_rgb = color( 0,0,0 );
    }
    
    void mouseEntered ()
    {
        on = true;
    }
    
    void mouseExited ()
    {
        on = false;
    }
    
    void mousePressed ( float mx, float my )
    {
        if ( left.isInside( mx, my ) ) { 
          println( "left!" );
          activeHandle = left;
        } else if ( right.isInside( mx, my ) ) { 
          println( "right!" );
          activeHandle = right;
        } else {
          println( "beuh...." );
        }
    }
    
    void mouseDragged ( float mx, float my )
    {
        if ( activeHandle == null ) return;
        
        float vx = mx - activeHandle.width/2;
        
        vx = constrain( vx, x, x+width-activeHandle.width );
        
        if ( activeHandle == left )
        {
            if ( vx > right.x - activeHandle.width ) vx = right.x - activeHandle.width;
            values[0] = map( vx, x, x+width-activeHandle.width, 0, 1 );
        }
        else
        {
            if ( vx < left.x + activeHandle.width ) vx = left.x + activeHandle.width;
            values[1] = map( vx, x, x+width-activeHandle.width, 0, 1 );
        }
        
        activeHandle.x = vx;
    }
    
    void draw ()
    {
        noStroke();
        fill( bg_rgb );
        rect( x, y, width, height );
        fill( on ? over_rgb : handler_rgb );
        rect( left.x, left.y, right.x-left.x+right.width, right.height );
    }
    
    public boolean isInside ( float mx, float my )
    {
        return left.isInside(mx,my) || right.isInside(mx,my);
    }
}

class SliderHandle
{
    float x,y,width,height;
    
    SliderHandle ( float xx, float yy, float ww, float hh )
    {
        this.x = xx; this.y = yy; this.width = ww; this.height = hh;
    }
    
    void draw ()
    {
        rect( x, y, width, height );
    }
    
    public boolean isInside ( float mx, float my )
    {
        return Interactive.insideRect( x, y, width, height, mx, my );
    }
}