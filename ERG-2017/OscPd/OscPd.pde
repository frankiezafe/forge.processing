import oscP5.*;
import netP5.*;

OscP5 oscP5;
float r, g, b, tx, ty;

void setup() {
  size( 200, 200 );
  oscP5 = new OscP5(this, 12000);
  r = 0;
  g = 0;
  b = 0;
  tx = 10;
  ty = 10;
}

void draw() {
  background( r, g, b );
  fill( 255 );
  text( frameCount, tx, ty );
}

float toFloat( OscMessage msg, int at ) {
  char c = msg.typetag().charAt( at );
  if ( c == 'i' ) {
    return msg.get(at).intValue();
  } else if ( c == 'f' ) {
    return msg.get(at).floatValue();
  } else {
    return 0;
  }
}

void oscEvent( OscMessage msg ) {

  if ( msg.addrPattern().equals( "/rgb" ) ) {
    r = toFloat(msg, 0);
    g = toFloat(msg, 1);
    b = toFloat(msg, 2);
  } else if ( msg.addrPattern().equals( "/tx" ) ) {
    tx = toFloat(msg, 0);
  } else if ( msg.addrPattern().equals( "/ty" ) ) {
    ty = toFloat(msg, 0);
  }
}