class MovingColor {
  
  public static final int hnum = 1000;
  public float value;
  public float dir;
  public float speed;
  public float[] historic;
  public boolean use_cos;
  private float angle;
  private int hcurrent;
  
  public MovingColor() {
    value = 0;
    dir = 1;
    speed = 1;
    historic = new float[ hnum ];
    use_cos = false;
    angle = 0;
    hcurrent = 0;
  }
  
  public void step() {
    if ( use_cos ) {
      
      angle += dir * speed;
      if ( angle < 0 ) { 
        angle += 255;
      }
      else if ( angle > 255 ) { 
        angle -= 255;
      }
      value = ( -cos( ( angle / 255 ) * TWO_PI ) + 1 ) * 127.5;
      
    } else {
      
      value += dir * speed;
      if ( value < 0 ) { 
        value *= -1;
        if ( dir < 0 ) dir *= -1;
      }
      else if ( value > 255 ) { 
        value = 255 - (value - 255);
        if ( dir > 0 ) dir *= -1;
      }
      
    }
    if ( hcurrent == hnum - 1 ) {
      for( int i = 0; i < hnum-1; ++i ) {
        historic[ i ] = historic[ i+1 ];
      }
    }
    if ( hcurrent < hnum - 1 ) hcurrent++;
    historic[ hcurrent ] = value;
  }
  
  public void display() {
    float gap = ( width - 20.f ) / hnum;
    pushMatrix();
      translate( 10, height - 10 );
      for ( int i = 0; i < hcurrent; ++i ) {
        line( i * gap, -historic[ i ], ( i + 1 ) * gap, -historic[ i + 1 ] );
      }
    popMatrix();
  }
  
}

MovingColor r;
MovingColor g;
MovingColor b;

boolean displayH;

void setup () {

  size( 800, 600 );
  r = new MovingColor();
  g = new MovingColor();
  b = new MovingColor();
  
  r.use_cos = true;
  r.speed = 0.5;
  //g.speed = 0;
  b.speed = 0;
  
  displayH = false;
  
}

void draw() {
  
  r.step();
  g.step();
  b.step();
  
  background( r.value, g.value, b.value );
  
  if ( !displayH ) return;
  stroke( 255,0,0 );
  r.display();
  stroke( 0,255,0 );
  g.display();
  stroke( 0,0,255 );
  b.display();
  
}

void keyReleased() {

  if ( key == ' ' ) {
    displayH = !displayH;
  }
  
}