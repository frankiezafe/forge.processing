private Plane p;
private PVector rot;
private PVector tor;
private PVector[] pt;

public void setup() {

  size( 600, 600, OPENGL );
  p = new Plane( 0, -30, 0 );
  rot = new PVector();
  tor = new PVector();
  pt = new PVector[ 2 ];
  for ( int i = 0; i < pt.length; i++ ) {
    pt[ i ] = new PVector( random( -150, 150 ), random( -150, 150 ), random( -150, 150 ) );
  }
}

public void draw() {

  //  p.abc.x = sin( frameCount * 0.001 );
  //  p.abc.x = 0;
  //  p.abc.y = 0;
  //  p.abc.z = cos( frameCount * 0.0095 );
  
  
  
  rot.x =  -0.05;
  rot.y = frameCount * 0.007;

  tor.set( rot );
  tor.mult( -1 );
  
  /*
  p.w.y = mouseX * 4.f / width;
  p.w.z = 4 - ( mouseX * 4.f / width );
  p.xyz.y = mouseY - height / 2;
  */
  
  p.w.y = sin( frameCount * 0.004 );
  p.w.z = 1 - sin( frameCount * 0.004 );
  // p.xyz.y = mouseY - height / 2;
  
  p.update();

  background( 10 );

  pushMatrix();
  translate( width * 0.5, height * 0.5, 0 );  
  rotateX( rot.x );
  rotateY( rot.y );
  rotateZ( rot.z );

  strokeWeight( 3 );
  stroke( 255, 0, 0 );
  line( 0, 0, 0, 50, 0, 0 );
  stroke( 0, 255, 0 );
  line( 0, 0, 0, 0, 50, 0 );
  stroke( 0, 0, 255 );
  line( 0, 0, 0, 0, 0, 50 );

  p.draw( tor );
  PVector[] projected = p.projectPoints( pt );
  // PVector[] sorted = p.sortPoints( projected );

  for ( int i = 0; i < pt.length; i++ ) {
    
    stroke( 255, 100 );
    line( pt[ i ].x, pt[ i ].y, pt[ i ].z, projected[ i ].x, projected[ i ].y, projected[ i ].z );
    
    stroke( 255 );
    fill( 255, 50 );
    pushMatrix();
    translate( pt[ i ].x, pt[ i ].y, pt[ i ].z );
    rotateZ( tor.z );
    rotateY( tor.y );
    rotateX( tor.x );
    ellipse( 0, 0, 3, 3 );
    popMatrix();
    
    pushMatrix();
    translate( projected[ i ].x, projected[ i ].y, projected[ i ].z );
    rotateZ( tor.z );
    rotateY( tor.y );
    rotateX( tor.x );
    ellipse( 0, 0, 5, 5 );
    popMatrix();
    
  }
  
  popMatrix();
  
}

