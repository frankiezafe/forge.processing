class Plane {

  PVector normale; // normale
  PVector v;
  PVector w;
  PVector xyz; // point

  PVector[] pts;
  float size = 500;

  float[][] mat;
  
  float[][] testpts2D;
  PVector[] testpts;

  public Plane( float x, float y, float z ) {
    v = new PVector( 1, 0, 0 );
    v.normalize();
    w = new PVector( 0, 0, 1 );
    w.normalize();
    normale = v.cross( w );
    normale.normalize();
    xyz = new PVector( x, y, z );
    mat = new float[4][4];
    pts = new PVector[4];
    testpts = new PVector[4];
    testpts2D = new float[ 4 ][ 2 ];
    for ( int i = 0; i < 4; i++ ) {
      pts[ i ] = new PVector();
      testpts[ i ] = new PVector();
      testpts2D[ i ][ 0 ] = random( -size * 0.5, size * 0.5 );
      testpts2D[ i ][ 1 ] = random( -size * 0.5, size * 0.5 );
    }
    update();
  }

  public PVector[] projectPoints( PVector[] projs ) {
    
    PVector[] output = new PVector[ projs.length ];
    float locd = xyz.dot( normale );
    for ( int i = 0; i < projs.length; i++ ) {
      float d = projs[ i ].dot( normale ) - locd;
      output[ i ] = new PVector();
      output[ i ].set( normale );
      output[ i ].mult( -d );
      output[ i ].add( projs[ i ] );
    }
    return output;
    
  }

  public void pointOnPlane( PVector pt, PVector rot ) {

    float d = pt.dot( normale ) - xyz.dot( normale ); 
    PVector tmp = new PVector();
    tmp.set( normale );
    tmp.mult( -d );
    tmp.add( pt );
    
    // display
    pushMatrix();
    strokeWeight( 1 );
    stroke( 255, 50 );
    line( pt.x, pt.y, pt.z, tmp.x, tmp.y, tmp.z );
    translate( tmp.x, tmp.y, tmp.z );
    rotateZ( rot.z );
    rotateY( rot.y );
    rotateX( rot.x );
    stroke( 255, 0, 0 );
    fill( 255, 0, 0, 100 );
    ellipse( 0, 0, 4, 4 );
    popMatrix();
    
  }
  
  
  public PVector[] sortPoints( PVector[] pts ) {
    
    PVector[] output = new PVector[ pts.length ];
    
    PVector barycenter = new PVector();
    for ( int i = 0; i < pts.length; i++ ) {
      barycenter.add( pts[ i ] );
    }
    barycenter.mult( 1.f / pts.length );
    
    pushMatrix();
    translate( barycenter.x, barycenter.y, barycenter.z );
    
    pushMatrix();
    rotateZ( tor.z );
    rotateY( tor.y );
    rotateX( tor.x );
    ellipse( 0, 0, 100, 100 );
    popMatrix();
    
    for ( int i = 0; i < pts.length; i++ ) {
      
      PVector pv = pts[ i ].cross( v );
      float dv = pts[ i ].dot( v );
      PVector pw = pts[ i ].cross( w );
      float dw = pts[ i ].dot( w );
      
      output[ i ] = new PVector();
      output[ i ].add( pv );
      output[ i ].add( pw );
      
      line( 0,0,0, output[ i ].x, output[ i ].y, output[ i ].z );
    }
    
    popMatrix();
    
    // dists
    float[] thetas = new float[ pts.length ];
    for ( int i = 0; i < pts.length; i++ ) {
      
    }
    
    return output;
    
  }


  // http://www.flipcode.com/documents/matrfaq.html
  // http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToMatrix/index.htm
  // heading(y), attitude(z) & bank(x) representation: 
  // http://www.euclideanspace.com/maths/geometry/rotations/euler/aeroplaneGlobal1.png
  // https://www.opengl.org/discussion_boards/showthread.php/159883-converting-a-3D-vector-into-three-euler-angles
  // http://stackoverflow.com/questions/21622956/how-to-convert-direction-vector-to-euler-angles

  public void update() {

    v.normalize();
    w.normalize();
    normale = v.cross( w );
    normale.normalize();

    PVector ptv = new PVector();
    PVector ptw = new PVector();
    int pIndex = 0;
    for ( float y = -0.5; y <= 0.5; y++ ) {
      for ( float x = -0.5; x <= 0.5; x++ ) {
        ptv.set( v );
        ptv.mult( x * size );
        ptw.set( w );
        ptw.mult( y * size );
        pts[ pIndex ].mult( 0 );
        pts[ pIndex ].add( ptv );
        pts[ pIndex ].add( ptw );
        pts[ pIndex ].add( xyz );
        pIndex++;
      }
    }
    for ( int i = 0; i < testpts.length; i++ ) {
      ptv.set( v );
      ptv.mult( testpts2D[ i ][ 0 ] );
      ptw.set( w );
      ptw.mult( testpts2D[ i ][ 1 ] );
      testpts[ i ].mult( 0 );
      testpts[ i ].add( ptv );
      testpts[ i ].add( ptw );
      testpts[ i ].add( xyz );
    }
    
  }
  
  public void draw( PVector rot ) {

    strokeWeight( 1 );
    stroke( 255, 127 );
    noFill();
    beginShape( QUADS );
    vertex( pts[ 0 ].x, pts[ 0 ].y, pts[ 0 ].z );
    vertex( pts[ 1 ].x, pts[ 1 ].y, pts[ 1 ].z );
    vertex( pts[ 3 ].x, pts[ 3 ].y, pts[ 3 ].z );
    vertex( pts[ 2 ].x, pts[ 2 ].y, pts[ 2 ].z );
    endShape();
    
    pushMatrix();
    translate( xyz.x, xyz.y, xyz.z );
    stroke( 255, 255, 0 );
    line( 0, 0, 0, 150 * v.x, 150 * v.y, 150 * v.z );
    stroke( 255, 0, 255 );
    line( 0, 0, 0, 150 * w.x, 150 * w.y, 150 * w.z );
    stroke( 127 );
    line( 0, 0, 0, 150 * normale.x, 150 * normale.y, 150 * normale.z );
    popMatrix();
    
    noStroke();
    for ( int i = 0; i < testpts.length; i++ ) {
      pushMatrix();
      translate( testpts[ i ].x, testpts[ i ].y, testpts[ i ].z );
      rotateZ( rot.z );
      rotateY( rot.y );
      rotateX( rot.x );
      fill( 255, 0, 255 );
      ellipse( 0,0, 4,4 );
      fill( 255 );
      text( "data: " + testpts2D[ i ][ 0 ] + "," + testpts2D[ i ][ 1 ], 10, 10 );
      PVector tmp = testpts[ i ].cross( normale );
      text( "rendered: " + tmp.x + "," + tmp.y + "," + tmp.z, 10, 25 );
      popMatrix();
    }
    
  }
  
}

