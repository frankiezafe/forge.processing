import saito.objloader.*;
import processing.video.*;

Movie mov;

PVector offset;

// declare that we need a OBJModel and we'll be calling it "model"
OBJModel model;
float rotX;
float rotY;

void setup() {
  
  size(600, 600, P3D);

  // making an object called "model" that is a new instance of OBJModel
  model = new OBJModel( this, "sphere3.obj" );

  // turning on the debug output (it's all the stuff that spews out in the black box down the bottom)
  model.enableDebug();

  noStroke();
  
  mov = new Movie(this, "transit.mov");
  mov.loop();
  
  offset = new PVector();

}


void movieEvent(Movie movie) {
  mov.read();  
}

void draw()
{
  
  model.setTexture( mov );
  
  background(128);
  lights();

  pushMatrix();
  translate( width/2 + offset.x, height/2 + offset.y, offset.z );
  rotateX(rotY);
  rotateY(rotX);

  model.draw();
  popMatrix();

  // image(mov, 0, 0);
  
}


void mouseDragged()
{
  rotX += (mouseX - pmouseX) * 0.01;
  // rotY -= (mouseY - pmouseY) * 0.01;
}

void keyPressed() {

  if ( keyCode == UP ) {
    offset.z--;
  } else if ( keyCode == DOWN ) {
    offset.z++;
  } else if ( keyCode == LEFT ) {
    offset.x--;
  } else if ( keyCode == RIGHT ) {
    offset.x++;
  }

}
