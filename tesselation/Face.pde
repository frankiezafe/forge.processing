class Face {

  PVector[] pts;
  PVector center;
  Segment[] edges;
  
  public Face( PVector p1, PVector p2, PVector p3, Segment s1, Segment s2, Segment s3 ) {
    
    pts = new PVector[ 3 ];
    pts[ 0 ] = p1;
    pts[ 1 ] = p2;
    pts[ 2 ] = p3;
    
    center = new PVector();
    for ( int i = 0; i < 3; i++ )
      center.add( pts[ i ] );
    center.mult( 1.f / 3.f );
    
    edges = new Segment[ 3 ];
    edges[ 0 ] = s1;
    edges[ 1 ] = s2;
    edges[ 2 ] = s3;
    
  }
  
  public void draw() {
  
    beginShape( TRIANGLES );
    for ( int i = 0; i < 3; i++ )
      vertex( pts[ i ].x, pts[ i ].y, pts[ i ].z );
    endShape();
  
  }

}
