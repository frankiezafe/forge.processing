import java.util.*; 

// used in plane to calculate U & V vector
public static final PVector UP = new PVector( 0,1,0 );
public PVector locked;
float selectrad = 10;
Polygon p;

public void setup() {

  size( 800, 600, P3D );
  p = new Polygon( );
  locked = null;
  
}

public void draw() {
  
  if ( p.isProcessed() ) {
    p.process();
  }
  
  background( 5 );
  p.draw();
  
  PVector pt = locked;
  if ( pt == null && p.isProcessed() ) {
    pt = p.getNear( mouseX, mouseY, selectrad );
    if ( pt == null ) {
      pt = p.getMid( mouseX, mouseY, selectrad );
    }
  }
  if ( pt != null ) {
    noFill();
    stroke( 0,255,255 );
    ellipse( pt.x, pt.y, selectrad * 2, selectrad * 2 );
  }
  
  fill( 255 );
  text( frameRate, 10, 15 );
  text( "points: " + p.pts.size(), 10, 30 );
  if ( p.isProcessed() ) {
    text( "faces: " + p.faces.size(), 10, 45 );
  }
  
}

public void mouseDragged() {
  if ( locked != null ) {
    locked.x = mouseX;
    locked.y = mouseY;
  }
}

public void mousePressed() {
  if ( p.isProcessed() ) {
    locked = p.getNear( mouseX, mouseY, selectrad );
    if ( locked == null ) {
      p.addMid( mouseX, mouseY, selectrad );
      locked = p.getNear( mouseX, mouseY, selectrad );
    }
  }
}

public void mouseReleased() {
  if ( p.isProcessed() ) {
    locked = null;
  } else {
    p.add( mouseX, mouseY );
  }
}

public void keyPressed() {
  
  if ( key == 'r' ) {
    p.removePt( p.getNear( mouseX, mouseY, selectrad ) );
  }
  
  if ( key == 'i' )
    p.init();

  if ( key == 'p' )
    p.process();
  
}
