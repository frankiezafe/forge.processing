class Segment {

  PVector p1;
  PVector p2;
  PVector dir;
  PVector out;
  
  // used when segment are part of a path
  Segment previous;
  Segment next;
  
  public Segment( PVector p1, PVector p2 ) {
    this.p1 = p1;
    this.p2 = p2;
    dir = new PVector( p2.x - p1.x, p2.y - p1.y, p2.z - p1.z );
    out = new PVector( dir.x, dir.y );
    out.normalize();
    float a = atan2( out.y, out.x ) - HALF_PI;
    out.set( cos( a ), sin( a ) );
    previous = null;
    next = null;
  }
  
  public void draw() {
    this.draw( null );
  }
  
  public void draw( PVector rot ) {
    
    stroke( 255, 100 );
    line( p1.x, p1.y, p1.z, p2.x, p2.y, p2.z );
    noStroke();
    fill( 255 );
    pushMatrix();
    translate( p1.x, p1.y, p1.z );
    if( rot != null ) { 
      rotateZ( rot.z );
      rotateY( rot.y );
      rotateX( rot.x );
    }
    ellipse( 0,0, 5,5 );
    popMatrix();
    pushMatrix();
    translate( p2.x, p2.y, p2.z );
    if( rot != null ) { 
      rotateZ( rot.z );
      rotateY( rot.y );
      rotateX( rot.x );
    }
    ellipse( 0,0, 5,5 );
    popMatrix();
    
  }
  
  // 2D!
  public void drawDir() {
    noStroke();
    fill( 255, 100 );
    float a = atan2( dir.y, dir.x );
    float l = dir.mag() - 12;
    pushMatrix();
    translate( p1.x + cos( a ) * l, p1.y + sin( a ) * l, p1.z );
    rotateZ( a );
    beginShape( TRIANGLES );
    vertex( 10, 0, 0 );
    vertex( 0, -5, 0 );
    vertex( 0, 5, 0 );
    endShape();
    popMatrix();
  }
  
  public void drawOut() {
    stroke( 255, 100 );
    float l = 30;
    line( 
      p1.x + ( p2.x - p1.x ) * 0.5,
      p1.y + ( p2.y - p1.y ) * 0.5,
      p1.z + ( p2.z - p1.z ) * 0.5,
      p1.x + ( p2.x - p1.x ) * 0.5 + out.x * l,
      p1.y + ( p2.y - p1.y ) * 0.5 + out.y * l,
      p1.z + ( p2.z - p1.z ) * 0.5 + out.z * l
      );
  }
  
}
