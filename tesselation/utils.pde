class PointDrawer {

  PVector viewrot;
  
  public PointDrawer() {
    viewrot = new PVector();
  }
  
  public void drawpt( PVector v ) {
    drawpt( v, 2 );
  }
  
  public void drawpt( PVector v, float s ) {
    drawpt( v.x, v.y, v.z, s );
  }
  
  public void drawpt( float x, float y, float z, float s ) {
    pushMatrix();
    translate( x, y, z );
    rotateZ( viewrot.z );
    rotateY( viewrot.y );
    rotateX( viewrot.x );
    ellipse( 0,0, s,s );
    popMatrix();
  }
  
}

