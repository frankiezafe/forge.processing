// 2D polygon!

class Polygon {
  
  public ArrayList< PVector > pts;
  private Segment[] edges;
  private ArrayList< Face > faces;
  private ArrayList< Segment > inneredges;
  
  private PVector beststart;
  private Segment beststarte;
  private boolean processed;
  
  ArrayList< Segment > worksgts;
  
  private Polygon sub;
  public float shrinkAmount;
  
  public Polygon( boolean havesub ) {
    if ( havesub )
      sub = new Polygon();
    else
      sub = null;
    shrinkAmount = 50;
    init();
  }
  
  public Polygon() {
    sub = null;
    shrinkAmount = 0;
    init();
  }
  
  public void init() {
    pts = new ArrayList< PVector >();
    edges = null;
    faces = null;
    inneredges = null;
    processed = false;
    if ( sub != null )
      sub.init();
  }
  
  public void add( float x, float y ) {
    if ( !processed )
      pts.add( new PVector( x, y ) );
  }
  public boolean isProcessed() {
    return processed;
  }
  
  public void process() {
    
    if ( pts.size() < 3 ) {
      println( "Impossible to process a polygon of less then 3 points!!!" );
      return;
    }
    
    worksgts = new ArrayList< Segment >();
    edges = new Segment[ pts.size() ];
    for ( int i = 0; i < pts.size(); i++ ) {
      int j = ( i + 1 ) % pts.size();
      edges[ i ] = new Segment( pts.get( i ), pts.get( j ) );
      if ( i > 0 ) {
        edges[ i - 1 ].next  = edges[ i ];
        edges[ i ].previous = edges[ i - 1 ];
      }
      if ( i == pts.size() - 1 ) {
        edges[ i ].next = edges[ 0 ];
        edges[ 0 ].previous = edges[ i ];
      }
      worksgts.add( edges[ i ] );
    }
    
    // tesselation:
    // as long as the worksgts length is bigger then 3
    // we replace 2 edges by a new one
    
    faces = new ArrayList< Face >();
    inneredges = new ArrayList< Segment >();
    
    int counter = 0;
    while ( worksgts.size() > 3 ) {
      
      float best = 100000;
      beststart = null;
      beststarte = null;
      for ( Segment s : worksgts ) {
        float d = s.dir.dot( s.next.dir );
        float d2 = s.out.dot( s.next.dir );
        if (
            best > d 
            && d2 < 0
          ) {
          beststart = s.p2;
          beststarte = s;
          best = d;
        }
      }
      
      if ( beststart == null ) {
        println( "skipped before the end!" );
        break;
      }
      
      Segment news = new Segment( beststarte.p1, beststarte.next.p2 );
      
      // registration of the next & previous for the new segment
      news.previous = beststarte.previous;
      news.next = beststarte.next.next;
      
      // adaptation of next and previous in existing segments
      news.previous.next = news;
      news.next.previous = news;
      
      inneredges.add( news );
      Face newf = new Face( 
        beststarte.p2, beststarte.next.p2, beststarte.p1, 
        beststarte, news, beststarte.previous );
      faces.add( newf );
      
      worksgts.remove( beststarte );
      worksgts.remove( beststarte.next );
      worksgts.add( news );
      
      // last face!
      if ( worksgts.size() == 3 ) {
        newf = new Face( 
          worksgts.get( 0 ).p2, worksgts.get( 0 ).next.p2, worksgts.get( 0 ).p1,
          worksgts.get( 0 ), worksgts.get( 0 ).next, worksgts.get( 0 ).previous
          );
          worksgts.clear();
        faces.add( newf );
      }
      
      counter++;
      
    }
    
    // reconstruction of path
    for ( int i = 0; i < edges.length; i++ ) {
      int j = ( i + 1 ) % edges.length;
      edges[ i ].next = edges[ j ];
      edges[ j ].previous = edges[ i ];
    }
    
    processed = true;
    
    if ( sub != null ) {
      if ( sub.pts.size() != pts.size() ) {
        sub.init();
        for ( PVector pt : pts ) {
          PVector subpt = new PVector();
          subpt.set( pt );
          sub.pts.add( subpt );
        }
        sub.process();
      } else {
        for ( int i = 0; i < pts.size(); i++ ) {
          sub.pts.get( i ).set( pts.get( i ) );
        }
      }
      sub.shrink( shrinkAmount );
      
    }
    
  }
  
  public void shrink( float size ) {
    
    if ( !processed )
      return;
      
    PVector ptnorm = new PVector();
    for ( Segment s : edges ) {
      ptnorm.set( s.out );
      ptnorm.add( s.next.out );
      ptnorm.normalize();
      ptnorm.mult( -size );
      s.p2.set( s.p2.x + ptnorm.x, s.p2.y + ptnorm.y, s.p2.z + ptnorm.z ); 
    }
    
  }
  
  public void drawFaces( float white, boolean displaytext ) {
    int fcount = 0;
    for ( Face f : faces ) {
      fill( white, 50 );
      f.draw();
      if ( displaytext ) {
        fill( white, 200 );
        text( fcount, f.center.x - 4, f.center.y - 4 );
      }
      fcount++;
    }
  }
  
  public void drawSegments() {
    for ( int i = 0; i < edges.length; i++ ) {
      edges[ i ].draw();
      edges[ i ].drawDir();
      edges[ i ].drawOut();
    }
  }
  
  public void draw() {
    
    if ( !processed ) {
      noFill();
      stroke( 255, 180 );
      PVector prev = null;
      for ( PVector pt : pts ) {
        if ( prev != null ) {
          line( prev.x, prev.y, pt.x, pt.y );
        }
        ellipse( pt.x, pt.y, 5,5 );
        prev = pt;
      }
      
    } else {
    
      stroke( 255, 150 );
      drawFaces( 200, true );
      
      // strokeWeight( 5 );
      // drawSegments();
      strokeWeight( 1 );
      
      noStroke();
      if ( sub != null ) {
        sub.drawFaces( 255, false );
        // sub.drawSegments();
      }
      
      /*
      for ( Segment s : worksgts ) {
        s.draw();
        s.drawDir();
        s.drawOut();
      }
      */
      
      for ( Segment s : inneredges ) {
        s.draw();
        s.drawDir();
        s.drawOut();
      }
      
      noFill();
      stroke( 255, 0, 0, 255 );
      if ( beststart != null ) {
        ellipse( beststart.x, beststart.y, 10, 10 );
      }
      
    }
    
  }
  
  public PVector getNear( float x, float y, float r ) {
    for ( PVector pt : pts ) {
      if ( pt.x > x - r && pt.x < x + r && pt.y > y - r && pt.y < y + r ) {
        return pt;
      }  
    }
    return null;
  }
  
  public PVector getMid( float x, float y, float r ) {
    for ( int i = 0; i < pts.size(); i++ ) {
      int j = ( i + 1 ) % pts.size();
      PVector pt = pts.get( i );
      PVector pt1 = pts.get( j );
      PVector mid = new PVector(
        pt.x + ( pt1.x - pt.x ) * 0.5,
        pt.y + ( pt1.y - pt.y ) * 0.5,
        pt.z + ( pt1.z - pt.z ) * 0.5
      );
      if ( mid.x > x - r && mid.x < x + r && mid.y > y - r && mid.y < y + r ) {
        return mid;
      }
    }
    return null;
  }
  
  public void addMid( float x, float y, float r ) {
    for ( int i = 0; i < pts.size(); i++ ) {
      int j = ( i + 1 ) % pts.size();
      PVector pt = pts.get( i );
      PVector pt1 = pts.get( j );
      PVector mid = new PVector(
        pt.x + ( pt1.x - pt.x ) * 0.5,
        pt.y + ( pt1.y - pt.y ) * 0.5,
        pt.z + ( pt1.z - pt.z ) * 0.5
      );
      if ( mid.x > x - r && mid.x < x + r && mid.y > y - r && mid.y < y + r ) {
        pts.add( j, mid );
        return;
      }
    }
  }
  
  public void removePt( PVector pt ) {
    pts.remove( pt );
  }
  
  public void printConstruction() {
    for ( PVector pt : pts )
      println( "p.add( " + pt.x + ", " + pt.y + ");" );
  }

}
