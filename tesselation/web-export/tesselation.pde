import java.util.*; 

// used in plane to calculate U & V vector
public static final PVector UP = new PVector( 0,1,0 );
public PVector locked;
float selectrad = 10;
Polygon p;

public void setup() {

  size( 800, 600, P3D );
  p = new Polygon();
  locked = null;
  
}

public void draw() {
  
  if ( p.isProcessed() ) {
    p.process();
  }
  
  background( 5 );
  p.draw();
  
  PVector pt = locked;
  if ( pt == null && p.isProcessed() ) {
    pt = p.getNear( mouseX, mouseY, selectrad );
  }
  if ( pt != null ) {
    noFill();
    stroke( 0,255,255 );
    ellipse( pt.x, pt.y, selectrad * 2, selectrad * 2 );
  }
  
  fill( 255 );
  text( frameRate, 10, 15 );
  
}

public void mouseDragged() {
  if ( locked != null ) {
    locked.x = mouseX;
    locked.y = mouseY;
  }
}

public void mousePressed() {
  if ( p.isProcessed() )
    locked = p.getNear( mouseX, mouseY, selectrad );
}

public void mouseReleased() {
  if ( p.isProcessed() )
    locked = null;
  else
    p.add( mouseX, mouseY );
}

public void keyPressed() {

  if ( key == 'i' )
    p.init();

  if ( key == 'p' )
    p.process();
  
}
class Face {

  PVector[] pts;
  PVector center;
  Segment[] edges;
  
  public Face( PVector p1, PVector p2, PVector p3, Segment s1, Segment s2, Segment s3 ) {
    
    pts = new PVector[ 3 ];
    pts[ 0 ] = p1;
    pts[ 1 ] = p2;
    pts[ 2 ] = p3;
    
    center = new PVector();
    for ( int i = 0; i < 3; i++ )
      center.add( pts[ i ] );
    center.mult( 1.f / 3.f );
    
    edges = new Segment[ 3 ];
    edges[ 0 ] = s1;
    edges[ 1 ] = s2;
    edges[ 2 ] = s3;
    
  }
  
  public void draw() {
  
    beginShape( TRIANGLES );
    for ( int i = 0; i < 3; i++ )
      vertex( pts[ i ].x, pts[ i ].y, pts[ i ].z );
    endShape();
  
  }

}
// 2D polygon!

class Polygon {
  
  public ArrayList< PVector > pts;
  private Segment[] edges;
  private ArrayList< Face > faces;
  private ArrayList< Segment > inneredges;
  
  private PVector beststart;
  private Segment beststarte;
  private boolean processed;
  
  ArrayList< Segment > worksgts;
  
  public Polygon() {
    init();
  }
  
  public void init() {
    pts = new ArrayList< PVector >();
    edges = null;
    faces = null;
    inneredges = null;
    processed = false;
  }
  
  public void add( float x, float y ) {
    if ( !processed )
      pts.add( new PVector( x, y ) );
  }
  public boolean isProcessed() {
    return processed;
  }
  
  public void process() {
    
    if ( pts.size() < 3 ) {
      println( "Impossible to process a polygon of less then 3 points!!!" );
      return;
    }
    
    worksgts = new ArrayList< Segment >();
    edges = new Segment[ pts.size() ];
    for ( int i = 0; i < pts.size(); i++ ) {
      int j = ( i + 1 ) % pts.size();
      edges[ i ] = new Segment( pts.get( i ), pts.get( j ) );
      if ( i > 0 ) {
        edges[ i - 1 ].next  = edges[ i ];
        edges[ i ].previous = edges[ i - 1 ];
      }
      if ( i == pts.size() - 1 ) {
        edges[ i ].next = edges[ 0 ];
        edges[ 0 ].previous = edges[ i ];
      }
      worksgts.add( edges[ i ] );
    }
    
    // tesselation:
    // as long as the worksgts length is bigger then 3
    // we replace 2 edges by a new one
    
    faces = new ArrayList< Face >();
    inneredges = new ArrayList< Segment >();
    
    int counter = 0;
    while ( worksgts.size() > 3 ) {
      
      float best = 100000;
      beststart = null;
      beststarte = null;
      for ( Segment s : worksgts ) {
        float d = s.dir.dot( s.next.dir );
        float d2 = s.out.dot( s.next.dir );
        if (
            best > d 
            && d2 < 0
          ) {
          beststart = s.p2;
          beststarte = s;
          best = d;
        }
      }
      
      if ( beststart == null ) {
        println( "skipped before the end!" );
        break;
      }
      
      Segment news = new Segment( beststarte.p1, beststarte.next.p2 );
      
      // registration of the next & previous for the new segment
      news.previous = beststarte.previous;
      news.next = beststarte.next.next;
      
      // adaptation of next and previous in existing segments
      news.previous.next = news;
      news.next.previous = news;
      
      inneredges.add( news );
      Face newf = new Face( 
        beststarte.p2, beststarte.next.p2, beststarte.p1, 
        beststarte, news, beststarte.previous );
      faces.add( newf );
      
      worksgts.remove( beststarte );
      worksgts.remove( beststarte.next );
      worksgts.add( news );
      
      // last face!
      if ( worksgts.size() == 3 ) {
        newf = new Face( 
          worksgts.get( 0 ).p2, worksgts.get( 0 ).next.p2, worksgts.get( 0 ).p1,
          worksgts.get( 0 ), worksgts.get( 0 ).next, worksgts.get( 0 ).previous
          );
          worksgts.clear();
        faces.add( newf );
      }
      
      counter++;
      
    }
    
    // tesselation
    // finding the smallest angle
//    for ( int i = 0; i < edges.length; i++ ) {
//      int j = ( i + 1 ) % edges.length;
//      float d = edges[ i ].dir.dot( edges[ j ].dir );
//      float d2 = edges[ i ].out.dot( edges[ j ].dir );
//      if ( 
//        beststart == null ||
//        ( 
//          best > d 
//          && d2 < 0
//        ) 
//        ) {
//        beststart = edges[ i ].p2;
//        beststarte = edges[ i ];
//        best = d;
//      }
//    }
    
    // and the serious stuff begin!
    
//    Segment news = new Segment( beststarte.next.p2, beststarte.p1 );
//    inneredges.add( news );
//    Face newf = new Face( 
//      beststarte.p2, beststarte.next.p2, beststarte.p1, 
//      beststarte, news, beststarte.previous );
//    faces.add( newf );
//    
//    next1 = beststarte.next.p2;
//    next1e = beststarte.next;
//    next2 = beststarte.p1;
//    next2e = beststarte.previous;
//    
    // going one step further
    // we know that one edge of the next triangle is the new one
    // the tricky point now is to choose the right edge
    // 
    
    processed = true;
  }
  
  public void draw() {
    
    if ( !processed ) {
      noFill();
      stroke( 255, 180 );
      PVector prev = null;
      for ( PVector pt : pts ) {
        if ( prev != null ) {
          line( prev.x, prev.y, pt.x, pt.y );
        }
        ellipse( pt.x, pt.y, 5,5 );
        prev = pt;
      }
      
    } else {
    
      stroke( 0, 255, 255, 150 );
      int fcount = 0;
      for ( Face f : faces ) {
        fill( 255, 50 );
        f.draw();
        fill( 255, 200 );
        text( fcount, f.center.x - 4, f.center.y - 4 );
        fcount++;
      }
      
      strokeWeight( 5 );
      for ( int i = 0; i < edges.length; i++ ) {
        edges[ i ].draw();
        edges[ i ].drawDir();
        edges[ i ].drawOut();
      }
      
      strokeWeight( 1 );
      
      /*
      for ( Segment s : worksgts ) {
        s.draw();
        s.drawDir();
        s.drawOut();
      }
      */
      
      for ( Segment s : inneredges ) {
        s.draw();
        s.drawDir();
        s.drawOut();
      }
      
      noFill();
      stroke( 255, 0, 0, 255 );
      if ( beststart != null ) {
        ellipse( beststart.x, beststart.y, 10, 10 );
      }
      stroke( 255, 0, 255, 255 );
      if ( next1 != null ) {
        ellipse( next1.x, next1.y, 10, 10 );
      }
      stroke( 0, 255, 255, 255 );
      if ( next2 != null ) {
        ellipse( next2.x, next2.y, 10, 10 );
      }
      
    }
    
  }
  
  public PVector getNear( float x, float y, float r ) {
    for ( PVector pt : pts ) {
      if ( pt.x > x - r && pt.x < x + r && pt.y > y - r && pt.y < y + r ) {
        return pt;
      }  
    }
    return null;
  }
  
  public void printConstruction() {
    for ( PVector pt : pts )
      println( "p.add( " + pt.x + ", " + pt.y + ");" );
  }

}
class Segment {

  PVector p1;
  PVector p2;
  PVector dir;
  PVector out;
  
  // used when segment are part of a path
  Segment previous;
  Segment next;
  
  public Segment( PVector p1, PVector p2 ) {
    this.p1 = p1;
    this.p2 = p2;
    dir = new PVector( p2.x - p1.x, p2.y - p1.y, p2.z - p1.z );
    out = new PVector( dir.x, dir.y );
    out.normalize();
    float a = atan2( out.y, out.x ) - HALF_PI;
    out.set( cos( a ), sin( a ) );
    previous = null;
    next = null;
  }
  
  public void draw() {
    this.draw( null );
  }
  
  public void draw( PVector rot ) {
    
    stroke( 255, 100 );
    line( p1.x, p1.y, p1.z, p2.x, p2.y, p2.z );
    noStroke();
    fill( 255 );
    pushMatrix();
    translate( p1.x, p1.y, p1.z );
    if( rot != null ) { 
      rotateZ( rot.z );
      rotateY( rot.y );
      rotateX( rot.x );
    }
    ellipse( 0,0, 5,5 );
    popMatrix();
    pushMatrix();
    translate( p2.x, p2.y, p2.z );
    if( rot != null ) { 
      rotateZ( rot.z );
      rotateY( rot.y );
      rotateX( rot.x );
    }
    ellipse( 0,0, 5,5 );
    popMatrix();
    
  }
  
  // 2D!
  public void drawDir() {
    noStroke();
    fill( 255, 100 );
    float a = atan2( dir.y, dir.x );
    float l = dir.mag() - 12;
    pushMatrix();
    translate( p1.x + cos( a ) * l, p1.y + sin( a ) * l, p1.z );
    rotateZ( a );
    beginShape( TRIANGLES );
    vertex( 10, 0, 0 );
    vertex( 0, -5, 0 );
    vertex( 0, 5, 0 );
    endShape();
    popMatrix();
  }
  
  public void drawOut() {
    stroke( 255, 100 );
    float l = 30;
    line( 
      p1.x + ( p2.x - p1.x ) * 0.5,
      p1.y + ( p2.y - p1.y ) * 0.5,
      p1.z + ( p2.z - p1.z ) * 0.5,
      p1.x + ( p2.x - p1.x ) * 0.5 + out.x * l,
      p1.y + ( p2.y - p1.y ) * 0.5 + out.y * l,
      p1.z + ( p2.z - p1.z ) * 0.5 + out.z * l
      );
  }
  
}
class PointDrawer {

  PVector viewrot;
  
  public PointDrawer() {
    viewrot = new PVector();
  }
  
  public void drawpt( PVector v ) {
    drawpt( v, 2 );
  }
  
  public void drawpt( PVector v, float s ) {
    drawpt( v.x, v.y, v.z, s );
  }
  
  public void drawpt( float x, float y, float z, float s ) {
    pushMatrix();
    translate( x, y, z );
    rotateZ( viewrot.z );
    rotateY( viewrot.y );
    rotateX( viewrot.x );
    ellipse( 0,0, s,s );
    popMatrix();
  }
  
}


