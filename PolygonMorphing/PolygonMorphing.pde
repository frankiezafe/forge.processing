// tableau de pvector
PVector cricle[];
PVector shapes[][];

// distance des cricle au centre (0,0)
float radius = 230;
int max = 8;
int pt_num = 0;

int current_shape = 0;
float current_alpha = 0;

boolean play = true;

// function found here: https://www.geeksforgeeks.org/smallest-number-divisible-first-n-numbers/
long gcd(long a, long b) { 
  if (a%b != 0)  
    return gcd(b, a%b); 
  else 
  return b;
}
// function returns the lcm of first n numbers 
long lcm(long n) { 
  long ans = 1;     
  for (long i = 1; i <= n; i++) 
    ans = (ans * i)/(gcd(ans, i)); 
  return ans;
}

PVector[] generate_polygon( int sides ) {

  PVector out[] = new PVector[pt_num];
  int gap = pt_num / sides;
  int id = 0;
  for ( int i = 0; i < pt_num; i += gap ) {
    PVector start = cricle[i].copy();
    PVector end = cricle[(i+gap) % pt_num].copy();
    start.mult(radius);
    end.mult(radius);
    PVector dir = new PVector( end.x - start.x, end.y - start.y );
    PVector next = new PVector( 0, 0 );
    for ( int j = 0; j < gap; ++j ) {
      out[id] = new PVector( start.x + next.x, start.y + next.y );
      next.x += dir.x / gap;
      next.y += dir.y / gap;
      id++;
    }
  }
  return out;
}

void setup() {

  size( 1000, 600, P3D );

  pt_num = int(lcm(max));
  println( pt_num );
  for ( int i = 2; i <= max; ++i ) {
    println( "" + i + " > " + (pt_num/i) );
  }

  float a = 0;
  float gapa = TWO_PI / pt_num;
  cricle = new PVector[pt_num];
  for ( int i = 0; i < pt_num; ++i ) {
    cricle[i] = new PVector( cos( a ), sin( a ) );
    a += gapa;
  }

  // preprocessing of shapes
  shapes = new PVector[max-1][pt_num];
  for ( int i = 2; i <= max; ++i ) {
    shapes[i-2] = generate_polygon(i);
  }
}

void draw() {

  background(0);


  pushMatrix();
  translate( width/4, height/2 );

  float pc = sin( current_alpha );
  float pci = 1 - pc;

  strokeWeight(3);
  stroke(255);
  noFill();
  // passing by a circle each time
  for ( int i = 0; i < pt_num; ++i ) {
    int j = (i+1) % pt_num;
    line(
      shapes[current_shape][i].x * pc + cricle[i].x * radius * pci, 
      shapes[current_shape][i].y * pc + cricle[i].y * radius * pci, 
      shapes[current_shape][j].x * pc + cricle[j].x * radius * pci, 
      shapes[current_shape][j].y * pc + cricle[j].y * radius * pci
      );
  }
  noStroke();
  fill(255, 0, 0);
  float gapa = TWO_PI / (current_shape+2);
  for ( int i = 0; i < current_shape+2; ++i ) {
    ellipse( cos(i*gapa) * radius, sin(i*gapa) * radius, 10, 10 );
  }

  translate( width/2, 0 );

  strokeWeight(3);
  stroke(255);
  noFill();
  pc = (1 + sin( -HALF_PI + current_alpha)) * 0.5;
  pci = 1 - pc;

  // going to next shape
  int next_shape = (current_shape+1) % shapes.length;
  for ( int i = 0; i < pt_num; ++i ) {
    int j = (i+1) % pt_num;
    line(
      shapes[current_shape][i].x * pci + shapes[next_shape][i].x * pc, 
      shapes[current_shape][i].y * pci + shapes[next_shape][i].y * pc, 
      shapes[current_shape][j].x * pci + shapes[next_shape][j].x * pc, 
      shapes[current_shape][j].y * pci + shapes[next_shape][j].y * pc
      );
  }
  noStroke();
  fill(255, 0, 0, pci*255);
  for ( int i = 0; i < current_shape+2; ++i ) {
    ellipse( cos(i*gapa) * radius, sin(i*gapa) * radius, 10, 10 );
  }
  fill(255, 0, 0, pc*255);
  gapa = TWO_PI / (next_shape+2);
  for ( int i = 0; i < next_shape+2; ++i ) {
    ellipse( cos(i*gapa) * radius, sin(i*gapa) * radius, 10, 10 );
  }

  if ( play ) {
    current_alpha += 0.025;
  }
  while ( current_alpha > PI ) {
    current_alpha -= PI;
    current_shape++;
    current_shape %= shapes.length;
  }

  popMatrix();

  fill(255);
  text( "sides: " + (current_shape+2), 10, 25 );

}

void keyPressed() {

  switch( key ) {
    case ' ':
      play = !play;
      break;
  }

}
