class Plane {

  final PVector UPv = new PVector( 0,1,0 );
  
  PVector normale; // normale
  PVector v;
  PVector w;
  PVector xyz; // point
  
  float[][] mat;

  public Plane( float x, float y, float z ) {
    v = new PVector( 1, 0, 0 );
    v.normalize();
    w = new PVector( 0, 0, 1 );
    w.normalize();
    normale = v.cross( w );
    normale.normalize();
    xyz = new PVector( x, y, z );
    mat = new float[4][4];
    update();
  }

  public void draw( PVector rot ) {
    
    strokeWeight( 1 );
    noStroke();
    fill( 255, 50 ); 
    for ( int y = -200; y <= 200; y += 40  ) {
      for ( int x = -200; x <= 200; x += 40  ) {
        PVector ptv = new PVector();
        ptv.set( v );
        ptv.mult( x );
        PVector ptw = new PVector();
        ptw.set( w );
        ptw.mult( y );
        PVector pt = new PVector();
        pt.add( ptv );
        pt.add( ptw );
        pt.add( xyz );
        pushMatrix();
        translate( pt.x, pt.y, pt.z );
        rotateZ( rot.z );
        rotateY( rot.y );
        rotateX( rot.x );
        ellipse( 0, 0, 2, 2 );
        popMatrix();
      }
    }

    pushMatrix();
    translate( xyz.x, xyz.y, xyz.z );
    stroke( 255, 255, 0 );
    line( 0, 0, 0, 150 * v.x, 150 * v.y, 150 * v.z );
    stroke( 255, 0, 255 );
    line( 0, 0, 0, 150 * w.x, 150 * w.y, 150 * w.z );
    stroke( 127 );
    line( 0, 0, 0, 150 * normale.x, 150 * normale.y, 150 * normale.z );
    popMatrix();

    /*
    pushMatrix();
    applyMatrix( 
    mat[0][0], mat[1][0], mat[2][0], mat[3][0], 
    mat[0][1], mat[1][1], mat[2][1], mat[3][1], 
    mat[0][2], mat[1][2], mat[2][2], mat[3][2], 
    mat[0][3], mat[1][3], mat[2][3], mat[3][3] );
    rectMode( CENTER );
    stroke( 255 );
    fill( 255, 100  );
    rect( 0, 0, 200, 200 );
    strokeWeight( 3 );
    line( 0, 0, 0, 0, 0, 75 );
    popMatrix();
    */
    
    /*
    pushStyle();
    stroke( 255, 0, 0 );
     strokeWeight( 1 );
     line( 0,0,0, 100,0,0 );
     strokeWeight( 3 );
     line( 0,0,0, 100 * normale.x,0,0 );
     stroke( 0, 255, 0 );
     strokeWeight( 1 );
     line( 0,0,0, 0,100,0 );
     strokeWeight( 3 );
     line( 0,0,0, 0,100 * normale.y,0 );
     stroke( 0, 0, 255 );
     strokeWeight( 1 );
     line( 0,0,0, 0,0,100 );
     strokeWeight( 3 );
     line( 0,0,0, 0,0,100 * normale.z );
     popMatrix();
     stroke( 127 );
     strokeWeight( 1 );
     line( 0,0,0, 150 * normale.x, 150 * normale.y, 150 * normale.z );
     popStyle();
     */
  }

  public void pointOnPlane( PVector pt, PVector rot ) {
    
    float d = pt.dot( normale ) - xyz.dot( normale ); 
    
    PVector tmp = new PVector();
    tmp.set( normale );
    tmp.mult( -d );
    tmp.add( pt );
    
    pushMatrix();
    strokeWeight( 1 );
    stroke( 255, 50 );
    line( pt.x, pt.y, pt.z, tmp.x, tmp.y, tmp.z );
    translate( tmp.x, tmp.y, tmp.z );
    rotateZ( rot.z );
    rotateY( rot.y );
    rotateX( rot.x );
    stroke( 255, 0, 0 );
    fill( 255, 0, 0, 100 );
    ellipse( 0,0, 4,4 );
    
    /*
    fill( 255 );
    text( "dist: " + d, 5,5 );
    */
    
    popMatrix();
    
  }

  // http://www.flipcode.com/documents/matrfaq.html

  // http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToMatrix/index.htm
  // heading(y), attitude(z) & bank(x) representation: 
  // http://www.euclideanspace.com/maths/geometry/rotations/euler/aeroplaneGlobal1.png
  
  //https://www.opengl.org/discussion_boards/showthread.php/159883-converting-a-3D-vector-into-three-euler-angles
  
  // http://stackoverflow.com/questions/21622956/how-to-convert-direction-vector-to-euler-angles
  
  public void update() {
    
    v.normalize();
    w.normalize();
    
    normale = v.cross( w );
    normale.normalize();
    
    if ( 1 == 1 )
      return;
    
    
    PVector H = new PVector( normale.x, normale.y, 0 );
    float angle_H = atan2( H.y, H.x );
    float angle_A = asin( normale.z );
    PVector W0 = new PVector( -normale.y, normale.x, 0 );
    PVector U0 = W0.cross( normale );
    float angle_B = 0;
    
    float ch = cos( angle_H );
    float sh = sin( angle_H );
    float ca = cos( angle_A );
    float sa = sin( angle_A );
    float cb = cos( angle_B );
    float sb = sin( angle_B );

    mat[0][0] = ch * ca;
    mat[0][1] = sh*sb - ch*sa*cb;
    mat[0][2] = ch*sa*sb + sh*cb;
    mat[1][0] = sa;
    mat[1][1] = ca*cb;
    mat[1][2] = -ca*sb;
    mat[2][0] = -sh*ca;
    mat[2][1] = sh*sa*cb + ch*sb;
    mat[2][2] = -sh*sa*sb + ch*cb;

    mat[3][0] = xyz.x;
    mat[3][1] = xyz.y;
    mat[3][2] = xyz.z;
    
    mat[3][3] = 1;
    
  }
}

