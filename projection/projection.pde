Plane p;
PVector rot;
PVector tor;
PVector[] pt;

void setup() {

  size( 600, 600, OPENGL );
  p = new Plane( 0, -30, 0 );
  rot = new PVector();
  tor = new PVector();
  pt = new PVector[ 20 ];
  for ( int i = 0; i < pt.length; i++ )
    pt[ i ] = new PVector( random( -100, 100 ), random( -100, 100 ), random( -300, 300 ) );
}

void draw() {

  //  p.abc.x = sin( frameCount * 0.001 );
  //  p.abc.x = 0;
  //  p.abc.y = 0;
  //  p.abc.z = cos( frameCount * 0.0095 );

  rot.x =  -0.15;
  rot.y = frameCount * 0.007;

  tor.set( rot );
  tor.mult( -1 );

  p.w.y = mouseX * 4.f / width;
  p.w.z = 4 - ( mouseX * 4.f / width );
  p.xyz.y = mouseY - height / 2;

  p.update();

  background( 10 );

  pushMatrix();
  translate( width * 0.5, height * 0.5, 0 );  
  rotateX( rot.x );
  rotateY( rot.y );
  rotateZ( rot.z );

  strokeWeight( 3 );
  stroke( 255, 0, 0 );
  line( 0, 0, 0, 50, 0, 0 );
  stroke( 0, 255, 0 );
  line( 0, 0, 0, 0, 50, 0 );
  stroke( 0, 0, 255 );
  line( 0, 0, 0, 0, 0, 50 );

  p.draw( tor );

  for ( int i = 0; i < pt.length; i++ ) {
    pushMatrix();
    translate( pt[ i ].x, pt[ i ].y, pt[ i ].z );
    rotateZ( tor.z );
    rotateY( tor.y );
    rotateX( tor.x );
    strokeWeight( 1 );
    stroke( 255 );
    fill( 255, 50 );
    ellipse( 0, 0, 8, 8 );
    popMatrix();
    p.pointOnPlane( pt[ i ], tor );
  }
  
  popMatrix();
  
}

