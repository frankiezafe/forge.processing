import processing.video.*;

Capture cam;
Timer timer;

int Compteur = 0;

int DelayTimer = 100; // delais entre chaque photos (millisecondes)

void setup() {
  size(1280, 800);

  String[] cameras = Capture.list();
  
  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } else {
    println("Available cameras:");
    for (int i = 0; i < cameras.length; i++) {
      println(cameras[i]);
    }
    
    // The camera can be initialized directly using an 
    // element from the array returned by list():
    cam = new Capture(this, cameras[0] );
    cam.start();     
  }      
  
  timer = new Timer(DelayTimer);
  timer.start();
  
}

// ajout de zéros au début de la chaîne en fonction de la longueur définie
public String leadingZeros( int val, int len ) {
  String out = "";
  while( val < pow( 10, len - 1 ) && len > 1 ) {
    out += "0";
    len--;
  }
  out += val;
  return out;
}

// generation d'une chaine exprimant la date 
// dont chaque partie est préfixée d'un zéro si plus petit que 10
// (conserve l'ordre dans un gestionnaire de fichier)
public String timestamp() {
  int m = ( (int) millis() % 1000 );
  return leadingZeros( year(), 4 ) + "." +
    leadingZeros( month(), 2 ) + "." +
    leadingZeros( day(), 2 ) + "." +
    leadingZeros( hour(), 2 ) + "." +
    leadingZeros( minute(), 2 ) + "." +
    leadingZeros( second(), 2 ) + "." +
    leadingZeros( m, 3 );
}

void draw() {

  if (cam.available() == true) {
    cam.read();
  }
  
  
  image(cam, 0, 0, width, height);
  
  if ((timer.isFinished())) {
    saveFrame("ws_electonik/"+timestamp()+".tiff");
    // RESET TIMER
    timer = new Timer(DelayTimer);
    timer.start();
    
    Compteur++;
  }
 
  
 
}
