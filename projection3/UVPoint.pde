class UVPoint {

  float x;
  float y;
  float z;
  float u;
  float v;
  
  public UVPoint( float u, float v ) {
    x = 0;
    y = 0;
    z = 0;
    u = u;
    v = v;
  }
  
  public void update( Plane p ) {
    PVector tmp1 = new PVector( p.u.x, p.u.y, p.u.z );
    tmp1.mult( u );
    PVector tmp2 = new PVector( p.v.x, p.v.y, p.v.z );
    tmp2.mult( v );
    tmp1.add( tmp2 );
    tmp1.add( p.xyz );
    x = tmp1.x;
    y = tmp1.y;
    z = tmp1.z;
  }

}
