class Plane {

  // plane defined by one point and a normale only
  // ax + by+ cz + d = 0, abc being the normale  
  PVector normale; // normale
  PVector u;
  PVector v;
  float d;
  PVector xyz; // ref point
  PVector[] pts;
  PVector[] intersects;
  PVector center;
  float size = 400;
  PVector viewrot;
  PVector tmpv = new PVector();
  PVector tmpv2 = new PVector();

  public Plane( float x, float y, float z ) {
    normale = new PVector( 1,1,1 );
    normale.normalize();
    xyz = new PVector( x, y, z );
    u = new PVector();
    v = new PVector();
    render();
    intersects = new PVector[ 3 ];
    for ( int i = 0; i < 3; i++ )
      intersects[ i ] = new PVector();
    center = new PVector();
    pts = new PVector[4];
    for ( int i = 0; i < 4; i++ ) {
      pts[ i ] = new PVector();
    }
    viewrot = new PVector();
    update();
  }
  
  private void render() {
    normale.normalize();
    u = normale.cross( UP );
    u.normalize();
    v = normale.cross( u );
    v.normalize();
    d = -normale.x * xyz.x -normale.y * xyz.y -normale.z * xyz.z;
  }

  public PVector[] projectPoints( PVector[] projs ) {
    PVector[] output = new PVector[ projs.length ];
    float locd = xyz.dot( normale );
    for ( int i = 0; i < projs.length; i++ ) {
      float d = projs[ i ].dot( normale ) - locd;
      output[ i ] = new PVector();
      output[ i ].set( normale );
      output[ i ].mult( -d );
      output[ i ].add( projs[ i ] );
    }
    return output;
  }
  
  public void update() {
    render();
    PVector ptv = new PVector();
    PVector ptw = new PVector();
    int pIndex = 0;
    for ( float y = -0.5; y <= 0.5; y++ ) {
      for ( float x = -0.5; x <= 0.5; x++ ) {
        pts[ pIndex ].x = x * size * normale.x;
        pts[ pIndex ].y = y * size * normale.y;
        pts[ pIndex ].z = ( d - normale.x * pts[ pIndex ].x - normale.y * pts[ pIndex ].y ) / normale.z;
        pIndex++;
      }
    }
    for ( int i = 0; i < 3; i++ )
      intersects[ i ].set( 0,0,0 );
    if ( normale.x != 0 )
      intersects[ 0 ].x = -d / normale.x;
    if ( normale.y != 0 )
      intersects[ 1 ].y = -d / normale.y;
    if ( normale.z != 0 )
      intersects[ 2 ].z = -d / normale.z;
    center.set( 0,0,0 );
    for ( int i = 0; i < 3; i++ )
      center.add( intersects[ i ] );
    center.mult( 1.f / 3 );
  }
  
  public void draw( PVector rot ) {
    viewrot = rot;
    
    /*
    // crossing axis
    strokeWeight( 1 );
    stroke( 255, 127 );
    noFill();
    beginShape( TRIANGLES );
    for ( int i = 0; i < 3; i++ )
      vertex( intersects[ i ].x, intersects[ i ].y, intersects[ i ].z );
    endShape();
    for ( int i = 0; i < 3; i++ ) {
      switch( i ) {
        case 0:
          stroke( 255, 0, 0 );
          break;
        case 1:
          stroke( 0, 255, 0 );
          break;
        case 2:
          stroke( 0, 0, 255 );
          break;
      }
      pushMatrix();
      translate( intersects[ i ].x, intersects[ i ].y, intersects[ i ].z );
      rotateZ( rot.z );
      rotateY( rot.y );
      rotateX( rot.x );
      ellipse( 0,0, 5 + i * 4, 5 + i * 4 );
      popMatrix();
    }
    */
    
    stroke( 255 );
    noFill();
    drawpt( xyz, 8 );
    /*
    stroke( 255 );
    noFill();
    for ( float y = -size * 0.5; y <= size * 0.5; y += size * 0.1 ) {
    for ( float x = -size * 0.5; x <= size * 0.5; x += size * 0.1 ) {
      tmpv.set( u );
      tmpv.mult( x );
      tmpv2.set( v );
      tmpv2.mult( y );
      tmpv.add( tmpv2 );
      tmpv.add( xyz );
      drawpt( tmpv );
    }
    }
    */
    
    pushMatrix();
    translate( xyz.x, xyz.y, xyz.z );
    stroke( 255,0,0 );
    line( 0, 0, 0, 150 * u.x, 150 * u.y, 150 * u.z );
    stroke( 0,255,0 );
    line( 0, 0, 0, 150 * v.x, 150 * v.y, 150 * v.z );
    stroke( 127 );
    line( 0, 0, 0, 150 * normale.x, 150 * normale.y, 150 * normale.z );
    popMatrix();
    
  }
  
  public void drawpt( PVector v ) {
    drawpt( v, 2 );
  }
  
  public void drawpt( PVector v, float s ) {
    pushMatrix();
    translate( v.x, v.y, v.z );
    rotateZ( viewrot.z );
    rotateY( viewrot.y );
    rotateX( viewrot.x );
    ellipse( 0,0, s,s );
    popMatrix();
  }
  
}

