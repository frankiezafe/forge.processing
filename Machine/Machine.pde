int pxlnum;
int colorA;
int colorB;
int[] pp;
boolean mp;

void setup() {

  size( 1280, 720, P2D );
  background( 255 );
  loadPixels();

  colorA = color( 255 );
  colorB = color( 0 );

  pxlnum = width * height;
  pp = new int[pxlnum]; 
  for ( int i = 0; i < pxlnum; ++i ) { pp[i] = colorA; }
  
  for ( int i = 0; i < 20; ++i ) {
    pp[int(random(pxlnum))] = colorB;
  }
  
  //int start = int(random(pxlnum) - 2000);
  //for ( int i = 0; i < 2000; ++i ) {
  //  pp[start + i] = colorB;
  //}

  mp = false;
}

void stripesA( int i, int prev, int next, int up, int down ) {
  if ( pp[next] == colorA && pp[prev] == colorB ) pixels[i] = colorA;
  else if ( pp[up] == colorA && pp[prev] == colorB ) pixels[i] = colorA;
  else if ( pp[up] == colorA && pp[prev] == colorA ) pixels[i] = colorB;
  else pixels[i] = colorB;
}

void stripesB( int i, int prev, int next, int up, int down ) {
  if ( pp[next] == colorA && pp[prev] == colorB ) pixels[i] = colorB;
  else if ( pp[up] == colorA && pp[prev] == colorB ) pixels[i] = colorA;
  else if ( pp[up] == colorA && pp[prev] == colorA ) pixels[i] = colorA;
  else pixels[i] = colorB;
}

void stripesC( int i, int prev, int next, int up, int down ) {
  if ( pp[next] == colorA && pp[prev] == colorA ) pixels[i] = colorB;
  else if ( pp[up] == colorA && pp[prev] == colorB ) pixels[i] = colorA;
  else if ( pp[up] == colorA && pp[prev] == colorA ) pixels[i] = colorA;
  else pixels[i] = colorB;
}

void stripesD( int i, int prev, int next, int up, int down ) {
  if ( pp[next] == colorA && pp[prev] == colorA ) pixels[i] = colorB;
  else if ( pp[up] == colorA && pp[prev] == colorB ) pixels[i] = colorA;
  else if ( pp[up] == colorB && pp[prev] == colorA ) pixels[i] = colorA;
  else pixels[i] = colorB;
}

void stripesE( int i, int prev, int next, int up, int down ) {
  if ( pp[next] == colorB && pp[prev] == colorA ) pixels[i] = colorB;
  else if ( pp[next] == colorA && pp[prev] == colorB ) pixels[i] = colorA;
  else pixels[i] = colorB;
}

void golA( int i, int prev, int next, int up, int down ) {
  if ( 
    pp[i] == colorA &&
    pp[prev] == colorA && 
    pp[next] == colorA && 
    pp[up] == colorA && 
    pp[down] == colorA
    ) { 
    pixels[i] = colorA;
  } else if ( 
    pp[i] == colorA &&
    (
    pp[prev] == colorA || 
    pp[next] == colorA || 
    pp[up] == colorA || 
    pp[down] == colorA
    )
    ) { 
    pixels[i] = colorB;
  } else {
    pixels[i] = colorA;
  }
}

void golB( int i, int prev, int next, int up, int down ) {
  if ( 
    pp[i] == colorA &&
    pp[prev] == colorA && 
    pp[next] == colorA && 
    pp[up] == colorA && 
    pp[down] == colorA
    ) { 
    pixels[i] = colorA;
  } else if ( 
    pp[i] == colorA &&
    (
    pp[prev] == colorB || 
    pp[next] == colorB && 
    pp[up] == colorA ||
    pp[down] == colorB
    )
    ) { 
    pixels[i] = colorB;
  } else {
    pixels[i] = colorA;
  }
}

void golC( int i, int prev, int next, int up, int down ) {
  if ( 
    pp[i] == colorA &&
    pp[prev] == colorA && 
    pp[next] == colorA && 
    pp[up] == colorA && 
    pp[down] == colorA
    ) { 
    pixels[i] = colorB;
  } else if (
    pp[prev] == colorB && 
    pp[next] == colorB && 
    pp[up] == colorB && 
    pp[down] == colorA
  ){ 
    pixels[i] = colorA;
  } else if (
    pp[prev] == colorB && 
    pp[next] == colorA && 
    pp[up] == colorB && 
    pp[down] == colorB
  ){ 
    pixels[i] = colorA;
  } else if (
    pp[prev] == colorA && 
    pp[next] == colorB && 
    pp[up] == colorB && 
    pp[down] == colorB
  ){ 
    pixels[i] = colorA;
  }
}

void golD( int i, int prev, int next, int up, int down ) {
  if ( 
    pp[i] == colorA &&
    pp[prev] == colorA && 
    pp[next] == colorA && 
    pp[up] == colorA && 
    pp[down] == colorA
    ) { 
    pixels[i] = colorB;
  } else if (
    pp[prev] == colorA && 
    pp[next] == colorB && 
    pp[up] == colorA && 
    pp[down] == colorA
  ){ 
    pixels[i] = colorB;
  } else if (
    pp[prev] == colorB && 
    pp[next] == colorA && 
    pp[up] == colorB && 
    pp[down] == colorB
  ){ 
    pixels[i] = colorA;
  } else if (
    pp[prev] == colorA && 
    pp[next] == colorB && 
    pp[up] == colorB && 
    pp[down] == colorA
  ){ 
    pixels[i] = colorB;
  }
}

void draw() {
  
  for ( int y = 0; y < height; ++y ) {
    for ( int x = 0; x < width; ++x ) {
      int i = x + y * width;
      int prev, next, up, down;
      if ( x == 0 ) { prev = (width-1) + y * width; }
      else { prev = i-1; }
      if ( x == width-1 ) { next = y * width; }
      else { next = i+1; }
      if ( y == 0 ) { up = x + (height-1) * width; }
      else { up = i-width; }
      if ( y == height-1 ) { down = x; }
      else { down = i+width; }
      
      //stripesA( i, prev, next, up, down );
      //stripesB( i, prev, next, up, down );
      //stripesC( i, prev, next, up, down );
      //stripesD( i, prev, next, up, down );
      //stripesE( i, prev, next, up, down );
      //golA( i, prev, next, up, down );
      //golB( i, prev, next, up, down );
//golC( i, prev, next, up, down );
      golD( i, prev, next, up, down );
      //pixels[i] = pp[i];
    }
  }
  
  if ( mp && mouseX >= 0 && mouseY >=0 && mouseX < width && mouseY < height ) {
    int rad = 20;
    for ( int y = mouseY - rad; y <= mouseY + rad; ++y ) {
    for ( int x = mouseX - rad; x <= mouseX + rad; ++x ) {
      if ( x < 0 || y < 0 || x >= width || y >= height ) continue;
      if ( dist( mouseX, mouseY, x, y ) < rad ) {
        pixels[ x + y * width ] = colorB;
      }
    }
    }
  }
  
  for ( int i = 0; i < pxlnum; ++i ) pp[i] = pixels[i]; 

  updatePixels();
}

void mousePressed() {
  mp = true;
}

void mouseReleased() {
  mp = false;
}
