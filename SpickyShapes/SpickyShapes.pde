
public void setup() {
  size(800, 800);
  smooth();
  noStroke();
  background(255);
  stroke(255, 0, 0);
  noFill();
  // fill(255,20);
}

public void draw() {
  background(255);

  noFill();
  stroke(0, 100);
  ellipse(150, 150, 200, 200);
  stroke(255, 0, 0);
  fill(255, 0, 0, 50);
  ExtendedShapes.Polygon(this, 150, 150, 100, 100, 3 + mouseX/50, (float)mouseY/200f);

  noFill();
  stroke(0, 100);
  ellipse(400, 150, 200, (float)mouseY/4f);
  stroke(255, 0, 0);
  fill(255, 0, 0, 50);
  ExtendedShapes.Polygon(this, 400, 150, 100, (float)mouseY/8f, 3 + mouseX/50, 0);

  noFill();
  stroke(0, 100);
  ellipse(650, 150, 200, 200);
  stroke(255, 0, 0);
  fill(255, 0, 0, 50);
  ExtendedShapes.Polygon(this, 650, 150, 100, 100, 3 + mouseX/50, 0, (float) mouseY / 800f);

  noFill();
  stroke(0, 100);
  ellipse(150, 400, 200, 200);
  stroke(0, 50);
  ellipse(150, 400, 140, 140);
  stroke(255, 0, 0);
  fill(255, 0, 0, 50);
  ExtendedShapes.Star(this, 150, 400, 100, 100, 70, 70, 3 + mouseX/50, (float)mouseY/200f, (float)mouseY/1600f);

  noFill();
  stroke(0, 100);
  ellipse(400, 400, 200, 200);
  stroke(0, 50);
  ellipse(400, 400, (float)mouseY/4f, (float)mouseY/4f);
  stroke(255, 0, 0);
  fill(255, 0, 0, 50);
  ExtendedShapes.Star(this, 400, 400, 100, 100, (float)mouseY/8f, (float)mouseY/8f, 3 + mouseX/50, 0, -(float)mouseY/1600f);

  noFill();
  stroke(0, 100);
  ellipse(650, 400, 200, (float)mouseX/4f);
  stroke(0, 50);
  ellipse(650, 400, 200, (float)mouseY/4f);
  stroke(255, 0, 0);
  fill(255, 0, 0, 50);
  ExtendedShapes.Star(this, 650, 400, 100, (float)mouseX/8f, 100, (float)mouseY/8f, 3 + mouseX/50, 0, -(float)mouseY/1600f);

  noFill();
  stroke(0, 100);
  ellipse(150, 650, 200, 200);
  stroke(255, 0, 0);
  fill(255, 0, 0, 50);
  ExtendedShapes.CornerPolygon(this, 150, 650, 100, 100, 3 + mouseX/50, 0, (float)mouseY/2000f);

  noFill();
  stroke(0, 100);
  ellipse(400, 650, 200, 200);
  stroke(255, 0, 0);
  fill(255, 0, 0, 50);
  ExtendedShapes.RoundedPolygon(this, 400, 650, 100, 100, 3 + mouseX/50, 0, (float)mouseY/2000f);

  noFill();
  stroke(0, 100);
  ellipse(650, 650, 200, 200);
  stroke(0, 50);
  ellipse(650, 650, mouseX*2/9, mouseX*2/9);
  stroke(255, 0, 0);
  fill(255, 0, 0, 50);
  ExtendedShapes.RoundedStar(this, 650, 650, 100, 100, mouseX/9, mouseX/9, 7, 0, 0, (float)mouseY/2000f);
}

public void mousePressed() {
}
