void circularGradient( PGraphics p ) {
  circularGradient( p, 0, 255 );
}

void circularGradient( PGraphics p, float min_alpha, float max_alpha ) {
  
  int pw = p.width;
  int ph = p.height;
  float half_p = pw * 0.5f;
  float midx = pw * 0.5f;
  float midy = ph * 0.5f;
  
  p.loadPixels();
  for ( int y = 0; y < ph; y ++ ) {
  for ( int x = 0; x < pw; x ++ ) {
    int i = x + y * pw;
    float d = dist( x, y, midx, midy ) / half_p;
    if ( d > 1 )
      d = 1;
    p.pixels[ i ] = color( 255, min_alpha + ( 1 - d ) * ( max_alpha - min_alpha ) );
  } 
  }
  p.updatePixels();
  
}
