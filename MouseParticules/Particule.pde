public static final float PARTICULE_INITIAL_DEVIATION = 0.1f;
public static final float PARTICULE_INITIAL_REPULSION = 25;

class Particule {

  PVector pos;
  PVector dir;
  int time2liveInit;
  int time2live;
  float life;
  float repulsion = PARTICULE_INITIAL_REPULSION;
  
  public Particule( float x, float y, float angl, float rad, int time2live ) {
    pos = new PVector( x, y );
    dir = new PVector( cos( angl ) * rad, sin( angl ) * rad );
    float l = dir.mag();
    dir.x += random( -PARTICULE_INITIAL_DEVIATION, PARTICULE_INITIAL_DEVIATION );
    dir.y += random( -PARTICULE_INITIAL_DEVIATION, PARTICULE_INITIAL_DEVIATION );
    dir.normalize();
    dir.mult( l );
    time2liveInit = time2live;
    this.time2live = time2live;
    life = 1;
  }
  
  public Particule( float x, float y, PVector v, int time2live ) {
    pos = new PVector( x, y );
    dir = new PVector();
    dir.set( v );
    float l = dir.mag();
    dir.x += random( -PARTICULE_INITIAL_DEVIATION, PARTICULE_INITIAL_DEVIATION );
    dir.y += random( -PARTICULE_INITIAL_DEVIATION, PARTICULE_INITIAL_DEVIATION );
    dir.normalize();
    dir.mult( l );
    time2liveInit = time2live;
    this.time2live = time2live;
    life = 1;
  }
  
  public void update() {
    
    if ( time2live == 0 ) {
      return;
    }
    
    life = time2live * 1.f / time2liveInit;
    
    PVector tmp = new PVector();
    tmp.set( pos );
    tmp.add( dir );
    if ( tmp.x < 0 || tmp.x >= width ) {
      dir.x *= -1;
    }
    if ( tmp.y < 0 || tmp.y >= height ) {
      dir.y *= -1;
    }
    pos.add( dir );
    if ( pos.x < 0 ) { pos.x = 0; }
    if ( pos.x > width ) { pos.x = width; }
    if ( pos.y < 0 ) { pos.y = 0; }
    if ( pos.y > height ) { pos.y = height; }
    
    dir.mult( 0.95f );
    
    time2live--;
    
  }


}
