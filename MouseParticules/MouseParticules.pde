
ArrayList< Particule > particules;
PVector lastmouse;
PGraphics texture;

void setup() {
  size( 600, 600 );
  particules = new ArrayList< Particule >();
  lastmouse = new PVector();
  texture = createGraphics( 64, 64 );
  texture.beginDraw();
  texture.background(color(0,0,0,0));
  texture.endDraw();
  circularGradient( texture, 0, 120 );
  background( 5 );
}

void draw() {

  for ( int i = 0; i < particules.size(); i++ ) {
    Particule p = particules.get( i );
    p.update();
    if ( p.time2live <= 0 ) {
      particules.remove( p );
      i--;
    }
  }
  
  background( 5 );
  
  PVector push = new PVector();
  for ( int i = 0; i < particules.size(); i++ ) {
    
    Particule p = particules.get( i );
    
    noStroke();
    fill( 255, 0, 0, 200 * p.life );
    float r = p.dir.mag() * 30;
    image( texture, p.pos.x - r * 0.5f, p.pos.y - r * 0.5f, r, r );
    
    strokeWeight( 1 + 3 * p.life );
    stroke( 255, 10 + 200 * p.life );
    // point( p.pos.x, p.pos.y );
    strokeWeight( 1 );
    int linked = 0;
    for ( int j = 0; j < particules.size(); j++ ) {
      Particule o = particules.get( j );
      if ( j != i ) {
        float d = dist( o.pos.x, o.pos.y, p.pos.x, p.pos.y );
        if ( d < o.repulsion ) {
          push.set( p.pos );
          push.x -= o.pos.x;
          push.y -= o.pos.y;
          push.mult( ( 1 - d / o.repulsion ) * 0.04f );
          p.dir.add( push );
          line( p.pos.x, p.pos.y, o.pos.x, o.pos.y );
          linked++;
        }
        if ( linked > 10 ) {
          break;
        }
      }
    }
  }
  
}

void mousePressed() {
  lastmouse.set( mouseX, mouseY );
}

void mouseDragged() {
  PVector mdir = new PVector( mouseX, mouseY );
  mdir.x -= lastmouse.x;
  mdir.y -= lastmouse.y;
  mdir.mult( 0.1f );
  Particule p = new Particule( mouseX, mouseY, mdir, 400 );
  // p.repulsion = mdir.mag() * 10;
  particules.add( p );
  lastmouse.set( mouseX, mouseY );
}
