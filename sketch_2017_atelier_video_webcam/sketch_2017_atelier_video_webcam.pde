import processing.video.*;
Movie elair_mask, elwater, elwater_mask;
MovieA elair;

Capture cam;
float elair_timespot;
float moviespeed = 0.05;  

void setup() {

  size(600, 800, OPENGL);
  
  String[] cameras = Capture.list();

  if (cameras == null) {
    println("Failed to retrieve the list of available cameras, will try the default...");
    cam = new Capture(this, 640, 480);
  } if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } else {
    println("Available cameras:");
    printArray(cameras[0]);


    cam = new Capture(this, cameras[0]);

    cam.start();
    
    textSize(18);
    textAlign(CENTER);
  }
  
  frameRate(24);

  //elair = new MovieA(this, "atelier_cuve1.mov");
  elair = new MovieA(this, "frames_1.mov");
  elair_mask = new Movie(this, "atelier_cuve1_mask.mov");
  /*elair = new Movie(this, "atelier_cuve2.mov");
  elair_mask = new Movie(this, "atelier_cuve2_mask.mov");*/

//elair_mask.play();
/*elair.play();

elair.loop();*/
//elair_mask.loop();

}

void movieEvent(Movie m) {
  m.read();
}

void movieAEvent(MovieA m) {
  m.read();
}

void captureEvent(Capture c) {
  c.read();
}


void draw() {
  background(0);
  imageMode(CORNER);
  if (cam.available() == true) {
    cam.read();
  }
  image(cam, 0, 0, 1066, height);


//if (elair_timespot < elair.duration()) {
//  elair_timespot += moviespeed;
//} else {
//  elair_timespot = 0;
//}
//elair.jump(elair_timespot);
//elair_mask.jump(elair_timespot);

//elair.mask(elair);

//elair.loadPixels();
//for (int i = 0; i < elair.pixels.length; i++) {
//    int a = ((elair.pixels[i] & 0xff));
//  int r = ((elair.pixels[i] & 0xff) << 24);
//  int g = ((elair.pixels[i] & 0xff) << 16);
//  int b = ((elair.pixels[i] & 0xff) << 8);
//    elair.pixels[i] =  color( a,a,a );
//  //elair.pixels[i] = ((elair.pixels[i] & 0xff) << 24)
//}
//elair.format = ARGB;
//elair.updatePixels();

imageMode(CENTER);
image(elair, width/2, height/2); 


  int s = second();  // Values from 0 - 59
  int m = minute() + 15;  // Values from 0 - 59
  int h = hour() - 3;    // Values from 0 - 23

text("durée d'incubation : " + "9" + "j " + h + "h " + m + "m " + s + "s", width/2, height-20);

}