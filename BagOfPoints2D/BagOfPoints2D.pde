
float margins = 100;
float biggestdist = 0;
MeshPoint barycenter;
public ArrayList < MeshPoint > mesh;
public ArrayList < MeshPoint > pts;
float meshdefinition = 50.f;

boolean showstructure = true;
boolean wasshowstructure = true;
PGraphics tmprender;

void setup() {
  
  size( 800,600, P3D );
  tmprender = createGraphics( width, height, P3D );
  
  barycenter = new MeshPoint();
  
  pts = new ArrayList < MeshPoint >();
  for ( int i = 0; i < 10; i++ ) {
    MeshPoint mp = new MeshPoint( 
      random( margins, width-margins ), 
      random( margins, height-margins ), 
      0 );
    mp.size = 30 + i * 10;
    mp.direction.set( random( -1,1 ), random( -1,1 ), 0 );
    mp.direction.normalise();
    pts.add( mp );
  }

  mesh = new ArrayList < MeshPoint >();
  for ( int i = 0; i < meshdefinition; i++ ) {
    float a = ( i / meshdefinition ) * TWO_PI;
    MeshPoint mp = new MeshPoint();
    mesh.add( mp );
  }
  
}

void update() {
  
  for ( int i = 0; i < pts.size(); i++ ) {
    MeshPoint pt = pts.get(i);
    pt.plus( pt.direction );
    if ( pt.x + pt.direction.x < margins || pt.x + pt.direction.x > width-margins )
      pt.direction.x *= -1;
    if ( pt.y + pt.direction.y < margins || pt.y + pt.direction.y > height-margins )
      pt.direction.y *= -1;
  }
  
  float totalsize = 0;
  for ( int i = 0; i < pts.size(); i++ ) { totalsize += pts.get(i).size; }
  barycenter.set( 0,0,0 );
  MeshPoint tmp = new MeshPoint( true );
  for ( int i = 0; i < pts.size(); i++ ) {
    tmp.set( pts.get(i) );
    tmp.multiply( pts.get(i).size / totalsize );
    barycenter.plus( tmp );
  }
  biggestdist = 0;
  for ( int i = 0; i < pts.size(); i++ ) {
    float tmd = barycenter.dist( pts.get(i) ) + ( pts.get(i).size * 0.5f ) + 10;
    if ( tmd > biggestdist ) {
      biggestdist = tmd;
    }
  }
  float a = 0;
  float agap = TWO_PI / mesh.size();
  int previousclosest = -1;
  for ( int i = 0; i < mesh.size(); i++ ) {
    MeshPoint m = mesh.get(i);
    m.set( 
      barycenter.x + cos(a) * biggestdist, 
      barycenter.y + sin(a) * biggestdist,
      barycenter.z );
    // closest point
    int closest = 0;
    float d = 0;
    for ( int p = 0; p < pts.size(); p++ ) {
      MeshPoint pt = pts.get(p);
      if ( p == 0 ) {
        d = m.dist( pt );
      } else {
        float tmd = m.dist( pt ) - pt.size * 0.5f;
        if ( d > tmd ) {
          closest = p;
          d = tmd;
        }
      }
    }
    // setting the direction towards the closest point:
    m.direction.set( pts.get( closest ) );
    m.direction.minus( m );
    float tml = m.direction.len();
    m.direction.normalise();
    m.direction.multiply( tml - pts.get( closest ).size * 0.5f );
    
    // smooth gaps
    if ( i > 0 && previousclosest != closest ) {
      // the current mesh point points to a different point then the prvious one!
      // creating a point in the middle
      MeshPoint prevmp = mesh.get( i-1 );
      MeshPoint middle = new MeshPoint( true );
      middle.set( prevmp.x + prevmp.direction.x, prevmp.y + prevmp.direction.y, prevmp.z + prevmp.direction.z );
      middle.minus( m.x + m.direction.x, m.y + m.direction.y, m.z + m.direction.z );
      middle.multiply( 1.f / 3 );
      m.direction.plus( middle );
      prevmp.direction.minus( middle );
    }
    
    a += agap;
    previousclosest = closest;
  }
}

void draw() {
  
  update();

  if ( showstructure ) {
    
    background( 255 );
    
    // drawing the meshpoints
    strokeWeight( 2 );
    for ( int i = 0; i < mesh.size(); i++ ) {
      noStroke();
      fill( 255,0,0 );
      MeshPoint mp = mesh.get(i);
      pushMatrix();
      translate( mp.x, mp.y, mp.z );
      ellipse( 0,0, 5,5 );
      popMatrix();
      stroke( 255,0,0 );
      line( mp.x, mp.y, mp.z, mp.x + mp.direction.x, mp.y + mp.direction.y, mp.z + mp.direction.z );
    }
    // and the bag
    stroke( 0,255,0 );
    fill( 0,255,0, 150 );
    beginShape();
    for ( int i = 0; i < mesh.size(); i++ ) {
      MeshPoint mp = mesh.get(i);
      vertex( mp.x + mp.direction.x, mp.y + mp.direction.y );
    }
    endShape(CLOSE);
    strokeWeight( 1 );
    
    fill( 255,0,255 );
    pushMatrix();
    translate( barycenter.x,barycenter.y, barycenter.z );
    ellipse( 0,0, 20,20 );
    popMatrix();
    
    // drawing the balls
    noFill();
    for ( int i = 0; i < pts.size(); i++ ) {
      MeshPoint pt = pts.get(i);
      pushMatrix();
      translate( pt.x, pt.y, pt.z );
      stroke( 0,0,0 );
      line( -5,0, 5,0 );
      line( 0,-5, 0,5 );
      stroke( 0,0,0 );
      strokeWeight( 2 );
      ellipse( 0,0, pt.size,pt.size );
      strokeWeight( 1 );
      popMatrix();
    }
    
  } else {
    
    if ( wasshowstructure )
      background( 255 );
    
    // fill( 255, 15 );
    // rect( 0,0, width,height );
    
    // nicely drawing the bag
    tmprender.beginDraw();
      tmprender.background( 0,0 );
      float biggsetdist2bary = 0;
      tmprender.noStroke();
      tmprender.fill( 0,255,0 );
      tmprender.beginShape();
      for ( int i = 0; i < mesh.size(); i++ ) {
        MeshPoint mp = mesh.get(i);
        tmprender.curveVertex( mp.x + mp.direction.x, mp.y + mp.direction.y );
        if ( i == 0 )
          tmprender.curveVertex( mp.x + mp.direction.x, mp.y + mp.direction.y );
        else if ( i == mesh.size()-1 )
          tmprender.curveVertex( mesh.get(0).x + mesh.get(0).direction.x, mesh.get(0).y + mesh.get(0).direction.y );
        float tmd = mp.dist( barycenter );
        if ( tmd > biggsetdist2bary ) {
          biggsetdist2bary = tmd;
        }
      }
      tmprender.endShape(CLOSE);
    tmprender.endDraw();
    
    
    tmprender.loadPixels();
      println( tmprender.pixels[0] );
      MeshPoint pixelp = new MeshPoint( true );
      int bg = color( 0,0 );
      int pi = 0;
      for ( int y = 0; y < tmprender.height; y++ ) {
        for ( int x = 0; x < tmprender.width; x++ ) {
          if ( tmprender.pixels[pi] != bg ) {
            // the pixel is NOT white
            pixelp.set( x,y,0 );
            float tmw = pixelp.dist( barycenter ) / biggsetdist2bary;
            int w = (int) ( tmw * 255.f);
            tmprender.pixels[pi] = color( w, tmw * w, tmw * w, 50 );
          }
          pi++;
        }
      }
    tmprender.updatePixels();
    
    // do not ask me why, but processing seems to invert the pixels vertically
    // after updatePixels... i had to rotate on Y to keep a correct visualisation! 
    pushMatrix();
    rotateX( PI );
    image( tmprender, 0,-height );
    popMatrix();
    
  }

  wasshowstructure = showstructure;
  
}

void keyPressed() {
  
  if ( keyCode == 32 ) { // [ space ]
    showstructure = !showstructure;
  } else {
    println( keyCode );  
  }

}
