final PVector PERPENDICULAR = new PVector(0, 0, 1);

final int DIVISION_TYPE_TRI = 0;
final int DIVISION_TYPE_TRII = 1;
final int DIVISION_TYPE_QUAD = 2;
final int DIVISION_TYPE_QUADI = 3;

float division_factor = 2.0/3;
float division_offset = 0;
float height_multiplier = 1;

class FractalLine {

  private PVector A;
  private PVector B;
  private PVector vec;
  private PVector vec_norm;
  private float len;
  private ArrayList<FractalLine> subs;
  private boolean have_subs;

  public FractalLine( PVector start, PVector end ) {
    A = start.copy();
    B = end.copy();
    vec = new PVector( B.x - A.x, B.y - A.y, B.z - A.z );
    len = vec.mag();
    vec_norm = vec.copy();
    vec_norm.normalize();
    subs = new ArrayList<FractalLine>();
    have_subs = false;
  }

  private void sub_tri( float pc, float way ) {
    float hpc = pc * 0.5;
    PVector tmpA = new PVector( A.x + vec.x * ( hpc + division_offset ), A.y + vec.y * ( hpc + division_offset ), A.z + vec.z * ( hpc + division_offset ) );
    PVector tmpB = new PVector( B.x - vec.x * ( hpc - division_offset ), B.y - vec.y * ( hpc - division_offset ), B.z - vec.z * ( hpc - division_offset ) );
    PVector mid = new PVector( A.x + vec.x * ( 0.5 + division_offset ), A.y + vec.y * ( 0.5 + division_offset ), A.z + vec.z * ( 0.5 + division_offset ) );
    PVector perp = vec_norm.cross(PERPENDICULAR);
    perp.normalize();
    // A2 + B2 = C2 -> A2 = C2 - B2
    float mult = sqrt( 1 - (pc*pc) ) * len * (1-pc);
    mid.x += perp.x * height_multiplier * mult * way;
    mid.y += perp.y * height_multiplier * mult * way;
    mid.z += perp.z * height_multiplier * mult * way;
    subs.add( new FractalLine( A, tmpA ) );
    subs.add( new FractalLine( tmpA, mid ) );
    subs.add( new FractalLine( mid, tmpB ) );
    subs.add( new FractalLine( tmpB, B ) );
    have_subs = true;
  }
  
  private void sub_quad( float pc, float way ) {
    float hpc = pc * 0.5;
    PVector tmpA = new PVector( A.x + vec.x * ( hpc + division_offset ), A.y + vec.y * ( hpc + division_offset ), A.z + vec.z * ( hpc + division_offset ) );
    PVector tmpB = new PVector( B.x - vec.x * ( hpc - division_offset ), B.y - vec.y * ( hpc - division_offset ), B.z - vec.z * ( hpc - division_offset ) );
    PVector upA = tmpA.copy();
    PVector upB = tmpB.copy();
    PVector perp = vec_norm.cross(PERPENDICULAR);
    perp.normalize();
    float pci = (1-pc) * len;
    upA.x += perp.x * height_multiplier * pci * way;
    upA.y += perp.y * height_multiplier * pci * way;
    upA.z += perp.z * height_multiplier * pci * way;
    upB.x += perp.x * height_multiplier * pci * way;
    upB.y += perp.y * height_multiplier * pci * way;
    upB.z += perp.z * height_multiplier * pci * way;
    subs.add( new FractalLine( A, tmpA ) );
    subs.add( new FractalLine( tmpA, upA ) );
    subs.add( new FractalLine( upA, upB ) );
    subs.add( new FractalLine( upB, tmpB ) );
    subs.add( new FractalLine( tmpB, B ) );
    have_subs = true;
  }

  public void subdivide( int type, float pc ) {
    if ( have_subs ) {
      for ( FractalLine fl : subs ) {
        fl.subdivide( type, pc );
      }
      return;
    }
    switch( type ) {
    case DIVISION_TYPE_TRI:
      sub_tri( pc, 1 );
      break;
    case DIVISION_TYPE_TRII:
      sub_tri( pc, -1 );
      break;
    case DIVISION_TYPE_QUAD:
      sub_quad( pc, 1 );
      break;
    case DIVISION_TYPE_QUADI:
      sub_quad( pc, -1 );
      break;
    default:
      return;
    }
  }

  public void reset() {
    subs = new ArrayList<FractalLine>();
    have_subs = false;
  }

  public void draw() {
    if ( have_subs ) {
      for ( FractalLine fl : subs ) {
        fl.draw();
      }
      return;
    }
    line( A.x, A.y, A.z, B.x, B.y, B.z );
  }
}

FractalLine fls[];

void primitive( int slices, int sides, float radius, float depth, float rotation ) {
  fls = new FractalLine[slices*sides];
  int sid = 0;
  float z = -depth * 0.5;
  float angl = 0;
  for ( int j = 0; j < slices; ++j ) {
    angl += rotation;
    PVector start = new PVector( cos( angl ) * radius, sin( angl ) * radius, z ); 
    PVector prev = start.copy();
    for ( int i = 1; i < sides; ++i ) {
      float a = angl + TWO_PI * i / sides;
      PVector vec = new PVector( cos( a ) * radius, sin( a ) * radius, z );
      fls[sid] = new FractalLine( prev, vec );
      sid++;
      prev = vec.copy();
    }
    fls[sid] = new FractalLine( prev, start );
    sid++;
    if ( slices > 1) {
      z += depth / (slices-1);
    }
  }
  println( sid );
  println( fls.length );
}

void setup() {
  size(600, 600, P3D);
  primitive( 1, 3, width * 0.3, 0, 0 );
}

void draw() {
  background(0);
  stroke(255);
  pushMatrix();
  translate( width / 2, height / 2, 0 );
  rotateX( frameCount * 0.002 );
  rotateY( frameCount * 0.004 );
  for ( FractalLine fl : fls ) {
    fl.draw();
  }
  rotateX( PI * 0.5 );
  for ( FractalLine fl : fls ) {
    fl.draw();
  }
  popMatrix();
}

void keyPressed() {

  for ( FractalLine fl : fls ) {
    switch( key ) {
    case 't':
      fl.subdivide( DIVISION_TYPE_TRI, division_factor );
      break;
    case 'y':
      fl.subdivide( DIVISION_TYPE_TRII, division_factor );
      break;
    case 'q':
      fl.subdivide( DIVISION_TYPE_QUAD, division_factor );
      break;
    case 's':
      fl.subdivide( DIVISION_TYPE_QUADI, division_factor );
      break;
    case 'r':
      fl.reset();
      break;
    }
  }
}
