int window_x = 100;
int window_y = 100;
boolean check_window_pos = true;
int window_width = 1024;
int window_height = 768;

public void validatePosition() {
  if ( !check_window_pos )
    return;
  try {
    if ( frame.getLocation().x != window_x || frame.getLocation().y != window_y ) {
      frame.dispose();
      frame.setUndecorated(true);
      frame.setLocation( window_x, window_y );
    } else {
      check_window_pos = false;
    }
  } catch( NullPointerException e ) {
    println( e );
  }
}

void setup() {
  size( 1024, 768 );
}

void draw() {
  validatePosition();
  background( 255 );
}
