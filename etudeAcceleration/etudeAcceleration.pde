PVector prev_pos;
PVector pos;
PVector acc;
PVector gravity;
boolean touchground;

boolean[] arrows;

float[][] plateformes;

void setup() {
  size( 600,600 );
  arrows = new boolean[] { false,false,false,false };
  prev_pos = new PVector( 0,0 );
  pos = new PVector( 0,0 );
  acc = new PVector( 1,0 );
  gravity = new PVector( 0, 0.1 );
  
  plateformes = new float[3][4];
  plateformes[ 0 ] = new float[] {   -400,280,    400,350   };
  plateformes[ 1 ] = new float[] {   -400,10,     -50,100   };
  plateformes[ 2 ] = new float[] {   -70,-10,    -10,50   };
  
}

void draw() {
  
  // modification de la gravité
  // gravity.x = cos( frameCount * 0.01 );
  // gravity.y = sin( frameCount * 0.01 );
  // gravity.normalize();
  // gravity.mult( 0.1 );
  
  if ( arrows[ 0 ] )
    acc.x -= 0.1;
  if ( arrows[ 1 ] )
    acc.y -= 5;
  if ( arrows[ 2 ] )
    acc.x += 0.1;
  if ( arrows[ 3 ] )
    acc.y += 0.5;
  acc.x *= 0.99;
  acc.y *= 0.9;
  acc.add( gravity );
  prev_pos.set( pos );
  pos.add( acc );
  
  for ( int i = 0; i < plateformes.length; i++ ) {
    float[] p = plateformes[ i ];
    if ( 
      pos.x >= p[ 0 ] && pos.x <= p[ 2 ] && 
      pos.y >= p[ 1 ] && pos.y <= p[ 3 ] &&
      prev_pos.y <= p[ 1 ] ) {
      pos.y = p[ 1 ];
    }
  }
  
  background( 255 );
  translate( width * 0.5, height * 0.5 );
  
    
  noStroke();
  fill( 0,127 );
  for ( int i = 0; i < plateformes.length; i++ ) {
    float[] p = plateformes[ i ];
    rect( p[0], p[1], p[2] - p[0], p[3] - p[1] );
  }
  
  noStroke();
  fill( 0 );
  ellipse( pos.x, pos.y, 5,5 );
  stroke( 255,0,0 );
  line( pos.x, pos.y, pos.x + acc.x * 100, pos.y );
  stroke( 0,255,0 );
  line( pos.x, pos.y, pos.x, pos.y + acc.y * 100 );
  // gravity
  noStroke();
  fill( 0 );
  ellipse( 0,0, 3,3 );  
  stroke( 255,0,255 );
  line( 0,0, gravity.x * 100, gravity.y * 100 );
  
}

void keyPressed() {
  // println( keyCode );
  switch( keyCode ) {
    case 37:
    case 38:
    case 39:
    case 40:
      arrows[ keyCode - 37 ] = true;
      break;
    default:
      println( keyCode );
      break;
  }
}
void keyReleased() {
  // println( keyCode );
  switch( keyCode ) {
    case 37:
    case 38:
    case 39:
    case 40:
      arrows[ keyCode - 37 ] = false;
      break;
    default:
      println( keyCode );
      break;
  }
}
