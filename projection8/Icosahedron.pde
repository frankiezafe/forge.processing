class Icosahedron extends PointDrawer {
  
  private PVector[] origins;
  public PVector[] pts;
  public Segment[] edges;
  
  public Icosahedron( float radius ) {
    
    super();
    
    pts = new PVector[ 12 ];
    
    float c = dist( cos(0) * radius, sin(0) * radius, cos(radians(72)) * radius, sin(radians(72)) * radius );
    float b = radius;
    float a = ( float )( Math.sqrt( ( (c*c)-(b*b) ) ) );
    float triHt = ( float )( Math.sqrt( (c*c) - ( (c/2) * (c/2) ) ) );
    
    // top
    pts[ 0 ] = new PVector(0, 0, triHt/2.0f+a);
    // top circle
    float angl = 0;
    for ( int i = 1; i < 6; i++ ){
      pts[ i ] = new PVector( cos(angl) * radius, sin(angl) * radius, triHt/2.0f );
      angl += radians(72);
    }
    // bottom circle
    angl = 72.0f / 2.0f;
    for ( int i = 6; i < 11; i++ ){
      pts[ i ] = new PVector( cos(angl) * radius, sin(angl) * radius, -triHt/2.0f );
      angl += radians(72);
    }
    pts[ 11 ] = new PVector( 0, 0, -( triHt / 2.0f + a ));
    
    // copying for later use
    origins = new PVector[ 12 ];
    for ( int i = 0; i < 12; i++ ) {
      origins[ i ] = new PVector( pts[ i ].x, pts[ i ].y, pts[ i ].z );
    }
    
    // creating edges
    int eid = 0;
    edges = new Segment[ 30 ];
    for ( int i = 0; i < 5; i++ ) {
      edges[ eid ] = new Segment( pts[ 0 ], pts[ 1 + i ] );
      eid++;
    }
    for ( int i = 0; i < 5; i++ ) {
      int j = ( i + 1 ) % 5;
      edges[ eid ] = new Segment( pts[ 1 + i ], pts[ 1 + j ] );
      eid++;
    }
    for ( int i = 0; i < 5; i++ ) {
      int j = ( i + 1 ) % 5;
      int h = ( i + 2 ) % 5;
      edges[ eid ] = new Segment( pts[ 1 + i ], pts[ 6 + h ] );
      eid++;
      edges[ eid ] = new Segment( pts[ 1 + i ], pts[ 6 + j ] );
      eid++;
    }
    for ( int i = 0; i < 5; i++ ) {
      int j = ( i + 1 ) % 5;
      edges[ eid ] = new Segment( pts[ 6 + i ], pts[ 6 + j ] );
      eid++;
    }
    for ( int i = 0; i < 5; i++ ) {
      edges[ eid ] = new Segment( pts[ 6 + i ], pts[ 11 ] );
      eid++;
    }
  
  }
  
  public void move( float x, float y, float z  ) {
    for ( int i = 0; i < 12; i++ ) {
      pts[ i ].set( origins[ i ].x + x, origins[ i ].y + y, origins[ i ].z + z );
    }
  }
  
  public void draw() {
  
    for ( int i = 0; i < edges.length; i++ ) {
      if ( edges[ i ] != null ) {
        edges[ i ].draw( viewrot );
      }
    }
    
  }
  
}
