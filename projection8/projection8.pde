import java.util.*; 

// view rotation
private PVector rot;
// inverted view rotation (to draw ellipse parallel to camera)
private PVector tor;

private int plnum = 30;
private float plgap = 500;
private float plgap_target = 500;
private Plane[] planes;

private Icosahedron ico;

// used in plane to calculate U & V vector
public static final PVector UP = new PVector( 0,1,0 );

private PImage tex;

private boolean displayMode;
private boolean displayIco;
private boolean displayPlanes;
private boolean displaySlices;
private boolean useText;

public void setup() {

  size( 800, 600, P3D );

  PVector planesOr = new PVector( 1,0,0 );
  planesOr.normalize();
  planes = new Plane[ plnum ];
  float ppos = plgap * ( plnum - 2 ) * -0.25;
  
  for ( int i = 0; i < plnum; i++ ) {
    if ( i == round( plnum / 2.f ) ) {
      planesOr = new PVector( 0,0,1 );
      ppos = plgap * ( plnum - 2 ) * -0.25;
    }
    planes[ i ] = new Plane( planesOr.x * ppos, planesOr.y * ppos, planesOr.z * ppos );
    planes[ i ].normale.set( 
      planesOr.x,
      planesOr.y,
      planesOr.z );
    planes[ i ].update();
    ppos += plgap;
  }

  rot = new PVector();
  tor = new PVector();
  
  ico = new Icosahedron( 200 );
  
  tex = loadImage( "texture.jpg" );
  textureWrap( REPEAT );
  
  displayMode = true;
  displayPlanes = true;
  displayIco = false;
  displaySlices = false;
  useText = false;
  
}

public void draw() {

  if ( frameCount == 300 ) {
    displayIco = true;
  } else if ( frameCount == 600 ) {
    displaySlices = true;
  } else if ( frameCount == 900 ) {
    plgap_target = 30;
  } else if ( frameCount == 1800 ) {
    displayPlanes = false;
  } else if ( frameCount == 1900 ) {
    displayIco = false;
  } else if ( frameCount == 2000 ) {
    useText = true;
  }
  
  plgap += ( plgap_target - plgap ) * 0.01;
  float ppos = plgap * ( plnum - 2 ) * -0.25;
  for ( int i = 0; i < plnum; i++ ) {
    if ( i == round( plnum / 2.f ) )
      ppos = plgap * ( plnum - 2 ) * -0.25;
    planes[ i ].xyz.set( planes[ i ].normale.x * ppos, planes[ i ].normale.y * ppos, planes[ i ].normale.z * ppos );
    planes[ i ].update();
    ppos += plgap;
  }
  
  ico.move( sin( frameCount * 0.02 ) * 100, 0, cos( frameCount * 0.02 ) * 100 );
  
  if ( displayMode ) {
    
    rot.x += ( -0.9 - rot.x ) * 0.1;
    rot.y += 0.003;
    
  } else {
    
    rot.x = ( mouseY - height * 0.5) / 100;
    rot.y = ( -mouseX + width * 0.5) / 100;
    
  }
  tor.set( rot );
  tor.mult( -1 );
  
  for ( int i = 0; i < plnum; i++ )
    planes[ i ].viewrot = tor;
  
  ico.viewrot = tor;
  
  background( 10 );

  pushMatrix();
  translate( width * 0.5, height * 0.5, -width * 0.2 );  
  rotateX( rot.x );
  rotateY( rot.y );
  rotateZ( rot.z );
  
  // 3D axis
  strokeWeight( 3 );
  stroke( 255, 0, 0 );
  line( 0, 0, 0, 50, 0, 0 );
  stroke( 0, 255, 0 );
  line( 0, 0, 0, 0, 50, 0 );
  stroke( 0, 0, 255 );
  line( 0, 0, 0, 0, 0, 50 );
  strokeWeight( 1 );
  
  if ( displayIco )
    ico.draw();
  
  for ( int p = 0; p < plnum; p++ ) {
    
    planes[ p ].draw();
    
    if ( displayPlanes ) {
    
      int uvi = 0;
      UVPoint[] borders = new UVPoint[ 4 ];
      for ( float y  = -200; y <= 200; y+= 400 ) {
      for ( float x  = -200; x <= 200; x+= 400 ) {
        borders[ uvi ] = new UVPoint( x, y );
        borders[ uvi ].render( planes[ p ] );
        uvi++;
      }
      }
      UVFace fs0 = new UVFace( borders[ 0 ], borders[ 1 ], borders[ 2 ] );
      UVFace fs1 = new UVFace( borders[ 1 ], borders[ 2 ], borders[ 3 ] );
      beginShape( TRIANGLES );
      fs0.setColor( 0,255,255,255 );
      fs0.printVertices();
      fs1.setColor( 0,255,255,255 );
      fs1.printVertices();
      endShape();
    
    }
    
    if ( displaySlices ) {
      
      ArrayList< UVPoint > collected = new ArrayList< UVPoint >();
      
      for ( int i = 0; i < ico.edges.length; i++ ) {
        UVPoint tmpuv = planes[ p ].intersect( ico.edges[ i ] );
        if ( tmpuv != null )
          collected.add( tmpuv );
      }
      
      if ( collected.size() > 2 ) {
        UVPoint[] colpts = collected.toArray( new UVPoint[ collected.size() ] );
        UVPoint barycenter = sortUVpts( colpts );
        UVFace[] projfaces = new UVFace[ colpts.length ];
        for ( int i = 0; i < colpts.length; i++ ) {
          int j = ( i + 1 ) % colpts.length;
          projfaces[ i ] = new UVFace( colpts[ i ], colpts[ j ], barycenter );
          if ( useText ) {
            projfaces[ i ].stroked = false;
            projfaces[ i ].filled = false;
          }
          float grey = 50 + ( p * 1.f / ( plnum - 1 ) ) * 200;
          projfaces[ i ].setColor( grey, grey, grey, 255 );
        }
        
        if ( !useText ) {
          stroke( 255 );
          fill( 180 );
        } else {
          noStroke();
        }
        beginShape( TRIANGLES );
        if ( useText )
          texture( tex );
        for ( int i = 0; i < projfaces.length; i++ ) {
          if ( !useText )
            fill( projfaces[ i ].r );
          projfaces[ i ].printVertices( false );
        }
        endShape();
      }
      
    }
  
  }
  
  popMatrix();
  
  fill( 255 );
  text( frameRate, 10, 25 );
  text( frameCount, 10, 40 );
  
}

public void keyPressed() {

  if ( key == 'm' )
    displayMode = !displayMode;

  if ( key == 'a' )
    plgap_target = 150;

  if ( key == 'b' )
    plgap_target = 30;
    
  if ( key == 'c' )
    useText = !useText;

}


