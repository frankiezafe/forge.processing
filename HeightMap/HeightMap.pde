PImage src;
PGraphics tex;
PVector map_scale = new PVector(1,1,40);
ArrayList<PVector> verts;
ArrayList<PVector> uvs;
PShape dots;
PShape mesh;
float rotz = 0;

float rgb0[] = { 0,1,0.95 };
float rgb1[] = { 1,0.25,0 };
float rgb2[] = { 1,0,0 };

color interpolate( float alpha ) {
  float rgb[] = { 0,0,0 };
  float a0, a1, a2 = 0;
  
  for ( int i = 0; i < 3; i++ ) {
    rgb[i] = rgb0[i] * (1-alpha) + rgb1[i] * alpha;
  }
  return color( rgb[0]*255, rgb[1]*255, rgb[2]*255 );
}

void add_vertuv( int id ) {
    PVector v = verts.get(id);
    PVector u = uvs.get(id);
    mesh.vertex( v.x, v.y, v.z, u.x, u.y );
}

void setup() {

  size(800, 600, P3D);
  
  src = loadImage("heightmap.jpg");
  //src = loadImage("terrain.htmlcd_Image18.png");
  //src = loadImage("vtest.png");
  //src = loadImage("billie_jean.jpg");
  
  tex = createGraphics( src.width, src.height );
  tex.beginDraw();
  tex.background(102);
  tex.endDraw();
  
  verts = new ArrayList<PVector>();
  uvs = new ArrayList<PVector>();
  
  src.loadPixels();
  tex.loadPixels();
  float hw = src.width * 0.5;
  float hh = src.height * 0.5;
  for ( int y = 0; y < src.height; y++ ) {
    for ( int x = 0; x < src.width; x++ ) {
      int id = x + y * src.width;
      float red = red(src.pixels[id]) * 0.003921569;
      PVector vert = new PVector( 
        (-hw + x) * map_scale.x, 
        (-hh + y) * map_scale.y, 
        red * map_scale.z );
      PVector uv = new PVector( 
        float(x),
        float(y)
        );
      verts.add( vert );
      uvs.add( uv );
      tex.pixels[id] = interpolate(red);
    }
  }
  tex.updatePixels();

  dots = createShape();
  dots.beginShape(POINTS);
  dots.stroke( 255 );
  dots.strokeWeight( 1 );
  for ( int i = 0; i < verts.size(); ++i ) {
    PVector v = verts.get(i);
    PVector u = uvs.get(i);
    dots.vertex( v.x, v.y, v.z, u.x, u.y );
  }
  dots.endShape();
  
  mesh = createShape();
  mesh.beginShape(TRIANGLES);
  mesh.noStroke();
  mesh.fill(255);
  mesh.texture( tex );
  for ( int y = 1; y < src.height; y++ ) {
  for ( int x = 1; x < src.width; x++ ) {
    int id0 = (x-1) + (y-1) * src.width;
    int id1 = (x-0) + (y-1) * src.width;
    int id2 = (x-0) + (y-0) * src.width;
    int id3 = (x-1) + (y-0) * src.width;
    add_vertuv( id0 );
    add_vertuv( id1 );
    add_vertuv( id2 );    
    add_vertuv( id0 );
    add_vertuv( id2 );
    add_vertuv( id3 );
  }
  }
  mesh.endShape();
  
}

void draw() {

  background(0);
  
  stroke(255);
  pushMatrix();
  translate( width / 2, height / 2, 0 );
  rotateX( PI * 0.20 );
  rotz += 0.1 * pow( (mouseX-width*0.5) / (width*0.5), 3 );
  rotateZ( rotz );
  scale( 1,1, -10 * (mouseY-height*0.5) / (height*0.5) );
  //shape( dots );
  shape( mesh );
  popMatrix();
  
  hint(DISABLE_DEPTH_TEST);
  image( src, 10,10,100,100 );
  image( tex, 120,10,100,100 );
  hint(ENABLE_DEPTH_TEST);
  
}
