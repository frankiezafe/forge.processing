import java.util.Arrays;

int COLUMNS = 4;
int THUMB_WIDTH = 800;
int THUMB_MARGIN = 10;
int TOPBAR_HEIGHT = 27;
int BOTTOM_SPACE = 25;

void setup() {
  //size(800, 600);

  println( dataPath("") );
  File curDir = new File(dataPath("")).getParentFile().getParentFile();
  File[] filesList = curDir.listFiles();
  File[] images = null;
  for (File f : filesList) {
    if (f.isDirectory() && f.getName().equals( "screenshots" ) ) {
      images = f.listFiles();
    }
  }
  
  Arrays.sort(images); 
  
  float im_width =( THUMB_WIDTH - THUMB_MARGIN * (COLUMNS + 1.0) ) / COLUMNS;
  
  int coli = 0;
  float latesth = 0;
  int total_height = THUMB_MARGIN;
  ArrayList<String> names = new ArrayList<String>();
  ArrayList<PImage> pimages = new ArrayList<PImage>();
  if ( images != null ) {
    for ( File i : images ) {
      println( "adding " + i.getName() );
      names.add(i.getName());
      PImage im = loadImage( i.getAbsolutePath() );
      pimages.add( im );
      float h = ( im_width * ((im.height - TOPBAR_HEIGHT) * 1.f / im.width)) + BOTTOM_SPACE;
      if ( latesth == 0 || latesth < h ) {
        latesth = h;
      }
      coli++;
      if (coli >= COLUMNS) {
        coli = 0;
        total_height += latesth;
        latesth = 0;
      }
    }
    println( latesth );
    total_height += latesth;
  }
  
  PGraphics out = createGraphics( THUMB_WIDTH, total_height );
  out.beginDraw();
  out.background(20);
  int offsetx = THUMB_MARGIN;
  int offsety = THUMB_MARGIN;
  int i = 0;
  coli = 0;
  for ( PImage im : pimages ) {
    PGraphics thumb = createGraphics( im.width, im.height - TOPBAR_HEIGHT );
    thumb.beginDraw();
    thumb.background(0);
    thumb.image( im, 0, -TOPBAR_HEIGHT );
    thumb.endDraw();
    float h = im_width * (thumb.height * 1.f / thumb.width);
    if (latesth == 0 || latesth < h) {
      latesth = h;
    }
    out.image( thumb, offsetx, offsety, im_width, h );
    String n = names.get(i);
    int last_underscore = n.lastIndexOf('_');
    if ( last_underscore != -1 ) {
      n = n.substring(0,last_underscore);
    }
    out.text( n, offsetx, offsety + h + 15 );
    coli++;
    if ( coli >= COLUMNS ) {
      coli = 0;
      offsetx = THUMB_MARGIN;
      offsety += latesth + BOTTOM_SPACE;
      latesth = 0;
    } else {
      offsetx += im_width + THUMB_MARGIN;
    }
    i++;
  }
  out.endDraw();
  out.save("thumbs.png");
  
  exit();
  
}
