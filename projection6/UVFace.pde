class UVFace {

  UVPoint p1;
  UVPoint p2;
  UVPoint p3;
  float r;
  float g;
  float b;
  float a;
  
  boolean stroked = true;
  boolean filled = false;
  
  public UVFace( UVPoint p1, UVPoint p2, UVPoint p3 ) {
    this.p1 = p1;
    this.p2 = p2;
    this.p3 = p3;
    r = random( 0, 255 );
    g = random( 0, 255 );
    b = random( 0, 255 );
    a = 255;
  }
  
  public void setColor( float r, float g, float b, float a ) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
  }
  
  public void printVertices() {
    if ( stroked )
      stroke( r,g,b,a );
    else
      noStroke();
    if ( filled )
      fill( r,g,b,a );
    else
      noFill();
    vertex( p1.x, p1.y, p1.z, p1.u, p1.v );
    vertex( p2.x, p2.y, p2.z, p2.u, p2.v );
    vertex( p3.x, p3.y, p3.z, p3.u, p3.v );
  } 
  
}
