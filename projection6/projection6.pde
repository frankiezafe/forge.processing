import java.util.*; 

// view rotation
private PVector rot;
// inverted view rotation (to draw ellipse parallel to camera)
private PVector tor;

private Plane p;

private Segment[] lines;

// used in plane to calculate U & V vector
public static final PVector UP = new PVector( 0,1,0 );

public void setup() {

  size( 800, 600, P3D );
  p = new Plane( 50, -50, 50 );
  rot = new PVector();
  tor = new PVector();
  
  lines = new Segment[ 20 ];
  for ( int i = 0; i < lines.length; i++ ) {
    lines[ i ] = new Segment( 
      new PVector( random( -300, 300 ), random( -300, 300 ), random( -300, 300 ) ),
      new PVector( random( -300, 300 ), random( -300, 300 ), random( -300, 300 ) )
    );
  }
  
}

public void draw() {

  p.xyz.x = cos( frameCount * 0.02 ) * 300;
  p.update();
  
  rot.x = ( mouseY - height * 0.5) / 100;
  rot.y = ( -mouseX + width * 0.5) / 100;
  tor.set( rot );
  tor.mult( -1 );
  
  p.viewrot = tor;
  
  background( 10 );

  pushMatrix();
  translate( width * 0.5, height * 0.5, -width * 0.5 );  
  rotateX( rot.x );
  rotateY( rot.y );
  rotateZ( rot.z );
  
  // 3D axis
  strokeWeight( 3 );
  stroke( 255, 0, 0 );
  line( 0, 0, 0, 50, 0, 0 );
  stroke( 0, 255, 0 );
  line( 0, 0, 0, 0, 50, 0 );
  stroke( 0, 0, 255 );
  line( 0, 0, 0, 0, 0, 50 );
  strokeWeight( 1 );
  
  p.draw( tor );
  
  UVPoint proj = null;
  for ( int i = 0; i < lines.length; i++ ) {
    lines[ i ].draw( tor );
    proj = p.intersect( lines[ i ] );
    noStroke();
    if ( proj != null )
      p.drawpt( proj.x, proj.y, proj.z, 10 );
  }
  
  popMatrix();
  
  fill( 255 );
  text( frameRate, 10, 25 );
  
}


