float radius = 300;
PVector[] tri;
PVector[] heights;
float hsize = 0;
float esize = 0;
float[] projections;

void setup() {
  size( 800, 800 );
  
  tri = new PVector[3];
  for ( int i = 0; i < 3; ++i ) {
    float a = i * TWO_PI / 3;
    tri[i] = new PVector( cos(a) * radius, sin(a) * radius );
  }
  heights = new PVector[3];
  for ( int i = 0, j = 1, h = 2; i < 3; ++i, ++j, ++h ) {
    j %= 3;
    h %= 3;
    // mid of opposite seg:
    PVector mid = new PVector( tri[j].x + ( tri[h].x - tri[j].x ) * 0.5, tri[j].y + ( tri[h].y - tri[j].y ) * 0.5 );
    heights[i] = new PVector( mid.x - tri[i].x, mid.y - tri[i].y );
    if ( hsize == 0 ) {
      hsize = heights[i].mag();
      esize = new PVector( tri[h].x - tri[j].x, tri[h].y - tri[j].y ).mag();
    }
    heights[i].normalize();
  }
  projections = new float[3];
}

void draw() {
  
  background( 0 );
  pushMatrix();
  translate( width * 0.5, height * 0.5 );
  stroke(255);
  for ( int i = 0, j = 1; i < 3; ++i, ++j ) {
    j %= 3;
    line( tri[i].x, tri[i].y, tri[j].x, tri[j].y );
  }
  stroke(255,0,0,200);
  for ( int i = 0; i < 3; ++i ) {
    switch( i ) {
      case 0:
        stroke(255,255,0,200);
        break;
      case 1:
        stroke(255,0,255,200);
        break;
      default:
        stroke(0,255,255,200);
        break;
    }
    line( tri[i].x, tri[i].y, tri[i].x + heights[i].x * hsize, tri[i].y + heights[i].y * hsize );
  }
  //stroke(255,0,255,200);
  //for ( int i = 0, j = 1, h = 2; i < 3; ++i, ++j, ++h ) {
  //  j %= 3;
  //  h %= 3;
  //  PVector mid = new PVector( tri[h].x + (tri[j].x-tri[h].x) * 0.5, tri[h].y + (tri[j].y-tri[h].y) * 0.5 );
  //  line( mid.x, mid.y, mid.x + heights[i].x * 30, mid.y + heights[i].y * 30 );
  //}
  // projections on each axis
  PVector m = new PVector( mouseX - width * 0.5, mouseY - height * 0.5 );
  noStroke();
  fill( 255, 150 );
  ellipse( m.x, m.y, 15, 15 );
  // making sure m is inside the triangle
  for ( int i = 0, j = 1; i < 3; ++i, ++j ) {
    // using the height at + 2
    j %= 3;
    int h = ( i + 2 ) % 3;
    PVector rel = new PVector( m.x - tri[i].x, m.y - tri[i].y );
    rel.mult( 1 / esize );
    float d = heights[h].dot( rel );
    if ( d > 0 ) {
      m.x -= heights[h].x * d * esize;
      m.y -= heights[h].y * d * esize;
    }
    rel = new PVector( m.x - tri[i].x, m.y - tri[i].y );
    rel.mult( 1 / esize );
    PVector dir = new PVector( tri[j].x - tri[i].x, tri[j].y - tri[i].y );
    dir.normalize();
    d = dir.dot( rel );
    if ( d < 0 ) {
      m.x -= dir.x * d * esize;
      m.y -= dir.y * d * esize;
    } else if ( d > 1 ) {
      m.x += dir.x * (1-d) * esize;
      m.y += dir.y * (1-d) * esize;
    }
  }
  fill( 0, 255, 0 );
  ellipse( m.x, m.y, 10, 10 );
  for ( int i = 0; i < 3; ++i ) {
    PVector rel = new PVector( m.x - tri[i].x, m.y - tri[i].y );
    rel.mult( 1 / hsize );
    float d = heights[i].dot( rel );
    //if ( d < 0 ) { d = 0; }
    //else if ( d > 1 ) { d = 1; }
    projections[i] = 1 - d;
    PVector proj = new PVector( heights[i].x * d * hsize, heights[i].y * d * hsize );  
    switch( i ) {
      case 0:
        stroke(255,255,0,120);
        break;
      case 1:
        stroke(255,0,255,120);
        break;
      default:
        stroke(0,255,255,120);
        break;
    }
    strokeWeight( 4 );
    line( tri[i].x + proj.x, tri[i].y + proj.y, tri[i].x + heights[i].x * hsize, tri[i].y + heights[i].y * hsize );
    strokeWeight( 1 );
    noStroke();
    fill( 0, 255, 0 );
    ellipse( tri[i].x + proj.x, tri[i].y + proj.y, 7, 7 );
    text( projections[i], tri[i].x + proj.x, tri[i].y + proj.y - 8 );
    stroke( 0, 255, 0 );
    line( tri[i].x + proj.x, tri[i].y + proj.y, m.x, m.y );
  }
  popMatrix();
  
  fill( 255 );
  text( "total: " + (projections[0] + projections[1] + projections[2]), 10, 25 );
  
  // percentages
  float y = 50;
  noStroke();
  for ( int i = 0; i < 3; ++i ) {
    switch( i ) {
      case 0:
        fill(255,255,0,200);
        break;
      case 1:
        fill(255,0,255,200);
        break;
      default:
        fill(0,255,255,200);
        break;
    }
    float h = projections[i] * 300;
    rect( 10, y, 30, h );
    y += h;
  }
  
}