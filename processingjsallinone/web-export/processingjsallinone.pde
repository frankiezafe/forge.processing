void setup() {
  
  size( 600, 600 );
  
}

void draw() {

  background( random( 150,210 ), random( 150,210 ), random( 150,210 ) );
  stroke( 0 );
  for ( int i = 0; i < 50; i++ ) {
    line( 
      random( 10, height - 10 ), 
      random( 10, height - 10 ),
      random( 10, height - 10 ), 
      random( 10, height - 10 ) );
  }

}

