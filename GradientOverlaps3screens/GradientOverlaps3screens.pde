/* OpenProcessing Tweak of *@*http://www.openprocessing.org/sketch/79860*@* */
/* !do not delete the line above, required for linking your tweak if you upload again */
/*
This sketch is a research about the best way to generate a gradient to merge two projections
together. To be tested, it obviously requires 2 projectors :)
- Use arrow up & down to adapt the size of the overlap1
- Use arrow left & right to switch gradient calculation method
- Use 'f' to enable or disable fullscreen
- Use 'p' to print the current conifguration
*/

import java.awt.*;
import controlP5.*;

int screenw = 400;
int screenh = 720;
// String mode = P2D;

PGraphics screen1;
PGraphics screen2;
PGraphics screen3;

PImage mire;


int overlap1 = 50;
PGraphics screen1overlap_R;
PGraphics screen2overlap_L;

int overlap2 = 50;
PGraphics screen2overlap_R;
PGraphics screen3overlap_L;

public static final int GRADIENT_NONE =     0;
public static final int GRADIENT_LINEAR =   1;
public static final int GRADIENT_SINUS =    2;
public static final int GRADIENT_SQUARE =   3;
public static final int GRADIENT_EXP =      4;
int gradientmode = GRADIENT_LINEAR;
DropdownList gradient_list;

boolean fullscreenenabled = false;

ControlP5 cp5;

public void init() {
  frame.removeNotify();
  frame.setUndecorated(true);
  frame.addNotify();
  super.init();
}

void setup() {
  
  //size( screenw * 3, screenh, mode ); // for openprocessing, using (screenw * 2, screenh) is not working...
  size( screenw * 3, screenh );
  
  mire = loadImage("mire_1024x768.png");
  
//  screen1 = createGraphics( screenw, screenh, mode );
//  screen2 = createGraphics( screenw, screenh, mode );
//  screen3 = createGraphics( screenw, screenh, mode );
  screen1 = createGraphics( screenw, screenh );
  screen2 = createGraphics( screenw, screenh );
  screen3 = createGraphics( screenw, screenh );
  
  renderOverlaps();
  
  printConfiguration();
  
  // UI
  cp5 = new ControlP5(this);
  cp5.addSlider("overlap1").setPosition( 50, 50 ).setRange( 0, 255 ).setSize( 200, 20 );
  cp5.addSlider("overlap2").setPosition( 50, 75 ).setRange( 0, 255 ).setSize( 200, 20 );
  gradient_list = cp5.addDropdownList("gradient mode").setPosition( 50, 110 );
  gradient_list.addItem( "NONE", 0 );
  gradient_list.addItem( "LINEAR", 1 );
  gradient_list.addItem( "SINUS", 2 );
  gradient_list.addItem( "SQUARE", 3 );
  gradient_list.addItem( "EXP", 4 );
  cp5.addButton("apply").setValue(0).setPosition( 10,50 ).setSize( 35, 35 );
  cp5.addButton("save").setValue(0).setPosition( 10,90 ).setSize( 35, 35 );
  cp5.addButton("print_config").setValue(0).setPosition( 300,50 ).setSize( 60, 35 );

}

void apply( int i ) {
  renderOverlaps();
}

void save( int i ) {
  renderOverlaps();
  screen1overlap_R.save( "overlap1_right.png" );
  screen2overlap_L.save( "overlap2_left.png" );
  screen2overlap_R.save( "overlap2_right.png" );
  screen3overlap_L.save( "overlap3_left.png" );
}

void print_config( int i ) {
  printConfiguration();
}

void controlEvent(ControlEvent theEvent) {
  
  if (theEvent.isGroup()) {
    if ( theEvent.getGroup() == gradient_list ) {
      int newv = (int) theEvent.getGroup().getValue();
      gradientmode = newv;
    }
    //println("event from group : "+theEvent.getGroup().getValue()+" from "+theEvent.getGroup());
  } else if (theEvent.isController()) {
    // println("event from controller : "+theEvent.getController().getValue()+" from "+theEvent.getController());
  }
}

void setPlainWhiteBg( PGraphics pg ) {
  pg.beginDraw();
  pg.fill( 255 );
  pg.noStroke();  
  pg.rect( 0, 0, pg.width, pg.height );
  pg.endDraw();
}

void renderOverlaps() {
//  screen1overlap_R = createGraphics( overlap1, screenh, mode );
//  screen2overlap_L = createGraphics( overlap1, screenh, mode );
  screen1overlap_R = createGraphics( overlap1, screenh );
  screen2overlap_L = createGraphics( overlap1, screenh );
  renderOverlap( screen1overlap_R, true );
  renderOverlap( screen2overlap_L, false );
  
  screen2overlap_R = createGraphics( overlap2, screenh );
  screen3overlap_L = createGraphics( overlap2, screenh );
  renderOverlap( screen2overlap_R, true );
  renderOverlap( screen3overlap_L, false );
  
  renderMire();
  
}

void renderMire() {
  screen1.beginDraw();
  screen1.image( mire, 0, 0, screenw * 3 - (overlap1+overlap2), screenh );
  screen1.endDraw();
  screen2.beginDraw();
  screen2.image( mire, -(screenw - overlap1), 0, screenw * 3 - (overlap1+overlap2), screenh );
  screen2.endDraw();
  screen3.beginDraw();
  screen3.image( mire, -((screenw * 2) - (overlap1+overlap2)), 0, screenw * 3 - (overlap1+overlap2), screenh );
  screen3.endDraw();
}

void renderOverlap( PGraphics pg, boolean revert ) {
    
  /*  
  setPlainWhiteBg( pg );
  
  pg.beginDraw();
  pg.stroke( 0 );
  for ( int i = 0; i < 100; i++ ) {
    pg.line( random( 0, 100 ), random( 0, height ), random( 0, 100 ), random( 0, height ) );
  }
  pg.endDraw();
  */
  
  pg.loadPixels();
  int[] blackvalues = new int[ pg.width ];
  
  float b;
  float bgap;
  
  switch (gradientmode) {
    
    case GRADIENT_NONE:
      for ( int x = 0; x < pg.width; x++ )
        blackvalues[ x ] = color(0,0,0,255);
      break;
    
    case GRADIENT_LINEAR:
      b = 0;
      bgap = 255.f / (pg.width-1);
      if (revert) {
        b = 255;
        bgap *= -1;
      }
      for ( int x = 0; x < pg.width; x++ ) {
        blackvalues[ x ] = color(0,0,0, 255 - b);
        b += bgap;
      }
      break;
      
    case GRADIENT_SINUS:
      b = 0;
      bgap = (PI * 0.5f) / (pg.width-1);
      if (revert) {
        b = PI * 0.5f;
        bgap *= -1;
      }
      for ( int x = 0; x < pg.width; x++ ) {
        blackvalues[ x ] = color(0,0,0, 255 - sin( b ) * 255 );
        b += bgap;
      }
      break;
      
    case GRADIENT_EXP:
      b = 0;
      bgap = 1.f / (pg.width-1);
      if (revert) {
        b = 1;
        bgap *= -1;
      }
      float emax = exp(1);
      for ( int x = 0; x < pg.width; x++ ) {
        blackvalues[ x ] = color(0,0,0, 255 - (1 - (emax - exp(b)) / emax ) * 255 );
        b += bgap;
      }
      break;
      
    case GRADIENT_SQUARE:
      b = 1;
      bgap = -1.f / (pg.width-1);
      if (revert) {
        b = 0;
        bgap *= -1;
      }
      for ( int x = 0; x < pg.width; x++ ) {
        blackvalues[ x ] = color(0,0,0, 255 - (1 - (b * b)) * 255 );
        b += bgap;
      }
      break;
      
  }
  
  for ( int x = 0; x < pg.width; x++ ) {
    for ( int y = 0; y < pg.height; y++ )
      pg.pixels[ x + y * pg.width ] = blackvalues[x];
      // pg.pixels[ x + y * pg.width ] = color( 255, 0, 0 );
  }
  pg.updatePixels();
  
}

void draw() {
  
  frame.setBounds( 0,0, width, height );
  frame.setVisible( true );
  
  background(255);
  image( screen1, 0, 0 );
  image( screen2, screenw, 0);
  image( screen3, screenw * 2, 0);
  image( screen1overlap_R, screenw - overlap1, 0);
  image( screen2overlap_L, screenw, 0);
  image( screen2overlap_R, screenw * 2 - overlap2, 0);
  image( screen3overlap_L, screenw * 2, 0);
  
  stroke( 255, 0, 0 );
  line( screenw - overlap1, 0, screenw - overlap1, height );
  line( screenw + overlap1, 0, screenw + overlap1, height );
  line( screenw * 2 - overlap2, 0, screenw * 2 - overlap2, height );
  line( screenw * 2 + overlap2, 0, screenw * 2 + overlap2, height );
  
}

void keyPressed() {
  switch ( keyCode ) {
    
    case 38: // '[arrow up]'
      overlap1++;
      if (overlap1 > screenw)
        overlap1 = screenw;
      // renderOverlaps();
      break;
    
    case 40: // '[arrow down]'
      overlap1--;
      if ( overlap1 < 0 )
        overlap1 = 0;
      // renderOverlaps();
      break;
      
    case 82:
      renderOverlaps();
      break;
    
    case 37: // '[arrow left]'
      gradientmode--;
      if ( gradientmode < 0 )
        gradientmode = GRADIENT_EXP;
      renderOverlaps();
      break;
    
    case 39: // '[arrow right]'
      gradientmode++;
      if ( gradientmode > GRADIENT_EXP )
        gradientmode = GRADIENT_NONE;
      renderOverlaps();
      break;
    case 70: // 'f'
      if ( frame != null && fullscreenenabled ) {
        frame.setResizable(true);
        frame.dispose();
        frame.setUndecorated(false);
        frame.resize(screenw * 2, screenh);
        frame.setVisible(true);
        fullscreenenabled = false;
        this.requestFocusInWindow();
      } else if ( frame != null && !fullscreenenabled ) {
        frame.setResizable(true);
        frame.dispose();
        frame.setUndecorated(true);
        frame.resize(
           (int) java.awt.Toolkit.getDefaultToolkit().getScreenSize().getWidth(),
           (int) java.awt.Toolkit.getDefaultToolkit().getScreenSize().getHeight()
           );
        frame.setVisible(true);
        this.requestFocusInWindow();
        fullscreenenabled = true;
      }
      break;
    case 80:
      printConfiguration();
      break;
    default:
      println( keyCode );
  }
}

void printConfiguration() {
 String s = "Current configuration is: \n";
 s += "-screens overlap1: " + overlap1 +"\n";
 s += "-screens overlap2: " + overlap2 +"\n";
 s += "-mode:";
  switch( gradientmode ) {
    case GRADIENT_NONE:
      s += "GRADIENT_NONE ("+GRADIENT_NONE+")";
      break;
    case GRADIENT_LINEAR:
      s += "GRADIENT_LINEAR ("+GRADIENT_LINEAR+")";
      break;
    case GRADIENT_SINUS:
      s += "GRADIENT_SINUS ("+GRADIENT_SINUS+")";
      break;
    case GRADIENT_EXP:
      s += "GRADIENT_EXP ("+GRADIENT_EXP+")";
      break;
    case GRADIENT_SQUARE:
      s += "GRADIENT_SQUARE ("+GRADIENT_SQUARE+")";
      break;
    default:
      s += "???";
      break;
  }
  s += "\n--------------------------";
  println(s);
}
