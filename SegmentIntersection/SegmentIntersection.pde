DraggableSegment segments[];
DraggableSegment dragged_line;
DraggablePoint dragged_pt;

void setup() {

  size(600,600);
  
  segments = new DraggableSegment[]{
    new DraggableSegment( 100, height/2, width-100, height/2 ),
    new DraggableSegment( width/2, 100, width/2, height-100 )
  };
  dragged_line = null;
  dragged_pt = null;
  
}

void draw() {
  
  for ( int i = 0; i < segments.length; ++i ) { segments[i].update(); }
  
  background(50);
  
  for ( int i = 0; i < segments.length; ++i ) { segments[i].draw(); }
  
  PVector intersect = segments[0].intersect( segments[1] );
  if ( intersect != null ) {
    noStroke();
    fill(255,0,0);
    ellipse( intersect.x, intersect.y, 5, 5 );
    noFill();
    stroke(255,0,0);
    ellipse( intersect.x, intersect.y, 30, 30 );
  }
  
}

DraggablePoint touched() {
  DraggablePoint t = null;
  for ( int i = 0; i < segments.length; ++i ) {
    DraggablePoint touched = segments[i].touch( mouseX, mouseY );
    if ( touched != null && t == null ) {
      dragged_line = segments[i];
      t = touched;
    }
  }
  return t;
}

void mouseMoved() {
  if ( dragged_pt == null ) {
    touched();
  }
}

void mouseDragged() {
  if ( dragged_pt != null ) {
    dragged_pt.drag( mouseX, mouseY );
  }
}

void mousePressed() {
  dragged_pt = touched();
}

void mouseReleased() {
  dragged_line = null;
  dragged_pt = null;
}
