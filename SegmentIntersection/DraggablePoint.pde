static float DP_MAX_DIST = 10;
static final int DP_DRAW_CIRCLE = 0;
static final int DP_DRAW_CROSS = 1;

class DraggablePoint extends PVector {
  
  private PVector offset;
  private boolean touched;
  private boolean dragged;
  
  public DraggablePoint(float x, float y) {
    this.x = x;
    this.y = y;
    offset = null;
    touched = false;
    dragged = false;
  }
  
  public PVector set( PVector src ) {
    this.x = src.x;
    this.y = src.y;
    this.z = src.z;
    return this;
  }
  
  public boolean touch( float x, float y ) {
    if ( dragged ) {
      dragged = false;
      offset = null;
    }
    touched = pow(x-this.x, 2) + pow(y-this.y, 2) <= DP_MAX_DIST * DP_MAX_DIST;
    return touched;
  }
  
  public void drag( float x, float y ) {
    if ( offset == null ) {
      offset = new PVector( this.x - x, this.y - y );
    }
    this.x = x + offset.x;
    this.y = y + offset.y;
    dragged = true;
  }
  
  public void draw( int draw_style ) {
    switch( draw_style ) {
      case DP_DRAW_CIRCLE:
        noFill();
        stroke( 255, 100 );
        if ( dragged ) {
          stroke( 255, 0, 0, 255 );
        } else if ( touched ) {
          stroke( 255 );
        }
        ellipse( this.x, this.y, DP_MAX_DIST * 2, DP_MAX_DIST * 2 );
        fill(255);
        noStroke();
        rect( this.x - 2, this.y - 2, 4, 4 );
        break;
      case DP_DRAW_CROSS:
        stroke( 255, 100 );
        if ( dragged ) {
          stroke( 255, 0, 0, 255 );
        } else if ( touched ) {
          stroke( 255 );
        }
        line( this.x - DP_MAX_DIST, this.y, this.x + DP_MAX_DIST, this.y );
        line( this.x, this.y - DP_MAX_DIST, this.x, this.y + DP_MAX_DIST );
        break;
    }
  }
  
  public void line_to( DraggablePoint other ) {
    line( this.x, this.y, other.x, other.y );
  }
  
  
}
