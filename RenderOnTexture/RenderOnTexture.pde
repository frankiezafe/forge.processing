PGraphics render;

void setup() {

  size( 800, 600, P3D );
  render = createGraphics( 1024,1024,P3D );
  
}

void draw() {

  // drawing in render texture
  render.beginDraw();
  render.background( 0,0,0 );
  for ( int i = 0; i < 300; ++i ) {
    render.stroke( random(0,255),random(0,255),random(0,255) );
    render.line( random(0,100),random(0,100),random(0,100), random(924,1024),random(924,1024),random(924,1024) );
  }
  render.endDraw();
  
  background( int( frameCount * 0.02 ) % 255, int( 120 + frameCount * 0.02 ) % 255, 0 );
  // display of texture in main viewport
  pushMatrix();
  // centering on screen
  translate( width*0.5, height*0.5, 0 );
  rotateY( frameCount * 0.01 );
  scale( 0.5, 0.5, 0.5 );
  image( render, -512, -512 );
  popMatrix();

}