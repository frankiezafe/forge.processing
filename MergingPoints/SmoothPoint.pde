public static final float MAX_ACCEL = 10;

class SmoothPoint extends PVector {

  public PVector lastpos;
  public PVector target;
  public PVector dir;
  public PVector accel;
  public float accel_length;
  public float reactivity;
  public float inertia;
  
  public SmoothPoint() {
    super();
    lastpos = new PVector();
    target = new PVector();
    dir = new PVector();
    accel = new PVector();
    reactivity  = 0.15f;
    inertia = 0.9;
  }
  
  public float deltaPos() {
    return dist( this, lastpos );
  }
  
  public void update() {
    
    lastpos.set( this.x, this.y, this.z );
    
    // vector to target
    PVector force = new PVector(
      target.x - this.x,
      target.y - this.y,
      target.z - this.z
    );
    
    PVector m = new PVector();
    m.set( force );
    m.mult( reactivity );
    PVector n = new PVector();
    n.set( dir );
    n.mult( 1 - reactivity );
    dir.set( m );
    dir.add( n );
    
    accel.add( dir );
    accel.mult( 1 - inertia );
    if ( accel.mag() > MAX_ACCEL ) {
      accel.normalize();
      accel.mult( MAX_ACCEL );
    }
    accel_length = accel.mag();
    this.add( accel );
    
  }
  
  

}
