class Point3D {

  public PVector pos;
  public float weight;
  
  public Point3D( float x, float y, float z, float w ) {    
    pos = new PVector( x,y,z );
    weight = w;
  }
  
  public void draw() {
  
    noFill();
    
    pushMatrix();
    translate( pos.x, pos.y, pos.z );
    rotateZ( -rz );
    rotateY( -ry );
    rotateX( -rx );
    stroke( 255 );
    strokeWeight( 3 );
    point( 0,0 );
    strokeWeight( 1 );
    stroke( 255, 120 );
    ellipse( 0,0, weight * 100, weight * 100 );
    rotateX( HALF_PI );
    ellipse( 0,0, weight * 100, weight * 100 );
    rotateY( HALF_PI );
    ellipse( 0,0, weight * 100, weight * 100 );
    popMatrix();
  
  }


}
