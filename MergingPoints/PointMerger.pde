static class PointMerger {
  
  public static float[] minmaxWeight( Point3D pts[] ) {
    float out[] = new float[] { 1000,0,0 };
    for ( int i = 0; i < pts.length; i++ ) {
      if ( out[ 0 ] > pts[ i ].weight ) {
        out[ 0 ] = pts[ i ].weight;
      }
      if ( out[ 1 ] < pts[ i ].weight ) {
        out[ 1 ] = pts[ i ].weight;
      }
    }
    out[ 2 ] = out[ 1 ] - out[ 0 ];
    return out;
  }
  
  public static float totalWeight( Point3D pts[] ) {
    float totalw = 0;
    for ( int i = 0; i < pts.length; i++ ) {
      totalw += pts[ i ].weight;
    }
    return totalw;
  }
  
  public static void pull( PVector p, float damping, Point3D pts[] ) {
    
    if ( pts.length == 0 ) {
      return;
    }
    
    float totalw = totalWeight( pts );
    
    for ( int i = 0; i < pts.length; i++ ) {
      PVector pull = new PVector(
        pts[ i ].pos.x - p.x,
        pts[ i ].pos.y - p.y,
        pts[ i ].pos.z - p.z
      );
      pull.mult( damping );
      pull.mult( pow( pts[ i ].weight / totalw, 2 ) );
      p.x += pull.x;
      p.y += pull.y;
      p.z += pull.z;
    }
    
  }
  
  public static PVector merge( Point3D pts[] ) {
  
    if ( pts.length == 0 ) {
      return null;
    }
    
    PVector res = new PVector( 0,0,0 );
    
    float totalw = 0;
    for ( int i = 0; i < pts.length; i++ ) {
      totalw += pow( pts[ i ].weight, 2 );
    }
    
    for ( int i = 0; i < pts.length; i++ ) {
      PVector pull = new PVector(
        pts[ i ].pos.x - res.x,
        pts[ i ].pos.y - res.y,
        pts[ i ].pos.z - res.z
      );
      pull.mult( pow( pts[ i ].weight, 2 ) / totalw );
      res.x += pull.x;
      res.y += pull.y;
      res.z += pull.z;
    }
    
    return res;
    
    /*
    if( pts.length == 1 ) {
      return pts[ 0 ].pos;
    } else if ( pts.length > 1 ) {
      float totalw = totalWeight( pts );
      PVector res = new PVector( 0,0,0 );
      for ( int i = 0; i < pts.length; i++ ) {
        float relw = pts[ i ].weight / totalw;
        res.x += pts[ i ].pos.x * relw;
        res.y += pts[ i ].pos.y * relw;
        res.z += pts[ i ].pos.z * relw;
      }
      return res;
    }
    return null;
    */
  
  } 
  

}
