// ATTENTION: le réglage de la reactivité et de l'inertie du point doit être adapté en fonction du nombre d'attracteurs!
// SI 1 >> beaucoup plus réactif +!!! PointMerger.pull doit lui aussi prendre en compte le nombre de points trackés
// DONC >> 2 réglages: PointMerger & SmoothPoint en fonction du nombre d'attracteurs
// essai en cours: mettre à jour plus souvent le SmoothPoint que les Point3D ( ~ multi-threading )
// signal tout pourri > noise + mise à jour aléatoire de Point3D

int maxHisto = 400;
float NOISE = 4;

Point3D pts[];
float hw;
float rx;
float ry;
float rz;

PVector pos;
SmoothPoint smoothp;
float minmax[];

Histo[] histos;

void setup() {

  size( 900, 900, P3D ); 

  frameRate( 120 );

  hw = width * 0.2f;

  pts = new Point3D[ 3 ];
  for ( int i = 0; i < pts.length; i++ ) {
    pts[ i ] = new Point3D( random( -hw, hw ), random( -hw, hw ), random( -hw, hw ), 1 );
  }
  
  histos = new Histo[ 7 ];
  for ( int i = 0; i < 6; i++ ) {
    histos[ i ] = new Histo( 100, color( 255 ) );
    histos[ i ].setMinMax( -hw, hw );
  }
  // delta pos
  histos[ 6 ] = new Histo( 50, color( 255 ) );
  histos[ 6 ].setMinMax( -10, 10 );
  // acceleration length
//  histos[ 7 ] = new Histo( 50, color( 255 ) );
//  histos[ 7 ].setMinMax( -5, 5 );
  
  histos[ 0 ].setColor( color( 255, 20, 20, 150 ) );
  histos[ 1 ].setColor( color( 20, 255, 20, 150 ) );
  histos[ 2 ].setColor( color( 20, 20, 255, 150 ) );
  histos[ 3 ].setColor( color( 255, 20, 20 ) );
  histos[ 4 ].setColor( color( 20, 255, 20 ) );
  histos[ 5 ].setColor( color( 20, 20, 255 ) );

  pos = new PVector();
  smoothp = new SmoothPoint();
  
  minmax = new float[] { 0,0 };

}

void update() {

  if ( frameCount % 4 == 0 ) {
    
    if ( random( 0, 1 ) > 0.9f ) {
      pts[ 0 ].pos.x = cos( frameCount * 0.01 ) * hw;
      pts[ 0 ].pos.y = sin( frameCount * 0.01 ) * hw;
    }
    pts[ 0 ].pos.x += random( -NOISE, NOISE );
    pts[ 0 ].pos.y += random( -NOISE, NOISE );
    pts[ 0 ].pos.z += random( -NOISE, NOISE );

    if ( pts.length > 1 ) {
      if ( random( 0, 1 ) > 0.9f ) {
        pts[ 1 ].pos.x = cos( frameCount * 0.013 ) * hw;
        pts[ 1 ].pos.y = sin( frameCount * 0.013 ) * hw;
      }
      pts[ 1 ].pos.x += random( -NOISE, NOISE );
      pts[ 1 ].pos.y += random( -NOISE, NOISE );
      pts[ 1 ].pos.z += random( -NOISE, NOISE );
    }
    
    if ( pts.length > 2 ) {
      if ( random( 0, 1 ) > 0.9f ) {
        pts[ 2 ].pos.x = cos( frameCount * 0.003 ) * hw;
        pts[ 2 ].pos.y = sin( frameCount * 0.003 ) * hw;
      }
      pts[ 2 ].pos.x += random( -NOISE, NOISE );
      pts[ 2 ].pos.y += random( -NOISE, NOISE );
      pts[ 2 ].pos.z += random( -NOISE, NOISE );
    }

    for ( int i = 0; i < pts.length; i++ ) {
      pts[ i ].weight += random( -0.05f, 0.05f );
      if ( pts[ i ].weight < 0.0001f ) {
        pts[ i ].weight += 0.5f;
      } else if ( pts[ i ].weight > 0.5f ) {
        pts[ i ].weight -= 0.5f;
      }
    }
    
  }

  minmax = PointMerger.minmaxWeight( pts );
  smoothp.reactivity = mapClamped( minmax[ 1 ], 0.1f, 0.5f, 0.2f, 0.4f );
  
  //pos = PointMerger.merge( pts );
  PointMerger.pull( smoothp.target, 1, pts );
  smoothp.update();
  pos.set( smoothp.target.x, smoothp.target.y, smoothp.target.z );

  histos[ 0 ].push( pos.x );
  histos[ 1 ].push( pos.y );
  histos[ 2 ].push( pos.z );
  histos[ 3 ].push( smoothp.x );
  histos[ 4 ].push( smoothp.y );
  histos[ 5 ].push( smoothp.z );
  histos[ 6 ].push( smoothp.deltaPos() );
  // histos[ 7 ].push( smoothp.accel_length );
  
}

void draw() {
  
  update();

  //  rx = frameCount * 0.001f;
  //  ry = frameCount * 0.0012f;
  //  rz = frameCount * 0.00105f;

  background( 5 );

  pushMatrix();
  translate( width * 0.5f, height * 0.5f, 0 );
  rotateX( rx );
  rotateY( ry );
  rotateZ( rz );

  strokeWeight( 2 );
  stroke( 200, 0, 0 );
  line( 0, 0, 0, 100, 0, 0 );
  stroke( 0, 200, 0 );
  line( 0, 0, 0, 0, 100, 0 );
  stroke( 0, 0, 200 );
  line( 0, 0, 0, 0, 0, 100 );

  for ( int i = 0; i < pts.length; i++ ) {
    pts[ i ].draw();
  }

  strokeWeight( 10 );
  point( pos.x, pos.y, pos.z );
  point( smoothp.x, smoothp.y, smoothp.z );
  
  int ps = histos[ 0 ].getSize();
  float prevx = 0;
  float prevy = 0;
  float prevz = 0;
  
  strokeWeight( 1 );
  
  prevx = pos.x;
  prevy = pos.y;
  prevz = pos.z;
  stroke( 180 );
  beginShape( LINES );
  for ( int i = ps - 1; i >= 0; i-- ) {
    vertex( prevx, prevy, prevz );
    prevx = histos[ 0 ].get( i );
    prevy = histos[ 1 ].get( i );
    prevz = histos[ 2 ].get( i );
    vertex( prevx, prevy, prevz );
  }
  endShape();
  
  strokeWeight( 3 );
  
  prevx = smoothp.x;
  prevy = smoothp.y;
  prevz = smoothp.z;
  stroke( 255 );
  beginShape( LINES );
  for ( int i = ps - 1; i >= 0; i-- ) {
    vertex( prevx, prevy, prevz );
    prevx = histos[ 3 ].get( i );
    prevy = histos[ 4 ].get( i );
    prevz = histos[ 5 ].get( i );
    vertex( prevx, prevy, prevz );
  }
  endShape();

  stroke( 0, 200, 200 );
  PVector tp = new PVector();
  for ( int i = 0; i < pts.length; i++ ) {
    line( pos.x, pos.y, pos.z, pts[ i ].pos.x, pts[ i ].pos.y, pts[ i ].pos.z );
  }
  popMatrix();

  strokeWeight( 2 );
  pushMatrix();
  for ( int i = 0; i < 3; i++ ) {
    histos[ i ].draw();
    histos[ i + 3 ].draw();
    translate( 0, histos[ i ].getHeight(), 0 );
  }
  for ( int i = 6; i < histos.length; i++ ) {
    histos[ i ].draw();
    translate( 0, histos[ i ].getHeight(), 0 );
  }
  popMatrix();
  
  fill( 255 );
  int y = 25;
  text( "fps: " + frameRate, 10, y ); y += 15;
  text( "total weight: " + PointMerger.totalWeight( pts ), 10, y ); y += 15;
  text( "min weight: " + minmax[ 0 ], 10, y ); y += 15;
  text( "max weight: " + minmax[ 1 ], 10, y ); y += 15;
  text( "delta weight: " + minmax[ 2 ], 10, y ); y += 15;
  text( "reactivity: " + smoothp.reactivity, 10, y ); y += 15;
  
}

void keyPressed() {

  if ( key == 'r' ) {
    for ( int i = 0; i < pts.length; i++ ) {
      pts[ i ].pos.set( random( -hw, hw ), random( -hw, hw ), random( -hw, hw ) );
      pts[ i ].weight = random( 0.1, 1 );
    }
  } else if ( key == 'w' ) {
    for ( int i = 0; i < pts.length; i++ ) {
      pts[ i ].weight = random( 0.1, 1 );
    }
  }
  
}

public float mapClamped( float v, float start_entree, float stop_entree, float start_sortie, float stop_sortie ) {

  float out = map( v, start_entree, stop_entree, start_sortie, stop_sortie );
  if ( out < start_sortie ) {
    out = start_sortie;
  } else if ( out > stop_sortie ) {
    out = stop_sortie;
  }
  return out;
}

