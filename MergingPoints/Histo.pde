public static int HISTO_MAX_VALUES = 400;

class Histo {

  private float height;
  private ArrayList< Float > values;
  private float[] minmax;
  private color col;
  
  public Histo( float h, color col ) {
    values = new ArrayList< Float >();
    minmax = new float[] { 0, 1 };
    this.col = col;
    this.height = h;
  }
  
  public void setMinMax( float min, float max ) {
    minmax[ 0 ] = min;
    minmax[ 1 ] = max;
  }
  
  public void setColor( color col ) {
    this.col = col;
  }
  
  public void push( float v ) {
    values.add( v );
    while ( values.size() > HISTO_MAX_VALUES ) {
      values.remove( 0 );
    }
  }
  
  public float getHeight() {
    return height;
  }
  
  public int getSize() {
    return values.size();
  }
  
  public float get( int i ) {
    return values.get( i );
  }
  
  public void draw() {
    float normh = minmax[ 1 ] - minmax[ 0 ];
    if ( values.size() > 1 ) {
      stroke( col );
      beginShape( LINES );
      for ( int i = 1; i < values.size (); i++ ) {
        float pdx = ( i - 1 ) * 1.f / maxHisto * width;
        float dx = i * 1.f / maxHisto * width;
        float v1 = values.get( i - 1 );
        float v2 = values.get( i );
        vertex( pdx, ( v1 - minmax[ 0 ] ) / normh * height );
        vertex( dx, ( v2 - minmax[ 0 ] ) / normh * height );
      }
      endShape();
    }
  }
  

}
