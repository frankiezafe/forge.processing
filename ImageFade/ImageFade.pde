int im1[];
int im2[];
float ratio[][];
float energy[][];
float weight[];
int index;
int lastIndex;
int range = 1;

boolean mpressed;

void setup() {

  size( 400, 400, P3D );

  PImage i1 = loadImage( "img1.png" );
  i1.loadPixels();
  im1 = new int[ width * height ];
  for ( int i = 0; i < width * height; i++ )
    im1[ i ] = i1.pixels[ i ];

  PImage i2 = loadImage( "img2.png" );
  i2.loadPixels();
  im2 = new int[ width * height ];
  for ( int i = 0; i < width * height; i++ )
    im2[ i ] = i2.pixels[ i ];

  ratio = new float[ width * height ][ 2 ];
  weight = new float[ width * height ];
  energy = new float[ width * height ][ 2 ];
  for ( int i = 0; i < width * height; i++ ) {
    weight[ i ] = 0;
    energy[ i ][ 0 ] = 0;
    energy[ i ][ 1 ] = 0;
    ratio[ i ][ 0 ] = 0;
    ratio[ i ][ 1 ] = 0;
  }

  index = 0;
  lastIndex = 1;

  mpressed = false;
  
}

void draw() {


  int tmpi = index;
  index = lastIndex;
  lastIndex = tmpi;
  
  if ( mpressed ) {
    
    int p = mouseX + mouseY * width;
    
    if ( weight[ p ] > 0.5f ) {
      weight[ p ] = 0;
    } else {
      weight[ p ] = 1;
    }
    energy[ p ][ lastIndex ] = 1;
    
//    if ( ratio[ p ][ index ] <= 0 ) {
//      
//      ratio[ p ][ 0 ] = 1;
//      ratio[ p ][ 1 ] = 1;
//      
//    } else {
//      
//      ratio[ p ][ 0 ] = -1;
//      ratio[ p ][ 1 ] = -1;
//      
//    }

    mpressed = false;
    
  }

  loadPixels();

  for ( int y = 0; y < height; y++ ) {
    for ( int x = 0; x < width; x++ ) {
      
      int i = x + y * width;
      
      for ( int ny = y - range; ny <= y + range; ny++ ) {
        for ( int nx = x - range; nx <= x + range; nx++ ) {
          if ( nx == x && ny == y )
            continue;
          if ( nx < 0 || ny < 0 || nx >= width || ny >= height )
            continue;
          int ni = nx + ny * width;
          
        }
      }
      
      
      // most energy around me
      int neighb = -1;
      float biggst = 0;
      if ( x > 0 ) {
        int ni = ( x - 1 ) + y * width;
        if ( energy[ ni ][ lastIndex ] > biggst ) {
          neighb = ni;
          biggst = energy[ ni ][ lastIndex ];
        }
      }
      if ( y > 0 ) {
        int ni = x + ( y - 1 ) * width;
        if ( energy[ ni ][ lastIndex ] > biggst ) {
          neighb = ni;
          biggst = energy[ ni ][ lastIndex ];
        }
      }
      if ( x < width - 1 ) {
        int ni = ( x + 1 ) + y * width;
        if ( energy[ ni ][ lastIndex ] > biggst ) {
          neighb = ni;
          biggst = energy[ ni ][ lastIndex ];
        }
      }
      if ( y < height - 1 ) {
        int ni = x + ( y + 1 ) * width;
        if ( energy[ ni ][ lastIndex ] > biggst ) {
          neighb = ni;
          biggst = energy[ ni ][ lastIndex ];
        }
      }
      
      if ( neighb > -1 ) {
        energy[ i ][ index ] += ( biggst - energy[ i ][ lastIndex ] ) * 0.05f;
        if ( energy[ i ][ index ] > 1 ) {
          energy[ i ][ index ] = 1;
        }
      }
      
      pixels[ i ] = color( 15 + energy[ i ][ index ] * 240 );
      
      
//      ratio[ i ][ index ] *= 0.4f;
//      for ( int ny = y - range; ny <= y + range; ny++ ) {
//        for ( int nx = x - range; nx <= x + range; nx++ ) {
//          if ( nx == x && ny == y )
//            continue;
//          if ( nx < 0 || ny < 0 || nx >= width || ny >= height )
//            continue;
//          int ni = nx + ny * width;
//          // if ( ratio[ i ][ 2 ] > ratio[ ni ][ 2 ] )
//          //   continue;
//          // ratio[ i ][ index ] += ( ratio[ ni ][ lastIndex ] - ratio[ i ][ lastIndex ] ) * 0.1f;
//          ratio[ i ][ index ] += ratio[ ni ][ lastIndex ] * 0.1f;
//        }
//      }
//
//      if ( ratio[ i ][ index ] > 1 )
//        ratio[ i ][ index ] = 1;
//      if ( ratio[ i ][ index ] < -1 )
//        ratio[ i ][ index ] = -1;
//      float r = ( 1 + ratio[ i ][ index ] ) * 0.5f;
//      pixels[ i ] = (int) ( im1[ i ] * ( 1 - r ) + im2[ i ] * r );
      // pixels[ i ] = color(  127 + ratio[ i ][ index ] * 100, 0, 127 + ratio[ i ][ lastIndex ] * 100 );
      
    }
  }

  for ( int i = 0; i < width * height; i++ ) {
    ratio[ i ][ lastIndex ] *= 0.9f;
  }
  
  updatePixels();
  
}

void mousePressed() {
  mpressed = true;
}

