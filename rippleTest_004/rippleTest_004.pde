/* OpenProcessing Tweak of *@*http://www.openprocessing.org/sketch/51814*@* */
/* !do not delete the line above, required for linking your tweak if you upload again */
float a, b;
Ripple r1 = new Ripple(0, 0);
float x, y;

void setup () {
  size(640, 480, P3D);
  background(0);
  camera(500, -500, -500, 
  0.0, 100.0, 0.0, 
  0.0, 1.0, 0.0);
  noLoop();
}
int frameCounter = 1,disappearCount = 0;
void draw() {

  background(0);  
  r1.draw();
  if(frameCounter%2 == 0){
    r1.calm(disappearCount);
println(disappearCount);
    disappearCount++;
  }
  frameCounter++;
}


