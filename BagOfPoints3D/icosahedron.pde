// copied and adapted from http://code.google.com/p/webgltimer/source/browse/src/net/icapsid/counter/client/Icosahedron.java?r=170e4fcc41bf20700dcb6dc67272073af112c65c

import java.util.Iterator;
import java.util.List;

public class Icosahedron {
        
        /*
        public List< MeshPoint > pts = new ArrayList< MeshPoint >();
        public List< Integer[] > faces = new ArrayList< Integer[] >();
        public List< MeshPoint > faceNormals = new ArrayList< MeshPoint >();
        */
        
        public MeshPoint[] pts;
        public int[][] faces;
        public MeshPoint[] faceNormals;
        
        // public List<Float> vertexNormalsList = new ArrayList<Float>();
        // public List<Float> vertexList = new ArrayList<Float>();
        
        private final float X = 0.525731112119133606f;
        private final float Z = 0.850650808352039932f;
        private final float vdata[][] = { 
                        { -X, 0.0f, Z }, 
                        { X, 0.0f, Z },
                        { -X, 0.0f, -Z }, 
                        { X, 0.0f, -Z }, 
                        { 0.0f, Z, X },
                        { 0.0f, Z, -X }, 
                        { 0.0f, -Z, X }, 
                        { 0.0f, -Z, -X },
                        { Z, X, 0.0f }, 
                        { -Z, X, 0.0f }, 
                        { Z, -X, 0.0f },
                        { -Z, -X, 0.0f }
                      };
        private final int tindices[][] = { 
                        { 0, 4, 1 }, { 0, 9, 4 }, { 9, 5, 4 }, { 4, 5, 8 }, 
                        { 4, 8, 1 }, { 8, 10, 1 }, { 8, 3, 10 }, { 5, 3, 8 }, 
                        { 5, 2, 3 }, { 2, 7, 3 }, { 7, 10, 3 }, { 7, 6, 10 }, 
                        { 7, 11, 6 }, { 11, 0, 6 }, { 0, 1, 6 }, { 6, 1, 10 }, 
                        { 9, 0, 11 }, { 9, 11, 2 }, { 9, 2, 5 }, { 7, 2, 11 } };
        
        int divisions;
        int ptsNum;
        int ptsCurrentIndex;
        int facesNum;
        int facesCurrentIndex;
        
        public Icosahedron( int divisions ) {
          
          this.divisions = divisions;
          ptsCurrentIndex = 0;
          ptsNum = renderPtsNum();
          facesCurrentIndex = 0;
          facesNum = renderFacesNum();
          
          // println( "divisions: " + divisions + " => ptsNum: "+ptsNum +" / facesNum: "+ facesNum );
          
          pts = new MeshPoint[ ptsNum ];
          faces = new int[ facesNum ][ 3 ];
          faceNormals = new MeshPoint[ facesNum ];
          
          // Iterate over points
          for (int i = 0; i < 20; ++i) {
              subdivide(
                vdata[tindices[i][0]], 
                vdata[tindices[i][1]],
                vdata[tindices[i][2]], divisions );
          }
          renderNormals();
          
        }
        
        public int renderPtsNum() {
          int output = 12;
          int d = divisions;
          while( d > 0 ) {
            output = ( ( output - 2 ) * 4 ) + 2;
            d--;
          }
          return output;
        }
        
        public int renderFacesNum() {
          return (int) (20 * pow( 4, divisions ));
        }
        
        public void renderNormals() {
          
          float third = 1.f / 3;
          
          int i = 0;
          while ( i < facesNum ) {
            
            int[] li = faces[i];
            MeshPoint mp = faceNormals[i];
            
            mp.set( 0,0,0 );
            mp.direction.set( 0,0,0 );
            
            MeshPoint ref = pts[ li[0] ];
            mp.plus( ref.x,ref.y,ref.z );
            mp.direction.plus( ref.direction.x,ref.direction.y,ref.direction.z );
            
            ref = pts[ li[1] ];
            mp.plus( ref.x,ref.y,ref.z );
            mp.direction.plus( ref.direction.x,ref.direction.y,ref.direction.z );
            
            ref = pts[ li[2] ];
            mp.plus( ref.x,ref.y,ref.z );
            mp.direction.plus( ref.direction.x,ref.direction.y,ref.direction.z );
            
            mp.multiply( third );
            mp.direction.multiply( third );
            
            i++;
            
          }
          
        }
        
        private void norm(float v[]){
                
                float len = 0;
                
                for(int i = 0; i < 3; ++i){
                        len += v[i] *  v[i];
                }
                
                len = (float) Math.sqrt(len);
                
                for(int i = 0; i < 3; ++i){
                        v[i] /= len;
                }
        }
        
        private int add(float v[]){
          int found = -1;
          for ( int i = 0; i < ptsCurrentIndex; i++ ) {
            MeshPoint mp = pts[i];
            if ( mp.x == v[0] && mp.y == v[1] && mp.z == v[2] ) {
              found = i;
              break;
            }
          }
          if ( found == -1 ) {
            pts[ptsCurrentIndex] = new MeshPoint();
            pts[ptsCurrentIndex].set( v[0], v[1], v[2] );
            pts[ptsCurrentIndex].direction.set( v[0], v[1], v[2] );
            found = ptsCurrentIndex;
            ptsCurrentIndex++;
          }
          return found;
        }
        
        private void subdivide(float v1[], float v2[], float v3[], int depth) {
          
                if (depth == 0) {
                        int f1 = add(v1);
                        int f2 = add(v2);
                        int f3 = add(v3);
                        faces[facesCurrentIndex] = new int[] { f1,f2,f3 };
                        faceNormals[facesCurrentIndex] = new MeshPoint();
                        facesCurrentIndex++;
                        return;
                }
                
                float v12[] = new float[3];
                float v23[] = new float[3];
                float v31[] = new float[3];

                for (int i = 0; i < 3; ++i) {
                        v12[i] = (v1[i] + v2[i]) / 2f;
                        v23[i] = (v2[i] + v3[i]) / 2f;
                        v31[i] = (v3[i] + v1[i]) / 2f;
                }

                norm(v12);
                norm(v23);
                norm(v31);
                                
                subdivide(v1, v12, v31, depth - 1);
                subdivide(v2, v23, v12, depth - 1);
                subdivide(v3, v31, v23, depth - 1);
                subdivide(v12, v23, v31, depth - 1);
        }
}
