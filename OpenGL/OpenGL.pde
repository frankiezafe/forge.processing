PShape obj;
PVector vdata[];

void setup() {
  
  size( 1280, 720, P3D);
  
  obj = createShape( GROUP );
  vdata = new PVector[ 20000 ];
  
  int di = 0;
  
  for ( int k = 0; k < 4; k++ ) {
    
    PShape face = createShape();
    face.beginShape( LINES );
    face.stroke( color( random( 0,255 ), 255, 255 ) );
    face.noFill();
    face.beginContour();
    // float agap = TWO_PI / 200;
    // float a  = 0;
    for ( int i = 0; i < vdata.length / 4; i++ ) {
      vdata[ di ] = new PVector( 
        random( -width * 0.2f, width * 0.2f ),
        random( -height * 0.2f, height * 0.2f ),
        random( -width * 0.2f, width * 0.2f ) );
      face.vertex( vdata[ di ].x, vdata[ di ].y, vdata[ di ].z );
      di++;
    }
    face.endContour();
    face.endShape(CLOSE);
    obj.addChild(face);
    
  }
  println( di );
  
  
}

void draw() {
  
  obj.setStrokeWeight( random( 1, 3 ) );
  PShape face = obj.getChild( 2 );
  // face.setStroke( color( random( 0,255 ), 255, 255 ) );
  int di = 0;
  for ( int k = 0; k < 4; k++ ) {
    face = obj.getChild(k);
    for ( int i = 0; i < vdata.length / 4; i++ ) {
      vdata[ di ].set(
        vdata[ di ].x * 0.999f,
        vdata[ di ].y * 0.999f,
        vdata[ di ].z * 0.999f
      );
      face.setVertex( i, vdata[ i ].x, vdata[ i ].y, vdata[ i ].z );
      di++;
    }
  }
  
  
  background(5);
  pushMatrix();
  translate(width/2, height/2);
  rotateX(frameCount * 0.01f);
  rotateY(frameCount * 0.01f);
  shape( obj );
  popMatrix();
  stroke( 255 );
  text(frameRate, 10, 25);
  
}
