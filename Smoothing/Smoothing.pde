
SmoothVector p;
SmoothVector p2;
SmoothVector p3;
PVector target;

int maxdirs = 500;
ArrayList< Float > dirmag;
ArrayList< Float > dirdeviation;
ArrayList< Float > inertias;

float rotx;
float roty;

void setup() {

  size( 800, 800, P3D );
  
  p = new SmoothVector( -100,0 );
  
  p2 = new SmoothVector( 100,0 );
  p2.max_speed = 2;
  p2.slowdown_range = 15;
  
  p3 = new SmoothVector( 0,0 );
  p3.inertia_decay = 0.001f;
  p3.orientation_speed = 0.29f;
  p3.max_speed = 10;
  p3.slowdown_range = 20;
  p3.log = true;
  
  target = new PVector( 0,0,0 );
  
  dirmag = new ArrayList< Float >();
  dirdeviation = new ArrayList< Float >();
  inertias = new ArrayList< Float >();
  
  rotx = 5 / 360.f * PI;
  roty = 5 / 360.f * PI;
  
}

void addDeviation( float x, float y, float z ) {

  PVector v = new PVector( x, y, z );
  if ( dirdeviation.size() > maxdirs ) {
    dirdeviation.remove( 0 );
  }
  dirdeviation.add( v.mag() );

} 

void addMag( float m ) {

  if ( dirmag.size() > maxdirs ) {
    dirmag.remove( 0 );
  }
  dirmag.add( m );

} 

void addInertia( float i ) {

  if ( inertias.size() > maxdirs ) {
    inertias.remove( 0 );
  }
  inertias.add( i );

} 

String displayNum( float v ) {

  String out = "" + v;
  if ( v > 0 ) {
    if ( v < 0.0005f ) {
      out = "~ 0";
    } else if ( v < 0.005f ) {
      out = "< 0.005f";
    }
  } else if ( v < 0 ) {
    if ( v > -0.0005f ) {
      out = "~ 0";
    } else if ( v > -0.005f ) {
      out = "> -0.005f";
    }
  } else {
    out = "0";
  }

  return out;

}

void drawSmoothVector( SmoothVector sv, color c ) {
  
    noFill();
    stroke( c );
    if ( sv.dir != null ) {
      line( sv.x, sv.y, sv.z, sv.x + sv.dir.x * sv.slowdown_range, sv.y + sv.dir.y * sv.slowdown_range, sv.z + sv.dir.z * sv.slowdown_range );
    }
    
    for ( int i = 0; i < sv.history.size() - 1; i++ ) {
      stroke( 255, 30 + ( i * 1.f / sv.history.size() ) * 160 );
      float[] vi = sv.history.get( i );
      float[] vj = sv.history.get( i + 1 );
      line( vi[ 0 ], vi[ 1 ], vi[ 2 ], vj[ 0 ], vj[ 1 ], vj[ 2 ] );
    }
    
    stroke( 255 );
    pushMatrix();
      translate( sv.x, sv.y, sv.z );
      rotateY( -roty );
      rotateX( -rotx );
      ellipse( 0, 0, 5, 5 );
    popMatrix();
    
    if ( sv.target != null ) {
      float SDR2 = sv.slowdown_range * 2;
      pushMatrix();
        translate( sv.target.x, sv.target.y, sv.target.z );
        rotateY( -roty );
        rotateX( -rotx );
        ellipse( 0, 0, 10, 10 );
        stroke( c );
        rotateX( rotx );
        rotateY( roty );
        ellipse( 0, 0, SDR2, SDR2 );
        rotateY( HALF_PI );
        ellipse( 0, 0, SDR2, SDR2 );
        rotateX( HALF_PI );
        ellipse( 0, 0, SDR2, SDR2 );
      popMatrix();
    }
  
}

void draw() {

  p.update();
  p2.update();
  p3.update();
  
//  float fov = PI/4.0;
//  float cameraZ = (height/2.0) / tan(fov/2.0);
//  perspective(fov, float(width)/float(height), 
//          cameraZ/10.0, cameraZ*10.0);
  
  background( 5 );
  
  noStroke();
  
  // colorMode( HSB, 255 );
  fill( 127 + p.x / width * 120, 127 + p.y / height * 120, 127 + p.z / height * 120 );
  rect( 5, 5, 100, 100 );
  // colorMode( RGB, 255 );
  
  
  translate( width * 0.5f, height * 0.5f );
  
  strokeWeight( 2 );
  
  // rotx += 0.01f;
  roty += -0.002f;
  
  pushMatrix();
    
    rotateX( rotx );
    rotateY( roty );
    
    scale( 0.6 );

    drawSmoothVector( p, color( 255, 120, 0 ) );
    drawSmoothVector( p2, color( 0, 120, 255 ) );
    drawSmoothVector( p3, color( 120, 255, 120 ) );
    
//    float SDR2 = SLOWDOWNRANGE * 2;
//    pushMatrix();
//      translate( target.x, target.y, target.z );
//      rotateY( -roty );
//      rotateX( -rotx );
//      ellipse( 0, 0, 10, 10 );
//      stroke( 255, 120, 0 );
//      rotateX( rotx );
//      rotateY( roty );
//      ellipse( 0, 0, SDR2, SDR2 );
//      rotateY( HALF_PI );
//      ellipse( 0, 0, SDR2, SDR2 );
//      rotateX( HALF_PI );
//      ellipse( 0, 0, SDR2, SDR2 );
//    popMatrix();
    
    stroke( 80 );
    box( width, height, height );
    
  popMatrix();
  
  strokeWeight( 1 );
  
  // drawing mag
  float offsetx = -( ( width * 0.5f ) - 10 );
  float offsety = ( height * 0.5f ) - 110;
  float tablew = width - 20;
  float tableh = 100;
  float tgap = tablew / maxdirs;
  float dunit = tableh / MAXSPEED;
  stroke( 80 );
  rect( offsetx - 5, offsety - 5, tablew + 10, tableh + 10 );
  stroke( 120,255,80 );
  fill( 120,255,80 );
  int j = 0;
  for( int i = 1; i < dirmag.size(); i++ ) {
    if ( i == 1 ) {
      text( "speed " + displayNum( dirmag.get( dirmag.size() - 1 ) ), offsetx, offsety - 45 );
    }
    line(
      offsetx + j * tgap,
      offsety + tableh - ( dirmag.get( j ) ) * dunit,
      offsetx + i * tgap,
      offsety + tableh - ( dirmag.get( i ) ) * dunit
    );
    j = i;
  }
  
  stroke( 255,60,210 );
  fill( 255,60,210 );
  j = 0;
  for( int i = 1; i < dirdeviation.size(); i++ ) {
    if ( i == 1 ) {
      text( "deviation " + displayNum( dirdeviation.get( dirdeviation.size() - 1 ) ), offsetx, offsety - 30 );
    }
    line(
      offsetx + j * tgap,
      offsety + tableh - ( dirdeviation.get( j ) ) * 0.5f * tableh,
      offsetx + i * tgap,
      offsety + tableh - ( dirdeviation.get( i ) ) * 0.5f * tableh
    );
    j = i;
  }
  
  stroke( 255,255,0 );
  fill( 255,255,0 );
  j = 0;
  for( int i = 1; i < inertias.size(); i++ ) {
    if ( i == 1 ) {
      text( "inertia " + displayNum( inertias.get( inertias.size() - 1 ) ), offsetx, offsety - 15 );
    }
    line(
      offsetx + j * tgap,
      offsety + tableh - ( inertias.get( j ) ) * tableh,
      offsetx + i * tgap,
      offsety + tableh - ( inertias.get( i ) ) * tableh
    );
    j = i;
  }

}

void mousePressed() {

  target.x = ( mouseX - width * 0.5f );
  target.y = ( mouseY - height * 0.5f );
  target.z = random( -height * 0.5f, height * 0.5f );
  p.setTarget( target );
  p2.setTarget( target );
  p3.setTarget( target );
  
}

void mouseDragged() {

  target.x = ( mouseX - width * 0.5f );
  target.y = ( mouseY - height * 0.5f );
  p.setTarget( target );
  p2.setTarget( target );
  p3.setTarget( target );
  
}
