
public static float MAXSPEED = 5.5f;
public static float CHANGESPEED = 0.1f;
public static float SLOWDOWNRANGE = 55.f;
public static float ORIENTATIONSPEED = 0.1f;
public static float INERTIADECAY = 0.05f;
public static int HISTORYMAX = 1000;

class SmoothVector extends PVector {

  public PVector dir;
  public PVector target;
  
  public float speed;
  public float actual_speed;
  public float max_speed;
  public float change_speed;
  public float orientation_speed;
  
  public float inertia;
  public float inertia_decay;
  public float slowdown_range;
  
  public boolean log = false;
  public ArrayList< float[] > history;
  
  public SmoothVector( float x, float y ) {
    super( x, y );
    max_speed = MAXSPEED;
    change_speed = CHANGESPEED;
    orientation_speed = ORIENTATIONSPEED;
    inertia_decay = INERTIADECAY;
    slowdown_range = SLOWDOWNRANGE;
    history = new ArrayList< float[] >();
    this.reset();
  }
  
  public SmoothVector( float x, float y, float z ) {
    super( x, y, z );
    max_speed = MAXSPEED;
    change_speed = CHANGESPEED;
    orientation_speed = ORIENTATIONSPEED;
    inertia_decay = INERTIADECAY;
    slowdown_range = SLOWDOWNRANGE;
    history = new ArrayList< float[] >();
    this.reset();
  }
  
  public void reset() {
      target = null;
      dir = null;
      speed = 0;
      actual_speed = 0;
      inertia = 1;
  }
  
  public void setTarget( PVector t ) {
    
    if ( actual_speed != speed ) {
      speed = actual_speed;
    }
    
    if ( t != null && x == t.x && y == t.y && z == t.z ) {
      // position reached!
      reset();
      return;
    }
    
    target = t;
    
  }
  
  public void update() {
  
    if ( target != null ) {
      
      PVector tmpt = new PVector();
      
      if ( dir == null ) {
        
        dir = new PVector();
        dir.x = target.x - x;
        dir.y = target.y - y;
        dir.z = target.z - z;
        dir.normalize();
        
      } else {
        
        float m = orientation_speed;
        // if too close, pointing more directly towards target
        float dt = dist( target );
        if ( dt < slowdown_range ) {
          m = orientation_speed + ( ( 1 - orientation_speed ) * ( 1 - ( dt / slowdown_range ) ) );
        }
        
        tmpt.set( target );
        tmpt.x -= x;
        tmpt.y -= y;
        tmpt.z -= z;
        
        // anti-locking
        // using imprecision to avoid locks
        boolean[] same = new boolean[] { false, false, false };
        if ( target.x == x ) {
          same[ 0 ] = true;
        }
        if ( target.y == y ) {
          same[ 1 ] = true;
        }
        if ( target.z == z ) {
          same[ 2 ] = true;
        }
        if ( 
          ( same[ 0 ] && same[ 1 ] ) || 
          ( same[ 1 ] && same[ 2 ] ) || 
          ( same[ 0 ] && same[ 1 ] && same[ 2 ] )
          ) {
          if ( same[ 0 ] ) {
            target.x += random( -0.001f, 0.001f );
          }
          if ( same[ 1 ] ) {
            target.y += random( -0.001f, 0.001f );
          }
          if ( same[ 2 ] ) {
            target.z += random( -0.001f, 0.001f );
          }
        }
        
        tmpt.normalize();
        
        if ( log ) {
          addDeviation( tmpt.x - dir.x, tmpt.y - dir.y, tmpt.z - dir.z );
        }
        
        dir.x += ( tmpt.x - dir.x ) * m;
        dir.y += ( tmpt.y - dir.y ) * m;
        dir.z += ( tmpt.z - dir.z ) * m;
        dir.normalize();
        
      }
      
      if ( inertia > 0 ) {
        inertia -= inertia_decay;
        if ( inertia < 0 ) {
          inertia = 0;
        }
      }
      
      if ( speed < max_speed ) {
        speed += ( max_speed - speed ) * change_speed * ( 1 - inertia );
      }
      
      // distance reelle
      tmpt.set( 0,0,0 );
      tmpt.add( target );
      tmpt.x -= x;
      tmpt.y -= y;
      tmpt.z -= z;
      float d2target = tmpt.mag();
      actual_speed = speed;
      if ( d2target <= slowdown_range ) {
        // 0, meaning 
        float a = map( ( d2target / slowdown_range ), 0, 1, HALF_PI, -HALF_PI );
        actual_speed = ( d2target / slowdown_range ) * speed;
        // float a2 = HALF_PI * sin( a );
        // actual_speed = ( sin( a ) + 1 ) * 0.5f * speed;
        inertia = ( sin( a ) + 1 ) * 0.5f;
      }
      
      tmpt.set( 0,0,0 );
      tmpt.add( dir );
      tmpt.mult( actual_speed );
      
      if ( log ) {
        addMag( tmpt.mag() );
        addInertia( inertia );
      }
      
      add( tmpt );
      
      if ( dist( target ) < 0.001f ) {
        reset();
      }
      
      if ( history.size() > HISTORYMAX ) {
        history.remove( 0 );
      }
      history.add( new float[] { x, y, z } );
      
    }
    
  }

}
