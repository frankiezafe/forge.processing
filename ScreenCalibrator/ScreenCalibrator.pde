
PVector size;
PVector pos1;
PVector pos2;
float r1;
float r2;

String mode = "NONE";

void setup() {

  size( 1920, 540 );
  size = new PVector( width * 0.3f, height * 0.4f );
  pos1 = new PVector( 0, 0 );
  r1 = 0;
  pos2 = new PVector( width * 0.5f, 0 );
  r2 = 0;
  stroke( 255,0,0 );
  fill( 255 );

}

void draw() {

  background( 0 );
  pushMatrix();
    translate( pos1.x + size.x * 0.5f, pos1.y + size.y * 0.5f );
    rotate( r1 );
    translate( -size.x * 0.5f, -size.y * 0.5f );
    rect( 0, 0, size.x, size.y );
  popMatrix();
  pushMatrix();
    translate( pos2.x + size.x * 0.5f, pos2.y + size.y * 0.5f );
    rotate( r2 );
    translate( -size.x * 0.5f, -size.y * 0.5f );
    rect( 0, 0, size.x, size.y );
  popMatrix();
  
}

void keyPressed() {

  if ( key == 'p' ) {
    println( "size: " + size.x + " x " + size.y + " ( " + ( size.x * 2 ) + " x " + size.y + " )" );
    println( "position 1: " + pos1.x + " x " + pos1.y );
    println( "rotation 1: " + r1 );
    println( "position 2: " + pos2.x + " x " + pos2.y );
    println( "rotation 2: " + r2 );
  } else if ( key == 's' ) {
    mode = "SIZE";
    println( "size ON" );
  } else if ( key == '1' ) {
    mode = "POS1";
    println( "pos 1 ON" );
  } else if ( key == '3' ) {
    mode = "ROT1";
    println( "rot 1 ON" );
  } else if ( key == '2' ) {
    mode = "POS2";
    println( "pos 2 ON" );
  } else if ( key == '4' ) {
    mode = "ROT2";
    println( "rot 2 ON" );
  } else if ( keyCode == 37 ) {
    if ( mode == "SIZE" ) {
      size.x -= 1;
    } else if ( mode == "POS1" ) {
      pos1.x -= 1;
    } else if ( mode == "POS2" ) {
      pos2.x -= 1;
    } else if ( mode == "ROT1" ) {
      r1 -= 0.001;
    } else if ( mode == "ROT2" ) {
      r2 -= 0.001;
    }
  } else if ( keyCode == 38 ) {
    if ( mode == "SIZE" ) {
      size.y -= 1;
    } else if ( mode == "POS1" ) {
      pos1.y -= 1;
    } else if ( mode == "POS2" ) {
      pos2.y -= 1;
    }
  } else if ( keyCode == 39 ) {
    if ( mode == "SIZE" ) {
      size.x += 1;
    } else if ( mode == "POS1" ) {
      pos1.x += 1;
    } else if ( mode == "POS2" ) {
      pos2.x += 1;
    } else if ( mode == "ROT1" ) {
      r1 += 0.001;
    } else if ( mode == "ROT2" ) {
      r2 += 0.001;
    }
  } else if ( keyCode == 40 ) {
    if ( mode == "SIZE" ) {
      size.y += 1;
    } else if ( mode == "POS1" ) {
      pos1.y += 1;
    } else if ( mode == "POS2" ) {
      pos2.y += 1;
    }
  } else {
    println( keyCode );
  }
  
}
