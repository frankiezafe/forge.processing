import java.awt.*;
// JFrame doc: http://docs.oracle.com/javase/7/docs/api/javax/swing/JFrame.html

void setup() {
  size( 800,600 );
  
  frame.dispose();
  frame.setUndecorated(true);
  frame.pack();
  frame.setVisible( true );
  
}

void draw() {
  
  // http://www.javaexamples.org/java/java.awt/how-to-use-pointerinfo.html
  PointerInfo pointerInfo = MouseInfo.getPointerInfo();
  Point location = pointerInfo.getLocation();
  frame.setBounds( (int) ( location.getX() - width * 0.5f ), (int) ( location.getY() - height * 0.5f ), width, height );
  
  background( random( 200,255 ), random( 200,255 ), random( 200,255 ) );
  
}
