
import org.jnativehook.*;
import org.jnativehook.keyboard.*;
import org.jnativehook.mouse.*;

public class GlobalListener implements 
  NativeKeyListener, 
  NativeMouseListener,
  NativeMouseMotionListener,
  NativeMouseWheelListener  {

  UserSpy parent;
  
  public GlobalListener( Object parent ) {
    
    this.parent = (UserSpy) parent;
    try {
      GlobalScreen.registerNativeHook();
    } catch (NativeHookException ex) {
      System.err.println("There was a problem registering the native hook.");
      System.err.println(ex.getMessage());
      return;
      // System.exit(1);
    }    
    GlobalScreen.getInstance().addNativeKeyListener( this );
    GlobalScreen.getInstance().addNativeMouseListener( this );
    GlobalScreen.getInstance().addNativeMouseMotionListener( this );
    GlobalScreen.getInstance().addNativeMouseWheelListener( this );
  }
  
  // KEYS
  @Override
  public void nativeKeyPressed(NativeKeyEvent e) {
    parent.nativeKeyPressed( (int) e.getKeyCode() );
    System.out.println("Key Pressed: " + NativeKeyEvent.getKeyText(e.getKeyCode()));
//    if (e.getKeyCode() == NativeKeyEvent.VC_ESCAPE) {
//      GlobalScreen.unregisterNativeHook();
//    }
  }
  @Override
  public void nativeKeyReleased(NativeKeyEvent e) {
    parent.nativeKeyReleased( (int) e.getKeyCode() );
    System.out.println("Key Released: " + NativeKeyEvent.getKeyText(e.getKeyCode()));
  }
  @Override
  public void nativeKeyTyped(NativeKeyEvent e) {
    parent.nativeKeyTyped( (int) e.getKeyCode() );
    System.out.println("Key Typed: " + NativeKeyEvent.getKeyText(e.getKeyCode()));
  }
  
  // MOUSE
  @Override
  public void nativeMouseClicked(NativeMouseEvent e) {
    parent.nativeMouseClicked( e.getX(), e.getY(), e.getButton() );
    System.out.println( "Mouse clicked: " + e.getX() + " / " + e.getY() + ", button: " + e.getButton() );
  }
  @Override
  public void nativeMousePressed(NativeMouseEvent e) {
    parent.nativeMousePressed( e.getX(), e.getY() , e.getButton() );
    System.out.println("Mouse pressed: " + e.getX() + " / " + e.getY() + ", button: " + e.getButton() );
  
  }
  @Override
  public void nativeMouseReleased(NativeMouseEvent e) {
    parent.nativeMouseReleased( e.getX(), e.getY() , e.getButton() );
    System.out.println("Mouse released: " + e.getX() + " / " + e.getY() + ", button: " + e.getButton() );
  }
  
  // MOUSE MOTION
  @Override
  public void nativeMouseMoved(NativeMouseEvent e) {
    parent.nativeMouseMoved( e.getX(), e.getY() );
    System.out.println("Mouse moved: " + e.getX() + " / " + e.getY() );
  }
  @Override
  public void nativeMouseDragged(NativeMouseEvent e) {
    parent.nativeMouseDragged( e.getX(), e.getY() );
    System.out.println("Mouse dragged: " + e.getX() + " / " + e.getY() );
  }
  
  // MOUSE WHEEL
  @Override
  public void nativeMouseWheelMoved(NativeMouseWheelEvent e) {
    parent.nativeMouseWheel( e.getWheelRotation() );
    System.out.println("Mouse wheel: " + e.getWheelRotation() );
  }
  
}

