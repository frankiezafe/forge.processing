
GlobalListener glistener;

void setup() {
  
  glistener = new GlobalListener( this );
  
}

void draw() {
  
}

void nativeMousePressed( int x, int y, int button ) {}
void nativeMouseReleased( int x, int y, int button ) {}
void nativeMouseClicked( int x, int y, int button ) {}
void nativeMouseMoved( int x, int y ) {}
void nativeMouseDragged( int x, int y ) {}
void nativeMouseWheel( int rot ) {}

void nativeKeyPressed( int nk ) {}
void nativeKeyReleased( int nk ) {}
void nativeKeyTyped( int nk ) {}

