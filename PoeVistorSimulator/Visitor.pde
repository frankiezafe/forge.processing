class Visitor {

  public float x1;
  public float x2;
  public float y;
  
  public PVector dir;
  
  public Visitor( float x1, float x2, float y ) {
    this.x1 = x1;
    this.x2 = x2;
    this.y = y;
    dir = new PVector();
  }
  
  public boolean hit( float x, float y ) {
    if ( y > this.y - 0.03 && y < this.y + 0.03 && x >= x1 - 0.015f && x <= x2 + 0.015f )
      return true;
    return false;
  }

  public void stop() {
    dir.x = 0;
    dir.y = 0;
  }

  public void update() {
  
    if ( dir.mag() > 0.01f ) {
      dir.normalize();
      dir.mult( 0.01f );
    }
    
    if ( 
      x1 + dir.x < -1 || x1 + dir.x > 1 ||
      x2 + dir.x < -1 || x2 + dir.x > 1
    ) {
      dir.x *= -1;
    }
    
    if ( y + dir.y < -1 || y + dir.y > 1 ) {
      dir.y *= -1;
    }
    
    x1 += dir.x;
    x2 += dir.x;
    y += dir.y;
    
  }

}
