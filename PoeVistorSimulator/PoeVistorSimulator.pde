import oscP5.*;
import netP5.*;

ArrayList< Visitor > visitors;
Visitor vselected;

float last_relmx;
float last_relmy;

float relmx;
float relmy;

OscP5 oscP5;
NetAddress poeapp;

void setup() {
  size( 600,600 );
  visitors = new ArrayList< Visitor >();
  relmx = 0;
  relmy = 0;
  vselected = null;
  
  oscP5 = new OscP5( this, 12000 );
  poeapp = new NetAddress( "127.0.0.1", 23000 );
  
}

void relMouse() {
  relmx = ( ( mouseX / ( width * 1.f ) ) - 0.5f ) * 2;
  relmy = ( ( mouseY / ( height * 1.f ) ) - 0.5f ) * 2;
}

void draw() {
  
  last_relmx = relmx;
  last_relmy = relmy;
  
  relMouse();
  
  background( 0 );
  translate( width * 0.5, height * 0.5f );
  
  for ( int i = 0; i < visitors.size(); i++ ) {
  
    Visitor v = visitors.get( i );
    v.update();
    float w = v.x2 - v.x1;
    float vx = v.x1 + w * 0.5;
    if ( ( vselected == null && v.hit( relmx, relmy ) ) || ( vselected == v ) ) {
      fill( 255 );
      stroke( 255,0,0 );
    } else {
      noStroke();
      fill( 200 );
    }
    ellipse( vx * width * 0.5f, v.y * height * 0.5f, w * width * 0.5f, 10 );
  
  }
  
  noFill();
  stroke( 0,255,255 );
  ellipse( relmx * width * 0.5f, relmy * height * 0.5f, 20, 20 );
  
  stroke( 255 );
  line( -20,0, 20,0 );
  line( 0,-20, 0,20 );
  text( "0,0", 5,15 );
  
  OscMessage msg = new OscMessage("/poe/w");
  msg.add( visitors.size() );
  for ( int i = 0; i < visitors.size(); i++ ) {
    Visitor v = visitors.get( i );
    msg.add( v.x1 * 0.5f );
    msg.add( v.x2 * 0.5f );
    msg.add( v.y );
  }
  oscP5.send( msg, poeapp ); 
  
}

void mousePressed() {

  relMouse();
  
  for ( int i = 0; i < visitors.size(); i++ ) {
    Visitor v = visitors.get( i );
    if ( v.hit( relmx, relmy ) ) {
      vselected = v;
      vselected.stop();
      break;
    }
  }
  
  if ( mouseButton == LEFT && vselected == null  ) {
    
    Visitor v = new Visitor( relmx - 0.01f, relmx + 0.01f, relmy );
    v.stop();
    visitors.add( v );
    vselected = v;
    
  } else if ( mouseButton == RIGHT && vselected != null  ) {
    
    visitors.remove( vselected );
    vselected = null;
    
  }
  
}

void mouseDragged() {
  
  relMouse();
  if ( vselected != null ) {
    vselected.x1 = relmx - 0.01f;
    vselected.x2 = relmx + 0.01f;
    vselected.y = relmy;
  }
  
}

void mouseReleased() {
  
  if ( vselected != null ) {
    vselected.dir.x = relmx - last_relmx;
    vselected.dir.y = relmy - last_relmy;
    vselected = null;
  }
  
}


