public float nextRadius( float currentr ) {
  return impact_gapoffest + currentr * impact_gapmult;
}

void generateFracture() {

  impact_min = random( 0.005, 0.025 );
  impact_radius = random( impact_min * 3 , 0.3f );
  
  fracture.clear();
  edges.clear();
  faces.clear();

  ArrayList< ArrayList< Vertice > > levels = new ArrayList< ArrayList< Vertice > >();
  ArrayList< ArrayList< Edge > > e_levels = new ArrayList< ArrayList< Edge > >();
  ArrayList< Vertice > lvln;

  Vertice newp = new Vertice( hitpoint[ 0 ], hitpoint[ 1 ], 0 );
  lvln = new ArrayList< Vertice >();
  lvln.add( newp );
  levels.add( lvln );
  fracture.add( newp );


  // counting circles
  float csz = impact_min;
  impact_circles = 0;
  while( csz < impact_radius ) {
    csz = nextRadius( csz );
    // println( csz + " > " + impact_radius );
    impact_circles++;
  }

  // generating points
  csz = impact_min;
  float lastr = 0;
  
  for ( int i = 0; i < impact_circles; i++ ) {

    lvln = new ArrayList< Vertice >();

    int lvl_impacts = impact_divisions + i * impact_lvl_mutliplier;

    float a = random( -PI, PI );
    float agap = TWO_PI / lvl_impacts;
    float arandom = agap * impact_angle_deviation;

    for ( int j = 0; j < lvl_impacts; j++ ) {
      float ra =  a + random( -1, 1 ) * arandom;
      float rr = csz - lastr;
      rr *= random( -impact_distance_deviation, impact_distance_deviation );
      rr += csz;
      newp = new Vertice (
      hitpoint[ 0 ] + cos( ra ) * rr, 
      hitpoint[ 1 ] + sin( ra ) * rr, 
      i + 1 // level!
      );
      lvln.add( newp );
      a += agap;
    }
    
    lastr = csz;
    csz = nextRadius( csz );
    levels.add( lvln );
    
  }
  
  // generating edges
  for ( int l = 1; l < levels.size(); l++ ) {
    ArrayList< Vertice > prevl = levels.get( l - 1 );
    lvln = levels.get( l );
    Vertice prevp = null;
    for ( int i = 0; i < lvln.size(); i++ ) {
      // current point
      Vertice p = lvln.get( i );
      if ( i == 0 )
        prevp = lvln.get( lvln.size() - 1 );
      // connecting to level below
      float closest = 0;
      Vertice cp = null;
      for ( int j = 0; j < prevl.size(); j++ ) {
        Vertice tmp = prevl.get( j );
        if ( j == 0 ) {
          cp = tmp;
          closest = dist( tmp.x, tmp.y, p.x, p.y );
          continue;
        }
        float d = dist( tmp.x, tmp.y, p.x, p.y );
        if ( d < closest ) {
          cp = tmp;
          closest = d;
        }
      }
      // edge creation
      if ( cp != null ) {
        p.bottom_connection = true;
        cp.top_connection = true;
        Edge e = new Edge( p, cp );
        edges.add( e );
      }
      Edge e = new Edge( p, prevp );
      edges.add( e );
      // populating fracture
      fracture.add( p );
      // for next run
      prevp = p;
    }
  }

  // verification of top connection, creating "optional" edges
  for ( int l = 0; l < levels.size() - 1; l++ ) {
    ArrayList< Vertice > nextl = levels.get( l + 1 );
    lvln = levels.get( l );
    for ( int i = 0; i < lvln.size(); i++ ) {
      Vertice p = lvln.get( i );
      if ( !p.top_connection ) {
        // connecting to level above
        float closest = 0;
        Vertice cp = null;
        for ( int j = 0; j < nextl.size(); j++ ) {
          Vertice tmp = nextl.get( j );
          if ( j == 0 ) {
            cp = tmp;
            closest = dist( tmp.x, tmp.y, p.x, p.y );
            continue;
          }
          float d = dist( tmp.x, tmp.y, p.x, p.y );
          if ( d < closest ) {
            cp = tmp;
            closest = d;
          }
        }
        // edge creation
        if ( cp != null ) {
          p.top_connection = true;
          cp.bottom_connection = true;
          Edge e = new Edge( p, cp );
          e.optional = true;
          edges.add( e );
        }
      }
    }
  }

  // generating all faces between level 0 & 1
  for ( int i = 0; i < edges.size(); i++ ) {
    Edge e = edges.get( i );
    if ( e.level == 1 ) {
      ArrayList< Edge > connected = getConnected( e );
      Edge c1 = null;
      Edge c2 = null;
      for ( int j = 0; j < connected.size(); j++ ) {
        Edge tmpe = connected.get( j );
        if ( tmpe.level == e.level - 0.5 ) {
          if ( tmpe.connectedTo( e.pt1 ) )
            c1 = tmpe;
          else if ( tmpe.connectedTo( e.pt2 ) )
            c2 = tmpe;
          else
            System.err.println( "WTF!!!" );
        }
      }
      Face f = new Face( e, c1, c2 );
      faces.add( f );
    } else if ( e.level > 1 ) {
      break;
    }
  }
  
  int currenl = 1;
  boolean facecreated = true;
  while( currenl < impact_circles ) {
    
    facecreated = false;
    
    for ( int i = 0; i < edges.size(); i++ ) {
      Edge e = edges.get( i );
      if ( e.level == currenl && e.facescount > 0 ) {
        ArrayList< Edge > connected = getConnected( e );
        ArrayList< Edge > c1 = new ArrayList< Edge >();
        ArrayList< Edge > c2 = new ArrayList< Edge >();
        for ( int j = 0; j < connected.size(); j++ ) {
          Edge tmpe = connected.get( j );
          if ( tmpe.level == e.level + 0.5 && tmpe.facescount == 0 ) {
            if ( tmpe.connectedTo( e.pt1 ) )
              c1.add( tmpe );
            else if ( tmpe.connectedTo( e.pt2 ) )
              c2.add( tmpe );
            else
              System.err.println( "WTF!!!" );
          }
        }
        for ( int n = 0; n < c1.size(); n++ ) {
          Edge tmpc1 = c1.get( n );
          Vertice vc1 = tmpc1.getOther( e.pt1 );
          for ( int m = 0; m < c2.size(); m++ ) {
            Edge tmpc2 = c2.get( m );
            Vertice vc2 = tmpc2.getOther( e.pt2 );
            if ( vc1 == vc2 ) {
              // System.out.println( "A TRIANGLE!" );
              Face f = new Face( e, tmpc1, tmpc2 );
              faces.add( f );
              c1.remove( n );
              c2.remove( m );
              facecreated = true;
            }
          }
        }
      }
    }
    if ( !facecreated )
      currenl++;
  }
  
}

public Edge getConnected( Edge ref1, Edge ref2, Edge not ) {
  for ( int i = 0; i < edges.size(); i++ ) {
    Edge e = edges.get( i );
    if ( 
      e != not && 
      e.connectedTo( ref1.pt1, ref1.pt2 ) && 
      e.connectedTo( ref2.pt1, ref2.pt2 ) )
      return e;
  }
  return null;
}

public ArrayList< Edge > getConnected( Edge ref ) {
  ArrayList< Edge > cns = new ArrayList< Edge >();
  for ( int i = 0; i < edges.size(); i++ ) {
    Edge e = edges.get( i );
    if ( e.connectedTo( ref.pt1, ref.pt2 ) )
      cns.add( e );
  }
  return cns;
}

public boolean faceExist( Edge e1, Edge e2, Edge e3 ) {
  Face tmpf = new Face( e1, e2, e3 );
  for ( int i = 0; i < faces.size(); i++ ) {
    Face f = faces.get( i );
    if ( f.isSame( tmpf ) )
      return true;
  }
  return false;
}
