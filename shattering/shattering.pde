
float[] window = new float[] { 600, 600 };
float[] hwin = new float[] { window[0] * 0.5f, window[1] * 0.5 };

float[] hitpoint = new float[] { Float.NaN, Float.NaN };
ArrayList< Vertice > fracture = new ArrayList< Vertice >();
ArrayList< Edge > edges = new ArrayList< Edge >();
ArrayList< Face > faces = new ArrayList< Face >();

float impact_min = 0.02;
float impact_drawmin = 0.03;
float impact_radius = 0.5;

float impact_gapoffest = 0.015f;
float impact_gapmult = 1.1f;

int impact_circles;
int impact_divisions = 30;
int impact_lvl_mutliplier = -1;

float impact_angle_deviation = 0.45;
float impact_distance_deviation = 0.25;

void setup() {

  size( 600, 600, P3D );
  smooth();
  background( 255 );
  
}

void mousePressed() {

  hitpoint[ 0 ] = mouseX / window[ 0 ];
  hitpoint[ 1 ] = mouseY / window[ 1 ];
  generateFracture();
  
}

void draw() {
  
  if ( Float.isNaN( hitpoint[ 0 ] ) )
    return;
    
  hitpoint[ 0 ] = Float.NaN;
  
  /*
  noStroke();
  fill( 255, 120 );
  rect( 0,0, window[0], window[1] );
  */
  
  strokeWeight( 4 );
  drawFacture( 0, 0.5f );
  // filter( BLUR, 1 );
  
  strokeWeight( 1 );
  drawFacture( 0, 1 );
  
}

void drawFacture( float white, float alphamult ) {
  
  pushMatrix();

  for ( int i = 0; i < edges.size(); i++ ) {
    Edge e = edges.get( i );
    if ( e.optional )
      continue;
    else if ( e.connect_same_level && e.length > impact_drawmin )
      continue;
    else
      stroke( white, map( e.level, 0, impact_circles, 245, 0 ) * alphamult );
    line( 
    e.pt1.x * window[0], e.pt1.y * window[1], 
    e.pt2.x * window[0], e.pt2.y * window[1]
      );
  }

  noStroke();
  fill( white, 255 * alphamult );
  beginShape( TRIANGLES );
  for ( int i = 0; i < faces.size(); i++ ) {
    Face f = faces.get( i );
    if ( f.type == FACE_TRIANGLE ) {
      vertex( f.pts[ 0 ].x * window[0], f.pts[ 0 ].y * window[1] );
      vertex( f.pts[ 1 ].x * window[0], f.pts[ 1 ].y * window[1] );
      vertex( f.pts[ 2 ].x * window[0], f.pts[ 2 ].y * window[1] );
    }
  }
  endShape();
  
  for ( int i = 0; i < faces.size(); i++ ) {
    Face f = faces.get( i );
    if ( f.type == FACE_QUAD ) {
      beginShape();
      vertex( f.pts[ 0 ].x * window[0], f.pts[ 0 ].y * window[1] );
      vertex( f.pts[ 1 ].x * window[0], f.pts[ 1 ].y * window[1] );
      vertex( f.pts[ 2 ].x * window[0], f.pts[ 2 ].y * window[1] );
      vertex( f.pts[ 3 ].x * window[0], f.pts[ 3 ].y * window[1] );
      endShape();
    }
  }

  popMatrix();
  
}
