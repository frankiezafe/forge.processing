
public static int FACE_ID = 0;
public static int FACE_TRIANGLE = 0;
public static int FACE_QUAD = 1;

class Face {
  
  Edge e1;
  Edge e2;
  Edge e3;
  Edge e4;
  
  int side;
  int fID;
  int type;
  
  Vertice[] pts;
  Vertice center;
  
  public Face( Edge e1, Edge e2, Edge e3 ) {
    
    fID = FACE_ID;
    FACE_ID++;
    type = FACE_TRIANGLE;
    
    this.e1 = e1;
    this.e2 = e2;
    this.e3 = e3;
    this.e4 = null;
    
    pts = new Vertice[ 3 ];
    finish();
    
  }
  
  public Face( Edge e1, Edge e2, Edge e3, Edge e4 ) {
    
    fID = FACE_ID;
    FACE_ID++;
    type = FACE_QUAD;
    
    this.e1 = e1;
    this.e2 = e2;
    this.e3 = e3;
    this.e4 = e4;
    
    pts = new Vertice[ 4 ];
    finish();
    
  }
  
  private void finish() {
    
    pts[ 0 ] = e1.pt1;
    pts[ 1 ] = e1.pt2;
    int n = 2;
    
    if ( pts.length == 4 ) {
      n = addpt( n, e3.pt1 );
      n = addpt( n, e3.pt2 );
      n = addpt( n, e2.pt1 );
      n = addpt( n, e2.pt2 );
      n = addpt( n, e4.pt1 );
      n = addpt( n, e4.pt2 );
      println( "finally: " + n );
    } else {
      n = addpt( n, e2.pt1 );
      n = addpt( n, e2.pt2 );
      n = addpt( n, e3.pt1 );
      n = addpt( n, e3.pt2 );
    }
    
    e1.facescount++;
    e2.facescount++;
    e3.facescount++;
    if ( pts.length == 4 )
      e4.facescount++;
    
    center = new Vertice( 0,0 );
    for ( int i = 0; i < pts.length; i++ )
      center.add( pts[ i ] );
    center.mult( 1.f / pts.length );
    
    Vertice mid = new Vertice();
    mid.blend( pts );
    
  }
  
  private Edge getEdge( Vertice pt1, Vertice pt2 ) {
    if ( e1.isMyPoint( pt1, pt2 ) )
      return e1;
    if ( e2.isMyPoint( pt1, pt2 ) )
      return e2;
    if ( e3.isMyPoint( pt1, pt2 ) )
      return e3;
    return null;
  }
  
  private int addpt( int alreadyin, Vertice p ) {
    
    if ( alreadyin == pts.length )
      return alreadyin;
    
    for ( int i = 0; i < alreadyin; i++ ) {
      if ( pts[ i ] == p ) {
        // println( "found at: " + i +" (" + alreadyin + ")" );
        return alreadyin;
      }
    }
    // println( "adding at: " + alreadyin );
    pts[ alreadyin ] = p;
    alreadyin++;
    return alreadyin;
  
  }
  
  public boolean isSame( Face f ) {
    
    int found = 0;
    for ( int i = 0; i < 3; i++ ) {
      Vertice fp = f.pts[ i ];
      for ( int j = 0; j < 3; j++ ) {
        if ( fp == pts[ j ] ) {
          found++;
          break;
        }
      }
    }
    if ( found == 3 )
      return true;
    return false;
    
  }
  
  public Vertice getClosestPoint( Edge e, ArrayList< Vertice > availablepoints ) {
    
    Vertice closest = null;
    float shortest = Float.NaN;
    PVector v =  new PVector();
    for ( int i = 0; i < availablepoints.size(); i++ ) {
      
      Vertice p = availablepoints.get( i );
      float d = dist( p.x, p.y, e.middle.x, e.middle.y );
      
      if (Float.isNaN( shortest ) || d < shortest ) {
        
        Vertice awayL = new Vertice ( e.middle.x + e.left.x, e.middle.y + e.left.y );
        Vertice awayR = new Vertice ( e.middle.x + e.right.x, e.middle.y + e.right.y );
        
        float dL = dist( awayL.x, awayL.y, p.x, p.y );
        float dR = dist( awayR.x, awayR.y, p.x, p.y );
        
        if ( abs( dL - dR ) > e.length * 1.f )
          continue;
        
        int dir = EDGE_LEFT;
        if ( dL > dR )
          dir = EDGE_RIGHT;
        
        if ( dir == side )
          continue;
        
        shortest = d;
        closest = p;
        
      }
    }
    
    return closest;
  
  }
  
}
