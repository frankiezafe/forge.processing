class Vertice extends PVector {

  int level;
  
  boolean top_connection = false;
  boolean bottom_connection = false;
  
  public Vertice() {
    super();
    this.level = -1;
  }
  
  public Vertice( float x, float y ) {
    super( x,y );
    this.level = -1;
  }
  
  public Vertice( float x, float y, int level ) {
    super( x,y );
    this.level = level;
  }
  
  public void blend( Vertice v1, Vertice v2 ) {
    x = v1.x * 0.5f + v2.x * 0.5f;
    y = v1.y * 0.5f + v2.y * 0.5f;
  }
  
  public void blend( Vertice[] vs ) {
    x = 0;
    y = 0;
    float div = 1.f / vs.length;
    for ( int i = 0; i < vs.length; i++ ) {
      x = vs[ i ].x * div;
      y = vs[ i ].y * div;
    }
  }

}
