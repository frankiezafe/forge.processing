class Plane {

  // plane defined by one point and a normale only
  // ax + by+ cz + d = 0, abc being the normale  
  PVector normale; // normale
  PVector u;
  PVector v;
  float d;
  PVector xyz; // ref point
  
  // utils
  PVector viewrot;
  PVector tmpv = new PVector();
  PVector tmpv2 = new PVector();

  public Plane( float x, float y, float z ) {
    normale = new PVector( 1,1,1 );
    normale.normalize();
    xyz = new PVector( x, y, z );
    u = new PVector();
    v = new PVector();
    render();
    viewrot = new PVector();
    update();
  }
  
  private void render() {
    normale.normalize();
    u = normale.cross( UP );
    u.normalize();
    v = normale.cross( u );
    v.normalize();
    d = -normale.x * xyz.x -normale.y * xyz.y -normale.z * xyz.z;
  }

  public UVPoint[] projectPoints( PVector[] projs ) {
    UVPoint[] output = new UVPoint[ projs.length ];
    float locd = xyz.dot( normale );
    for ( int i = 0; i < projs.length; i++ ) {
      float d = projs[ i ].dot( normale ) - locd;
      PVector tmp = new PVector();
      tmp.set( normale );
      tmp.mult( -d );
      tmp.add( projs[ i ] );
      tmp.x -= xyz.x;
      tmp.y -= xyz.y;
      tmp.z -= xyz.z;
      output[ i ] = new UVPoint( tmp.dot( u ), tmp.dot( v ) );
      output[ i ].render( p );
    }
    return output;
  }
  
  public void update() {
    render();
  }
  
  public void draw( PVector rot ) {
    
    viewrot = rot;
    
    stroke( 255 );
    noFill();
    drawpt( xyz, 8 );
    
    pushMatrix();
    translate( xyz.x, xyz.y, xyz.z );
    stroke( 255,0,0 );
    line( 0, 0, 0, 50 * u.x, 50 * u.y, 50 * u.z );
    stroke( 0,255,0 );
    line( 0, 0, 0, 50 * v.x, 50 * v.y, 50 * v.z );
    stroke( 127 );
    line( 0, 0, 0, 50 * normale.x, 50 * normale.y, 50 * normale.z );
    popMatrix();
    
  }
  
  public void drawpt( PVector v ) {
    drawpt( v, 2 );
  }
  
  public void drawpt( PVector v, float s ) {
    drawpt( v.x, v.y, v.z, s );
  }
  
  public void drawpt( float x, float y, float z, float s ) {
    pushMatrix();
    translate( x, y, z );
    rotateZ( viewrot.z );
    rotateY( viewrot.y );
    rotateX( viewrot.x );
    ellipse( 0,0, s,s );
    popMatrix();
  }
  
}

