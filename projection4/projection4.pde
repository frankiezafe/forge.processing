import java.util.*; 

private Plane p;
private PVector rot;
private PVector tor;

private int ptnum = 100;
private PVector[] pt;

private UVPoint[] uvs;
private UVFace[] ufs;

public static final PVector UP = new PVector( 0,1,0 );

private boolean displayGrid = false;
private boolean displayPoints = true;
private float smoothing;

public void setup() {

  size( 800, 600, P3D );
  p = new Plane( 10, -10, 20 );
  rot = new PVector();
  tor = new PVector();
  pt = new PVector[ ptnum ];
  for ( int i = 0; i < pt.length; i++ ) {
    pt[ i ] = new PVector( random( -250, 250 ), random( -250, 250 ), random( -350, 350 ) );
  }
  int def = 7;
  uvs = new UVPoint[ (int) pow( def + 1, 2 ) ];
  int uvi = 0;
  for ( int y = -175; y <= 175; y += 50 ) {
  for ( int x = -175; x <= 175; x += 50 ) {
    uvs[ uvi ] = new UVPoint( x, y );
    uvi++;
  }
  }
  ufs = new UVFace[ def * def * 2 ];
  int ufi = 0;
  for ( int r = 0; r < def; r++ ) {
  for ( int c = 0; c < def; c++ ) {
    int tl = c + ( r * ( def + 1 ) );
    int tr = c + 1 + ( r * ( def + 1 ) );
    int bl = c + ( ( r + 1 ) * ( def + 1 ) );
    int br = c + 1 + ( ( r + 1 ) * ( def + 1 ) );
    ufs[ ufi ] = new UVFace( uvs[ tl ], uvs[ tr ], uvs[ br ] );
    ufi++;
    ufs[ ufi ] = new UVFace( uvs[ br ], uvs[ bl ], uvs[ tl ] );
    ufi++;
  }
  }
  
  smoothing = 0.6;
  
}

public void draw() {

  // p.normale.x = sin( frameCount * 0.035 );
  p.normale.y = map( cos( frameCount * 0.0087 ), -1, 1, -0.5, 0.5 );
  p.normale.z = 0;
  rot.x =  -0.05;
  rot.y = frameCount * 0.0017;
  tor.set( rot );
  tor.mult( -1 );
  p.viewrot = tor;

  p.update();
  if ( displayGrid ) {
    for ( int i = 0; i < uvs.length; i++ )
      uvs[ i ].render( p );
  }
  
  background( 10 );

  pushMatrix();
  translate( width * 0.5, height * 0.5, 0 );  
  rotateX( rot.x );
  rotateY( rot.y );
  rotateZ( rot.z );

  if ( displayGrid ) {
    
    strokeWeight( 3 );
    stroke( 255, 0, 0 );
    line( 0, 0, 0, 50, 0, 0 );
    stroke( 0, 255, 0 );
    line( 0, 0, 0, 0, 50, 0 );
    stroke( 0, 0, 255 );
    line( 0, 0, 0, 0, 0, 50 );
  
    p.draw( tor );
    
    strokeWeight( 1 );
    stroke( 255, 100 );
    noFill();
    for ( int i = 0; i < uvs.length; i++ ) {
      pushMatrix();
      translate( uvs[ i ].x, uvs[ i ].y, uvs[ i ].z );
      rotateZ( tor.z );
      rotateY( tor.y );
      rotateX( tor.x );
      ellipse( 0, 0, 3, 3 );
      popMatrix();
    }
    
    beginShape( TRIANGLES );
    for ( int i = 0; i < ufs.length; i++ ) {
      ufs[ i ].printVertices();
    }
    endShape();
    
  }
  
  if ( !displayGrid ) {
    
    UVPoint[] projected = p.projectPoints( pt );
    
    strokeWeight( 1 );
    
    if ( displayPoints ) {
      for ( int i = 0; i < pt.length; i++ ) {
        stroke( 255, 70 );
        line( pt[ i ].x, pt[ i ].y, pt[ i ].z, projected[ i ].x, projected[ i ].y, projected[ i ].z );
        stroke( 255, 150 );
        p.drawpt( pt[ i ] );
        p.drawpt( projected[ i ].x, projected[ i ].y, projected[ i ].z, 5 );
      }
    }
    
    UVPoint barycenter = sortUVpts( projected );
    UVFace[] projfaces = new UVFace[ projected.length ];
    for ( int i = 0; i < projected.length; i++ ) {
      int j = i + 1;
      if ( j >= projected.length )
        j = 0;
      projfaces[ i ] = new UVFace( projected[ i ], projected[ j ], barycenter );
      projfaces[ i ].filled = false;
      projfaces[ i ].setColor( 255, 0, 0, 100 );
    }
    
    if ( displayPoints ) {
      beginShape( TRIANGLES );
      for ( int i = 0; i < projfaces.length; i++ ) {
        projfaces[ i ].printVertices();
      }
      endShape();
    }
    
    smoothUVpts( projected, smoothing );
    beginShape( TRIANGLES );
    for ( int i = 0; i < projfaces.length; i++ ) {
      projfaces[ i ].filled = true;
      projfaces[ i ].stroked = false;
      projfaces[ i ].printVertices();
    }
    endShape();
    
  }
  
  popMatrix();
  
  fill( 255 );
  text( frameRate, 10, 25 );
  text( "smooth: " + smoothing, 10, 40 );
  
}

public void keyPressed() {
  
  if ( key == 'g' )
    displayGrid = !displayGrid;

  else if ( key == 'p' )
    displayPoints = !displayPoints;
  
  else if ( keyCode == 38 )
    smoothing += 0.05;
    
  else if ( keyCode == 40 )
    smoothing -= 0.05;
  
  else
    println( keyCode );
  
}

