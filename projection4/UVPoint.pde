class UVPoint {

  float x;
  float y;
  float z;
  float u;
  float v;
  
  public UVPoint() {
    x = 0;
    y = 0;
    z = 0;
    u = 0;
    v = 0;
  }
  
  public UVPoint( float u, float v ) {
    x = 0;
    y = 0;
    z = 0;
    this.u = u;
    this.v = v;
  }
  
  public UVPoint( UVPoint uvp ) {
    x = uvp.x;
    y = uvp.y;
    z = uvp.z;
    u = uvp.u;
    v = uvp.v;
  }
  
  public void set( UVPoint uvp ) {
    x = uvp.x;
    y = uvp.y;
    z = uvp.z;
    u = uvp.u;
    v = uvp.v;
  }
  
  public void minus( UVPoint uvp ) {
    x -= uvp.x;
    y -= uvp.y;
    z -= uvp.z;
    u -= uvp.u;
    v -= uvp.v;
  }
  
  public void add( UVPoint uvp ) {
    x += uvp.x;
    y += uvp.y;
    z += uvp.z;
    u += uvp.u;
    v += uvp.v;
  }
  
  public void mult( float m ) {
    x *= m;
    y *= m;
    z *= m;
    u *= m;
    v *= m;
  }
  
  public void render( Plane p ) {
    PVector tmp1 = new PVector();
    tmp1.set( p.u );
    tmp1.mult( u );
    PVector tmp2 = new PVector();
    tmp2.set( p.v );
    tmp2.mult( v );
    tmp1.add( tmp2 );
    tmp1.add( p.xyz );
    x = tmp1.x;
    y = tmp1.y;
    z = tmp1.z;
  }

}
