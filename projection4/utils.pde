
public UVPoint sortUVpts( UVPoint[] uvs ) {
  
  UVPoint bary = new UVPoint( 0, 0 );
  for ( int i = 0; i < uvs.length; i++ ) {
    bary.add( uvs[ i ] );
  }
  bary.mult( 1.f / uvs.length );
  HashMap< Float, UVPoint > hm = new HashMap< Float, UVPoint >();
  for ( int i = 0; i < uvs.length; i++ ) {
    hm.put( atan2( uvs[ i ].v - bary.v, uvs[ i ].u - bary.u ), uvs[ i ] );
  }
  SortedSet< Float > keys = new TreeSet< Float >(hm.keySet());
  int i = 0;
  for ( Float key : keys) { 
    uvs[ i ] = hm.get( key );
    i++;
  }
  return bary;
  
}

public void smoothUVpts( UVPoint[] uvs, float amount ) {
  
  UVPoint[] tmps = new UVPoint[ uvs.length ];
  for ( int i = 0; i < uvs.length; i++ ) {
    tmps[ i ] = new UVPoint( uvs[ i ] );
  }
  float rest = 1 - amount;
  UVPoint prev = new UVPoint();
  UVPoint next = new UVPoint();
  for ( int i = 0; i < uvs.length; i++ ) {
    int h = i - 1;
    if ( h < 0 ) {
      h = uvs.length -1;
    }
    int j = i + 1;
    if ( j >= uvs.length ) {
      j = 0;
    }
    prev.set( tmps[ h ] );
    prev.mult( amount * 0.5 );
    next.set( tmps[ j ] );
    next.mult( amount * 0.5 );
    uvs[ i ].mult( rest );
    uvs[ i ].add( prev );
    uvs[ i ].add( next );
  }
  
}

