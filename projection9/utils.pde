class PointDrawer {

  PVector viewrot;
  
  public PointDrawer() {
    viewrot = new PVector();
  }
  
  public void drawpt( PVector v ) {
    drawpt( v, 2 );
  }
  
  public void drawpt( PVector v, float s ) {
    drawpt( v.x, v.y, v.z, s );
  }
  
  public void drawpt( float x, float y, float z, float s ) {
    pushMatrix();
    translate( x, y, z );
    rotateZ( viewrot.z );
    rotateY( viewrot.y );
    rotateX( viewrot.x );
    ellipse( 0,0, s,s );
    popMatrix();
  }
  
}

// sorts list of uvpoints by using the angle between each poitn and their barycentre
public UVPoint sortUVpts( UVPoint[] uvs ) {
  
  UVPoint bary = new UVPoint( 0, 0 );
  for ( int i = 0; i < uvs.length; i++ ) {
    bary.add( uvs[ i ] );
  }
  bary.mult( 1.f / uvs.length );
  HashMap< Float, UVPoint > hm = new HashMap< Float, UVPoint >();
  for ( int i = 0; i < uvs.length; i++ ) {
    hm.put( atan2( uvs[ i ].v - bary.v, uvs[ i ].u - bary.u ), uvs[ i ] );
  }
  SortedSet< Float > keys = new TreeSet< Float >(hm.keySet());
  int i = 0;
  for ( Float key : keys) { 
    uvs[ i ] = hm.get( key );
    i++;
  }
  return bary;
  
}

// merge an amount of the previous and next point with the current point
public void smoothUVpts( UVPoint[] uvs, float amount ) {
  
  UVPoint[] tmps = new UVPoint[ uvs.length ];
  for ( int i = 0; i < uvs.length; i++ ) {
    tmps[ i ] = new UVPoint( uvs[ i ] );
  }
  float rest = 1 - amount;
  UVPoint prev = new UVPoint();
  UVPoint next = new UVPoint();
  for ( int i = 0; i < uvs.length; i++ ) {
    int h = i - 1;
    if ( h < 0 ) {
      h = uvs.length -1;
    }
    int j = i + 1;
    if ( j >= uvs.length ) {
      j = 0;
    }
    prev.set( tmps[ h ] );
    prev.mult( amount * 0.5 );
    next.set( tmps[ j ] );
    next.mult( amount * 0.5 );
    uvs[ i ].mult( rest );
    uvs[ i ].add( prev );
    uvs[ i ].add( next );
  }
  
}

