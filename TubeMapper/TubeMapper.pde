import saito.objloader.*;
import processing.video.*;

Capture webcam;
Movie mov;

PVector offset;
float rotX;
float rotY;

Mesh cylinder;
Mesh cylinder2;

void setup() {
  
  size( 600, 600, P3D );
  
  smooth();
  
  webcam = new Capture( this, 640, 480 );
  webcam.start(); 
  
  mov = new Movie(this, "transit.mov");
  mov.loop();
  
  offset = new PVector();
  rotX = 1.2f;
  rotY = 0;
  
  cylinder = new Mesh();
  cylinder.textureSpace( 0, 0.2, 0.8, 0, 1 );
  cylinder.generateCylinder( 50, 300, 60, true );
  
  cylinder2 = new Mesh();
  cylinder2.generateCylinder( 70, 200, 60, false );
  cylinder2.move( -150, 0, 0 );
  
}

void captureEvent(Capture c) {
  c.read();
}

void movieEvent(Movie movie) {  
  mov.read();
}

void draw()
{
  
  cylinder.setTexture( 0, webcam );
  cylinder.setTexture( 1, mov );
  cylinder2.setTexture( 0, mov );

//  float fov = PI / 3.0;
//  fov = map( mouseX, 0, width, 0, PI );
//  float cameraZ = ( height / 2.0 ) / tan( PI / 3.0 );
//  perspective( fov, float( width ) / float( height ), cameraZ/10.0, cameraZ*10.0 );
  
  background( 10 );

  pushMatrix();
  translate( width/2 + offset.x, height/2 + offset.y, offset.z );
  rotateX(rotX);
  rotateY(rotY);

  cylinder.draw();
  cylinder2.draw();
  
  popMatrix();

  fill( 255 );
  text( frameRate, 10, 25 );
  
}


void mouseDragged() {
  rotY += (mouseX - pmouseX) * 0.01;
  rotX += (mouseY - pmouseY) * 0.01;
}

void keyPressed() {

  if ( keyCode == UP ) {
    offset.z--;
  } else if ( keyCode == DOWN ) {
    offset.z++;
  } else if ( keyCode == LEFT ) {
    offset.x--;
  } else if ( keyCode == RIGHT ) {
    offset.x++;
  }

}
