class Face {

  public Vertex v1 = null;
  public Vertex v2 = null;
  public Vertex v3 = null;
  public Vertex v4 = null;
  public int vnum = 0;
  
  public void finish() {
    
    if ( v1 == null ) {
      vnum = 0;
    } else if ( v2 == null ) {
      vnum = 1;
    } else if ( v3 == null ) {
      vnum = 2;
    } else if ( v4 == null ) {
      vnum = 3;
    } else {
      vnum = 4;
    }
    
  }

}
