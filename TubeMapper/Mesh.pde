class Mesh {

  private Face[] faces;
  private Vertex[] vertices;
  private PVector _pos;
  private PVector _scale;
  private PImage[] textures;
  private String type;
  private PImage defaulttexture;
  
  private HashMap< Integer, TextureSpace > texspaces;
  
  public Mesh() {
    
    _pos = new PVector();
    _scale = new PVector( 1,1,1 );
    faces = null;
    vertices = null;
    textures = null;
    type = "EMPTY";
    defaulttexture = generateTexture();
    
    texspaces = new HashMap< Integer, TextureSpace >();
    texspaces.put( 0, new TextureSpace() );
    
  }
  
  public void textureSpace( int id, float minx, float maxx, float miny, float maxy ) {
    
    TextureSpace ts = null;
    boolean create = false;
    if ( texspaces.containsKey( id ) ) {
      ts = texspaces.get( id );
    } else {
      ts = new TextureSpace();
      create = true;
    }
    ts.offset.set( minx, miny, ts.offset.z );
    ts.scale.set( maxx - minx, maxy - miny, ts.scale.z );
    if ( create ) {
      texspaces.put( id, ts );
    }
    
  }
  
  private void setTexCood( int tspaceid, int vid, float u, float v ) {
    
    TextureSpace ts = texspaces.get( tspaceid );
    if ( ts == null ) {
      ts = texspaces.get( 0 );
    }
    vertices[ vid ].texcoord.set(
      ts.offset.x + u * ts.scale.x,
      ts.offset.y + v * ts.scale.y,
      0
    );
  } 
  
  public void generateCylinder( float radius, float height, int definition, boolean caps ) {
    
    type = "TUBE";
    if ( caps ) {
      type = "CYLINDER";
    }
    
    // working with quads...
    int ptnum = definition * 2 + 2; // due to seams on one edge of the tube
    int facenum = definition;
    int texnum = 1;
    if ( caps ) {
      facenum += definition * 2;
      ptnum += definition * 2 + 2; // due to the central point of the 2 caps
      texnum = 2;
    }
    vertices = new Vertex[ ptnum ];
    faces = new Face[ facenum ];
    textures = new PImage[ texnum ];
    
    // generation of vertices
    // first of all, the tube
    int vi = 0;
    float agap = TWO_PI / definition;
    for ( float z = -height*0.5; z <= height * 0.5; z += height ) {
      for ( float a = 0; a <= TWO_PI; a += agap ) {
        float x = cos( a ) * radius;
        float y = sin( a ) * radius;
        vertices[ vi ] = new Vertex();
        vertices[ vi ].pos.set( x, y, z );
        setTexCood( 0, vi, ( a / TWO_PI ), 1 - ( ( z + height * 0.5 ) / height ) );
        vi++;
      }
    }
    // then the caps if there's some
    if ( caps ) {
      int j = definition * 2 + 2;
      int ref = 0;
      for ( int c = 0; c < 2; c++ ) {
        for ( int i = 0; i < definition; i++ ) {
          vertices[ j ] = new Vertex();
          vertices[ j ].pos.set( vertices[ ref ].pos.x, vertices[ ref ].pos.y, vertices[ ref ].pos.z );
          float a = i * TWO_PI / definition;
          setTexCood( 1, j, ( -cos( a ) + 1 ) * 0.5, ( sin( a ) + 1 ) * 0.5 );
          j++;
          ref++;
        }
        ref = definition + 1;
        vertices[ j ] = new Vertex();
        vertices[ j ].pos.set( 0,0, -height*0.5 + c * height );
        setTexCood( 1, j, 0.5, 0.5 );
        j++;
      }
    }
    
    // the faces now
    for ( int i = 0; i < definition; i++ ) {
      int nextv = i + 1;
      faces[ i ] = new Face();
      faces[ i ].v1 = vertices[ i ];
      faces[ i ].v2 = vertices[ nextv ];
      faces[ i ].v3 = vertices[ nextv + definition + 1 ];
      faces[ i ].v4 = vertices[ i + definition + 1 ];
      faces[ i ].finish();
    }
    
    if ( caps ) {
      // triangle fans
      int f = definition;
      int j = definition * 2 + 2;
      int middle = definition * 3 + 2;
      for ( int c = 0; c < 2; c++ ) {
        for ( int i = 0; i < definition; i++ ) {
          int nextv = i + 1;
          if ( nextv >= definition ) {
            nextv = 0;
          }
          faces[ f ] = new Face();
          faces[ f ].v1 = vertices[ middle ];
          faces[ f ].v2 = vertices[ j + i ];
          faces[ f ].v3 = vertices[ j + nextv ];
          faces[ f ].finish();
          f++;
        }
        j += definition + 1;
        middle += definition + 1;
      }
    }
    
    for ( int i = 0; i < texnum; i++ ) {
      textures[ i ] = defaulttexture;
    }
      
  }
  
  public void debug() {
    
    pushMatrix();
    translate( _pos.x, _pos.y, _pos.z );
    scale( _scale.x, _scale.y, _scale.z );
    
    // axis
    strokeWeight( 1 );
    stroke( 255, 0, 0 );
    line( 0,0,0, 100,0,0 );
    stroke( 0, 255, 0 );
    line( 0,0,0, 0,100,0 );
    stroke( 0, 0, 255 );
    line( 0,0,0, 0,0,100 );
    
    // points
    strokeWeight( 5 );
    stroke( 255, 127, 0 );
    beginShape( POINTS );
    for ( int i = 0; i < vertices.length; i++ ) {
      Vertex v = vertices[ i ];
      vertex( v.pos.x, v.pos.y, v.pos.z );
    }
    endShape();
    
    PImage t = textures[ 0 ];
    float tw = t.width;
    float th = t.height;
    
    strokeWeight( 1 );
    stroke( 127, 255, 0, 100 );
    beginShape( TRIANGLES );
    texture( t );
    for ( int i = 0; i < faces.length; i++ ) {
      Face f = faces[ i ];
      if ( f == null || f.vnum != 3 ) {
        continue;
      }
      vertex( f.v1.pos.x, f.v1.pos.y, f.v1.pos.z, f.v1.texcoord.x * tw, f.v1.texcoord.y * th );
      vertex( f.v2.pos.x, f.v2.pos.y, f.v2.pos.z, f.v2.texcoord.x * tw, f.v2.texcoord.y * th );
      vertex( f.v3.pos.x, f.v3.pos.y, f.v3.pos.z, f.v3.texcoord.x * tw, f.v3.texcoord.y * th );
    }
    endShape();
    
    strokeWeight( 1 );
    stroke( 127, 255, 0, 100 );
    beginShape( QUADS );
    texture( t );
    for ( int i = 0; i < faces.length; i++ ) {
      Face f = faces[ i ];
      if ( f == null || f.vnum != 4 ) {
        continue;
      }
      vertex( f.v1.pos.x, f.v1.pos.y, f.v1.pos.z, f.v1.texcoord.x * tw, f.v1.texcoord.y * th );
      vertex( f.v2.pos.x, f.v2.pos.y, f.v2.pos.z, f.v2.texcoord.x * tw, f.v2.texcoord.y * th );
      vertex( f.v3.pos.x, f.v3.pos.y, f.v3.pos.z, f.v3.texcoord.x * tw, f.v3.texcoord.y * th );
      vertex( f.v4.pos.x, f.v4.pos.y, f.v4.pos.z, f.v4.texcoord.x * tw, f.v4.texcoord.y * th );
    }
    endShape();
    
    popMatrix();
    
  }
  
  public void draw() {
  
    pushMatrix();
    translate( _pos.x, _pos.y, _pos.z );
    scale( _scale.x, _scale.y, _scale.z );
    
    if ( type.equals( "TUBE" ) ) {
      
      PImage t = textures[ 0 ];
      float tw = t.width;
      float th = t.height;
      
      noStroke();
      beginShape( QUADS );
      texture( t );
      for ( int i = 0; i < faces.length; i++ ) {
        Face f = faces[ i ];
        vertex( f.v1.pos.x, f.v1.pos.y, f.v1.pos.z, f.v1.texcoord.x * tw, f.v1.texcoord.y * th );
        vertex( f.v2.pos.x, f.v2.pos.y, f.v2.pos.z, f.v2.texcoord.x * tw, f.v2.texcoord.y * th );
        vertex( f.v3.pos.x, f.v3.pos.y, f.v3.pos.z, f.v3.texcoord.x * tw, f.v3.texcoord.y * th );
        vertex( f.v4.pos.x, f.v4.pos.y, f.v4.pos.z, f.v4.texcoord.x * tw, f.v4.texcoord.y * th );
      }
      endShape();
      
    } else if ( type.equals( "CYLINDER" ) ) {
      
      int quads = faces.length / 3;
      PImage t = textures[ 0 ];
      float tw = t.width;
      float th = t.height;
      
      noStroke();
      beginShape( QUADS );
      texture( t );
      for ( int i = 0; i < quads; i++ ) {
        Face f = faces[ i ];
        vertex( f.v1.pos.x, f.v1.pos.y, f.v1.pos.z, f.v1.texcoord.x * tw, f.v1.texcoord.y * th );
        vertex( f.v2.pos.x, f.v2.pos.y, f.v2.pos.z, f.v2.texcoord.x * tw, f.v2.texcoord.y * th );
        vertex( f.v3.pos.x, f.v3.pos.y, f.v3.pos.z, f.v3.texcoord.x * tw, f.v3.texcoord.y * th );
        vertex( f.v4.pos.x, f.v4.pos.y, f.v4.pos.z, f.v4.texcoord.x * tw, f.v4.texcoord.y * th );
      }
      endShape();
      
      t = textures[ 1 ];
      tw = t.width;
      th = t.height;
      
      beginShape( TRIANGLES );
      texture( t );
      for ( int i = quads; i < faces.length; i++ ) {
        Face f = faces[ i ];
        if ( f == null || f.vnum != 3 ) {
          continue;
        }
        vertex( f.v1.pos.x, f.v1.pos.y, f.v1.pos.z, f.v1.texcoord.x * tw, f.v1.texcoord.y * th );
        vertex( f.v2.pos.x, f.v2.pos.y, f.v2.pos.z, f.v2.texcoord.x * tw, f.v2.texcoord.y * th );
        vertex( f.v3.pos.x, f.v3.pos.y, f.v3.pos.z, f.v3.texcoord.x * tw, f.v3.texcoord.y * th );
      }
      endShape();
      
    }
    
    popMatrix();
    
  }
  
  public void move( float x, float y, float z ) {
    _pos.set( x, y, z );
  }
  
  public void scale( float x, float y, float z ) {
    _scale.set( x, y, z );
  }
  
  public void setTexture( int i, PImage im ) {
    if ( i >= textures.length ) {
      return;
    }
    textures[ i ] = im;
  }

}
