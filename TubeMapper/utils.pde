public PImage generateTexture() {

  PImage g = createImage( 512, 512, RGB );
  int i = 0;
  for ( int y = 0; y < 512; y++ ) {
  for ( int x = 0; x < 512; x++ ) {
    g.pixels[i] = color( 255 * ( x / 512.f ), 255 * ( y / 512.f ), 0 );
    i++;
  }
  }
  return g;

}
