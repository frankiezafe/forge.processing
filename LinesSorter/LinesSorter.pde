ArrayList< Line > lines;
ArrayList< Line > lopti;
float stupid_distance;
float optimised_distance;

boolean drawopti;

void generateLine( Line origin, int depth ) {

  PVector newdir = origin.dir;
  newdir.x += random( -1.f, 1.f );
  newdir.y += random( -1.f, 1.f );
  newdir.mult( origin.len * random( 0.85f, 0.9f ) );
  
  if ( newdir.mag() > 1 && depth < 5 ) {
    int c = (int) random( 2, 4 );
    for ( int i = 0; i < c; i++ ) {
      Line cl = new Line( origin.x2, origin.y2, origin.x2 + newdir.x, origin.y2 + newdir.y );
      lines.add( cl );
      generateLine( cl, depth + 1 );
    }
  }

}

void setup() {

  drawopti = false;
  
  size( 600, 600 );
  lines = new ArrayList< Line >();
  ArrayList< Line > tmp = new ArrayList< Line >();
  for ( int i = 0; i < 5; i++ ) {
    float lx = random( 250,350 );
    float ly = random( 250,350 );
    Line l = new Line( lx, ly, lx + random( -30, 30 ), lx + random( -30, 30 ) );
    lines.add( l );
    generateLine( l, 0 );
  }
  
  for ( int i = 0; i < lines.size(); i++ )
    tmp.add( lines.get( i ) );
  
//  for ( int i = 0; i < 1000; i++ ) {
//    Line l = new Line( random( 10, 590 ), random( 10, 590 ), random( 10, 590 ), random( 10, 590 ) );
//    lines.add( l );
//    tmp.add( l );
//  }
  
  stupid_distance = 0;
  
  float lx = -1;
  float ly = -1;
  for ( int i = 0; i < lines.size(); i++ ) {
    Line l = lines.get( i );
    if ( lx != -1 ) {
      stupid_distance += dist( lx, ly, l.x1, l.y1 );
    }
    stupid_distance += dist( l.x1, l.y1, l.x2, l.y2 );
    lx = l.x2;
    ly = l.y2;
  }
  
  lopti = new ArrayList< Line >();
  lopti.add( tmp.get( 0 ) );
  tmp.remove( 0 );
  while( tmp.size() > 0 ) {
  
    float lastx = tmp.get( tmp.size() - 1 ).x2;
    float lasty = tmp.get( tmp.size() - 1 ).y2;
    int sel = 0;
    float closest = width * height;
    for ( int i = 0; i < tmp.size(); i++ ) {
      Line l = tmp.get( i );
      float d = dist( l.x1, l.y1, lastx, lasty );
      if ( d < closest ) {
        closest = d;
        sel = i;
      }
    }
    
    Line l = tmp.get( sel );
    tmp.remove( sel );
    lopti.add( l );
  
  }
  
  optimised_distance = 0;
  lx = -1;
  ly = -1;
  for ( int i = 0; i < lopti.size(); i++ ) {
    Line l = lopti.get( i );
    if ( lx != -1 ) {
      optimised_distance += dist( lx, ly, l.x1, l.y1 );
    }
    optimised_distance += dist( l.x1, l.y1, l.x2, l.y2 );
    lx = l.x2;
    ly = l.y2;
  }
  

}

void draw() {

  background( 255 );
  
  float lx = -1;
  float ly = -1;
  
  if ( !drawopti ) {
    for ( int i = 0; i < lines.size(); i++ ) {
      Line l = lines.get( i );
      stroke( 255, 0, 0 );
      if ( lx != -1 ) {
        line( lx, ly, l.x1, l.y1 );
      }
      stroke( 0 );
      l.draw();
      lx = l.x2;
      ly = l.y2;
    }
  } else {
    for ( int i = 0; i < lopti.size(); i++ ) {
      Line l = lopti.get( i );
      stroke( 255, 0, 0 );
      if ( lx != -1 ) {
        line( lx, ly, l.x1, l.y1 );
      }
      stroke( 0 );
      l.draw();
      lx = l.x2;
      ly = l.y2;
    }
  }
  
  noStroke();
  fill( 255, 200 );
  rect( 0, 0, width, 100 );
  
  fill( 10 );
  text( "stupid = " + stupid_distance, 15, 30 );
  text( "optimised = " + optimised_distance, 15, 50 );
  text( "diff = " + ( stupid_distance - optimised_distance  ), 15, 70 );
  text( "draw opti = " + drawopti, 15, 90 );

}


class Line {

  float x1, y1, x2, y2;
  float len;
  PVector dir;
  
  public Line( float x1, float y1, float x2, float y2 ) {
  
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
    
    dir = new PVector( x2 - x1, y2 - y1 );
    len = dir.mag();
    dir.normalize();
  
  }

  public void draw() {
    
    line( x1, y1, x2, y2 );
    
  }

}

void keyPressed() {

  if ( key == 'o' ){
    drawopti = !drawopti;
  } 

}
