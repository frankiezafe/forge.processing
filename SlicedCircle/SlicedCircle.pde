// src: https://ps.zoethical.com/uploads/default/original/1X/10dcd3be4dbf954f94e6e39c634cb683f1439ada.png

PVector pt1_init, pt2_init;
PVector pt1, pt2;

int cpoint_cout;
CirclePoint[] cpoints;

int slice_count;
Slice[] slices;
Slice selected;

void setup() {

  size( 800, 600 );

  pt1 = new PVector( 10, 30 );
  pt2 = new PVector( -30, -10 );
  // placing points at center
  pt1.add( new PVector( CirclePoint.CENTER_X, CirclePoint.CENTER_Y ) );
  pt2.add( new PVector( CirclePoint.CENTER_X, CirclePoint.CENTER_Y ) );
  
  pt1_init = new PVector();
  pt2_init = new PVector();
  pt1_init.set( pt1 );
  pt2_init.set( pt2 );
  
  int highlight = color( 255 );
  
  cpoint_cout = 4;
  cpoints = new CirclePoint[cpoint_cout];
  cpoints[ 0 ] = new CirclePoint( -PI / 6 );
  cpoints[ 1 ] = new CirclePoint( PI * 3.f / 8 );
  cpoints[ 2 ] = new CirclePoint( PI * 5.f / 8 );
  cpoints[ 3 ] = new CirclePoint( PI * 7.5f / 8 );
  
  slice_count = 4;
  slices = new Slice[slice_count];
  slices[0] = new Slice( cpoints[ 0 ], pt2, pt1, cpoints[ 1 ], color( 255,0,0,170 ), color( 0,255,255) );
  slices[1] = new Slice( cpoints[ 1 ], pt1, pt1, cpoints[ 2 ], color( 0,255,0,170 ), color( 255,0,255) );
  slices[2] = new Slice( cpoints[ 2 ], pt1, pt2, cpoints[ 3 ], color( 0,0,255,170 ), color( 255,255,0) );
  slices[3] = new Slice( cpoints[ 3 ], pt2, pt2, cpoints[ 0 ], color( 255,255,0,170 ), color( 0,0,255) );
  
  selected = null;
  
}

void draw() {

  background( 80 );

  // moving central points
  pt1.x = pt1_init.x + 10 * cos( frameCount * 0.036998 );
  pt1.y = pt1_init.y + 10 * sin( frameCount * 0.036998 );
  pt2.x = pt2_init.x + 5 * cos( frameCount * 0.01 );
  pt2.y = pt2_init.x + 5 * sin( frameCount * 0.01 );
  
  // moving peripheric points
  for( int i = 0; i < cpoint_cout; ++i ) {
    cpoints[ i ].set( cpoints[ i ].init_radian + sin( frameCount * 0.012214 + i * 0.08 ) * 0.1 );
  }
  
  // drawing info
  int text_y = 400;
  fill( 255 );
  stroke( 255 );
  text( "center point 1", 550, text_y );
  line( 550, text_y, pt1.x, pt1.y );
  text_y += 18;
  text( "center point 2", 550, text_y );
  line( 550, text_y, pt2.x, pt2.y );
  text_y += 18;
  for( int i = 0; i < cpoint_cout; i++ ) {
    text( "cpoint " + i + ", rad = " + cpoints[ i ].radian, 550, text_y );
    line(
      550, text_y,
      cpoints[ i ].position.x, cpoints[ i ].position.y
      );
    text_y += 18;
  }
  
  // drawing slices
  for( int i = 0; i < slice_count; ++i ) {
    slices[i].draw();
  }

  stroke( 255 );
  line( CirclePoint.CENTER_X - 5, CirclePoint.CENTER_Y - 0, CirclePoint.CENTER_X + 5, CirclePoint.CENTER_Y + 0 );
  line( CirclePoint.CENTER_X - 0, CirclePoint.CENTER_Y - 5, CirclePoint.CENTER_X + 0, CirclePoint.CENTER_Y + 5 );
  noFill();
  //ellipse( CirclePoint.CENTER_X, CirclePoint.CENTER_Y, CirclePoint.RADIUS*2, CirclePoint.RADIUS*2 );

}

void mouseMoved() {
  
  for( int i = 0; i < slice_count; ++i ) {
    slices[i].hit( mouseX, mouseY );
  }
  
} 

class CirclePoint {
  
  public static final float RADIUS = 200;
  public static final float CENTER_X = 300;
  public static final float CENTER_Y = 300;
  
  public PVector position;
  public float init_radian;
  public float radian;
  
  public CirclePoint( float rad ) { // enter values between 0 and 2 PI, in radian
    init_radian = rad;
    set( init_radian );
  }
  
  public void set( float rad ) { // enter values between 0 and 2 PI, in radian
    radian = rad;
    position = new PVector(
      CENTER_X + cos( radian ) * RADIUS, 
      CENTER_Y + sin( radian ) * RADIUS
    );
  }
  
}

class Slice {

  public static final int CIRCLE_DEFINITION = 120; // one cord every 3°, or 0,026179939 rad

  public CirclePoint pt_in;
  public PVector pt_center_in;
  public PVector pt_center_out;
  public CirclePoint pt_out;
  
  public int rgba;
  public int highlight_rgba;
  public boolean highlighted;

  public Slice( 
    CirclePoint circle_in, 
    PVector central_in, 
    PVector central_out, 
    CirclePoint circle_out, 
    int rgba, int highlight_rgba
    ) {
    this.rgba = rgba;
    this.highlight_rgba = highlight_rgba;
    highlighted = false;
    pt_in = circle_in;
    pt_center_in = central_in;
    pt_center_out = central_out;
    pt_out = circle_out;
    
  }

  public void draw() {

    noStroke();
    if ( highlighted )
      fill(highlight_rgba);
    else
      fill(rgba);
      
    float rin = pt_in.radian;
    float rout = pt_out.radian;
    if ( rout < rin ) {
      rout += TWO_PI;
    }
      
    beginShape();
    vertex(pt_out.position.x, pt_out.position.y);
    vertex(pt_center_out.x, pt_center_out.y);
    vertex(pt_center_in.x, pt_center_in.y);
    vertex(pt_in.position.x, pt_in.position.y);
    float def = PI / CIRCLE_DEFINITION;
    float rad = rin;
    while ( rad < rout ) {
      rad += def;
      if ( rad >= rout ) {
        rad = rout;
      }
      vertex( 
        CirclePoint.CENTER_X + cos( rad ) * CirclePoint.RADIUS, 
        CirclePoint.CENTER_Y + sin( rad ) * CirclePoint.RADIUS );
    }
    endShape(CLOSE);
    
    //stroke( 255 );
    //PVector vin = new PVector( pt_in.x - pt_center_in.x, pt_in.y - pt_center_in.y );
    //vin.normalize();
    //PVector vin_normal = new PVector( vin.y, -vin.x );
    //line( pt_in.x, pt_in.y, pt_in.x + vin_normal.x * 50, pt_in.y + vin_normal.y * 50 );
    
  }
  
  public boolean hit( float x, float y ) {
    
    highlighted = false;
    
    if ( dist( x, y, CirclePoint.CENTER_X, CirclePoint.CENTER_Y ) > CirclePoint.RADIUS ) {
      return highlighted;
    }
    
    PVector vin = new PVector( 
      pt_in.position.x - pt_center_in.x, 
      pt_in.position.y - pt_center_in.y );
    vin.normalize();
    PVector vin_normal = new PVector( vin.y, -vin.x );
    PVector vout = new PVector( 
      pt_out.position.x - pt_center_in.x, 
      pt_out.position.y - pt_center_in.y );
    vout.normalize();
    PVector vout_normal = new PVector( vout.y, -vout.x );
    PVector vhit = new PVector( 
      x - ( pt_center_in.x + ( pt_center_out.x - pt_center_in.x ) * 0.5 ), 
      y - ( pt_center_in.y + ( pt_center_out.y - pt_center_in.y ) * 0.5 )
      );
    vhit.normalize();
    
    float rin = pt_in.radian;
    float rout = pt_out.radian;
    if ( rout < rin ) {
      rout += TWO_PI;
    }
    
    if ( pt_center_in == pt_center_out ) {
      
      if ( 
        rout - rin < PI &&
        vin_normal.dot( vhit ) <= 0 &&
        vout_normal.dot( vhit ) > 0
        ) {
          
        highlighted = true;
        
      } else if (  
        rout - rin > PI &&
        (
        vin_normal.dot( vhit ) <= 0 ||
        vout_normal.dot( vhit ) > 0
        )
        ) {
          
        highlighted = true;
        
      }
      
    } else {
    
      // one more!
      PVector vinout = new PVector( pt_center_out.x - pt_center_in.x, pt_center_out.y - pt_center_in.y );
      vinout.normalize();
      PVector vinout_normal = new PVector( vinout.y, -vinout.x );
      boolean inout_ok = vinout_normal.dot( vhit ) > 0;
      
      PVector vhit_in = new PVector( x - pt_center_in.x, y - pt_center_in.y );
      PVector vhit_out = new PVector( x - pt_center_out.x, y - pt_center_out.y );
      
      if ( 
        rout - rin < PI &&
        inout_ok &&
        vin_normal.dot( vhit_in ) <= 0 &&
        vout_normal.dot( vhit_out ) > 0
        ) {
        highlighted = true;
      }
      
    }
    
    return highlighted;
    //if ( pt_center_in == pt_center_out ) {
    //  PVector 
    //}
  }
  
}