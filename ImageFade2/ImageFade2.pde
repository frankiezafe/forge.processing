int im1[];
int im2[];
float energy[][];
int index;
int lastIndex;
int range = 1;

boolean mpressed;

boolean drawe = true;

void setup() {

  size( 400, 400, P3D );

  PImage i1 = loadImage( "img1.png" );
  i1.loadPixels();
  im1 = new int[ width * height ];
  for ( int i = 0; i < width * height; i++ )
    im1[ i ] = i1.pixels[ i ];

  PImage i2 = loadImage( "img2.png" );
  i2.loadPixels();
  im2 = new int[ width * height ];
  for ( int i = 0; i < width * height; i++ )
    im2[ i ] = i2.pixels[ i ];

  energy = new float[ width * height ][ 2 ];
  for ( int i = 0; i < width * height; i++ ) {
    energy[ i ][ 0 ] = 0;
    energy[ i ][ 1 ] = 0;
  }

  index = 0;
  lastIndex = 1;

  mpressed = false;
}

void draw() {


  int tmpi = index;
  index = lastIndex;
  lastIndex = tmpi;

  if ( mpressed ) {
    
    int p = mouseX + mouseY * width;
    for ( int y = mouseY - 1; y <= mouseY + 1; y++ ) {
    for ( int x = mouseX - 1; x <= mouseX + 1; x++ ) {
      if ( x < 0 || x >= width || y < 0 || y >= height )
        continue;
        p = x + y * width;
        energy[ p ][ lastIndex ] = 1;
    }
    }
    
    mpressed = false;
  }

  loadPixels();

  for ( int y = 0; y < height; y++ ) {
    for ( int x = 0; x < width; x++ ) {

      int i = x + y * width;

      int nbrs = 0;
      float totale = 0;

      for ( int ny = y - range; ny <= y + range; ny++ ) {
        for ( int nx = x - range; nx <= x + range; nx++ ) {
          if ( nx == x && ny == y )
            continue;
          if ( nx < 0 || ny < 0 || nx >= width || ny >= height )
            continue;
          int ni = nx + ny * width;
          nbrs++;
          totale += energy[ ni ][ lastIndex ];
          // surroundingEs.add( energy[ ni ][ lastIndex ] );
        }
      }

      energy[ i ][ index ] = energy[ i ][ lastIndex ] * 0.84f + ( totale / nbrs ) * 0.15f;
      if ( energy[ i ][ index ] > 1 ) {
        energy[ i ][ index ] = 1;
      } else if ( energy[ i ][ index ] < 0.0001f ) {
        energy[ i ][ index ] = 0;
      }
      
      if ( drawe ) {
        if ( energy[ i ][ index ] == 0 ) {
          pixels[ i ] = color( 0,0,255 );
        } else {
          pixels[ i ] = color( 15 + energy[ i ][ index ] * 240 );
        }
      }
    }
  }

  updatePixels();
}

void mousePressed() {
  mpressed = true;
}

void mouseDragged() {
  mpressed = true;
}

