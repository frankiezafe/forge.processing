class DraggableTri { //<>//

  private DraggablePoint points[];
  private DraggableSegment segments[];
  private DraggablePoint _center;

  private ArrayList<PVector> contact_points;

  public DraggableTri( float x0, float y0, float x1, float y1, float x2, float y2 ) {
    points = new DraggablePoint[] {
      new DraggablePoint( x0, y0 ), 
      new DraggablePoint( x1, y1 ), 
      new DraggablePoint( x2, y2 )
    };
    init_segments();
    _center = new DraggablePoint(0, 0);
    contact_points = new ArrayList<PVector>();
    render();
  }

  private void init_segments() {
    segments = new DraggableSegment[] {
      new DraggableSegment( points[0], points[1] ), 
      new DraggableSegment( points[1], points[2] ), 
      new DraggableSegment( points[2], points[0] )
    };
  }

  public PVector point( int i ) {
    if ( i < 0 || i > 2 ) {
      return null;
    }
    return points[i].copy();
  }
  
  public void set_point( int i, PVector src ) {
    if ( i < 0 || i > 2 ) {
      return;
    }
    points[i].set( src );
  }
  
  public DraggableSegment segment( int i ) {
    if ( i < 0 || i > 2 ) {
      return null;
    }
    return segments[i].copy();
  }

  public PVector center() {
    PVector out = new PVector();
    for ( int i = 0; i < 3; ++i ) {
      out.add( segments[i].point(0).add( segments[i].dir().mult( segments[i].len() * 0.5 ) ) );
    }
    return out.mult(1.f/3);
  } 
  
  public ArrayList<PVector> contacts() {
    return contact_points;
  }

  public void render() {

    _center.set( center() );

    for ( int i = 0; i < 3; ++i ) {
      segments[i].update();
    }

    // checking if triangle is inward, implying a normals flip
    PVector p = new PVector( _center.x - points[0].x, _center.y - points[0].y ).normalize();
    if ( segments[0].norm().dot( p ) > 0 ) {
      // inverting 2 points and computation of segments
      DraggablePoint tmp = points[0];
      points[0] = points[1];
      points[1] = tmp;
      init_segments();
    }
    
  }

  public DraggablePoint touch( float x, float y ) {
    DraggablePoint t = null;
    for ( int i = 0; i < 3; ++i ) {
      if ( points[i].touch( x, y ) && t == null ) {
        t = points[i];
      }
    }
    if ( t == null && _center.touch( x, y ) ) {
      return _center;
    }
    return t;
  }

  public void update( DraggablePoint dp ) {
    if ( dp == _center ) {
      PVector off = center();
      off.x -= _center.x;
      off.y -= _center.y;
      for ( int i = 0; i < 3; ++i ) {
        points[i].x -= off.x;
        points[i].y -= off.y;
      }
    }
    render();
  }

  public void draw() {
    draw(true);
  }

  public void draw( boolean debug ) {

    if (debug) {
      
      for ( int i = 0; i < 3; ++i ) {
        points[i].draw( DP_DRAW_CIRCLE );
      }
      _center.draw( DP_DRAW_CROSS );
    
      int vi = 0;
      for ( PVector v : contact_points ) {
        noStroke();
        fill( 255, 0, 0, 255 );
        ellipse( v.x, v.y, 4, 4 );
        fill(255);
        text( vi++, v.x - 5, v.y + 15 + 5 );
      }
      
    }
    
    noFill();
    
    for ( int i = 0; i < 3; ++i ) {
      stroke(255, 150);
      segments[i].draw(debug);
      // normal
      if ( debug ) {
        stroke(255, 0, 255, 150);
        PVector mid = segments[i].dir().mult( segments[i].len() / 2 );
        PVector norm = segments[i].norm().mult( DP_MAX_DIST );
        line( 
          segments[i].point(0).x + mid.x, 
          segments[i].point(0).y + mid.y, 
          segments[i].point(0).x + mid.x + norm.x, 
          segments[i].point(0).y + mid.y + norm.y
          );
        text( ""+i, points[i].x + DP_MAX_DIST, points[i].y + 4 );
      }
    }

  }

  public boolean inside( PVector v ) {
    for ( int i = 0; i < 3; ++i ) {
      PVector diff = segments[i].point(0).mult( -1 );
      diff.add( v );
      if ( segments[i].norm().dot( diff ) > 0 ) {
        return false;
      }
    }
    return true;
  }

  public void intersect( DraggableTri other ) {

    contact_points.clear();
    boolean all_in;
    boolean local_in[] = new boolean[]{ false,false,false };
    boolean foreign_in[] = new boolean[]{ false,false,false };

    // all points of other inside this triangle?
    all_in = true;
    for ( int i = 0; i < 3; ++i ) {
      foreign_in[ i ] = inside( other.point(i) );
      if ( all_in && !foreign_in[ i ] ) {
        all_in  = false;
      }
    }
    if ( all_in ) {
      for ( int i = 0; i < 3; ++i ) {
        contact_points.add( other.point(i) );
      }
      return;
    }
    
    // all points of this traingle inside other?
    all_in = true;
    for ( int i = 0; i < 3; ++i ) {
      local_in[ i ] = other.inside( point(i) );
      if ( all_in && !local_in[ i ] ) {
        all_in  = false;
      }
    }
    if ( all_in ) {
      for ( int i = 0; i < 3; ++i ) {
        contact_points.add( point(i) );
      } 
      return;
    }

    // ok... triangles are not include one in another
    // let's see if they cross each other
    
    for ( int i = 0; i < 3; ++i ) {
      
      // adding the local point at i if it is inside the other triangle
      if ( local_in[i] ) {
        contact_points.add( point(i) );
      }
      
      // getting the current segment to test
      DraggableSegment local = segments[i];
      
      // here is the tricky part:
      // there are maximum 2 intersection per segment (0,1 or 2)
      // but these intersection might not appear in clockwise order!
      // therefore, we store them in a temporary array: local_intersect
      // we process all the segments of the other triangle (line 253:275)
      // if there are 2 intersections, we have to test their distance to 
      // the head of the current segment BEFORE adding them to the contact_points array
      // if the first seen intersection is further then the second
      // local_intersect subarray 0 and 1 are swapped (line 282:293)
      // and only then points are added to contact_points array
      // this process ensure that all contact_points are stored in clockwise order,
      // making it super easy to render the intersection surface
      
      int intersect_num = 0;
      // intersection ids: 0 = point before intersection, 1 = intersection, 2 = point after intersection
      PVector local_intersect[][] = new PVector[][]{{null,null,null},{null,null,null}};
        
      for ( int j = 0; j < 3; ++j ) {
        
        DraggableSegment foreign = other.segment(j);
        PVector intersect = local.intersect( foreign, false );
        
        if ( intersect != null ) {
          if ( foreign_in[ j ] ) {
            // storing position of the point before
            local_intersect[intersect_num][0] = other.point(j);
            // avoiding point to be added again
            foreign_in[ j ] = false;
          }
          local_intersect[intersect_num][1] = intersect;
          int h = ( j + 1 ) % 3;
          if ( foreign_in[ h ] ) {
            // storing position of the point after
            local_intersect[intersect_num][2] = other.point(h);
            // avoiding point to be added again
            foreign_in[ h ] = false;
          }
          intersect_num++;
        }
      
      }
      
      if ( intersect_num == 2 ) {
        // checking distances between intersection, and
        // swapping them if required
        float d0 = point(i).dist( local_intersect[0][1] );
        float d1 = point(i).dist( local_intersect[1][1] );
        if ( d0 > d1 ) {
          // swapping results
          PVector tmp[] = local_intersect[0];
          local_intersect[0] = local_intersect[1];
          local_intersect[1] = tmp;
        }
      }
      
      for ( int j = 0; j < intersect_num; ++j ) {
        for ( int h = 0; h < 3; ++h ) {
          if ( local_intersect[j][h] != null ) {
            contact_points.add( local_intersect[j][h] );
          }
        }
      }
      
    }
  }
}
