class DraggableSegment {
  
  private DraggablePoint A;
  private DraggablePoint B;
  private PVector _dir;
  private PVector _norm;
  private float _len;
  
  public DraggableSegment( float x0, float y0, float x1, float y1 ) {
    A = new DraggablePoint( x0, y0 );
    B = new DraggablePoint( x1, y1 );
    update();
  }
  
  public DraggableSegment( DraggablePoint dp0, DraggablePoint dp1 ) {
    A = dp0;
    B = dp1;
    update();
  }
  
  public DraggableSegment copy() {
    return new DraggableSegment( A, B );
  }
  
  public PVector point(int i) {
    if ( i < 0 || i > 1 ) { return null; }
    else if ( i == 0 ) { return A.copy(); }
    else { return B.copy(); }
  }
  
  public PVector dir() {
    return _dir.copy();
  }
  
  public PVector norm() {
    return _norm.copy();
  }
  
  public float len() {
    return _len;
  }
  
  public void update() {

    _dir = new PVector( B.x - A.x, B.y - A.y );
    _len = _dir.mag();
    _dir.normalize();
    // clockwise normal!!!!
    _norm = new PVector( _dir.y, -_dir.x );
    
  }
  
  public void draw() {
    draw(true);
  }
  
  public void draw( boolean debug ) {
    
    if ( debug ) {
      A.draw( DP_DRAW_CIRCLE );
      B.draw( DP_DRAW_CIRCLE );
    }
    
    strokeWeight( 1 );
    stroke( 255, 140 );
    line( A.x, A.y, B.x, B.y );
    
    if ( debug ) {
      strokeWeight( 3 );
      line( A.x, A.y, A.x + _dir.x * DP_MAX_DIST, A.y + _dir.y * DP_MAX_DIST );
      strokeWeight( 1 );
    }
    
  }
  
  public DraggablePoint touch( float x, float y ) {
    DraggablePoint t = null;
    if ( A.touch(x,y) ) { t = A; }
    else if ( B.touch(x,y) ) { t = B; }
    return t;
  }
  
  
  public PVector intersect( DraggableSegment dl ) {
    return intersect( dl, true );
  }
  
  public PVector intersect( DraggableSegment dl, boolean debug ) {
    
    // no length => no segment => no intersection
    if (_len == 0) {
      return null;
    }
    
    // head of this segment to head and tail of the other segment, normalized depending on this segment length
    PVector toA = new PVector( dl.point(0).x - A.x, dl.point(0).y - A.y ).mult( 1 / _len );
    PVector toB = new PVector( dl.point(1).x - A.x, dl.point(1).y - A.y ).mult( 1 / _len );
    
    // projection of the 2 vectors onto the _direction
    float dot0 = _dir.dot( toA );
    float dot1 = _dir.dot( toB );
    
    // rendering of perpendicular vectors to this segment
    PVector perpA = new PVector( dl.point(0).x - (A.x + _dir.x * dot0 * _len), dl.point(0).y - (A.y + _dir.y * dot0 * _len) );
    PVector perpB = new PVector( dl.point(1).x - (A.x + _dir.x * dot1 * _len), dl.point(1).y - (A.y + _dir.y * dot1 * _len) );
    
    if (debug) {
      
      line( 
        A.x + _dir.x * dot0 * _len + perpA.x, 
        A.y + _dir.y * dot0 * _len + perpA.y,  
        A.x + _dir.x * dot0 * _len, 
        A.y + _dir.y * dot0 * _len );
      line( 
        A.x + _dir.x * dot1 * _len + perpB.x, 
        A.y + _dir.y * dot1 * _len + perpB.y,  
        A.x + _dir.x * dot1 * _len, 
        A.y + _dir.y * dot1 * _len );
      
    }
    
    // pointting the length of the perpendicular vectors to compute the ratio of dots
    // based on the fact that in rect triangle having same angles, sides are proportional
    //
    //         /| <- perpA
    //        / |
    //       /  |
    //      /|  | <-- perpB
    //     / |  |
    //    /__|__|
    //   
    //  dot difference ratio
    
    float distA = perpA.mag();
    float distB = perpB.mag();
    float middot = dot0 + ( dot1 - dot0 ) * ( distA / (distA+distB) );
    
    // this test is valid in 2D! 
    // for 3D, perpendicular vectors HAVE to be perfectly opposite, meaning belonging to the same line
    // therefore there are infinitively less chance to have crossing segments in 3D then in 2D
    // to compute, normalise both perpendicular vectors, then dot product MUST be equal to -1
    if ( middot < 0 || middot > 1 || perpA.dot( perpB ) > 0 ) {
      return null;
    }
    
    return new PVector( A.x + _dir.x * middot * _len, A.y + _dir.y * middot * _len );
    
  }

}
