
DraggableTri tris[];
DraggableTri dragged_tri;
DraggablePoint dragged_pt;

PVector[][] speeds;

void setup() {
  
  size(600,600);
  
  tris = new DraggableTri[]{
    new DraggableTri( 
      300 + cos(0) * 200, 300 + sin(0) * 200, 
      300 + cos(PI * 2 /3) * 200, 300 + sin(PI * 2 /3) * 200,
      300 + cos(PI * 4 /3) * 200, 300 + sin(PI * 4 /3) * 200 ),
    new DraggableTri( 
      300 + cos(PI * 1 /3) * 200, 300 + sin(PI * 1 /3) * 200, 
      300 + cos(PI * 3 /3) * 200, 300 + sin(PI * 3 /3) * 200,
      300 + cos(PI * 5 /3) * 200, 300 + sin(PI * 5 /3) * 200 )
  };
  dragged_tri = null;
  dragged_pt = null;
  speeds = null;
  
}

void draw() {
  
  if ( speeds != null ) {
    float speed_mult = 2;
    for ( int i = 0; i < tris.length; ++i ) {
      for ( int j = 0; j < 3; ++j ) {
        PVector s = speeds[ i * 3 + j ][1];
        PVector p = speeds[ i * 3 + j ][0];
        if ( p.x + s.x * speed_mult < 0 || p.x + s.x * speed_mult > width ) { s.x *= -1; }
        if ( p.y + s.y * speed_mult < 0 || p.y + s.y * speed_mult > height ) { s.y *= -1; }
        tris[i].set_point( j, p.add( s.x * speed_mult, s.y * speed_mult ) );
      }
      tris[i].render();
    }
  }

  background(50);
  tris[0].intersect( tris[1] );

  ArrayList<PVector> cps = tris[0].contacts();
  int cpnum = cps.size();
  if ( cpnum > 2 ) {
    
    if ( speeds != null ) {
      
       stroke(255);
       fill( 0,255,200, 180 );
       beginShape();
       for ( PVector v : cps ) {
         vertex( v.x, v.y );
       }
       endShape(CLOSE);
       
    } else {
      
      colorMode( HSB, 100 );
      int h = 10;
      PVector start = cps.get(0);
      int next = 2;
      while( next < cpnum ) {
        stroke( h, 100, 100 );
        fill( h, 100, 100, 40 );
        beginShape();
        vertex( start.x, start.y );
        for ( int i = 0; i < 2; ++i ) {
          PVector v = cps.get(next - i);
          vertex( v.x, v.y );
        }
        ++next;
        h += 10;
        endShape(CLOSE);
      }
      colorMode( RGB, 255 );
      
    }
    
  }
  
  for ( int i = 0; i < tris.length; ++i ) {
    tris[i].draw( speeds == null );
  }
  
}

DraggablePoint touched() {
  DraggablePoint t = null;
  for ( int i = 0; i < tris.length; ++i ) {
    DraggablePoint touched = tris[i].touch( mouseX, mouseY );
    if ( touched != null && t == null ) {
      dragged_tri = tris[i];
      t = touched;
    }
  }
  return t;
}

void mouseMoved() {
  if ( dragged_pt == null ) {
    touched();
  }
}

void mouseDragged() {
  if ( dragged_pt != null ) {
    dragged_pt.drag( mouseX, mouseY );
    dragged_tri.update( dragged_pt );
  }
}

void mousePressed() {
  if ( speeds != null ) { return; }
  dragged_pt = touched();
}

void mouseReleased() {
  dragged_tri = null;
  dragged_pt = null;
}

void keyPressed() {
  
  switch( key ) {
    case ' ':
      if ( speeds == null ) {
        
        speeds = new PVector[ tris.length * 3 ][2];
        for ( int i = 0; i < tris.length; ++i ) {
          for ( int j = 0; j < 3; ++j ) {
            speeds[ i * 3 + j ] = new PVector[]{ 
              tris[i].point(j),
              new PVector( random(-1,1),random(-1,1) ).normalize()
            };
            speeds[ i * 3 + j ][1].add( tris[i].segment(j).norm() );
            speeds[ i * 3 + j ][1].add( tris[i].segment((j+2)%3).norm() );
            speeds[ i * 3 + j ][1].normalize();
          }
        }
        mouseReleased();
        
      } else {
        speeds = null;
      }
      break;
    default:
      break;
  }
  
}
