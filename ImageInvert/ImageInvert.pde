float im1[][];
float cmx;
float cmy;
boolean mpressed;
boolean mreleased;
float mradius;

void setup() {

  size( 400, 400, P3D );

  PImage i1 = loadImage( "img2.png" );
  i1.loadPixels();
  im1 = new float[ width * height ][ 3 ];
  for ( int i = 0; i < width * height; i++ ) {
    im1[ i ][ 0 ] = red( i1.pixels[ i ] );
    im1[ i ][ 1 ] = green( i1.pixels[ i ] );
    im1[ i ][ 2 ] = blue( i1.pixels[ i ] );
  }
  
  mpressed = false;
  mreleased = false;
  
}

void draw() {

  if ( mreleased ) {
    
    for ( int y = 0; y < height; y++ ) {
    for ( int x = 0; x < height; x++ ) {  
       int i = x + y * width;
       if ( dist( x, y, cmx, cmy ) <= mradius ) {
         im1[ i ][ 0 ] = 255 - im1[ i ][ 0 ];
         im1[ i ][ 1 ] = 255 - im1[ i ][ 1 ];
         im1[ i ][ 2 ] = 255 - im1[ i ][ 2 ];
       }
      
    }
    }
    
    mreleased = false;
    
  }
  
  loadPixels();
  for ( int i = 0; i < width * height; i++ ) {
    pixels[ i ] = color(
              im1[ i ][ 0 ],
              im1[ i ][ 1 ],
              im1[ i ][ 2 ] );
  }
  updatePixels();
  
  if ( mpressed ) {
    noStroke();
    fill( 255,0,0 );
    ellipse( cmx, cmy, 5, 5 );
    noFill();
    stroke( 255, 0, 0 );
    ellipse( cmx, cmy, mradius * 2, mradius * 2 );
  }
  
}

void keyPressed() {
}

void mousePressed() {
  mpressed = true;
  cmx = mouseX;
  cmy = mouseY;
  mradius = 0;
}

void mouseReleased() {
  mpressed = false;
  mreleased = true;
}

void mouseDragged() {
  mradius = dist( cmx, cmy, mouseX, mouseY );
  mpressed = true;
}

