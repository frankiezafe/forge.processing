/* OpenProcessing Tweak of *@*http://www.openprocessing.org/sketch/26543*@* */
/* !do not delete the line above, required for linking your tweak if you re-upload */
public static int MAP_AB = 0;
public static int MAP_AC = 1;
public static int MAP_BC = 2;

Node[] nodes;
PGraphics rendering;

void setup () {
  
  size (600, 600, P2D);
  rendering = createGraphics(width, height, P2D);
  rendering.beginDraw();
  rendering.background(100);
  rendering.endDraw();
  
  background (0);
  noFill();
  smooth();
  
  ellipseMode (CENTER);

  nodes = new Node[3];
  for (int i = 0; i < nodes.length; i++) {
    nodes[i] = new Node((float)Math.random()*width, (float)Math.random()*height);
  }

  setupNodes();
}

void setupNodes () {
  
  background(0);  
  
  for (Node n : nodes)
    n.move();
    
  // uncomment to try mapping switch
  // nodes[0].y = nodes[1].y;
  // nodes[0].y = nodes[2].y;
  
  float rad = 0;
  Node CCC = getCCC (nodes[0], nodes[1], nodes[2]);
  if ( CCC != null ) {
    rendering.beginDraw();
    rendering.noFill();
    rendering.smooth();
    rendering.strokeWeight( 1.5f );
    rendering.stroke ( 100 + random( 155 ), 100 + random( 155 ), 100 + random( 155 ), 255 );
    rad = sqrt ((CCC.x - nodes[0].x)*(CCC.x - nodes[0].x) + (CCC.y - nodes[0].y)*(CCC.y - nodes[0].y));
    if ( rad > pow( 10,4 ) ) {
      CCC = null;
      System.out.println("far too much for processing: " + rad);
    } else {
      try {
        rendering.ellipse (CCC.x, CCC.y, rad*2, rad*2);
      } catch ( Exception e ) {
        System.out.println("a bit too much for processing: " + rad);
      }
    }
    rendering.endDraw();
  }
  
  stroke(255);
  fill(255);
  image(rendering, 0,0 );
  
  for (Node n : nodes) {
    stroke(255);
    fill( 0 );
    ellipse (n.x, n.y, 10, 10);
    noFill();
  }
  if ( CCC != null ) {
    try {
      ellipse (CCC.x, CCC.y, rad*2, rad*2);
    } catch ( Exception e ) {}
  }
  
  
}

void draw () {
  setupNodes();
};

void getSlopes( int mm, Slopes ss, Node n1, Node n2, Node n3 ) {
  // possible pairs:
  // A: 1 - 2 / B: 1 - 3
  // A: 2 - 1 / C: 2 - 3
  // B: 3 - 1 / C: 3 - 2
  if ( mm == MAP_AB ) {
    ss.s0 = -1/((n2.y-n1.y)/(n2.x-n1.x));
    ss.s1 = -1/((n3.y-n1.y)/(n3.x-n1.x));
  } else if ( mm == MAP_AC ) {
    ss.s0 = -1/((n2.y-n1.y)/(n2.x-n1.x));
    ss.s1 = -1/((n3.y-n2.y)/(n3.x-n2.x));
  } else if ( mm == MAP_BC ) {
    ss.s0 = -1/((n3.y-n1.y)/(n3.x-n1.x));
    ss.s1 = -1/((n3.y-n2.y)/(n3.x-n2.x));
  }
}

Node getCCC (Node n1, Node n2, Node n3) {
  
  /*This process fails when two of the nodes share the same 'x' due to step two*/
  // testing mappings
  int mapping = MAP_AB;
  Slopes ss = new Slopes( 0,0 );
  getSlopes( mapping, ss, n1, n2, n3 );
  if ( Float.isInfinite( ss.s0 ) && Float.isInfinite( ss.s1 ) ) {
    // no solution...
    System.out.println( "NO SOLUTIONS!" );
    return null;
  } else if ( Float.isInfinite( ss.s0 ) ) {
    System.out.println( "Switching to mapping BC" );
    mapping = MAP_BC;
  } else if ( Float.isInfinite( ss.s1 ) ) {
    System.out.println( "Switching to mapping AC" );
    mapping = MAP_AC;
  }
  if ( mapping != MAP_AB ) {
    getSlopes( mapping, ss, n1, n2, n3 );
    if ( Float.isInfinite( ss.s0 ) || Float.isInfinite( ss.s1 ) ) {
      System.out.println( "NO SOLUTIONS!" );
      return null;
    }
  }
  
  Node wn0, wn1, wn2;
  // nodes depend on mapping
  wn0 = n1;
  wn1 = n2;
  wn2 = n3;
  if ( mapping == MAP_AC ) {
    wn0 = n2;
    wn1 = n1;
    wn2 = n3;
  } else if ( mapping == MAP_BC ) {
    wn0 = n3;
    wn1 = n1;
    wn2 = n2;
  }
  
  //Calculate the midpoint of two of the sides
  Node n12 = new Node ((wn0.x+wn1.x)/2, (wn0.y+wn1.y)/2);
  Node n13 = new Node ((wn0.x+wn2.x)/2, (wn0.y+wn2.y)/2);
  
  /*
  stroke(0,255,255);
  line( n12.x, n12.y, wn0.x, wn0.y );
  line( n12.x, n12.y, wn1.x, wn1.y );
  ellipse (n12.x, n12.y, 15, 15);
  line( n13.x, n13.y, wn0.x, wn0.y );
  line( n13.x, n13.y, wn2.x, wn2.y );
  ellipse (n13.x, n13.y, 15, 15);
  */
  
  //Change y-y_1=m(x-x_1) to y=mx+b (these give me 'm')
  float b12 =  n12.y - ss.s0 * n12.x;  
  float b13 = n13.y - ss.s1 * n13.x;
  
  //Formula borrowed from http://www.geog.ubc.ca/courses/klink/gis.notes/ncgia/u32.html
  float rx = -(b12 - b13)/(ss.s0-ss.s1);
  float ry = b12 + ss.s0 * rx;
  
  return new Node (rx, ry);
}

void mousePressed () {
  setupNodes();
}
