class Plane {

  PVector dir;
  PVector offset;
  
  public Plane() {
    dir = new PVector( 1,1,0 );
    dir.normalize();
    offset = new PVector();
  }
  
  public void rot( float x, float y, float z ) {
    dir.set( x,y,z );
    float m = dir.mag();
    if ( m > 1 ) {
      dir.normalize();  
    }
  }
  
  public void draw() {
    
    rectMode( CENTER );
    pushMatrix();
      translate( offset.x, offset.y, offset.z );
      pushMatrix();
        rotateX( dir.x * PI );
        rotateY( dir.y * PI );
        rotateZ( dir.z * PI );
        rect( 0,0, 200,200 );
      popMatrix();
      pushStyle();
      stroke( 255,0,0 );
      line( 0,0,0, dir.x * 100,0,0 );
      stroke( 0,255,0 );
      line( 0,0,0, 0,dir.y * 100,0 );
      stroke( 0,0,255 );
      line( 0,0,0, 0,0,dir.z * 100 );
      popStyle();
    popMatrix();
    
  }

}
