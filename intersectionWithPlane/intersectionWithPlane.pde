
Icosahedron ico;
Plane plane;
PVector worldRot;

void setup() {
  size(640, 360, P3D);
  ico = new Icosahedron(75);
  plane = new Plane();
  worldRot = new PVector();
}

void draw() {

  worldRot.x = 15 + frameCount / 200.1f;
  worldRot.y = 15 + frameCount / 200.1f;

  background( 10 );
  lights();
  translate(width/2, height/2);
  rotateX( worldRot.x );
  rotateY( worldRot.y );

  stroke(170, 0, 0);
  noFill();
  ico.create();

  plane.rot( sin( frameCount / 200.1f ), 0, 0 );
  plane.draw();

  for ( int i = 0; i < ico.points.length; i++ ) {
    /*
    pushMatrix();
     translate( ico.points[ i ].x, ico.points[ i ].y, ico.points[ i ].z );
     rotateY( -worldRot.y );
     rotateX( -worldRot.x );
     ellipse( 0,0, 5,5 );
     popMatrix();
     */
    // projection on plane
    projectOnPlane( ico.points[ i ] );
  }
}

void projectOnPlane( PVector v ) {
  pushStyle();
  noStroke();
  fill( 255, 255, 0 );
  pushMatrix();
  translate( v.x, v.y, 0 );
  rotateY( -worldRot.y );
  rotateX( -worldRot.x );
  ellipse( 0, 0, 5, 5 );
  popMatrix();
  stroke( 255, 100 );
  line( v.x, v.y, v.z, v.x, v.y, 0 );
  popStyle();
}

