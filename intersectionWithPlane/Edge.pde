class Edge {

  PVector p1;
  PVector p2;
  
  public Edge( PVector p1, PVector p2 ) {
    this.p1 = p1;
    this.p2 = p2;
  }

}
