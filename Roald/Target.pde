class Target {
  
  ArrayList< Oscillator > origins;
  ArrayList< float[] > history;
  int hoffset;
  Oscillator destination;
  float value;
  boolean idle;
  
  public Target( float v ) {    
      origins = new ArrayList< Oscillator >();
      history = new ArrayList< float[] >();
      hoffset = 0;
      destination = null;
      value = v;
      idle = true;
  }
  
  void process() {
    idle = false;
    if ( origins.contains( destination ) ) {
      origins.remove( destination );
    }
  }
  
  void update() {
  
    if ( destination.transit ) {
      
      float pc = destination.transitionCurrent / float( destination.transitionTime );
      float[] h = new float[ origins.size() ];
      for ( int i = 0; i < origins.size(); i++ ) {
        h[ i ] = destination.translate( origins.get( i ).previous_value, value, pc );
      }
      history.add( h );
      
    } else {
      
      hoffset++;
      
    }
    
    while ( history.size() > HISTORY_SIZE ) {
      history.remove( 0 );
    }
    
    // optimised
    /*
    if ( !idle ) {
      if ( !destination.transit ) {
        idle = true;
      } else {
        float pc = destination.transitionCurrent / float( destination.transitionTime );
        float[] h = new float[ origins.size() ];
        for ( int i = 0; i < origins.size(); i++ ) {
          h[ i ] = destination.translate( origins.get( i ).previous_value, value, pc );
        }
        history.add( h );
      }
    }
    
    if ( !destination.transit && origins.size() > 0 ) {
      origins.clear();
      history.clear();
      println( "transition is finished!" );
    }
    */
    
  }
  
  void draw() {
    
    float[] fj = history.get( 0 );
    float px = ( width - 10 ) - ( ( float( width - 20 ) / ( HISTORY_SIZE - 1 ) ) * ( history.size() + hoffset ) );
    
    for ( int i = 1; i < history.size(); i++ ) {
      float[] fi = history.get( i );
      float x = px + ( float( width - 20 ) / ( HISTORY_SIZE - 1 ) );
      stroke( 255, 0, 0, 150 );
      for ( int j = 0; j < origins.size(); j++ ) {
        line( 
          px, 10 + fj[ j ] * ( height - 20 ), 
          x, 10 + fi[ j ] * ( height - 20 )
        );
      }
      px = x;
      fj = fi;
    }
    
  }
  
}
