
static final int TRANSITION_FRAMES_MIN = 100;
static final int TRANSITION_FRAMES_MAX = 100;
static final int HISTORY_SIZE = 300;
static final float TARGET_DISTANCE = 0.2f;

Oscillator[] oscs;
int oscnum = 50;
boolean randomize = false;
boolean newtargets = false;
boolean play = true;

ArrayList< Target > targets;
Targeter trgtr;

void setup() {

  size( 1280, 760, P3D );
  oscs = new Oscillator[ oscnum ];
  for ( int i = 0; i < oscnum; i++ ) {
    oscs[ i ] = new Oscillator();
  }
  
  trgtr = new Targeter( oscs );
  trgtr.generate( 10 );
  
  smooth();
  
}

void draw() {

  if ( play ) {
    if ( randomize ) {
      for ( int i = 0; i < oscnum; i++ ) {
        if ( random( 0, 1 ) > 0.5 ) {
          if ( oscs[ i ].idle ) {
            oscs[ i ].fadeIn( random( 0, 1 ), random( 0, 1 ) );
          } else {
            oscs[ i ].fadeIn( random( 0, 1 ) );
          }
        } else {
          oscs[ i ].fadeOut( random( 0, 1 ) );
        }
      }
      randomize = false;
    }
    if ( newtargets ) {
      int tnum = int( 5 + random( ( oscnum / 8 ), ( oscnum - ( oscnum / 8 ) ) / 2 ) );
      trgtr.generate( tnum );
      newtargets = false;
    
    }
    for ( int i = 0; i < oscnum; i++ ) {
      oscs[ i ].update();
    }
    trgtr.update();
  }
  
  background( 5 );
  
  trgtr.draw();
  for ( int i = 0; i < oscnum; i++ ) {
    oscs[ i ].draw();
  }
  
  noFill();
  stroke( 255,0,0 );
  float x = width - 10;
  float th = height * TARGET_DISTANCE;
  for ( int i = 0; i < trgtr.targets.size(); i++ ) {
    Target t = trgtr.targets.get( i );
    float y = 10 + t.value * ( height - 20 );
    line( x, y - th, x, y + th );
    line( x, y, x - 10, y );
  }
  
}

void keyReleased() {

  if ( key == 'r' && !randomize ) {
    randomize = true;
  } else if ( key == 't' && !newtargets ) {
    newtargets = true;
  } else if ( keyCode == 32 ) {
    play = !play;
  }  else {
    println( keyCode );
  }

}
class Oscillator {

  float value;
  float previous_value;
  float target_value;
  float weight;
  float previous_weight;
  float target_weight;
  ArrayList< float[] > history;
  boolean idle;
  
  boolean transit;
  int transitionTime;
  int transitionCurrent;

  public Oscillator() {
    
    value = 0;
    previous_value = 0;
    target_value = 0;
    weight = 0;
    previous_weight = 0;
    target_weight = 0;
    history = new ArrayList< float[] >();
    idle = true;
    
    transit = false;
    transitionTime = int( random( TRANSITION_FRAMES_MIN, TRANSITION_FRAMES_MAX ) );
    transitionCurrent = 0;
    
  }
  
  void fadeOut() {
    if ( idle ) {
      return;
    }
    previous_value = value;
    previous_weight = weight;
    target_weight = 0;
    transitionCurrent = 0;
    transit = true;
  }
  
  void fadeOut( float v ) {
    if ( idle ) {
      return;
    }
    previous_value = value;
    target_value = v;
    previous_weight = weight;
    target_weight = 0;
    transitionCurrent = 0;
    transit = true;
  }
  
  void fadeIn( float v ) {
    if ( idle ) {
      value = v;
    }
    previous_value = value;
    target_value = v;
    previous_weight = weight;
    target_weight = 1;
    transitionCurrent = 0;
    transit = true;
  }
  
  void fadeIn( float v, float from ) {
    value = from;
    previous_value = from;
    target_value = v;
    previous_weight = weight;
    target_weight = 1;
    transitionCurrent = 0;
    transit = true;
  }
  
  float translate( float prev, float target, float ratio ) {
    float r = ( sin( -HALF_PI + ratio * PI ) + 1 ) * 0.5f;
    return prev * ( 1 - r ) + target * r;
  }
  
  void update() {
    if ( transit ) {
      float pc = transitionCurrent / float( transitionTime );
      if ( pc > 1 ) {
        pc = 1;
      }
      value = translate( previous_value, target_value, pc );
      weight = translate( previous_weight, target_weight, pc );
      if ( pc == 1 ) {
        transit = false;
      }
      transitionCurrent++;
    }
    if ( weight == 0 ) {
      idle = true;
    } else {
      idle = false;
    }
    if ( idle ) {
      history.add( new float[]{ -1, 0 } );
    } else {
      history.add( new float[]{ value, weight } );
    }
    while ( history.size() > HISTORY_SIZE ) {
      history.remove( 0 );
    }
  }

  void draw() {
  
    if ( history.size() == 0 ) {
      return;
    }
    
    strokeWeight( 1.5 );
    
    float[] fj = history.get( 0 );
    float px = ( width - 10 ) - ( ( float( width - 20 ) / ( HISTORY_SIZE - 1 ) ) * history.size() );
    boolean last_was_idle = false;
    if ( fj[ 0 ] == -1 ) {
      last_was_idle = true;
    }
    
    for ( int i = 1; i < history.size(); i++ ) {
      float[] fi = history.get( i );
      float x = px + ( float( width - 20 ) / ( HISTORY_SIZE - 1 ) );
      if ( fi[ 0 ] == -1 || fj[ 0 ] == -1 ) {
        if ( !last_was_idle ) {
          noStroke();
          fill( 255, 0, 0 );
          ellipse( px, 10 + fj[ 0 ] * ( height - 20 ), 5, 5 );
        }
        last_was_idle = true;
      } else {
        stroke( 255, 20 + ( fi[ 1 ] ) * 235 );
        line( 
          px, 10 + fj[ 0 ] * ( height - 20 ), 
          x, 10 + fi[ 0 ] * ( height - 20 )
        );
        if ( last_was_idle ) {
          noStroke();
          fill( 0, 255, 0 );
          ellipse( px, 10 + fj[ 0 ] * ( height - 20 ), 5, 5 );
          last_was_idle = false;
        }
      }
      px = x;
      fj = fi;
    }
  
  }


}
class Target {
  
  ArrayList< Oscillator > origins;
  ArrayList< float[] > history;
  int hoffset;
  Oscillator destination;
  float value;
  boolean idle;
  
  public Target( float v ) {    
      origins = new ArrayList< Oscillator >();
      history = new ArrayList< float[] >();
      hoffset = 0;
      destination = null;
      value = v;
      idle = true;
  }
  
  void process() {
    idle = false;
    if ( origins.contains( destination ) ) {
      origins.remove( destination );
    }
  }
  
  void update() {
  
    if ( destination.transit ) {
      
      float pc = destination.transitionCurrent / float( destination.transitionTime );
      float[] h = new float[ origins.size() ];
      for ( int i = 0; i < origins.size(); i++ ) {
        h[ i ] = destination.translate( origins.get( i ).previous_value, value, pc );
      }
      history.add( h );
      
    } else {
      
      hoffset++;
      
    }
    
    while ( history.size() > HISTORY_SIZE ) {
      history.remove( 0 );
    }
    
    // optimised
    /*
    if ( !idle ) {
      if ( !destination.transit ) {
        idle = true;
      } else {
        float pc = destination.transitionCurrent / float( destination.transitionTime );
        float[] h = new float[ origins.size() ];
        for ( int i = 0; i < origins.size(); i++ ) {
          h[ i ] = destination.translate( origins.get( i ).previous_value, value, pc );
        }
        history.add( h );
      }
    }
    
    if ( !destination.transit && origins.size() > 0 ) {
      origins.clear();
      history.clear();
      println( "transition is finished!" );
    }
    */
    
  }
  
  void draw() {
    
    float[] fj = history.get( 0 );
    float px = ( width - 10 ) - ( ( float( width - 20 ) / ( HISTORY_SIZE - 1 ) ) * ( history.size() + hoffset ) );
    
    for ( int i = 1; i < history.size(); i++ ) {
      float[] fi = history.get( i );
      float x = px + ( float( width - 20 ) / ( HISTORY_SIZE - 1 ) );
      stroke( 255, 0, 0, 150 );
      for ( int j = 0; j < origins.size(); j++ ) {
        line( 
          px, 10 + fj[ j ] * ( height - 20 ), 
          x, 10 + fi[ j ] * ( height - 20 )
        );
      }
      px = x;
      fj = fi;
    }
    
  }
  
}
class Targeter {

  ArrayList< Target > targets;
  ArrayList< Target > tmp;
  int tnum;
  Oscillator[] oscs;

  public Targeter( Oscillator[] oscs ) {
    this.oscs = oscs;
    tnum = 0;
    targets = new ArrayList< Target >();
    tmp = new ArrayList< Target >();
  }
  
  public void generate( int tnum ) {
    this.tnum = tnum;
    targets.clear();
    tmp.clear();
    for ( int i = 0; i < tnum; i++ ) {
      targets.add( new Target( random( 0, 1 ) ) );
      tmp.add( targets.get( i ) );
    }
    allocate();
  }
  
  private void allocate() {
    
    // how many active osc?
    ArrayList< Oscillator > active_osc = new ArrayList< Oscillator >();
    ArrayList< Oscillator > idle_osc = new ArrayList< Oscillator >();
    for ( int i = 0; i < oscs.length; i++ ) {
      if ( !oscs[ i ].idle ) {
        active_osc.add( oscs[ i ] );
      } else {
        idle_osc.add( oscs[ i ] );
      }
    }

    for ( int i = 0; i < active_osc.size(); i++ ) {
      Oscillator activeo = active_osc.get( i );
      // seek if there is a target to send him
      float smallest = 0;
      Target sel = null;
      for ( int ti = 0; ti < tmp.size(); ti++ ) {
        Target t = tmp.get( ti );
        float d = abs( t.value - activeo.value );
        if ( d < TARGET_DISTANCE ) {
          t.origins.add( activeo );
        }
        if ( d < TARGET_DISTANCE && ( smallest == 0 || smallest > d ) ) {
          smallest = d;
          sel = t;
        }
      }
      if ( sel != null ) {
        activeo.fadeIn( sel.value );
        sel.destination = activeo;
        tmp.remove( sel );
      } else {
        activeo.fadeOut();
      }
    }
    
    for ( int i = 0; i < tmp.size(); i++ ) {
      Target t = tmp.get( i );
      if ( i >= idle_osc.size() ) {
        println( "dropped target, no more osc in stock..." );
        targets.remove( t );
        tnum--;
      } else {
        Oscillator idleo = idle_osc.get( i );
        t.destination = idleo;
        idleo.fadeIn( t.value );
      }
    }
      
    tmp.clear();
    
    int totallinenum = oscs.length;
    for ( int i = 0; i < tnum; i++ ) {
      targets.get( i ).process();
      totallinenum += targets.get( i ).origins.size();
    }
    println( "TARGETS: " + tnum );
    println( "ACTIVE OSC: " + active_osc.size() );
    println( "IDLE OSC: " + idle_osc.size() );
    println( "TOTAL NUM OF LINES: " + totallinenum );
    println( "****************" );
    
  }
  
  void update() {
    for ( int i = 0; i < tnum; i++ ) {
      targets.get( i ).update();
    }
  }
  
  void draw() {
    for ( int i = 0; i < tnum; i++ ) {
      targets.get( i ).draw();
    }
  }

}

