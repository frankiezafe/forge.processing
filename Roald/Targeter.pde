class Targeter {

  ArrayList< Target > targets;
  ArrayList< Target > tmp;
  int tnum;
  Oscillator[] oscs;

  public Targeter( Oscillator[] oscs ) {
    this.oscs = oscs;
    tnum = 0;
    targets = new ArrayList< Target >();
    tmp = new ArrayList< Target >();
  }
  
  public void generate( int tnum ) {
    this.tnum = tnum;
    targets.clear();
    tmp.clear();
    for ( int i = 0; i < tnum; i++ ) {
      targets.add( new Target( random( 0, 1 ) ) );
      tmp.add( targets.get( i ) );
    }
    allocate();
  }
  
  private void allocate() {
    
    // how many active osc?
    ArrayList< Oscillator > active_osc = new ArrayList< Oscillator >();
    ArrayList< Oscillator > idle_osc = new ArrayList< Oscillator >();
    for ( int i = 0; i < oscs.length; i++ ) {
      if ( !oscs[ i ].idle ) {
        active_osc.add( oscs[ i ] );
      } else {
        idle_osc.add( oscs[ i ] );
      }
    }

    for ( int i = 0; i < active_osc.size(); i++ ) {
      Oscillator activeo = active_osc.get( i );
      // seek if there is a target to send him
      float smallest = 0;
      Target sel = null;
      for ( int ti = 0; ti < tmp.size(); ti++ ) {
        Target t = tmp.get( ti );
        float d = abs( t.value - activeo.value );
        if ( d < TARGET_DISTANCE ) {
          t.origins.add( activeo );
        }
        if ( d < TARGET_DISTANCE && ( smallest == 0 || smallest > d ) ) {
          smallest = d;
          sel = t;
        }
      }
      if ( sel != null ) {
        activeo.fadeIn( sel.value );
        sel.destination = activeo;
        tmp.remove( sel );
      } else {
        activeo.fadeOut();
      }
    }
    
    for ( int i = 0; i < tmp.size(); i++ ) {
      Target t = tmp.get( i );
      if ( i >= idle_osc.size() ) {
        println( "dropped target, no more osc in stock..." );
        targets.remove( t );
        tnum--;
      } else {
        Oscillator idleo = idle_osc.get( i );
        t.destination = idleo;
        idleo.fadeIn( t.value );
      }
    }
      
    tmp.clear();
    
    int totallinenum = oscs.length;
    for ( int i = 0; i < tnum; i++ ) {
      targets.get( i ).process();
      totallinenum += targets.get( i ).origins.size();
    }
    println( "TARGETS: " + tnum );
    println( "ACTIVE OSC: " + active_osc.size() );
    println( "IDLE OSC: " + idle_osc.size() );
    println( "TOTAL NUM OF LINES: " + totallinenum );
    println( "****************" );
    
  }
  
  void update() {
    for ( int i = 0; i < tnum; i++ ) {
      targets.get( i ).update();
    }
  }
  
  void draw() {
    for ( int i = 0; i < tnum; i++ ) {
      targets.get( i ).draw();
    }
  }

}
