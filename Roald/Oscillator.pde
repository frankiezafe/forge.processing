class Oscillator {

  float value;
  float previous_value;
  float target_value;
  float weight;
  float previous_weight;
  float target_weight;
  ArrayList< float[] > history;
  boolean idle;
  
  boolean transit;
  int transitionTime;
  int transitionCurrent;

  public Oscillator() {
    
    value = 0;
    previous_value = 0;
    target_value = 0;
    weight = 0;
    previous_weight = 0;
    target_weight = 0;
    history = new ArrayList< float[] >();
    idle = true;
    
    transit = false;
    transitionTime = int( random( TRANSITION_FRAMES_MIN, TRANSITION_FRAMES_MAX ) );
    transitionCurrent = 0;
    
  }
  
  void fadeOut() {
    if ( idle ) {
      return;
    }
    previous_value = value;
    previous_weight = weight;
    target_weight = 0;
    transitionCurrent = 0;
    transit = true;
  }
  
  void fadeOut( float v ) {
    if ( idle ) {
      return;
    }
    previous_value = value;
    target_value = v;
    previous_weight = weight;
    target_weight = 0;
    transitionCurrent = 0;
    transit = true;
  }
  
  void fadeIn( float v ) {
    if ( idle ) {
      value = v;
    }
    previous_value = value;
    target_value = v;
    previous_weight = weight;
    target_weight = 1;
    transitionCurrent = 0;
    transit = true;
  }
  
  void fadeIn( float v, float from ) {
    value = from;
    previous_value = from;
    target_value = v;
    previous_weight = weight;
    target_weight = 1;
    transitionCurrent = 0;
    transit = true;
  }
  
  float translate( float prev, float target, float ratio ) {
    float r = ( sin( -HALF_PI + ratio * PI ) + 1 ) * 0.5f;
    return prev * ( 1 - r ) + target * r;
  }
  
  void update() {
    if ( transit ) {
      float pc = transitionCurrent / float( transitionTime );
      if ( pc > 1 ) {
        pc = 1;
      }
      value = translate( previous_value, target_value, pc );
      weight = translate( previous_weight, target_weight, pc );
      if ( pc == 1 ) {
        transit = false;
      }
      transitionCurrent++;
    }
    if ( weight == 0 ) {
      idle = true;
    } else {
      idle = false;
    }
    if ( idle ) {
      history.add( new float[]{ -1, 0 } );
    } else {
      history.add( new float[]{ value, weight } );
    }
    while ( history.size() > HISTORY_SIZE ) {
      history.remove( 0 );
    }
  }

  void draw() {
  
    if ( history.size() == 0 ) {
      return;
    }
    
    strokeWeight( 1.5 );
    
    float[] fj = history.get( 0 );
    float px = ( width - 10 ) - ( ( float( width - 20 ) / ( HISTORY_SIZE - 1 ) ) * history.size() );
    boolean last_was_idle = false;
    if ( fj[ 0 ] == -1 ) {
      last_was_idle = true;
    }
    
    for ( int i = 1; i < history.size(); i++ ) {
      float[] fi = history.get( i );
      float x = px + ( float( width - 20 ) / ( HISTORY_SIZE - 1 ) );
      if ( fi[ 0 ] == -1 || fj[ 0 ] == -1 ) {
        if ( !last_was_idle ) {
          noStroke();
          fill( 255, 0, 0 );
          ellipse( px, 10 + fj[ 0 ] * ( height - 20 ), 5, 5 );
        }
        last_was_idle = true;
      } else {
        stroke( 255, 20 + ( fi[ 1 ] ) * 235 );
        line( 
          px, 10 + fj[ 0 ] * ( height - 20 ), 
          x, 10 + fi[ 0 ] * ( height - 20 )
        );
        if ( last_was_idle ) {
          noStroke();
          fill( 0, 255, 0 );
          ellipse( px, 10 + fj[ 0 ] * ( height - 20 ), 5, 5 );
          last_was_idle = false;
        }
      }
      px = x;
      fj = fi;
    }
  
  }


}
