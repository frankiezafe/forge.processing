
static final int TRANSITION_FRAMES_MIN = 100;
static final int TRANSITION_FRAMES_MAX = 100;
static final int HISTORY_SIZE = 300;
static final float TARGET_DISTANCE = 0.05f;

Oscillator[] oscs;
int oscnum = 200;
boolean randomize = false;
boolean newtargets = false;
boolean play = true;

ArrayList< Target > targets;
Targeter trgtr;

void setup() {

  size( 1280, 760, P3D );
  oscs = new Oscillator[ oscnum ];
  for ( int i = 0; i < oscnum; i++ ) {
    oscs[ i ] = new Oscillator();
  }
  
  trgtr = new Targeter( oscs );
  trgtr.generate( 10 );
  
  smooth();
  
}

void draw() {

  if ( play ) {
    if ( randomize ) {
      for ( int i = 0; i < oscnum; i++ ) {
        if ( random( 0, 1 ) > 0.5 ) {
          if ( oscs[ i ].idle ) {
            oscs[ i ].fadeIn( random( 0, 1 ), random( 0, 1 ) );
          } else {
            oscs[ i ].fadeIn( random( 0, 1 ) );
          }
        } else {
          oscs[ i ].fadeOut( random( 0, 1 ) );
        }
      }
      randomize = false;
    }
    if ( newtargets ) {
      int tnum = int( 5 + random( ( oscnum / 8 ), ( oscnum - ( oscnum / 8 ) ) / 2 ) );
      trgtr.generate( tnum );
      newtargets = false;
    
    }
    for ( int i = 0; i < oscnum; i++ ) {
      oscs[ i ].update();
    }
    trgtr.update();
  }
  
  background( 5 );
  
  trgtr.draw();
  for ( int i = 0; i < oscnum; i++ ) {
    oscs[ i ].draw();
  }
  
  noFill();
  stroke( 255,0,0 );
  float x = width - 10;
  float th = height * TARGET_DISTANCE;
  for ( int i = 0; i < trgtr.targets.size(); i++ ) {
    Target t = trgtr.targets.get( i );
    float y = 10 + t.value * ( height - 20 );
    line( x, y - th, x, y + th );
    line( x, y, x - 10, y );
  }
  
}

void keyReleased() {

  if ( key == 'r' && !randomize ) {
    randomize = true;
  } else if ( key == 't' && !newtargets ) {
    newtargets = true;
  } else if ( keyCode == 32 ) {
    play = !play;
  }  else {
    println( keyCode );
  }

}
