import oscP5.*;
import netP5.*;

OscP5 oscP5;
PVector mouse;
PVector mouse2;
PVector mouse3;

void setup() {
  size(400,400);
  oscP5 = new OscP5( this, 12000 );
  mouse = new PVector();
  mouse2 = new PVector();
  mouse3 = new PVector();
}

void draw() {

  background( 0 );
  fill( 255 );
  text( mouse.x, 10, 25 );
  text( mouse.y, 10, 40 );
  text( mouse2.x, 10, 55 );
  text( mouse2.y, 10, 70 );
  text( mouse3.x, 10, 85 );
  text( mouse3.y, 10, 100 );
  
}

void oscEvent( OscMessage msg ) { 
  //println( "OSCReceiver:new message " + msg.addrPattern() );

  if ( msg.checkAddrPattern( "/YO/mouse/pos" ) ) {
    // println( "typetag " + msg.typetag() );
    mouse.set(
      msg.get( 0 ).intValue(),
      msg.get( 1 ).intValue()
    );
  }
  if ( msg.checkAddrPattern( "/ap/mouse/pos" ) ) {
    // println( "typetag " + msg.typetag() );
    mouse2.set(
      msg.get( 0 ).intValue(),
      msg.get( 1 ).intValue()
    );
  }
  if ( msg.checkAddrPattern( "/pb/mouse/pos" ) ) {
    // println( "typetag " + msg.typetag() );
    mouse3.set(
      msg.get( 0 ).intValue(),
      msg.get( 1 ).intValue()
    );
  }

}