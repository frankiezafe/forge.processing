import processing.video.*;

Capture webcam;
int [] tcolors;
int tcolorIndex;
int totalpixels;

boolean newframe;

void setup() {
  
  size( 1280, 480 );
  webcam = new Capture( this, 640, 480 );
  webcam.start();
  totalpixels = webcam.width * webcam.height;
  
  tcolors = new int[ 320 ];
  tcolorIndex = 0;
  
  newframe = false;

}

void captureEvent(Capture c) {
  c.read();
  newframe = true;
}

void draw() {

  if ( newframe ) {
  
    newframe = false;
    
    webcam.loadPixels();
    int totalr = 0;
    int totalg = 0;
    int totalb = 0;
    // on visite tous les pixels
    for ( int i = 0; i < totalpixels; ++i ) {
      // red [0,255]
      // green [0,255]
      // blue [0,255]
      // alpha [0,255]
      // int color = blue + (green * 256) + (red * 256 * 256) + (alpha * 256 * 256 * 256)
      int pxcolor = webcam.pixels[ i ];
      float red = red( pxcolor );
      float green = green( pxcolor );
      float blue = blue( pxcolor );
      // vérification en mélangeant les canaux
      // webcam.pixels[ i ] = color( blue, red, green );
      // on additionne toutes les canaux
      totalr += red;
      totalg += green;
      totalb += blue;
    }
    
    int totalcolor = color( totalr / totalpixels, totalg / totalpixels, totalb / totalpixels );
    // doit aller à la position 0 dans tcolors
    // MAIS avant ça, il faut tout bouger vers la fin du tableau
    for ( int i = tcolors.length - 1; i >= 1; --i ) {
      tcolors[ i ] = tcolors[ i - 1 ];
    }
    tcolors[ 0 ] = totalcolor;
  
  }
  
  background( 0 );
  image( webcam, 200, 0, 160, 120 );
  noStroke();
  for ( int i = 0; i < tcolors.length; ++i ) {
    fill( tcolors[ i ] );
    rect( i * 2 + 380, 0, 2, webcam.height );
  }
  
  String debug = "";
  for ( int i = 0; i < tcolors.length; ++i ) {
    debug += i + " = " + tcolors[ i ] + "\n";
  }
  fill( 255 );
  text( debug, 10, 25 );
  
}