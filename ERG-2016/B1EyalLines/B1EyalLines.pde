
int compteur=0;
float a=0;
float b=0;

long deltatime;
long lasttime;

void setup() {
  size(600, 600);
  strokeWeight(0.9);
  frameRate( 100 );
  deltatime = 0;
  lasttime = 0;
}

void draw() {
  
  long currenttime = millis();
  deltatime = currenttime - lasttime;
  lasttime = currenttime;
  
  background(0);

  translate (width/2, height/2);
  
  float aplus= 0.0001 * deltatime;
  float bplus= 0.0002 * deltatime;

  float d1=(300);
  float d2=(300);


  for ( float n=0; n < TWO_PI; n += TWO_PI / 100 ) {
    
    float tmpa = a + n;
    float tmpb = b + n;
    
    // stroke(#FFB719);
    // line( 0,0, cos( tmpa ) * d1, sin( tmpa ) * d1 );
    
    stroke(#4D3981);// VIOLET
    line(tan(tmpa)*d1, tan(tmpa)*d1, sin(tmpb)*d2, cos(tmpb)*d2);
    stroke(#00AC63);// vert
    line(cos(tmpb)*d2, sin(tmpb)*d2*PI, sin(tmpb)*d2, cos(tmpa)*d1);
    stroke(#FFB719);//JAUNE
    line(sin(tmpb)*d2*PI, cos(tmpb)*d2, sin(tmpb)*d2, cos(tmpa)*sin(d1));
    stroke(#CE4A4A);
    line(sin(tmpb)*d2, tan(tmpa)*d2, sin(tmpb)*cos(d2), tan(tmpa)*PI);

  }

  b+=bplus;
  a+=aplus;  
  while ( a > TWO_PI ) { a -= TWO_PI; }
  while ( b > TWO_PI ) { b -= TWO_PI; }
  
  fill( 255 );
  text( a, 10, 25 );
  text( b, 10, 40 );
  text( frameRate, 10, 55 );
  text( deltatime, 10, 70 );
  
  /*
  for (int n=10; n<100; n++) {
    b+=bplus;
    a+=aplus;
  }
  
  
  for (int n=10; n<100; n++) {
    b+=bplus;
    a+=aplus;
  }
  
  stroke(#CE4A4A);// ROUGE
  for (int n=10; n<100; n++) {
    b+=bplus;
    a+=aplus;
  }
  */
  
}