String getUniqueKey() {
  return "" + year() + "-" + month() + "-" + day() + "-" + hour() + "-" + minute() + "-" + second();
};

void setup() {
  
  // à modifier
  int imgcount = 1;
  size( 600, 600 );

  // pas besoin de toucher
  String uniquekey = getUniqueKey();

  float[] couleur1 = new float[] { 255, 0, 0 };
  float[] couleur2 = new float[] { 0, 255, 0 };
  float[] couleur3 = new float[] { 0, 0, 255 };
  float[] couleur4 = new float[] { 0, 0, 0 };
  
  float[] couleurA = new float[] { 0, 0, 0 };
  float[] couleurB = new float[] { 0, 0, 0 };
  float[] pcolor = new float[ 3 ];
  
  while ( imgcount > 0 ) {
      
    loadPixels();
    int i = 0;
    for ( int y = 0; y < height; ++y ) {
      
      float ry = y * 1.f / ( height - 1 );
      float ryi = 1 - ry;
      for ( int c = 0; c < 3; ++c ) {
        couleurA[ c ] = couleur1[ c ] * ryi + couleur4[ c ] * ry;
        couleurB[ c ] = couleur2[ c ] * ryi + couleur3[ c ] * ry;
      }
      
      for ( int x = 0; x < width; ++x ) {
        float ratio = x * 1.f / ( width - 1 );
        float oitar = 1 - ratio;
        for ( int c = 0; c < 3; ++c ) {
          pcolor[ c ] = couleurA[ c ] * oitar + couleurB[ c ] * ratio;
        }
        pixels[ i ] = color( pcolor[ 0 ], pcolor[ 1 ], pcolor[ 2 ] );
        ++i;
      }
      
    }
    updatePixels();
    save( "test_" + uniquekey + "_" + imgcount + ".png" );
    --imgcount;
  }
  // exit();
  
}

