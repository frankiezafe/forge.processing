import oscP5.*;
import netP5.*;

int UID = 0;

OscP5 oscP5;
NetAddress addr1;
NetAddress addr2;
NetAddress addr3;

PVector me;
PVector v0;
PVector v1;
PVector v2;

void setup() {
  size(400,400);
  oscP5 = new OscP5( this, 12000 );
  addr1 = new NetAddress( "192.168.1.5", 12000 );
  addr2 = new NetAddress( "192.168.1.4", 12000 );
  addr3 = new NetAddress( "192.168.1.89", 12000 );
  me = new PVector();
  v0 = new PVector();
  v1 = new PVector();
  v2 = new PVector();
  
  noCursor();
  
}

void draw() {
  
  background( 0 );
  
  OscMessage msg = new OscMessage("/mouse/pos");
  msg.add( UID );      // position 0
  msg.add( mouseX );   // position 1
  msg.add( mouseY );   // position 2
  oscP5.send( msg, addr1 ); 
  oscP5.send( msg, addr2 ); 
  oscP5.send( msg, addr3 );
  
  me.set( mouseX, mouseY );
  
  noFill();
  //fill( 255,0,255 );
  ellipse( me.x, me.y, 50, 50 );
  //fill( 255 );
  ellipse( v0.x, v0.y, 50, 50 );
  //fill( 255,0,0 );
  ellipse( v1.x, v1.y, 50, 50 );
  //fill( 255,255,0 );
  ellipse( v2.x, v2.y, 50, 50 );
  
  float d_me_v0 = me.dist( v0 );
  float d_me_v1 = me.dist( v1 );
  float d_me_v2 = me.dist( v2 );
  float d_v0_v1 = v0.dist( v1 );
  float d_v0_v2 = v0.dist( v2 );
  float d_v1_v2 = v1.dist( v2 );
  
  boolean c_me_v0 = false;
  boolean c_me_v1 = false;
  boolean c_me_v2 = false;
  boolean c_v0_v1 = false;
  boolean c_v0_v2 = false;
  boolean c_v1_v2 = false;
  
  stroke( 255 );
  
  if ( d_me_v0 < 50 ) {
    c_me_v0 = true;
    line( me.x, me.y, v0.x, v0.y );
  }
  if ( d_me_v1 < 50 ) {
    c_me_v1 = true;
    line( me.x, me.y, v1.x, v1.y );
  }
  if ( d_me_v2 < 50 ) {
    c_me_v2 = true;
    line( me.x, me.y, v2.x, v2.y );
  }
  if ( d_v0_v1 < 50 ) {
    c_v0_v1 = true;
    line( v0.x, v0.y, v1.x, v1.y );
  }
  if ( d_v0_v2 < 50 ) {
    c_v0_v2 = true;
    line( v0.x, v0.y, v2.x, v2.y );
  }
  if ( d_v1_v2 < 50 ) {
    c_v1_v2 = true;
    line( v1.x, v1.y, v2.x, v2.y );
  }
  
  fill(255);
  text( "me - eyeal " + c_me_v0 + " / " + d_me_v0, 10, 25 );
  text( "me - constant " + c_me_v1 + " / " + d_me_v1, 10, 40 );
  text( "me - pierre " + c_me_v2 + " / " + d_me_v2, 10, 55 );
  text( "eyeal - constant " + c_v0_v1 + " / " + d_v0_v1, 10, 70 );
  text( "eyeal - pierre " + c_v0_v2 + " / " + d_v0_v2, 10, 85 );
  text( "constant - pierre " + c_v1_v2 + " / " + d_v1_v2, 10, 100 );
  text("me", me.x, me.y );
  text("eyal", v0.x, v0.y );
  text("constant", v1.x, v1.y );
  text("pierre", v2.x, v2.y );
  
}

void oscEvent( OscMessage msg ) { 
  if ( msg.checkAddrPattern( "/mouse/pos" ) ) {
    println( "new msg from: " + msg.get( 0 ).intValue() );
    int msgUID = msg.get( 0 ).intValue();
    switch( msgUID ) {
      case 1:
        v0.set( msg.get( 1 ).intValue(), msg.get( 2 ).intValue() );
        break;
      case 4:
        v1.set( msg.get( 1 ).intValue(), msg.get( 2 ).intValue() );
        break;
      case 89:
        v2.set( msg.get( 1 ).intValue(), msg.get( 2 ).intValue() );
        break;
    }
  }
  // print( "new message" ); 
}
