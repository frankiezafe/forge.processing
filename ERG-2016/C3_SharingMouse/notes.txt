pour avoir son adresse IP

terminal, taper 'ifconfig' (mac/linux)
sur mac, en general, dans en1

cmd, taper 'ipconfig' (windows)

dans le cadre de C3_SharingMouse, on attribue un identifiant unique (UID) dans chaque application

ça permet de reconnaître l'expéditeur de chaque message OSC - voir oscEvent() 

* fz: 			192.168.1.3 	UID 0
* eyal: 		192.168.1.4		UID 1
* constant: 	192.168.1.5		UID 4
* pierre: 		192.168.1.89	UID 89
