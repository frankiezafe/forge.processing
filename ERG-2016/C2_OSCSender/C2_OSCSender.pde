import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress senderaddress;
NetAddress senderaddress2;
NetAddress senderaddress3;

void setup() {
  size(400,400);
  oscP5 = new OscP5( this, 12001 );
  senderaddress = new NetAddress( "192.168.1.2", 12000 );
  senderaddress2 = new NetAddress( "192.168.1.10", 12000 );
  senderaddress3 = new NetAddress( "192.168.1.4", 12000 );
}

void draw() {
  OscMessage msg = new OscMessage("/fz/mouse/pos");
  msg.add( mouseX );
  msg.add( mouseY );
  oscP5.send( msg, senderaddress ); 
  oscP5.send( msg, senderaddress2 ); 
  oscP5.send( msg, senderaddress3 ); 
}

void oscEvent( OscMessage msg ) { 
  print( "new message" ); 
}