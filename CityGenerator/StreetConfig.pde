public class StreetConfig {

	public static final float STREET_STEP_DEFAULT = 12;
	public static final float STREET_CROSS_MULT_DEFAULT = 1;
	public static final float STREET_DEVIATION_DEFAULT = 0.1f;
	public static final float STREET_GROUND_INFLUENCE_DEFAULT = 0.4f;
	public static final int STREET_MAX_ITERATIONS_DEFAULT = 150;
	public static final float STREET_BUILD_WIDTH_DEFAULT = 30;
	public static final float STREET_STREET_WIDTH_DEFAULT = 10;
	
	public float streetStep;
	public float streetCrossMultiplier;
	public float streetDeviation;
	public float streetGroundInfluence;
	public int streetMaxIterations;
	public float buildWidth;
	public float streetWidth;
	
	public StreetConfig() {
		
		streetStep = 				STREET_STEP_DEFAULT;
		streetCrossMultiplier = 	STREET_CROSS_MULT_DEFAULT;
		streetDeviation = 			STREET_DEVIATION_DEFAULT;
		streetGroundInfluence = 	STREET_GROUND_INFLUENCE_DEFAULT;
		streetMaxIterations = 		STREET_MAX_ITERATIONS_DEFAULT;
		buildWidth = 				STREET_BUILD_WIDTH_DEFAULT;
		streetWidth = 				STREET_STREET_WIDTH_DEFAULT;
	
	}
	
	public void set( StreetConfig src ) {
		streetStep = src.streetStep;
		streetCrossMultiplier = src.streetCrossMultiplier;
		streetDeviation = src.streetDeviation;
		streetGroundInfluence = src.streetGroundInfluence;
		streetMaxIterations = src.streetMaxIterations;
		buildWidth = src.buildWidth;
		streetWidth = src.streetWidth;
	}
	
}