//import ground.Ground;
//import generator.Organiser;
import controlP5.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

private Ground terrain;
private Organiser organiser;

private float depth;
private float view_scale;
private boolean display_data;

private ControlP5 ui;
private RadioButton ui_ground_display;
private Slider ui_streetNum;
private Slider ui_streetStep;
private Slider ui_streetCrossMultiplier;
private Slider ui_streetDeviation;
private Slider ui_streetGroundInfluence;
private Slider ui_streetMaxIterations;
private Slider ui_streetBuildWidth;
private Slider ui_streetWidth;
private boolean build_streets;
private Toggle ui_buildstreets;

  public DataColor buildable_color = new DataColor( 255,120,0 );
  public DataColor street_color = new DataColor( 255,0,0 );
  public DataColor garden_color = new DataColor( 255,0,255 );
  public DataColor house_color = new DataColor( 0,255,0 );

public void settings() {
  size( 1280, 720, P3D );
}

public void setup() {

  int c = color( 230, 0, 10 );
  view_scale = 1;
  depth = 100;

  terrain = new Ground( this );
  terrain.init( "greymap-smooth.png", 128, 72 );
  //		terrain.init( "greymap-tester.png", 64,36 );
  terrain.setDepth( depth );

  organiser = new Organiser( this, terrain );
  organiser.generateStreets( 20 );

  build_streets = false;
  display_data = false;

  int uix = 10;
  int uiy = 15;

  ui = new ControlP5( this );
  ui.addLabel( "GROUND", uix, uiy );
  uiy += 15;
  ui.addSlider( "depth", 0, 500, uix, uiy, 100, 10 ).setValue( depth );
  uiy += 15;
  ui.addSlider( "view_scale", 0.1f, 5, uix, uiy, 100, 10 ).setValue( view_scale );
  uiy += 15;
  ui_buildstreets = ui.addToggle( "display_data", uix, uiy, 10, 10 );
  uiy += 30;
  ui_ground_display = ui.addRadio( "ground_display", uix, uiy );
  ui_ground_display.addItem( "triangles", Ground.GROUND_DISPLAY_TRIANGLES );
  ui_ground_display.addItem( "quads", Ground.GROUND_DISPLAY_QUADS );
  ui_ground_display.addItem( "lines", Ground.GROUND_DISPLAY_LINES );
  ui_ground_display.addItem( "info", Ground.GROUND_DISPLAY_INFO );
  ui_ground_display.activate( terrain.getDisplayMode() );

  uix = 300;
  uiy = 15;
  ui.addLabel( "STREETS", uix, uiy );
  uiy += 15;
  ui_streetNum = ui.addSlider( "num", 1, 50, uix, uiy, 100, 10 ).setValue( 20 );
  uiy += 15;
  ui_streetStep = ui.addSlider( "step (px)", 1, 50, uix, uiy, 100, 10 ).setValue( StreetConfig.STREET_STEP_DEFAULT );
  uiy += 15;
  ui_streetMaxIterations = ui.addSlider( "max iterations", 0, 500, uix, uiy, 100, 10 ).setValue( StreetConfig.STREET_MAX_ITERATIONS_DEFAULT );
  uiy += 15;
  ui_streetCrossMultiplier = ui.addSlider( "cross mult", 0, 3, uix, uiy, 100, 10 ).setValue( StreetConfig.STREET_CROSS_MULT_DEFAULT );
  uiy += 15;
  ui_streetDeviation = ui.addSlider( "deviation", 0, 1, uix, uiy, 100, 10 ).setValue( StreetConfig.STREET_DEVIATION_DEFAULT );
  uiy += 15;
  ui_streetGroundInfluence = ui.addSlider( "ground influence", 0, 1, uix, uiy, 100, 10 ).setValue( StreetConfig.STREET_GROUND_INFLUENCE_DEFAULT );
  uiy += 15;
  ui_streetBuildWidth = ui.addSlider( "build with (px)", 0, 100, uix, uiy, 100, 10 ).setValue( StreetConfig.STREET_BUILD_WIDTH_DEFAULT );
  uiy += 15;
  ui_streetWidth = ui.addSlider( "street with (px)", 0, 100, uix, uiy, 100, 10 ).setValue( StreetConfig.STREET_STREET_WIDTH_DEFAULT );
  uiy += 15;
  ui_buildstreets = ui.addToggle( "build_streets", uix, uiy, 20, 20 );
}

public void resetStreets() {
  // collecting street configuration
  StreetConfig sc = new StreetConfig();
  sc.buildWidth = ui_streetBuildWidth.getValue();
  sc.streetCrossMultiplier = ui_streetCrossMultiplier.getValue();
  sc.streetDeviation = ui_streetDeviation.getValue();
  sc.streetGroundInfluence = ui_streetGroundInfluence.getValue();
  sc.streetMaxIterations = (int) ui_streetMaxIterations.getValue();
  sc.streetStep = ui_streetStep.getValue();
  sc.streetWidth = ui_streetWidth.getValue();
  organiser.setStreetConfig( sc );
  organiser.generateStreets( (int) ui_streetNum.getValue() );
}

public void draw() {

  terrain.setDepth( depth );
  organiser.setDisplaydata( display_data );

  if ( build_streets ) {
    resetStreets();
    ui_buildstreets.setValue( 0 );
    build_streets = false;
  }

  // vars from UI
  terrain.setDisplayMode( (int) ui_ground_display.getValue() );

  float hw = width * 0.5f;
  float hh = height * 0.5f;

  background( 5 );

  hint( ENABLE_DEPTH_TEST );
  lights();

  pushMatrix();
  translate( hw, hh, -300 );
  rotateX( 45 );
  rotateZ( frameCount * 0.001f );
  scale( view_scale, view_scale, view_scale );
  translate( -hw, -hh, 0 );
  terrain.draw();
  organiser.drawHouses();
  popMatrix();

  hint( DISABLE_DEPTH_TEST );
}



public PVector renderNormale( GPoint origin, GPoint end1, GPoint end2 ) {
  PVector v1 = new PVector (
    end1.x - origin.x, 
    end1.y - origin.y, 
    end1.z - origin.z
    );
  PVector v2 = new PVector (
    end2.x - origin.x, 
    end2.y - origin.y, 
    end2.z - origin.z
    );
  v1.normalize();
  v2.normalize();
  PVector n = v1.cross( v2 );
  n.normalize();
  return n;
}