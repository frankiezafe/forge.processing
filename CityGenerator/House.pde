public class House extends PVector implements PConstants {

	public float width;	// facade à rue ~x
	public float height;	// z axis
	public float depth;	// profondeur ~y
	public float rotz;
	
	public House( float x, float y, float z, float w, float h, float d ) {
		super( x,y,z );
		width = w;
		height = h;
		depth = d;
		rotz = 0;
	}
	
}