public class GPoint extends PVector {

	public float u;
	public float v;
	
	public float normx;
	public float normy;
	public float normz;
	public float normu;
	public float normv;
	
	public IntList faceIds;
	public PVector normale;
	
	public GPoint() {
		normale = new PVector();
		faceIds = new IntList();
		reset();
	}
	
	public GPoint( GPoint src ) {
		x = src.x;
		y = src.y;
		z = src.z;
		u = src.u;
		v = src.v;
		normx = src.normx;
		normy = src.normy;
		normz = src.normz;
		normu = src.normu;
		normv = src.normv;
		normale = new PVector();
		faceIds = new IntList();
	}
	
	public GPoint( float vs[] ) {
		super( vs[0], vs[1], vs[2] );
		if ( vs.length != 10 ) {
			System.err.println( "gPoint NOT correctly instanciated" );
			return;
		}
		u = vs[3];
		v = vs[4];
		normx = vs[5];
		normy = vs[6];
		normz = vs[7];
		normu = vs[8];
		normv = vs[9];
		normale = new PVector();
		faceIds = new IntList();
	}
	
	public void reset() {
		x = 0;
		y = 0;
		z = 0;
		u = 0;
		v = 0;
		normx = 0;
		normy = 0;
		normz = 0;
		normu = 0;
		normv = 0;
		normale.set( 0,0,0 );
		faceIds.clear();
	}
	
	public void add( GPoint src ) {
		x += src.x;
		y += src.y;
		z += src.z;
		u += src.u;
		v += src.v;
		normx += src.normx;
		normy += src.normy;
		normz += src.normz;
		normu += src.normu;
		normv += src.normv;
	}
	
	public void multiply( float m ) {
		x *= m;
		y *= m;
		z *= m;
		u *= m;
		v *= m;
		normx *= m;
		normy *= m;
		normz *= m;
		normu *= m;
		normv *= m;
	}
	
}