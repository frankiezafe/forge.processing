public class StreetChunk extends PVector {

	public final static int STREET_UNDEFINED = 	0; // zero connection
	public final static int STREET_END = 		1; // one connection
	public final static int STREET_NORMAL = 	2; // two connections
	public final static int STREET_CROSSROAD = 	3; // >2 connections
	
	private int type; 
	private ArrayList< StreetChunk > connections;
	
	public StreetChunk( float x, float y ) {
		super( x, y );
		type = STREET_UNDEFINED;
		connections = new ArrayList< StreetChunk >();	
	}
	
	public StreetChunk( float x, float y, StreetChunk other ) {
		super( x, y );
		type = STREET_UNDEFINED;
		connections = new ArrayList< StreetChunk >();
		connect( other );
	}
	
	public void connect( StreetChunk other ) {
		if ( connections.contains( other ) ) return;
		connections.add( other );
		// type automatic adaptation
		int csize = connections.size();
		if ( csize == 1 ) type = STREET_END;
		else if ( csize == 2 ) type = STREET_NORMAL;
		else type = STREET_CROSSROAD;
		other.connect( this );
	}

	public StreetChunk getFirstConnection() {
		return connections.get( 0 );
	}
	
	public int getType() {
		return type;
	}

	public ArrayList<StreetChunk> getConnections() {
		return connections;
	}
		
}