import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PVector;public class GFace implements PConstants {

	private int UID; 
	private GPoint p0;
	private GPoint p1;
	private GPoint p2;
	private GPoint center;
	private PVector normale;
	
	public GFace( int UID, GPoint p0, GPoint p1, GPoint p2 ) {
		
		this.UID = UID;
		this.p0 = p0;
		this.p1 = p1;
		this.p2 = p2;
		
		this.p0.faceIds.add( UID );
		this.p1.faceIds.add( UID );
		this.p2.faceIds.add( UID );
		
		center = new GPoint();
		normale = new PVector();
		
		update();
		
	}
	
	public void update() {
		
		// points may have changed => render of center + normale
		center.reset();
		center.add( p0 );
		center.add( p1 );
		center.add( p2 );
		center.multiply( 1.f / 3 );
		
//	    PVector u = new PVector (
//            p1.x - p0.x,
//            p1.y - p0.y,
//            p1.z - p0.z
//        );
//	    PVector v = new PVector (
//            p2.x - p0.x,
//            p2.y - p0.y,
//            p2.z - p0.z
//        );
//        u.normalize();
//        v.normalize();
//        u.cross( v, normale );
//        normale.normalize();
        
        normale = renderNormale( p0, p1, p2 );
		
	}

	public int getUID() {
		return UID;
	}

	public GPoint getP0() {
		return p0;
	}

	public void setP0(GPoint p0) {
		this.p0 = p0;
		update();
	}

	public GPoint getP1() {
		return p1;
	}

	public void setP1(GPoint p1) {
		this.p1 = p1;
		update();
	}

	public GPoint getP2() {
		return p2;
	}

	public void setP2(GPoint p2) {
		this.p2 = p2;
		update();
	}

	public GPoint getCenter() {
		return new GPoint( center );
	}

	public PVector getNormale() {
    PVector p = new PVector();
    p.add( normale );
		return p;
	}
	
	public GPoint[] getOthers( GPoint gp ) {
		if ( gp == p0 ) return new GPoint[]{ p1,p2 };
		else if ( gp == p1 ) return new GPoint[]{ p2,p0 };
		else if ( gp == p2 ) return new GPoint[]{ p0,p1 };
		else return null;
	}
	
	public boolean contains( GPoint p ) {
		if ( p0 == p ) return true;
		if ( p1 == p ) return true;
		if ( p2 == p ) return true;
		return false;
	}
	
	public float dist2d( float x, float y ) {
		float out = 0;
		out += PApplet.dist( x, y, p0.x, p0.y );
		out += PApplet.dist( x, y, p1.x, p1.y );
		out += PApplet.dist( x, y, p2.x, p2.y );
		out /= 3;
		return out;
	}
	
}