public class Ground implements PConstants {

	public static final int GROUND_DISPLAY_TRIANGLES = 	0;
	public static final int GROUND_DISPLAY_QUADS = 		1;
	public static final int GROUND_DISPLAY_LINES = 		2;
	public static final int GROUND_DISPLAY_INFO = 		3;
	
	private PApplet parent;
	private PImage imap;
	private PImage imtex;
	private PGraphics dyntex;
	
	private int gridx;
	private int gridy;
	private float cellwidth;	// used to get faces 
	private float cellheight;
	private float depth;
	private int ptsnum;
	private GPoint points[];
	private int facesnum;
	private GFace faces[];
	private float[] uvscale;
	private int displayMode;
	
	private float[] elevation;
	private float[] groundtype;
	
	public Ground( PApplet parent ) {
		
		this.parent = parent;
		imap = null;
		imtex = null;
		dyntex = null;
		gridx = 0;
		gridy = 0;
		ptsnum = 0;
		points = null;
		faces = null;
		depth = 200;
		uvscale = new float[]{ 1,1 };
		displayMode = GROUND_DISPLAY_TRIANGLES;
		elevation = null;
		groundtype = null;
	
	}
	
	public void init( String path, int gridx, int gridy ) {
		
		if ( imap != null ) {
			System.err.println( "Ground already initialised" );
			return;
		}
		
		imap = parent.loadImage( path );
		
		if ( imap == null ) {
			System.err.println( "Ground loading failed!" );
			return;
		}

		this.gridx = gridx + 1;
		this.gridy = gridy + 1;
		this.cellwidth = imap.width * 1.f / gridx;
		this.cellheight = imap.height * 1.f / gridy;
		ptsnum = this.gridx * this.gridy;
		points = new GPoint[ ptsnum ];
		
		// parsing the image
		imap.loadPixels();
		
		// conversion of colors to:
		// * elevation, depending on the red channel
		// * ground type (texture control), depending on the green channel
		elevation = new float[ imap.pixels.length ];
		groundtype = new float[ imap.pixels.length ];
		for ( int i = 0; i < elevation.length; ++i ) {
			elevation[ i ] = ( ( imap.pixels[ i ] >> 16 ) & 0xFF ) / 255.f;
			groundtype[ i ] = ( ( imap.pixels[ i ] >> 8 ) & 0xFF ) / 255.f;
		}
		
		float gapx = ( imap.width - 1.f ) / gridx;
		float gapy = ( imap.height - 1.f ) / gridy;
		System.out.println( gapx + " x " + gapy );
		
		int gx = 0;
		int gy = 0;
		for ( float y = 0; y < imap.height; y += gapy, ++gy ) {
			int iy = parent.floor( y );
			for ( float x = 0; x < imap.width; x += gapx, ++gx ) {
				int pid = (int) ( x + iy * imap.width );
				float lum = elevation[ pid ];
				int vid = gx + gy * this.gridx;
				float normx = ( gx * 1.f / gridx );
				float normy = ( gy * 1.f / gridy );
				float u = normx;
				float v = normy;
				float normu = normx;
				float normv = 1 - normy;
				if ( imtex != null ) {
					u = normu * imtex.width;
					v = normv * imtex.height;
				}
				float[] vs = {
					normx * imap.width, normy * imap.height, lum * depth, u, v,
					normx, normy, lum, normu, normv
				};
				points[ vid ] = new GPoint( vs );
			}
			gx = 0;
		}
		
		// faces generation, separated for more clarity
		facesnum = gridx * gridy * 2;
		faces = new GFace[ facesnum ]; // 2 triangles per square
		int fi = 0;
		for ( int y = 0; y < gridy; ++y ) {
		for ( int x = 0; x < gridx; ++x ) {
			int i0 = x + y * this.gridx;
			int i1 = ( x + 1 ) + y * this.gridx;
			int i2 = ( x + 1 ) + ( y + 1 ) * this.gridx;
			int i3 = x + ( y + 1 ) * this.gridx;
			faces[ fi ] = new GFace(
				fi,
				points[ i0 ],
				points[ i1 ],
				points[ i3 ]
			);
			fi++;
			faces[ fi ] = new GFace(
				fi,
				points[ i1 ],
				points[ i2 ],
				points[ i3 ]
			);
			fi++;
			
		}
		}
		
		updatePointsNormale();
		
		
	}
	
	private void updateFaces() {
		for ( int i = 0; i < facesnum; ++i ) faces[ i ].update();
	}
	
	private void updatePointsNormale() {
		
		for ( int i = 0; i < ptsnum; ++i ) {
			GPoint gp = points[ i ];
			int[] fids = gp.faceIds.get();
			int fidnum = fids.length;
			int normdiv = 0;
			gp.normale.set( 0,0,0 );
			for ( int j = 0; j < fidnum; ++j ) {
				GPoint[] others = faces[ fids[ j ] ].getOthers( gp );
				if ( others == null ) continue;
				gp.normale.add( renderNormale( gp , others[ 0 ], others[ 1 ] ) );
				normdiv++;
			}
			gp.normale.mult( 1.f / normdiv );
			gp.normale.normalize();
		}
		
	}
	
	public void loadTexture( String path ) {
		
		PImage prevt = imtex;
		
		imtex = parent.loadImage( path );
		if ( imtex == null ) {
			System.err.println( "Ground texture loading failed!" );
			imtex = prevt;
			return;
		}
		
		if ( dyntex != null ) dyntex = null;
		
		for ( int i = 0; i < ptsnum; ++i ) {
			GPoint gp = points[ i ];
			gp.u = gp.normu * imtex.width;
			gp.v = gp.normv * imtex.height;
		}
		
		updateFaces();
		updatePointsNormale();
		
	}
	
	public void loadTexture( PGraphics pgraphic ) {
		
		dyntex = pgraphic;
		
		if ( dyntex == null ) {
			return;
		}
		
		if ( imtex != null ) imtex = null;
		
		for ( int i = 0; i < ptsnum; ++i ) {
			GPoint gp = points[ i ];
			gp.u = gp.normu * dyntex.width;
			gp.v = gp.normv * dyntex.height;
		}
		
		updateFaces();
		updatePointsNormale();
		
	}
	
	public void draw() {
		
		if ( imap != null ) {
			
			parent.image( imap, 0,0 );
			
			switch ( displayMode ) {
			
				case GROUND_DISPLAY_LINES:
					// LINES
					parent.stroke( 200,100,0 );
					parent.beginShape( LINES );
					for ( int y = 0; y < gridy - 1; ++y ) {
					for ( int x = 0; x < gridx - 1; ++x ) {
						int i0 = x + y * gridx;
						int i = i0;
						// top line only on first row
						if ( y == 0 ) drawVertex( i );
						++i;
						if ( y == 0 ) drawVertex( i );
						
						drawVertex( i );
						i += gridx;
						drawVertex( i );
						drawVertex( i );
						--i;
						drawVertex( i );
						// left line only on first column
						if ( x == 0 ) {
							drawVertex( i );
							drawVertex( i0 );
						}
					}	
					}
					parent.endShape();	
					break;
				
				case GROUND_DISPLAY_QUADS:
					// QUADS
					parent.noStroke();
					parent.beginShape( QUADS );
					if ( imtex != null ) parent.texture( imtex );
					if ( dyntex != null ) parent.texture( dyntex );
					for ( int y = 0; y < gridy - 1; ++y ) {
					for ( int x = 0; x < gridx - 1; ++x ) {
						int i0 = x + y * gridx;
						int i = i0;
						drawVertex( i );
						++i;
						drawVertex( i );
						drawVertex( i );
						i += gridx;
						drawVertex( i );
						drawVertex( i );
						--i;
						drawVertex( i );	
						drawVertex( i );
						drawVertex( i0 );
					}	
					}
					parent.endShape();
					break;

				case GROUND_DISPLAY_TRIANGLES:
					// TEXTURED TRIANGLES
					parent.noStroke();
					parent.beginShape( TRIANGLES );
					if ( imtex != null ) parent.texture( imtex );
					if ( dyntex != null ) parent.texture( dyntex );
					for ( int i = 0; i < facesnum; ++i ) drawFace( i ); 
					parent.endShape();
					break;
					
				case GROUND_DISPLAY_INFO:
					// FACES INFO
					parent.stroke( 250,100,0 );
					parent.noFill();
					parent.beginShape( TRIANGLES );
					for ( int i = 0; i < facesnum; ++i ) drawFace( i );
					parent.endShape();
					parent.stroke( 255,0,255 );
					parent.beginShape( LINES );
					for ( int i = 0; i < facesnum; ++i ) drawFaceNormale( i );
					parent.stroke( 0,255,255 );
					for ( int i = 0; i < ptsnum; ++i ) drawPointNormale( i );
					parent.endShape();
					break;
					
				default:
					break;
			}
		
		}
	
	}
	
	private void drawFace( int i ) {
		GFace face = faces[ i ];
		drawVertex( face.getP0() );
		drawVertex( face.getP1() );
		drawVertex( face.getP2() );
	}
	
	private void drawFaceNormale( int i ) {
		GFace face = faces[ i ];
		GPoint center = face.getCenter();
		PVector normale = face.getNormale();
		normale.mult( 30 );
		parent.vertex( center.x, center.y, center.z );
		parent.vertex( 
				center.x + normale.x, 
				center.y + normale.y, 
				center.z + normale.z
				);
	}
	
	private void drawPointNormale( int i ) {
		GPoint gp = points[ i ];
		parent.vertex( gp.x, gp.y, gp.z );
		parent.vertex( 
				gp.x + gp.normale.x * 30, 
				gp.y + gp.normale.y * 30, 
				gp.z + gp.normale.z * 30
				);
	}
	
	private void drawVertex( int i ) {
		drawVertex( points[ i ] );
	}
	
	private void drawVertex( GPoint point ) {
		parent.vertex( point.x, point.y, point.z, point.u, point.v );
	}
	
	public float getDepth() {
		
		return depth;
	
	}
	
	public void setDepth( float depth ) {
		
		if ( this.depth != depth ) {
			
			this.depth = depth;
			
			for ( int i = 0; i < ptsnum; ++i ) {
				GPoint gp = points[ i ];
				gp.z = gp.normz * depth;
			}
			
			updateFaces();
			updatePointsNormale();
			
		}
		
	}
	
	public float[] getElevation() {
		return elevation;
	}
	
	public float[] getGroundtype() {
		return groundtype;
	}
	
	public int getGridx() {
		return gridx;
	}
	
	public int getGridy() {
		return gridy;
	}
	
	public int getPtsnum() {
		return ptsnum;
	}
	
	public GPoint[] getPoints() {
		return points;
	}
	
	public float[] getUvscale() {
		return uvscale;
	}

	public int getDisplayMode() {
		return displayMode;
	}

	public void setDisplayMode(int displayMode) {
		this.displayMode = displayMode;
	}
	
	public float getWidth() {
		if ( imap == null ) return -1;
		return imap.width;
	}
	
	public float getHeight() {
		if ( imap == null ) return -1;
		return imap.height;
	}
	
	public float getCellwidth() {
		return cellwidth;
	}

	public float getCellheight() {
		return cellheight;
	}

	public GFace getFace( float x, float y ) {

		GFace sel = null;
		
		// limitation of search range
		int gx = (int) ( x / imap.width * gridx );
		int gy = (int) ( y / imap.height * gridy );
		if ( gx < 0 || gx >= gridx || gy < 0 || gy >= gridy ) {
			return sel;
		}
		int pid = gx + gy * gridx;
		
		float closest = 0;
		for ( int i = 0; i < facesnum; ++i ) {
			if ( !faces[ i ].contains( points[ pid ] ) ) continue;
			float d = faces[ i ].dist2d( x, y );
			if ( closest == 0 ) {
				closest = d;
				sel = faces[ i ];
			} else if ( closest > d ) {
				closest = d;
				sel = faces[ i ];
			}
		}
		return sel;
		
	}
	
}