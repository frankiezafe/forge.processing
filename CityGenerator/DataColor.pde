public class DataColor {

	public static final float epsilon = 2.f;
	
	private float red;
	private float green;
	private float blue;
	
	public DataColor() {
		red = 0;
		green = 0;
		blue = 0;
	}
	
	public DataColor( float r, float g, float b ) {
		red = r;
		green = g;
		blue = b;
	}
	
	public DataColor( int r, int g, int b ) {
		red = r;
		green = g;
		blue = b;
	}
	
	public int pcolor() {
		return 0xff000000 | ( (int) ( red ) << 16) | ( (int) ( green ) << 8) | (int) ( blue );
	}
	
	public boolean match( int c ) {
		float r = ( ( c >> 16 ) & 0xFF );
		float g = ( ( c >> 8 ) & 0xFF );
		float b = ( c & 0xFF );
		if (
				Math.abs( r - red ) < epsilon &&
				Math.abs( g - green ) < epsilon &&
				Math.abs( b - blue ) < epsilon
			) {
			return true;
		}
		return false;
	}
	
}