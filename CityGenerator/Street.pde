public class Street {

	private ArrayList< StreetChunk > chunks;
	
	public Street() {
		chunks = new ArrayList< StreetChunk >();
	}
	
	public StreetChunk proximity( float x, float y, float range ) {
		float closest = 0;
		for ( StreetChunk c : chunks ) {
			if ( PApplet.abs( c.x - x ) <= range && PApplet.abs( c.x - x ) <= range ) {
				return c;
			}
		}
		return null;
	}
	
	public boolean addChunck( StreetChunk origin, StreetChunk newchunk, StreetConfig conf ) {
		
		// is it crossing another point on the same road?
		StreetChunk cossedchunk = detectCross( newchunk.x, newchunk.y, conf );
		if ( cossedchunk != null && cossedchunk != origin ) {
			// crossing itself!
			cossedchunk.connect( origin );
			return false;
		}
		if ( origin != null ) {
			chunks.add( newchunk );
			newchunk.connect( origin );
		} else {
			chunks.add( newchunk );
		}
		
		return true;
		
	}
	
	public StreetChunk detectCross( float x, float y, StreetConfig conf ) {
		float range = conf.streetStep * conf.streetCrossMultiplier;
		for ( StreetChunk sc : chunks ) {
			if ( PApplet.dist( sc.x, sc.y, x, y ) < range ) {
				return sc;
			}
		}
		return null;
	}
	
	public ArrayList<StreetChunk> getChunks() {
		return chunks;
	}
	
	public StreetChunk getLastChunck() {
		if ( chunks.size() == 0 ) return null;
		return chunks.get( chunks.size() - 1 );
	}
	
}