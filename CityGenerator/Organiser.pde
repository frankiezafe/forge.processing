public class Organiser implements PConstants {
	
	private PApplet parent;
	private Ground ground;
	private PGraphics texdata;
	
	private StreetConfig streetconf;
	private StreetBuilder streetnet;
	private HouseBuilder ubuilder;
	private TextureBuilder texbuilder;
	
	private boolean displaydata;
	
	public Organiser( PApplet parent, Ground ground ) {
		
		if ( ground == null || ground.getWidth() == -1 ) {
			System.err.println( "Undefined or not initialised ground!" );
			return;
		}
		this.parent = parent;
		this.ground = ground;
		texdata = parent.createGraphics( (int) ground.getWidth(), (int) ground.getHeight(), P2D );
		resetMap();
		
    texbuilder = new TextureBuilder();
		texbuilder.buildGroundTexture( parent, ground );
		
		// displaying the data texture on the ground
//		ground.loadTexture( texdata );
		ground.loadTexture( GROUND_TEX );
		displaydata = false;
		
		streetconf = new StreetConfig();
		streetnet = new StreetBuilder( streetconf, ground );
		
		ubuilder = new HouseBuilder( ground );
		
	}

	public void setDisplaydata( boolean displaydata ) {
		if ( this.displaydata != displaydata ) {
			this.displaydata = displaydata;
			if ( displaydata ) ground.loadTexture( texdata );
			else ground.loadTexture( GROUND_TEX );
		}
	}
	
	
	public void resetMap() {
		
		texdata.beginDraw();
		texdata.background( 134 );
		texdata.noStroke();
		texdata.fill( 120 );
		for ( int y = 0; y <= texdata.height; y += 10 ) {
			int x = 0;
			if ( ( y % 20 ) == 0 ) x = 10;
			for ( ; x <= texdata.width; x += 20 ) {
				texdata.rect( x, y, 10, 10 );
			}
		}
		texdata.endDraw();
		
	}
	
	public void generateStreets( int num ) {
		System.out.println( streetnet );
		streetnet.generate( num );
		resetMap();
		texdata.beginDraw();
		streetnet.drawData( texdata );
		texdata.endDraw();
		
		ubuilder.build( texdata, 100 );
		texbuilder.streetsOnGroundTexture( parent, streetnet.getStreets() );
		
	}
	
	public void setStreetConfig( StreetConfig src ) {
		streetconf.set( src );
	}
	
	public void drawHouses() {
		
		parent.noStroke();
		parent.fill( 255 );
		ArrayList< House > houses = ubuilder.getHouses();
		for ( House h : houses ) {
			parent.pushMatrix();
			parent.translate( h.x, h.y, h.z );
			parent.rotateZ( h.rotz );
			parent.box( h.width, h.depth, h.height );
			parent.popMatrix();
//			parent.line( h.x, h.y, h.z, h.face.getCenter().x, h.face.getCenter().y, h.face.getCenter().z );
		}
		
	}
		
}
