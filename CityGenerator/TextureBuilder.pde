// configuration
float ROCK_START_HEIGHT =   0.8f;
float ROCK_GRASS_GRADIENT =   0.2f;
float WATER_STOP_HEIGHT =   0;
float STREET_WALKWAY_WIDTH =  7.f;
float STREET_ROAD_WIDTH =    4.f;
float STREET_WHITE_LINE =    1.f;
int PATTERN_SIZE =       10;

// colors
int[] COLOR_WATER =       { 0, 38, 190, 255 };
int[] COLOR_WALKWAY =     { 150, 150, 140, 255 };
int[] COLOR_ROAD =       { 20, 20, 20, 255 };
int[] COLOR_WHITELINE =    { 250, 250, 250, 100 };

// textures
public PGraphics INTERNAL_GROUND_TEX = null;
public PGraphics GROUND_TEX = null;
public PGraphics INTERNAL_CONSTRUCTIBLE_TEX = null;
public PGraphics CONSTRUCTIBLE_TEX = null;

public class TextureBuilder implements PConstants {


  public static final String ROCK_TEX_PATH = "textures/rock.jpg";
  public static final String GRASS_TEX_PATH = "textures/grass.jpg";

  public void buildGroundTexture( PApplet parent, Ground ground ) {

    PImage rockstex = parent.loadImage( ROCK_TEX_PATH );
    PImage grasstex = parent.loadImage( GRASS_TEX_PATH );
    int watercol = color( COLOR_WATER[ 0 ], COLOR_WATER[ 1 ], COLOR_WATER[ 2 ] );

    int gw = (int) ground.getWidth();
    int gh = (int) ground.getHeight();

    // external
    GROUND_TEX = parent.createGraphics( gw, gh, P2D );

    // internal
    INTERNAL_GROUND_TEX = parent.createGraphics( gw, gh, P2D );
    INTERNAL_GROUND_TEX.beginDraw();
    INTERNAL_GROUND_TEX.background( 127 );
    INTERNAL_GROUND_TEX.endDraw();

    // temp text full of grass
    PGraphics allrock = parent.createGraphics( gw, gh, P2D );
    allrock.beginDraw();
    for ( int y = 0; y < gh; y += PATTERN_SIZE ) {
      for ( int x = 0; x < gw; x += PATTERN_SIZE ) {
        allrock.image( rockstex, x, y, PATTERN_SIZE, PATTERN_SIZE );
      }
    }
    allrock.endDraw();

    // temp tex full of rock
    PGraphics allgrass = parent.createGraphics( gw, gh, P2D );
    allgrass.beginDraw();
    for ( int y = 0; y < gh; y += PATTERN_SIZE ) {
      for ( int x = 0; x < gw; x += PATTERN_SIZE ) {
        allgrass.image( grasstex, x, y, PATTERN_SIZE, PATTERN_SIZE );
      }
    }
    allgrass.endDraw();

    // based on altitude, mixed the textures
    // should be done via shader to be efficient
    INTERNAL_GROUND_TEX.loadPixels();
    allrock.loadPixels();
    allgrass.loadPixels();
    float[] types = ground.getGroundtype();
    int typesn = types.length;
    float grass_stop_height = ROCK_START_HEIGHT - ROCK_GRASS_GRADIENT;
    for ( int i = 0; i < typesn; ++i ) {
      float t = types[ i ];
      if ( t > ROCK_START_HEIGHT ) {
        INTERNAL_GROUND_TEX.pixels[ i ] = allrock.pixels[ i ];
      } else if ( t > grass_stop_height ) { 
        float ratio = ( t - grass_stop_height ) / ROCK_GRASS_GRADIENT;
        float ratioi = 1 - ratio;
        float rr = ( allrock.pixels[ i ] >> 16 ) & 0xFF;
        float rg = ( allrock.pixels[ i ] >> 8 ) & 0xFF;
        float rb = ( allrock.pixels[ i ] ) & 0xFF;
        float gr = ( allgrass.pixels[ i ] >> 16 ) & 0xFF;
        float gg = ( allgrass.pixels[ i ] >> 8 ) & 0xFF;
        float gb = ( allgrass.pixels[ i ] ) & 0xFF;
        INTERNAL_GROUND_TEX.pixels[ i ] = color(
          rr * ratio + gr * ratioi, 
          rg * ratio + gg * ratioi, 
          rb * ratio + gb * ratioi, 
          255
          );
      } else if ( t > WATER_STOP_HEIGHT ) {
        INTERNAL_GROUND_TEX.pixels[ i ] = allgrass.pixels[ i ];
      } else {
        INTERNAL_GROUND_TEX.pixels[ i ] = watercol;
      }
    }
    INTERNAL_GROUND_TEX.updatePixels();

    GROUND_TEX.beginDraw();
    GROUND_TEX.image( INTERNAL_GROUND_TEX, 0, 0 );
    GROUND_TEX.endDraw();
  }

  private void streetsOnGroundTexture( ArrayList< Street > streets ) {

    int gth = GROUND_TEX.height;

    for ( Street st : streets ) {
      ArrayList< StreetChunk > chunks = st.getChunks();
      for ( StreetChunk sc : chunks ) {
        if ( sc.getType() != StreetChunk.STREET_UNDEFINED ) {
          StreetChunk other = sc.getFirstConnection();
          GROUND_TEX.line( other.x, gth - other.y, sc.x, gth - sc.y );
        }
        if ( sc.getType() == StreetChunk.STREET_CROSSROAD ) {
          ArrayList<StreetChunk> connections = sc.getConnections();
          for ( StreetChunk other : connections ) {
            GROUND_TEX.line( other.x, gth - other.y, sc.x, gth - sc.y );
          }
        }
      }
    }
  }

  public void streetsOnGroundTexture( PApplet parent, ArrayList< Street > streets ) {

    int walkwaycol = color( COLOR_WALKWAY[ 0 ], COLOR_WALKWAY[ 1 ], COLOR_WALKWAY[ 2 ] );
    int roadcol = color( COLOR_ROAD[ 0 ], COLOR_ROAD[ 1 ], COLOR_ROAD[ 2 ] );
    int whilelinecol = color( COLOR_WHITELINE[ 0 ], COLOR_WHITELINE[ 1 ], COLOR_WHITELINE[ 2 ], COLOR_WHITELINE[ 3 ] );

    GROUND_TEX.beginDraw();
    GROUND_TEX.image( INTERNAL_GROUND_TEX, 0, 0 );
    GROUND_TEX.strokeWeight( STREET_WALKWAY_WIDTH );
    GROUND_TEX.stroke( walkwaycol );
    streetsOnGroundTexture( streets );
    GROUND_TEX.strokeWeight( STREET_ROAD_WIDTH );
    GROUND_TEX.stroke( roadcol );
    streetsOnGroundTexture( streets );
    GROUND_TEX.strokeWeight( STREET_WHITE_LINE );
    GROUND_TEX.stroke( whilelinecol );
    streetsOnGroundTexture( streets );
    GROUND_TEX.endDraw();
  }

}
