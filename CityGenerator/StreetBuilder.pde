public class StreetBuilder implements PConstants {

	private Ground ground;
	private ArrayList< Street > streets;
	private float[] bounds;
	private StreetConfig conf;
	private Random rand;

	public StreetBuilder( StreetConfig conf, Ground ground ) {
		
		this.conf = conf;
		this.ground = ground;
		streets = new ArrayList< Street >();
		rand = new Random();
		bounds = new float[ 4 ];
		bounds[ 0 ] = 0;
		bounds[ 1 ] = 0;
		bounds[ 2 ] = ground.getWidth();
		bounds[ 3 ] = ground.getHeight();
	
	}
	
	public void clear() {
		streets.clear();
	}
	
	public void generate( int num ) {
		generate( num, true );
	}
	
	public void generate( int num, boolean reset ) {
		if ( reset ) clear();
		for ( int i = 0; i < num; ++i ) generateStreet();
	}
	
	private void generateStreet() {
		
		// choose a border to start from
		PVector pos = new PVector( bounds[ 0 ], bounds[ 1 ] );
		PVector dir = new PVector( 0,0 );
		PVector workv = new PVector();
		float r = rand.nextFloat() * 4;
		if ( r < 1 ) {
			// top
			pos.x += rand.nextFloat() * bounds[ 2 ];
			dir.set( 0, 1 );
		} else if ( r < 2 ) {
			// left
			pos.x += bounds[ 2 ];
			pos.y += rand.nextFloat() * bounds[ 3 ];
			dir.set( -1, 0 );
		} else if ( r < 3 ) {
			// bottom
			pos.x += rand.nextFloat() * bounds[ 2 ];
			pos.y += bounds[ 3 ];
			dir.set( 0, -1 );
		} else {
			// right
			pos.y = rand.nextFloat() * bounds[ 3 ];
			dir.set( 1, 0 );
		}
		dir.normalize();
		
		StreetChunk lastchunck = null;
		StreetChunk newchunck = null;
		
		Street nstreet = new Street();
		newchunck = new StreetChunk( pos.x, pos.y );
		nstreet.addChunck( lastchunck, newchunck, conf );
		workv.set( dir );
		workv.mult( conf.streetStep );
		pos.add( workv );
		lastchunck = newchunck;
		newchunck = new StreetChunk( pos.x, pos.y );
		nstreet.addChunck( lastchunck, newchunck, conf );
		
		int iterations = 0;
		while( 
			pos.x > 0 && 
			pos.y > 0 && 
			pos.x < bounds[ 2 ] && 
			pos.y < bounds[ 3 ] &&
			iterations < conf.streetMaxIterations
			) {
			++iterations;
			dir.x += -conf.streetDeviation + rand.nextFloat() * conf.streetDeviation  * 2;
			dir.y += -conf.streetDeviation + rand.nextFloat() * conf.streetDeviation  * 2;
			dir.z = 0;
			if ( conf.streetGroundInfluence > 0 ) {
				GFace f = ground.getFace( pos.x, pos.y );
				if ( f != null ) {
					workv.set( f.getNormale() );
					workv.mult( conf.streetGroundInfluence );
					dir.x += workv.x;
					dir.y += workv.y;
				}
			}
			dir.normalize();
			workv.set( dir );
			workv.mult( conf.streetStep );
			pos.add( workv );
			
			lastchunck = nstreet.getLastChunck();
			newchunck = new StreetChunk( pos.x, pos.y );
			boolean keepon = true;
			// new chunk might cross another street...
			for ( Street st : streets ) {
				StreetChunk crsschunk = st.detectCross( newchunck.x, newchunck.y, conf );
				if ( crsschunk != null ) {
					lastchunck.connect( crsschunk );
					keepon = false;
					break;
				}
			}
			if ( keepon ) keepon = nstreet.addChunck( lastchunck, newchunck, conf );
			if ( !keepon ) break;

		}
		
		streets.add( nstreet );
//		System.out.println( "new street with " + nstreet.getChunks().size() );
		
	}
	
	private void drawData( PGraphics texdata, float w, int c ) {

		texdata.strokeWeight( w );
		texdata.stroke( c );
		for ( Street st : streets ) {
			ArrayList< StreetChunk > chunks = st.getChunks();
			for ( StreetChunk sc : chunks ) {
				if ( sc.getType() != StreetChunk.STREET_UNDEFINED ) {
					StreetChunk other = sc.getFirstConnection();
					texdata.line( other.x, other.y, sc.x, sc.y );
				}
				if ( sc.getType() == StreetChunk.STREET_CROSSROAD ) {
					ArrayList<StreetChunk> connections = sc.getConnections();
					for ( StreetChunk other : connections ) {
						texdata.line( other.x, other.y, sc.x, sc.y );
					}
				}
			}
		}

	}
	
	public void drawData( PGraphics texdata ) {
		
		drawData( texdata, conf.buildWidth, buildable_color.pcolor() );
		drawData( texdata, conf.streetWidth, street_color.pcolor() );
		
		// displaying street chunks by type
//		texdata.noStroke();
//		for ( Street st : streets ) {
//			ArrayList< StreetChunk > chunks = st.getChunks();
//			for ( StreetChunk sc : chunks ) {
//				switch( sc.getType() ) {
//					case StreetChunk.STREET_CROSSROAD:
//						texdata.fill( 255, 0, 0 );
//						texdata.ellipse( sc.x, sc.y, 10, 10 );
//						break;
//					case StreetChunk.STREET_NORMAL:
//						texdata.fill( 0, 255, 255 );
//						texdata.ellipse( sc.x, sc.y, 5, 5 );
//						break;
//					case StreetChunk.STREET_END:
//						texdata.fill( 255 );
//						texdata.ellipse( sc.x, sc.y, 15, 15 );
//						break;
//					case StreetChunk.STREET_UNDEFINED:
//						texdata.fill( 0, 0, 255 );
//						texdata.ellipse( sc.x, sc.y, 5, 5 );
//						break;
//				
//				}
//			}
//		}
		
		// verification of faces extraction
//		target.strokeWeight( 2 );
//		for ( Street st : streets ) {
//			
//			ArrayList< StreetChunk > chunks = st.getChunks();
//			StreetChunk prev = null;
//			for ( StreetChunk sc : chunks ) {
//				GFace f = ground.getFace( sc.x, sc.y );
//				GPoint gpoint = null;
//				if ( f != null ) {
//					target.noStroke();
//					target.fill( 0,255,0 );
//					target.beginShape( TRIANGLES );
//					gpoint = f.getP0();
//					target.vertex( gpoint.x, gpoint.y );
//					gpoint = f.getP1();
//					target.vertex( gpoint.x, gpoint.y );
//					gpoint = f.getP2();
//					target.vertex( gpoint.x, gpoint.y );
//					target.endShape();
//				}
//				if ( prev != null ) {
//					target.stroke( 255,0,0 );
//					target.line( prev.x, prev.y, sc.x, sc.y );
//				}
//				prev = sc;
//			}
//		}
		
	}

	public ArrayList<Street> getStreets() {
		return streets;
	}
	
	
	
}