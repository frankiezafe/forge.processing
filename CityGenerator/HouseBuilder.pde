public class HouseBuilder implements PConstants {

	private Ground ground;
	private Random rand;
	
	private ArrayList< House > houses;
	
	public HouseBuilder( Ground ground ) {
		
		this.ground = ground;
		rand = new Random();
		houses = new ArrayList< House >();
		
	}
	
	public void build( PGraphics texdata, int num ) {

		houses.clear();
		
		texdata.loadPixels();
		int pxnum = texdata.width * texdata.height;
		for ( int i = 0; i < num; ++i ) {
			int maxtry = 2000;
			while( maxtry > 0 ) {
				int px = (int) ( rand.nextFloat() * pxnum );
				if ( buildable_color.match( texdata.pixels[ px ] ) ) {
					// SUCCESS!
					int y = px / texdata.width;
					int x = px - ( y * texdata.width );

					// searching closest road and randering the orientation of the house
					int closestredx = -1;
					int closestredy = -1;
					float closestreddist = 0;
					
					for ( int iy = y - 20; iy <= y + 20; ++iy ) {
						if ( iy < 0 ) continue;
						if ( iy >= texdata.height ) break;
						for ( int ix = x - 20; ix <= x + 20; ++ix ) {
							if ( ix < 0 ) continue;
							if ( ix >= texdata.width ) break;
							int iid = ix + iy * texdata.width;
							// avoiding 2 tests
							float d = PApplet.dist( ix, iy, x, y );
							int pxcolor = texdata.pixels[ iid ];
							boolean pxstreet = street_color.match( pxcolor );
							if ( d <= 10 ) {
								if ( buildable_color.match( pxcolor ) ) {
									texdata.pixels[ iid ] = garden_color.pcolor();
								} else if ( pxstreet ) {
									if ( closestredx == -1 || closestreddist > d ) {
										closestredx = ix;
										closestredy = iy;
										closestreddist = d;
									}
								}
							} else if ( pxstreet ) {
								if ( closestredx == -1 || closestreddist > d ) {
									closestredx = ix;
									closestredy = iy;
									closestreddist = d;
								}
							}
						}
					}
					
					texdata.pixels[ px ] = house_color.pcolor();
					GFace f = ground.getFace( x, y );
					// dists to points
					float d0 = PApplet.dist( x, y, f.getP0().x, f.getP0().y );
					float d1 = PApplet.dist( x, y, f.getP1().x, f.getP1().y );
					float d2 = PApplet.dist( x, y, f.getP2().x, f.getP2().y );
					float totald = d0 + d1 + d2;
					float z = f.getP0().z * d0 / totald + f.getP1().z * d1 / totald + f.getP2().z * d2 / totald;
					House newhouse = new House( x, y, z, 10, 25, 5 );
					if ( closestredx != -1 ) {
						newhouse.rotz = PApplet.atan2( closestredy - y, closestredx - x ) + HALF_PI;
					}
					houses.add( newhouse );
					break;
				}
				--maxtry;
			}
		}
		texdata.updatePixels();
		
	}

	public ArrayList<House> getHouses() {
		return houses;
	}
	
}