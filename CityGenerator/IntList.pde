public class IntList {
	
	private int[] list = new int[ 10 ];
	private int size = 0;
	
	public int[] get() {
		return Arrays.copyOf( list, size );
	}

	public int getSize() {
		return size;
	}
	
	public int at( int i ) {
		if ( i < 0 || i > size ) {
			return 0;
		}
		return list[ i ];
	}
	
	public void clear() {
		list = new int[ 10 ];
		size = 0;
	}
	
	public void add(int i) {
		ensureCapacity();
		list[size++] = i;
	}

	public int remove(int idx) {
		if (idx > size) {
			throw new ArrayIndexOutOfBoundsException(idx);
		}
		int move = size - idx - 1;
		int el = list[idx];
		if (move > 0) {
			System.arraycopy(list, idx + 1, list, idx, move);
		}
		size--;
		return el;
	}

	private void ensureCapacity() {
		if (size == list.length) {
			list = Arrays.copyOf(list, list.length * 3);
		}
	}
	
}