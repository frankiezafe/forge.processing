
public void loadConfig( int c ) {

  resetAll();

  ltravs.clear();

  if ( c == C_MOHR2 ) {
    display_color = color( 255 );
    MOVEEACH = 1;
    HISTORYSIZE = 2;
    TRAVTIME2LIVE = 80;
    grid = new Grid( 8, 300 );
  } else if ( c == C_MOHR4 ) {
    display_color = color( 255 );
    TRAVTIME2LIVE = 30;
    MOVEEACH = 1;
    HISTORYSIZE = 2;
    grid = new Grid( 3, 100 );
  } else if ( c == C_RESEAU ) {
    display_color = color( 255 );
    TRAVTIME2LIVE = 40;
    MOVEEACH = 1;
    HISTORYSIZE = 10;
    grid = new Grid( 30, 30 );
    vue_vitesse = 0.5f;
  } else if ( c == C_RESEAU2 ) {
    grid = new Grid( 20, 20 );
  } else {
    MOVEEACH = 1;
    HISTORYSIZE = 40;
    TRAVTIME2LIVE = 100;
    grid = new Grid( 100, 50 );
    grid.toggleLines();
    vue_vitesse = 0.5f;
  }

  conf = c;
}

public void applyConfig() {

  if ( conf == C_RESEAU ) {
    //scale(1.5);
    if ( freq_centre_moyenne > 0.3 ) {
      grid.toggleVibrations();
      for ( int i = 0; i < 2; i++ ) {

        // pas de limites, possibilité de noir et blanc
        /*
        int ltc = color( 
         freq_centre_indiv[ 0 ] * 127 + freq_centre_indiv[ 1 ] * 127,
         freq_centre_indiv[ 2 ] * 127 + freq_centre_indiv[ 3 ] * 127,
         freq_centre_indiv[ 4 ] * 127 + freq_centre_indiv[ 4 ] * 127,
         40 + energie_moyenne * 120 );
         */

        // minimum 55 sur chaque canal
        //        int ltc = color( 
        //          255 + ( freq_centre_indiv[ 0 ] * 127 * phase + freq_centre_indiv[ 1 ] * 127 * phase ),
        //          255 + ( freq_centre_indiv[ 2 ] * 10 + freq_centre_indiv[ 3 ] * 10 ),
        //          255 + ( freq_centre_indiv[ 4 ] * 10 + freq_centre_indiv[ 4 ] * 10 ),40 + energie_moyenne * 120 );

        //int ltc = color(255,0,0,60);
        int ltc = color( random( 180, 255 ), random( 0 ), random( 0, 80 ), 80 );
        float mx = RENDER_WIDTH * 0.5 + cos( energie_distri_polaire[ 1 ] ) * RENDER_WIDTH * energie_distri_polaire[ 0 ] * 15;
        float my = RENDER_HEIGHT * 0.5 + sin( energie_distri_polaire[ 1 ] ) * RENDER_HEIGHT * energie_distri_polaire[ 0 ] * 15;
        LineTraveller lt = new LineTraveller( grid.getClosestPoint( mx, my, 0 ), ltc );
        ltravs.add( lt );

        if (phase > 0.08) {
          int ltc2 = color( random( 180, 255 ), random( 0 ), random( 0, 80 ), 80 );
          float mx2 = RENDER_WIDTH * 0.5 + cos( energie_distri_polaire[ 1 ] ) * RENDER_WIDTH * energie_distri_polaire[ 0 ] * 15;
          float my2 = RENDER_HEIGHT * 0.5 + sin( energie_distri_polaire[ 1 ] ) * RENDER_HEIGHT * energie_distri_polaire[ 0 ] * 15;
          LineTraveller lt2 = new LineTraveller( grid.getClosestPoint( mx2, my2, 0 ), ltc2 );
          ltravs.add( lt2 );
        }
      }
    }

    if ( freq_centre_moyenne < 0.65 ) {
      // grid.toggleVibrations();
      MOVEEACH = 10;
    } else {
      MOVEEACH = 1;
    }
    // MOVEEACH = (int) ( 10 - mapClamped( freq_centre_moyenne, 0.4, 0.6, 0, 1 ) * 9 );
    // TRAVTIME2LIVE = (int) ( 3 + mapClamped( freq_centre_moyenne, 0.4, 0.6, 0, 1 ) * 100 );
    if ( freq_centre_moyenne > 0.5 ) {

      // les flammes
      //int ltc = color(255,0,0,30);
      int ltc = color( random( 200, 255 ), random( 0 ), random( 0, 40 ), 60 );

      float mx = RENDER_WIDTH * 0.5 + cos( energie_distri_polaire[ 1 ] ) * RENDER_WIDTH * energie_distri_polaire[ 0 ] * 15;
      float my = RENDER_HEIGHT * 0.5 + sin( energie_distri_polaire[ 1 ] ) * RENDER_HEIGHT * energie_distri_polaire[ 0 ] * 15;
      LineTraveller lt = new LineTraveller( grid.getClosestPoint( mx, my, 0 ), ltc );
      ltravs.add( lt );

      if ( energie_distri_polaire[ 0 ] > 0.5  ) {
        //  grid.toggleVibrations();
      }
    }
    if ( phase > 0 ) { 
      grid.togglePoints();
    }
    if ( freq_centre_moyenne > 0.6 ) {
      // grid.toggleVibrations();
    }
  }

  if ( conf == C_MOHR2 ) {

    // if ( timerOsc > 0 && timerOsc < 600 ) {
    // }

    if ( freq_centre_moyenne > 0.5 ) {

      if ( freq_centre_moyenne > 0.2 ) {
        float mx = RENDER_WIDTH * 0.5 + cos( energie_distri_polaire[ 1 ] ) * RENDER_WIDTH * energie_distri_polaire[ 0 ] * 20;
        float my = RENDER_HEIGHT * 0.5 + sin( energie_distri_polaire[ 1 ] ) * RENDER_HEIGHT * energie_distri_polaire[ 0 ] * 10;
        for ( int i = 0; i < energie_moyenne * 500; i++ ) {
          ltravs.add( new LineTraveller( grid.getClosestPoint( mx, my, 0 ) ) );
        }

        if ( freq_centre_indiv[ 1 ]  > 0.7  ) {
          grid.toggleVibrations();
        }

        if ( energie_moyenne  > 0.15  ) {
          grid.toggleVibrations();
        }

        if ( energie_distri_polaire[ 1 ]  > 5  ) {  
          //grid.toggleTriangles();
        }
        if ( freq_centre_moyenne > 0  ) {
          grid.togglePoints();
        }
      }
      //      if (energie_moyenne <= 0.15) {
      //        camera(RENDER_WIDTH/2.0, RENDER_HEIGHT/2.0, (RENDER_HEIGHT/2) / tan(PI*30.0 / 180.0), RENDER_WIDTH/2.0, RENDER_HEIGHT/2.0, 0, 0, 1, 0);
      //      } else {
      //        camera(RENDER_WIDTH/2, RENDER_HEIGHT/3, (RENDER_HEIGHT/3) / tan(PI*30.0 / 180.0), RENDER_WIDTH/2.0, RENDER_HEIGHT/2.0, 0, 0, 1, 0);
      //      }
    }
  }

  if ( conf == C_MOHR4 ) {


    if ( freq_centre_moyenne > 0.2 ) {
      float mx = RENDER_WIDTH * 0.5 + cos( energie_distri_polaire[ 1 ] ) * RENDER_WIDTH * energie_distri_polaire[ 0 ] * 20;
      float my = RENDER_HEIGHT * 0.5 + sin( energie_distri_polaire[ 1 ] ) * RENDER_HEIGHT * energie_distri_polaire[ 0 ] * 10;
      for ( int i = 0; i < energie_moyenne * 500; i++ ) {
        ltravs.add( new LineTraveller( grid.getClosestPoint( mx, my, 0 ) ) );
      }

      if ( freq_centre_indiv[ 1 ]  > 0.7  ) {
        grid.toggleVibrations();
      }

      if ( energie_moyenne  > 0.15  ) {
        grid.toggleVibrations();
      }

      if ( energie_distri_polaire[ 1 ]  > 6  ) {  
        //grid.toggleTriangles();
        // scale(2);
      }
      if ( freq_centre_moyenne > 0  ) {
        grid.togglePoints();
      }
      if ( phase == 0.1  ) {    
        scale(10);
      }
    }
    //    if (energie_moyenne <= 0.15)  
    //    {
    //      camera(RENDER_WIDTH/2.0, RENDER_HEIGHT/2.0, (RENDER_HEIGHT/2) / tan(PI*30.0 / 180.0), RENDER_WIDTH/2.0, RENDER_HEIGHT/2.0, 0, 0, 1, 0);
    //    } else {
    //      camera(RENDER_WIDTH/2, RENDER_HEIGHT/1.90, (RENDER_HEIGHT/1.90) / tan(PI*30.0 / 180.0), RENDER_WIDTH/2.0, RENDER_HEIGHT/2.0, 0, 0, 1, 0);
    //    }
  }
}

public float mapClamped( float v, float start_entree, float stop_entree, float start_sortie, float stop_sortie ) {

  float out = map( v, start_entree, stop_entree, start_sortie, stop_sortie );
  if ( v < start_sortie ) {
    v = start_sortie;
  } else if ( v > stop_sortie ) {
    v = stop_sortie;
  }
  return v;
}

