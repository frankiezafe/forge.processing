
class GridLine {

  public GridPoint p1;
  public GridPoint p2;
  public boolean horizontal;
  public boolean vertical;
  public boolean enabled;
  
  public GridLine( GridPoint p1, GridPoint p2 ) {
    
    this.p1 = p1;
    this.p2 = p2;
    horizontal = false;
    vertical = false;
    enabled = true;
    
    p1.setLine( this, p2 );
    p2.setLine( this, p1 );
    
  }
  
  public void update() {
    
  }

}
