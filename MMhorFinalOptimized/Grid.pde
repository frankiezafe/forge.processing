
public static float GRID_PADDING = 5.f;

class Grid {
  
  GridPoint[] points;
  GridLine[] lines;
  GridTriangle[] triangles;
  int resolutionX;
  int resolutionY;
  
  boolean borderslocked;
  boolean vibrationEnabled;
  boolean pointsEnabled;
  boolean linesEnabled;
  boolean trianglesEnabled;
  
  public Grid( int rx, int ry ) {
  
    resolutionX = rx;
    resolutionY = ry;
    
    points = new GridPoint[ rx * ry ];
    triangles = new GridTriangle[ ( rx - 1 ) * ( ry - 1 ) * 2 ];
    lines = new GridLine[ ( ( resolutionX - 1 ) * resolutionY ) + ( ( resolutionY - 1 ) * resolutionX ) + ( ( resolutionX - 1 ) * ( resolutionY - 1 ) * 2 ) ];
    
    float gapx = ( RENDER_WIDTH - GRID_PADDING * 2 ) / ( resolutionX - 1 );
    float gapy = ( RENDER_HEIGHT - GRID_PADDING * 2 ) / ( resolutionY - 1 );
    
    int li = 0;
    int ti = 0;
    
    for ( int y = 0; y < resolutionY; y++ ) {
    for ( int x = 0; x < resolutionX; x++ ) {
      int  i = x + y * resolutionX;
      points[ i ] = new GridPoint( x, y, GRID_PADDING + x * gapx, GRID_PADDING + y * gapy, 0 );
      if ( x > 0 ) {
        lines[ li ] = new GridLine( points[ i - 1 ], points[ i ] );
        lines[ li ].horizontal = true;
        li++;
      }
      if ( y > 0 ) {
        lines[ li ] = new GridLine( points[ i - resolutionX ], points[ i ] );
        lines[ li ].vertical = true;
        li++;
      }
      if ( x > 0 && y > 0 ) {
        lines[ li ] = new GridLine( points[ i - ( resolutionX + 1 ) ], points[ i ] );
        li++;
        lines[ li ] = new GridLine( points[ i - 1 ], points[ i - resolutionX ] );
        li++;
        triangles[ ti ] = new GridTriangle( points[ i - ( resolutionX + 1 ) ], points[ i - resolutionX ], points[ i - 1 ] );
        ti++;
        triangles[ ti ] = new GridTriangle( points[ i - resolutionX ], points[ i ], points[ i - 1 ] );
        ti++;
      }
    }
    }
    
    borderslocked = false;
    vibrationEnabled = false;
    pointsEnabled = true;
    linesEnabled = false;
    trianglesEnabled = false;
    
  }
  
  public void toggleLockBorders() {
    borderslocked = !borderslocked;
    for ( int y = 0; y < resolutionY; y++ ) {
    for ( int x = 0; x < resolutionX; x++ ) {
      if ( x == 0 || y == 0 || x == resolutionX - 1 || y == resolutionY - 1 ) {
        points[ x + y * resolutionX ].locked = borderslocked;
      }
    }
    }
  }
  
  public void toggleVibrations() {
    vibrationEnabled = !vibrationEnabled;
    for ( int i = 0; i < points.length; i++ ) {
      if ( vibrationEnabled ) {
        points[ i ].vibration = random( 0, 5f );
      } else {
        points[ i ].vibration = 0;
      }
    }
  }
  
  public void togglePoints() {
    pointsEnabled = !pointsEnabled;
  }
  
  public void toggleLines() {
    linesEnabled = !linesEnabled;
  }
  
  public void toggleTriangles() {
    trianglesEnabled = !trianglesEnabled;
  }
  
  
  public void pointSizes( float min, float max ) {
    for ( int i = 0; i < points.length; i++ ) {
      points[ i ].size = random( min, max );
    }
  }
  
  public GridPoint getClosestPoint( float x, float y, float z ) {
    int seli = 0;
    float closest = 0;
    for ( int i = 0; i < points.length; i++ ) {
       GridPoint gp = points[ i ];
      float d  = dist( gp.x, gp.y, gp.z, x, y, z );
      if ( i == 0 || ( closest > d ) ) {
        seli = i;
        closest = d;
      }
    }
    return points[ seli ];
  }
  
  public void draw() {
    
    update();

    if ( trianglesEnabled ) {
      render.noStroke();
      render.beginShape(TRIANGLES);
      for ( int i = 0; i < triangles.length; i++ ) {
        GridTriangle gt = triangles[ i ];
        render.fill( 255, ( gt.p1.size + gt.p2.size + gt.p3.size ) *10 );
        render.vertex( gt.p1.x, gt.p1.y, gt.p1.z );
        render.vertex( gt.p2.x, gt.p2.y, gt.p2.z );
        render.vertex( gt.p3.x, gt.p3.y, gt.p3.z );
      }
      render.endShape();
    }
    
    if ( linesEnabled ) {
      render.noFill();
      render.strokeWeight( 1.5 );
      render.beginShape( LINES );
      for ( int i = 0; i < lines.length; i++ ) {
        GridLine gl = lines[ i ];
        if ( gl.horizontal ) {
          render.stroke( 255,0,0, 200 );
        } else if ( gl.vertical ) {
          render.stroke( 0,255,0, 200 );
        } else {
          render.stroke( 255, 200 );
        }
        render.vertex( gl.p1.x, gl.p1.y, gl.p1.z );
        render.vertex( gl.p2.x, gl.p2.y, gl.p2.z );
      }
      render.endShape();
    }
    
    if ( pointsEnabled ) {
      render.stroke(255,0,0,50);
      render.beginShape( POINTS );
      for ( int i = 0; i < points.length; i++ ) {
        GridPoint gp = points[ i ];
        render.strokeWeight( gp.size * 2 );
        render.vertex( gp.x, gp.y, gp.z );
      }
      render.endShape();
    }
    
  }
  
  private void update() {
    for ( int i = 0; i < points.length; i++ ) {
      points[ i ].update();
    }
  }


}
