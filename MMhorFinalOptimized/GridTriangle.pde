
class GridTriangle {

  public GridPoint p1;
  public GridPoint p2;
  public GridPoint p3;
  public boolean enabled;
  
  public GridTriangle( GridPoint p1, GridPoint p2, GridPoint p3 ) {
    
    this.p1 = p1;
    this.p2 = p2;
    this.p3 = p3;
    enabled = true;
    
  }
  
  public void update() {
    
  }

}
