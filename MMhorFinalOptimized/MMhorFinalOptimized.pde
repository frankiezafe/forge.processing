// simple grid system
// drawing of points, lines and triangles
// based on Manfred Mohr studies

public int C_MOHR2 =  0;
public int C_MOHR4 = 1;
public int C_RESEAU = 2;
public int C_RESEAU2 = 3;
public int C_LAST =    4;

// controles de la vue
public PVector translation;
public PVector rotation;
public float echelle;
public PVector target_translation;
public PVector target_rotation;
public float target_echelle;
public float vue_vitesse;

int conf = C_LAST;

Grid grid;
ArrayList< LineTraveller > ltravs;

int RENDER_WIDTH = 1152;
int RENDER_HEIGHT = 337;
float half_width = RENDER_WIDTH * 0.5f;
float half_height = RENDER_HEIGHT * 0.5f;
PVector pos1 = new PVector( 314, 96 );
PVector pos2 = new PVector( 943, 89 );
float r1 = 0;
float r2 = -0.0849999;

PGraphics render;
PGraphics screen;

void setup() {

  frameRate( 200 );
  
  size( 1600, 540, OPENGL );
  
  render = createGraphics( RENDER_WIDTH, RENDER_HEIGHT, OPENGL );
  screen = createGraphics( RENDER_WIDTH / 2, RENDER_HEIGHT, OPENGL );
  
  smooth();
  
  translation = new PVector();
  rotation = new PVector();
  echelle = 1;
  
  target_translation = new PVector();
  target_rotation = new PVector();
  target_echelle = 1;
  vue_vitesse = 0.1f;
  
  ltravs = new ArrayList< LineTraveller >();
  
  loadConfig( conf );
  
  oscP5 = new OscP5( this, PORT_IN );
  background( 0 );
  
}

public void resetAll() {
  resetTranslation();
  resetRotation();
  resetEchelle();
  vue_vitesse = 0.1f;
}

public void resetTranslation() {
  translation.set( 0,0,0 );
  target_translation.set( 0,0,0 );
}
public void resetRotation() {
  rotation.set( 0,0,0 );
  target_rotation.set( 0,0,0 );
}
public void resetEchelle() {
  echelle = 1;
  target_echelle = 1;
}

public void setTranslation( float x, float y, float z ) {
  target_translation.set( x, y, z );
}
public void setRotation( float x, float y, float z ) {
  target_rotation.set( x, y, z );
}
public void setEchelle( float s ) {
  target_echelle = s;
}


void draw() {
  
  // lissage vue
  translation.x += ( target_translation.x - translation.x ) * vue_vitesse;
  translation.y += ( target_translation.y - translation.y ) * vue_vitesse;
  translation.z += ( target_translation.z - translation.z ) * vue_vitesse;
  rotation.x += ( target_rotation.x - rotation.x ) * vue_vitesse;
  rotation.y += ( target_rotation.y - rotation.y ) * vue_vitesse;
  rotation.z += ( target_rotation.z - rotation.z ) * vue_vitesse;
  echelle += ( target_echelle - echelle ) * vue_vitesse;
  
  // début du rendu
  render.beginDraw();
    
    if ( conf == C_MOHR2 ) {
      setEchelle( freq_centre_moyenne * 3f );
      setRotation( freq_centre_moyenne * 0.01f, freq_centre_moyenne * 0.01f, 0 );
      render.background( 0 );
    } else if ( conf == C_MOHR4 ) {
      setEchelle( freq_centre_moyenne * 3f );
      setRotation( freq_centre_moyenne * 0.01f, freq_centre_moyenne * 0.01f, 0 );
      render.background( 0 );
    } else if ( conf == C_RESEAU ) {
      setEchelle( freq_centre_moyenne * 3f );
      setRotation( freq_centre_moyenne * 0.01f, freq_centre_moyenne * 0.01f, 0 );
      render.background( 0 );
    } else if ( conf == C_RESEAU2 ) {
      render.background( 200 );
    } else {
      render.background( 5 );
      setRotation( mouseX * 0.01f, mouseY * 0.01f, 0 );
      setTranslation( mouseX, mouseY, 0 );
      setEchelle( mouseX * 0.1f );
    }
    if ( timerOsc > 1000 ) {
      timerOsc = 0;
    }
    for ( int i = 0; i < ltravs.size (); i++ ) {
      LineTraveller lt = ltravs.get( i );
      lt.update();
      if ( lt.time2live == 0 ) {
        ltravs.remove( lt );
        i--;
      }
    }
    applyConfig();
    render.pushMatrix();
      render.translate( RENDER_WIDTH * 0.5f, RENDER_HEIGHT * 0.5f, 0 );
      render.translate( translation.x, translation.y, translation.z );
      render.rotateX( rotation.x );
      render.rotateY( rotation.y );
      render.rotateZ( rotation.z );
      render.scale( echelle );
      render.translate( -RENDER_WIDTH * 0.5f, -RENDER_HEIGHT * 0.5f, 0 );
      grid.draw();
      for ( LineTraveller lt : ltravs ) {
        lt.draw();
      }
    render.popMatrix();
    
  render.endDraw();
  
  // background( 0 );
  
  // rendu écran 1
  screen.beginDraw();
  screen.image( render, 0, 0 );
  screen.endDraw();
  pushMatrix();
    translate( pos1.x + half_width, pos1.y + half_height );
    rotate( r1 );
    translate( -half_width, -half_height );
    image( screen, 0, 0 );
  popMatrix();
  
  // rendu écran 2
  screen.beginDraw();
  screen.image( render, -screen.width, 0 );
  screen.endDraw();
  pushMatrix();
    translate( pos2.x + half_width, pos2.y + half_height );
    rotate( r2 );
    translate( -half_width, -half_height );
    image( screen, 0, 0 );
  popMatrix();
  
  fill( 255 );
  displayOsc();
  text( "fps = " + frameRate, pos1.x + 10, pos1.y + 25 );
  text( "travellers = " + ltravs.size(), pos1.x + 10, pos1.y + 40 );

}

void mouseMoved() {
  
}

void mousePressed() {

  if ( conf == C_RESEAU ) {
    
    for ( int i = 0; i < 10; i++ ) {
      int ltc = color( random( 0, 80 ), random( 0, 200 ), random( 200, 210 ), 100 );
      LineTraveller lt = new LineTraveller( grid.getClosestPoint( mouseX, mouseY, 0 ), ltc );
      ltravs.add( lt );
    }
    
  } else {
    
    for ( int i = 0; i < 10; i++ ) {
      ltravs.add( new LineTraveller( grid.getClosestPoint( mouseX, mouseY, 0 ) ) );
    }
    
  }
  
}

void keyPressed() {

  if ( key == 'x' ) {
    for ( int i = 0; i < 2000; i++ ) {
      ltravs.add( new LineTraveller( grid.getClosestPoint( random( 0, width ), random( 0, height ), 0 ) ) );
    }
  } else if ( key == 'b' ) {
    grid.toggleLockBorders();
  } else if ( key == 'v' ) {
    grid.toggleVibrations();
  } else if ( key == 'm' ) {
    grid.pointSizes( 1, 1 );
  } else if ( key == 'n' ) {
    grid.pointSizes( 1, 2 );
  } else if ( key == 't' ) {
    grid.toggleTriangles();
  } else if ( key == 'l' ) {
    grid.toggleLines();
  } else if ( key == 'p' ) {
    grid.togglePoints();
  } else if ( keyCode == 32 ) { // [space]
    oscdisplayOn = !oscdisplayOn;
  } else if ( keyCode == 37 ) { // [left]
    conf--;
    if ( conf < 0 ) { conf = 0; }
    loadConfig( conf );
  } else if ( keyCode == 39 ) { // [right]
    conf++;
    if ( conf >= C_LAST ) { conf = C_LAST; }
    loadConfig( conf );
  } else {
    println( keyCode );
  }
}

