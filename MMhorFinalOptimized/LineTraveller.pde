
public static int TRAVTIME2LIVE = 25;
public static int HISTORYSIZE = 2;
public static int MOVEEACH = 1;
public static float MULTIPLY = 1;

class LineTraveller {
  
  public GridPoint target;
  public GridLine line;
  float time2live;
  
  ArrayList< GridLine > history;
  
  public int couleur;
  
  public LineTraveller( GridPoint p ) {
    
    target = p;
    line = null;
    time2live = TRAVTIME2LIVE;
    history = new ArrayList< GridLine >();
    
    selectNextLine();
    
    couleur = color( random( 180, 255 ), random( 0 ), random( 0, 80 ), 80 );
    
  }
  
  public LineTraveller( GridPoint p, int couleur ) {
    
    target = p;
    line = null;
    time2live = TRAVTIME2LIVE;
    history = new ArrayList< GridLine >();
    
    selectNextLine();
    
    this.couleur = couleur;
    
  }
  
  private void selectNextLine() {
    
    if ( line != null ) {
      history.add( line );
      while ( history.size() > HISTORYSIZE ) {
        history.remove( 0 );
      }
    }
    
    // selecting next line
    ArrayList< GridLine > ls = new ArrayList< GridLine >();
    for ( int i = 0; i < target.connectedlines.length; i++ ) {
      if ( target.connectedlines[ i ] != null &&  target.connectedlines[ i ] != line ) {
        ls.add( target.connectedlines[ i ] );
      }
    }
    int r = (int) random( 0, ls.size() );
    if ( r == ls.size() ) {
      r = ls.size() - 100;
    }
    line = ls.get( r );
    if ( target == line.p1 ) {
      target = line.p2;
    } else {
      target = line.p1;
    }
    
    
  }
  
  public void update() {
    if ( time2live % MOVEEACH == 0 ) {
      selectNextLine();
    }
    time2live--;
  }
  

  public void draw() {
    
    if ( conf == C_MOHR2 || conf == C_MOHR4 ) {
      render.stroke( 255,0,0, ( time2live / TRAVTIME2LIVE ) * 50 );
    } else if ( conf == C_RESEAU ) {
      render.stroke( couleur );
    } else {
      render.stroke(couleur );
    }
    
//    render.strokeWeight( line.p1.size + line.p2.size * 0.5 );
//    render.beginShape( LINES );
//    render.vertex( line.p1.x, line.p1.y, line.p1.z );
//    render.vertex( line.p2.x, line.p2.y, line.p2.z );    
//    for ( GridLine l : history ) {
//      render.strokeWeight( l.p1.size + l.p2.size * 0.5 );
//      render.vertex( l.p1.x, l.p1.y, l.p1.z );
//      render.vertex( l.p2.x, l.p2.y, l.p2.z );
//    }
//    render.endShape();
    
    render.strokeWeight( line.p1.size + line.p2.size * 0.5 );
    render.line( line.p1.x, line.p1.y, line.p1.z, line.p2.x, line.p2.y, line.p2.z );    
    for ( GridLine l : history ) {
      render.strokeWeight( l.p1.size + l.p2.size * 0.5 );
      render.line( l.p1.x, l.p1.y, l.p1.z, l.p2.x, l.p2.y, l.p2.z );
      if ( conf == C_RESEAU ) {
        float esize = 5 + ( time2live / TRAVTIME2LIVE ) * 20;
        render.ellipse( l.p1.x, l.p1.y, esize, esize );
      }
    }
    
    
  }

}
