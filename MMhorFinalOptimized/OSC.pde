
int PORT_IN = 8000;
int timerOsc = 0;

import oscP5.*;
import netP5.*;
  
OscP5 oscP5;

boolean oscdisplayOn = false;
String uID = "NONE";
float freq_centre_moyenne = 0;
float energie_moyenne = 0;
float[] energie_distri_cart = new float[ 2 ];
float[] energie_distri_polaire = new float[ 2 ];
float[] freq_centre_indiv = new float[ 8 ];
float[] energie_indiv = new float[ 8 ];
float phase = 0;
int display_color = color( 0 );

void oscEvent( OscMessage msg ) {
  if ( msg.checkAddrPattern( "/freq_centre_moyenne" ) ) {
    timerOsc++;
    freq_centre_moyenne = msg.get( 0 ).floatValue();
  } else if ( msg.checkAddrPattern( "/freq_centre_indiv" ) ) {
    for ( int i = 0; i < 8; i++ ) {
      freq_centre_indiv[ i ] = msg.get( i ).floatValue();
    }
  } else if ( msg.checkAddrPattern( "/energie_moyenne" ) ) {
    energie_moyenne = msg.get( 0 ).floatValue();
  } else if ( msg.checkAddrPattern( "/energie_distri_cart" ) ) {
    for ( int i = 0; i < 2; i++ ) {
      energie_distri_cart[ i ] = msg.get( i ).floatValue();
    }    
//    println( msg.typetag() );
  } else if ( msg.checkAddrPattern( "/energie_distri_polaire" ) ) {
    for ( int i = 0; i < 2; i++ ) {
      energie_distri_polaire[ i ] = msg.get( i ).floatValue();
    }    
//    println( msg.typetag() );
  } else if ( msg.checkAddrPattern( "/energie_indiv" ) ) {
    for ( int i = 0; i < 8; i++ ) {
      energie_indiv[ i ] = msg.get( i ).floatValue();
    }  
 //  println( msg.typetag() );
  } 
  else if ( msg.checkAddrPattern( "/energie_indiv" ) ) {
    for ( int i = 0; i < 8; i++ ) {
      energie_indiv[ i ] = msg.get( i ).floatValue();
    }  
 //  println( msg.typetag() );
  } 
  else if ( msg.checkAddrPattern( "/phase" ) ) {
    phase = msg.get( 0 ).floatValue();
  } 
  if ( msg.checkAddrPattern( "/unique_id" ) ) {
    uID = msg.get( 0 ).toString();
  } 
  
  else {
    println( msg.addrPattern() );
    println( msg.typetag() );
  }
}

void displayOsc() {
  
  if ( !oscdisplayOn ) {
    return;
  }
  
  int y = 25;
  int x = 25;
  fill( display_color );
  text( "unique_ID = " + uID, 10, y ); y+=15;
  text( "CONFIG = " + conf, 10, y ); y+=15;
  text( "TRAVTIME2LIVE = " + TRAVTIME2LIVE, 10, y ); y+=15;
  text( "HISTORYSIZE = " + HISTORYSIZE, 10, y ); y+=15;
  text( "MOVEEACH = " + MOVEEACH, 10, y ); y+=15;
  text( "frame rate = " + frameRate, 10, y ); y+=15;
  text( "frame count = " + frameCount, 10, y ); y+=15;
  text( "trav num = " + ltravs.size(), 10, y ); y+=15;
  text( "timer OSC = " + timerOsc, 10, y ); y+=15;
  text( "phase = " + phase, 10, y ); y+=15;
  text( "freq_centre_moyenne = " + freq_centre_moyenne, 10, y ); y+=15;
  text( "energie_moyenne = " + energie_moyenne, 10, y ); y+=15;
  for ( int i = 0; i < 2; i++ ) {
    text( "energie_distri_cart[" + i + "] = " + energie_distri_cart[ i ], 10, y ); y+=15;
  }
  for ( int i = 0; i < 2; i++ ) {
    text( "energie_distri_polaire[" + i + "] = " + energie_distri_polaire[ i ], 10, y ); y+=15;
  }
  
  for ( int i = 0; i < 8; i++ ) {
    text( "energie_indiv[" + i + "] = " + energie_indiv[ i ], 10, y ); y+=15;
    text( "freq_centre_indiv[" + i + "] = " + freq_centre_indiv[ i ], 10, y ); y+=15;
  }
    
  noFill();
  stroke( 0 );
  ellipse( 
    width * 0.5 + cos( energie_distri_polaire[ 1 ] ) * width * energie_distri_polaire[ 0 ] * 0.5, 
    height * 0.5 + sin( energie_distri_polaire[ 1 ] ) * height * energie_distri_polaire[ 0 ] * 0.5, 
    20,20
    );
  
}
