float im1[][];
float im2[][];
float energy[][];

int index;
int lastIndex;
int range = 1;
int pencil = 10;

boolean mpressed;
boolean eraser;
boolean drawe;

void setup() {

  size( 400, 400, P3D );

  PImage i1 = loadImage( "img1.png" );
  i1.loadPixels();
  im1 = new float[ width * height ][ 3 ];
  for ( int i = 0; i < width * height; i++ ) {
    im1[ i ][ 0 ] = red( i1.pixels[ i ] );
    im1[ i ][ 1 ] = green( i1.pixels[ i ] );
    im1[ i ][ 2 ] = blue( i1.pixels[ i ] );
  }

  PImage i2 = loadImage( "img2.png" );
  i2.loadPixels();
  im2 = new float[ width * height ][ 3 ];
  for ( int i = 0; i < width * height; i++ ) {
    im2[ i ][ 0 ] = red( i2.pixels[ i ] );
    im2[ i ][ 1 ] = green( i2.pixels[ i ] );
    im2[ i ][ 2 ] = blue( i2.pixels[ i ] );
  }

  energy = new float[ width * height ][ 2 ];
  for ( int i = 0; i < width * height; i++ ) {
    for ( int j = 0; j < 2; j++ ) {
      energy[ i ][ j ] = 0;
    }
  }

  index = 0;
  lastIndex = 1;
  drawe = false;
  mpressed = false;
  eraser = false;
  
}

void draw() {


  int tmpi = index;
  index = lastIndex;
  lastIndex = tmpi;

  if ( mpressed && mouseX >= 0 && mouseX < width && mouseY >= 0 && mouseY < height ) {
    
    int p = 0; 
    for ( int y = mouseY - pencil; y <= mouseY + pencil; y++ ) {
      for ( int x = mouseX - pencil; x <= mouseX + pencil; x++ ) {
        if ( x < 0 || x >= width || y < 0 || y >= height )
          continue;
        float d = 1 - ( dist( mouseX, mouseY, x, y ) / pencil );
        if ( d < 0 )
          continue;
        p = x + y * width;
        if ( eraser ) {
          d *= -1;
        }
        energy[ p ][ lastIndex ] += d;
        if ( energy[ p ][ lastIndex ] > 1 ) {
          energy[ p ][ lastIndex ] = 1;
        } else if ( energy[ p ][ lastIndex ] < 0 ) {
          energy[ p ][ lastIndex ] = 0;
        }
      }
    }
    
    mpressed = false;
  }

  loadPixels();

  for ( int y = 0; y < height; y++ ) {
    for ( int x = 0; x < width; x++ ) {

      int i = x + y * width;

      int nbrs = 0;
      float totale = 0;

      for ( int ny = y - range; ny <= y + range; ny++ ) {
        for ( int nx = x - range; nx <= x + range; nx++ ) {
          if ( nx == x && ny == y )
            continue;
          if ( nx < 0 || ny < 0 || nx >= width || ny >= height )
            continue;
          int ni = nx + ny * width;
          nbrs++;
          totale += energy[ ni ][ lastIndex ];
        }
      }

      energy[ i ][ index ] = energy[ i ][ lastIndex ] * 0.54f + ( totale / nbrs ) * 0.46f;
      if ( energy[ i ][ index ] > 1 ) {
        energy[ i ][ index ] = 1;
      } else if ( energy[ i ][ index ] < 0.0001f ) {
        energy[ i ][ index ] = 0;
      }
      
      if ( drawe ) {
        if ( energy[ i ][ index ] == 0 ) {
          pixels[ i ] = color( 0,0,255 );
        } else {
          pixels[ i ] = color( 15 + energy[ i ][ index ] * 240 );
        }
      } else {
        float e = energy[ i ][ index ];
        float ei = 1 - energy[ i ][ index ];
        pixels[ i ] = color(
            im1[ i ][ 0 ] * ei + im2[ i ][ 0 ] * e,
            im1[ i ][ 1 ] * ei + im2[ i ][ 1 ] * e,
            im1[ i ][ 2 ] * ei + im2[ i ][ 2 ] * e
        );
      }
    }
  }

  updatePixels();
  
  noFill();
  stroke( 255 );
  ellipse( mouseX, mouseY, pencil, pencil );
  
}

void keyPressed() {
  if ( key == 'e' ) {
    drawe = !drawe;
  } else  if ( key == 'x' ) {
    eraser = !eraser;
  }
}

void mousePressed() {
  mpressed = true;
}

void mouseDragged() {
  mpressed = true;
}

