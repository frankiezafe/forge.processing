class Wave {

  public static final int WAVE_RANDOM =   0;
  public static final int WAVE_NOISE =    1;
  
  private float[] yvalues;
  private float dx = 1;
  private float x = 0;
  private int period;
  private float localdephas;
  private float localdephas_speed;
  private float mapMin;
  private float mapMax;
  private float range;
  private float halfrange;
  private int method;
  private float noisev;
  private float noisecoef;
  
  public Wave( int definition ) {
    period = 1;
    yvalues = new float[ definition ];
    dx = PI * 2 / yvalues.length;
    x = 0;
    for (int i = 0; i < yvalues.length; i++) {
      yvalues[i] = sin(x);
      x += dx;
    }
    init();
  }
  
  public Wave( int definition, int period ) {
    this.period = abs(period);
    yvalues = new float[ definition ];
    dx = (PI * 2 / yvalues.length) * (float) period;
    x = 0;
    for (int i = 0; i < yvalues.length; i++) {
      yvalues[i] = sin(x);
      x += dx;
    }
    init();
  }
  
  private void init() {
    localdephas = 0;
    localdephas_speed = 0;
    mapMin = -1;
    mapMax = 1;
    range = 0;
    halfrange = 0;
    noisev = 0;
    noisecoef = 0.01f;
    method = WAVE_RANDOM;
  }
  
  // SETTERS
  public void setSpeed( float speed ) {
    localdephas_speed = speed;
  }
  
  public void setMap( float mapmin, float mapmax ) {
    if ( mapmax - mapmin <= 0 ) {
      System.err.println("min must be smaller than max! (" + mapmin +" / "+ mapmax + ")" );
      return;
    }
    mapMin = mapmin;
    mapMax = mapmax;
  }
  
  public void setRange( float range ) {
    if (range < 0 || range > 1) {
      System.err.println("range must be between [0,1]");
      return;
    }
    this.range = range;
    halfrange = range * 0.5f;
  }
  
  public void setMethod( int method ) {
    if ( method != WAVE_RANDOM &&  method != WAVE_NOISE ) {
      System.err.println("method must be WAVE_RANDOM or WAVE_NOISE");
      return;
    }
    this.method = method;
  }
  
  public void setNoiseScale( float noisescale ) {
    noisecoef = noisescale;
  }
  
  // GETTERS
  public float getValue() {
    return getValue(0);
  }
  
  public float getValue( float dephas ) {
    float reldephas =  (localdephas + dephas) * yvalues.length;
    while (reldephas < 0) { reldephas += yvalues.length; }
    while (reldephas > yvalues.length ) { reldephas -= yvalues.length; }
    int pointbelow = (int) floor( reldephas );
    if ( pointbelow >= yvalues.length )
      pointbelow = 0;
    int pointabove = pointbelow + 1;
    if ( pointabove >= yvalues.length )
      pointabove = 0;
    // need to calculate the distance between the 2 points
    float pc = reldephas - pointbelow;
    float v = ( yvalues[pointbelow] * (1-pc) ) + ( yvalues[pointabove] * pc );
    v = map( v, -1, 1, -(1-range), (1-range) );
    if (method == WAVE_RANDOM) {
      v += random( -range, range );
    } else if (method == WAVE_NOISE) {
      v += map( noise( noisev ), 0, 1, -range, range );
    }
    return map( v, -1, 1, mapMin, mapMax );
  }
  
  public void update() {
     localdephas += localdephas_speed;
     if ( method == WAVE_NOISE ) {
       noisev += noisecoef;
     }
  }
  
  public void render( float offsetx, float offsety, float w, float h ) {
        render( offsetx, offsety, w, h, 0 );
  }
  
  public void render( float offsetx, float offsety, float w, float h, float dephas ) {
    
    noFill();
    float h2 = h * 0.5f;
    
    // frame
    stroke(0, 0, 0);
    rect(offsetx, offsety, w, h);
    line( offsetx, offsety + h2, offsetx + w, offsety + h2 );
    
    float hrange = h * range;
    float hrange2 = hrange * 0.5f;
    if ( range > 0 ) {
      offsety += hrange2;
      h *= ( 1 - range);
      h2 = h * 0.5f;
    }
    float prevx = 0;

    // sine
    stroke(255, 0, 0);
    w /= PI * 2 * period;
    x = 0;
    
    // real current point:
    float reldephas =  (localdephas + dephas) * yvalues.length;
    while (reldephas < 0) { reldephas += yvalues.length; }
    while (reldephas > yvalues.length ) { reldephas -= yvalues.length; }
    // println( "relative dephas: " + (dephas / yvalues.length) );
    int pointbelow = (int) floor( reldephas );
    if ( pointbelow >= yvalues.length )
      pointbelow = 0;
    int pointabove = pointbelow + 1;
    if ( pointabove >= yvalues.length )
      pointabove = 0;
    // need to calculate the distance between the 2 points
    float pc = reldephas - pointbelow;
    float x = ( 1 - pc ) * dx;
    float currentyv =  ( yvalues[pointbelow] * (1-pc) ) + ( yvalues[pointabove] * pc );
    // float currentyv = getCurrentValue(dephas);
    
    int currenti = pointabove;
    int lasti = -1;
    
    float lx1 = 0, ly1 = 0, lx2 = 0, ly2 = 0;
        
    for ( int i = 0; i <= yvalues.length; i++ ) {
      
      if ( i > 0 && i < yvalues.length ) {
        // drawing normal lines
        lx1 = offsetx + prevx * w;
        ly1 = offsety + h2 + yvalues[lasti] * h2;
        lx2 = offsetx + x * w;
        ly2 = offsety + h2 + yvalues[currenti] * h2;
      } else if ( i == 0 ) {
        // drawing the end of the first line
        lx1 = offsetx + prevx * w;
        ly1 = offsety + h2 + currentyv * h2;
        lx2 = offsetx + x * w;
        ly2 = offsety + h2 + yvalues[currenti] * h2;
      } else if ( i == yvalues.length ) {
        // drawing the beginning of the last line
        x -= (1 - pc) * dx;
        lx1 = offsetx + prevx * w;
        ly1 = offsety + h2 + yvalues[lasti] * h2;
        lx2 = offsetx + x * w;
        ly2 = offsety + h2 + ( ( yvalues[lasti] * ( 1 - pc ) ) + ( yvalues[currenti] * pc ) ) * h2;
      }
      
      if (range == 0) {
        line(lx1, ly1, lx2, ly2);
      } else {
        noStroke();
        fill(0,50);
        beginShape();
        vertex(lx1, ly1 - hrange2);
        vertex(lx2, ly2 - hrange2);
        vertex(lx2, ly2 + hrange2);
        vertex(lx1, ly1 + hrange2);
        endShape(CLOSE);
        // line(lx1, ly1 - hrange2, lx2, ly2 - hrange2);
        // line(lx1, ly1 + hrange2, lx2, ly2 + hrange2);
        stroke(255, 0, 0);
        line(lx1, ly1, lx2, ly2);
      }
      
      prevx = x;
      x += dx;
      
      lasti = currenti;
      currenti++;
      if (currenti >= yvalues.length)
        currenti = 0;
      
    }
    
    // drawing the actual value
    fill( 255, 0, 0 );
    noStroke();
    ellipse( offsetx, offsety + h2 + currentyv * h2, 5, 5 );
  
  }
  
}
