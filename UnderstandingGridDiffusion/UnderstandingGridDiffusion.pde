int index;
int lastIndex;
float grid[][];
float lock[];
int range = 1;

float im1[];
float im2[];

boolean mpressed;

void setup() {

  size( 400, 400, P3D );

  grid = new float[ width * height ][ 2 ];
  lock = new float[ width * height ];
  for ( int i = 0; i < width * height; i++ ) {
    grid[ i ][ 0 ] = 1;
    grid[ i ][ 1 ] = 1;
    lock[ i ] = 0;
  }
  
  PImage i1 = loadImage( "img1.png" );
  i1.loadPixels();
  im1 = new float[ width * height * 3 ];
  for ( int i = 0; i < width * height; i++ ) {
    im1[ i * 3 ] = red( i1.pixels[ i ] );
    im1[ ( i * 3 ) + 1 ] = green( i1.pixels[ i ] );
    im1[ ( i * 3 ) + 2 ] = blue( i1.pixels[ i ] );
  }

  PImage i2 = loadImage( "img2.png" );
  i2.loadPixels();
  im2 = new float[ width * height * 3 ];
  for ( int i = 0; i < width * height; i++ ) {
    im2[ i * 3 ] = red( i2.pixels[ i ] );
    im2[ ( i * 3 ) + 1 ] = green( i2.pixels[ i ] );
    im2[ ( i * 3 ) + 2 ] = blue( i2.pixels[ i ] );
  }

  frameRate( 60 );
}

void draw() {

  int tmpi = index;
  index = lastIndex;
  lastIndex = tmpi;

  if ( mpressed ) {
    int p = mouseX + mouseY * width;
    float r = random( 0, 1 );
    for ( int y = 0; y < height; y++ ) {
      for ( int x = 0; x < width; x++ ) {
        
        if ( dist( x, y, mouseX, mouseY ) < 10 ) {
          
          int ti = x + y * width;
          
//          if ( r > 0.3f ) {
//            grid[ ti ][ lastIndex ] = 1;
//          } else {
//            grid[ ti ][ lastIndex ] = -1;
//          }

          grid[ ti ][ lastIndex ] = -1;
          
          lock[ ti ] = 2000;
          
        }
      }
    }


    mpressed = false;
  }

  float transfert = 0.5f;

  stroke( 255 );

  loadPixels();

  for ( int y = 0; y < height; y++ ) {
    for ( int x = 0; x < width; x++ ) {

      int i = x + y * width;

      float nbrs = 0;
      float totale = 0;

      if ( lock[ i ] == 0 ) {
        for ( int ny = y - range; ny <= y + range; ny++ ) {
          for ( int nx = x - range; nx <= x + range; nx++ ) {
            if ( nx < 0 || ny < 0 || nx >= width || ny >= height )
              continue;
            int ni = nx + ny * width;
            nbrs++;
            totale += grid[ ni ][ lastIndex ];
          }
        }

        grid[ i ][ index ] = grid[ i ][ lastIndex ] * ( 1 - transfert ) + ( totale / nbrs ) * transfert;
        if ( grid[ i ][ index ] > 1 ) {
          grid[ i ][ index ] = 1;
        } 
        else if ( grid[ i ][ index ] < -1 ) {
          grid[ i ][ index ] = -1;
        }
      }
      
      float pc = ( 1 + grid[ i ][ index ] ) * 0.5f;
      pixels[ i ] = color(
        im1[ i * 3 ] * ( 1 - pc ) + im2[ i * 3 ] * pc,
        im1[ ( i * 3 ) + 1 ] * ( 1 - pc ) + im2[ ( i * 3 ) + 1 ] * pc,
        im1[ ( i * 3 ) + 2 ] * ( 1 - pc ) + im2[ ( i * 3 ) + 2 ] * pc
      );
      
      /*
      // display effects control
      if ( grid[ i ][ index ] > 0 ) {
        pixels[ i ] = color( 10 + grid[ i ][ index ] * 245, 10, 10 );
      } 
      else if ( grid[ i ][ index ] < 0 ) {
        pixels[ i ] = color( 10, 10, 10 + grid[ i ][ index ] * -245 );
      } 
      else {
        pixels[ i ] = color( 10 );
      }
      */

      if ( lock[ i ] > 0 )
        lock[ i ]--;
    }
  }

  updatePixels();
}

void mousePressed() {
  // mpressed = true;
}

void mouseDragged() {
  int p = mouseX + mouseY * width;
  float r = random( 0, 1 );
  
  float v = -1;
  if ( mouseButton == 39 ) { // right click
    v = 1;
  }
  
  for ( int y = 0; y < height; y++ ) {
    for ( int x = 0; x < width; x++ ) {
      float d = dist( x, y, mouseX, mouseY );
      
      if ( d < 5 ) {  
        int ti = x + y * width;
        grid[ ti ][ lastIndex ] = v;
        lock[ ti ] = 200 * ( 1 - d / 5 );
      }
      
    }
  }
}

