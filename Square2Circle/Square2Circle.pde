// tableau de pvector
PVector points[];

// distance minimum et maximum des points de contrôle
float distance_min = -100;
float distance_max = 300;

// distance des points au centre (0,0)
float radius = 200;

void setup() {
  
  size( displayWidth, displayHeight );
  
  // création des 4 pvectors, à 45°, 135°, 225° et 315° 
  points = new PVector[] {
    new PVector( cos( PI * 1 / 4 ) * radius, sin( PI * 1 / 4 ) * radius ),
    new PVector( cos( PI * 3 / 4 ) * radius, sin( PI * 3 / 4 ) * radius ),
    new PVector( cos( PI * 5 / 4 ) * radius, sin( PI * 5 / 4 ) * radius ),
    new PVector( cos( PI * 7 / 4 ) * radius, sin( PI * 7 / 4 ) * radius )
  };
  
}

void draw() {
  
  // normalisation de la position de la souris entre 0 et 1
  float rx = mouseX * 1.f / width;
  
  // calcul de la distance des points de contrôle
  float dist = distance_min + rx * ( distance_max - distance_min );

  background(0);
  stroke(255);
  noFill();
  
  pushMatrix();
  
  // translation du centre de dessin au centre de la fenêtre
  translate( width/2, height/2 );
  
  strokeWeight(1);
  stroke(255,150);
  ellipse( 0,0,radius*2,radius*2 );
  float sq2 = sqrt(2);
  rect( -radius/sq2,-radius/sq2,radius/sq2*2,radius/sq2*2 );
  
  float a = PI * 1 / 4;
  
  // affichage des lignes et courbes
  for ( int i = 0; i < 4; ++i ) {
    
    // point suivant > une fois à 3, on lie au pvector 0
    int j = (i + 1) % 4;
    
    // angle des points de contrôle
    float a0 = a + PI * 0.5;
    float a1 = a;
    
    // coordonnées des points de contrôle
    float c0 = cos( a0 ) * dist;
    float s0 = sin( a0 ) * dist;
    float c1 = cos( a1 ) * dist;
    float s1 = sin( a1 ) * dist;
    
    // affichage des pointts de contrôle
    strokeWeight(1);
    stroke(255,0,0,150);
    line( points[i].x, points[i].y, points[i].x + c0, points[i].y + s0 );
    stroke(0,255,0,150);
    line( points[j].x, points[j].y, points[j].x + c1, points[j].y + s1 );
    stroke(255);
    
    // dessin de la courbe
    strokeWeight(3);
    bezier( 
      points[i].x, points[i].y,
      points[i].x + c0, points[i].y + s0,
      points[j].x + c1, points[j].y + s1,
      points[j].x, points[j].y);
    
    a += PI * 0.5;
    
  }
  
  fill(255);
  noStroke();
  
  // affichage des points
  a = PI * 1 / 4;
  for ( int i = 0; i < 4; ++i ) {
    rect( points[i].x-3, points[i].y-3, 6, 6 );
    rect( points[i].x + cos( a + PI * 0.5 ) * dist - 1.5, points[i].y + sin( a + PI * 0.5 ) * dist - 1.5, 3, 3 );
    rect( points[i].x + cos( a + PI * 1.5 ) * dist - 1.5, points[i].y + sin( a + PI * 1.5 ) * dist - 1.5, 3, 3 );
    a += PI * 0.5;
  }
  
  popMatrix();
  
  text( "distance: " + dist, 10, 25 );
  
}
