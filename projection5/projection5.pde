import java.util.*; 

// view rotation
private PVector rot;
// inverted view rotation (to draw ellipse parallel to camera)
private PVector tor;

private int plnum = 8;
private float plgap = 90;
private Plane[] planes;
private UVPoint[][] projected;

// random points projected on the plane
private int ptnum = 200;
private PVector[] pt;

// used in plane to calculate U & V vector
public static final PVector UP = new PVector( 0,1,0 );

private float smoothing;
private float maxdist2project;

private PImage tex;

public void setup() {

  size( 800, 600, P3D );
  
  PVector planesOr = new PVector( 1,1,1 );
  planesOr.normalize();
  planes = new Plane[ plnum ];
  projected = new UVPoint[ plnum ][ 0 ];
  float ppos = plgap * ( plnum - 1 ) * -0.5;
  for ( int i = 0; i < plnum; i++ ) {
    planes[ i ] = new Plane( planesOr.x * ppos, planesOr.y * ppos, planesOr.z * ppos );
    planes[ i ].normale.set( planesOr );
    planes[ i ].update();
    ppos += plgap;
  }
  
  rot = new PVector();
  tor = new PVector();
  pt = new PVector[ ptnum ];
  for ( int i = 0; i < pt.length; i++ ) {
    pt[ i ] = new PVector( random( -250, 250 ), random( -250, 250 ), random( -350, 350 ) );
  }
  
  tex = loadImage( "texture.jpg" );
  textureWrap( REPEAT ); 
  
  smoothing = 0.6;
  maxdist2project = plgap * 0.49;
  
}

public void draw() {

  rot.x = ( mouseY - height * 0.5) / 100;
  rot.y = ( -mouseX + width * 0.5) / 100;
  tor.set( rot );
  tor.mult( -1 );
  
  for ( int i = 0; i < plnum; i++ ) {
    planes[ i ].viewrot = tor;
  }
  
  
  background( 10 );

  pushMatrix();
  translate( width * 0.5, height * 0.5, -width * 0.5 );  
  rotateX( rot.x );
  rotateY( rot.y );
  rotateZ( rot.z );
  
  // 3D axis
  strokeWeight( 3 );
  stroke( 255, 0, 0 );
  line( 0, 0, 0, 50, 0, 0 );
  stroke( 0, 255, 0 );
  line( 0, 0, 0, 0, 50, 0 );
  stroke( 0, 0, 255 );
  line( 0, 0, 0, 0, 0, 50 );
  strokeWeight( 1 );
  
  for ( int i = 0; i < plnum; i++ ) {
    planes[ i ].draw( tor );
    projected[ i ] = planes[ i ].projectPoints( pt, maxdist2project );
    noStroke();
    fill( 255, 0, 0, 150 );
    for ( int j = 0; j < projected[ i ].length; j++ ) {
      planes[ i ].drawpt( projected[ i ][ j ].x, projected[ i ][ j ].y, projected[ i ][ j ].z, 4 );
    }
    if ( projected[ i ].length > 1 ) {
      UVPoint barycenter = sortUVpts( projected[ i ] );
      UVFace[] projfaces = new UVFace[ projected[ i ].length ];
      float cr = ( 255.f * i / ( plnum - 1 ) );
      for ( int a = 0; a < projected[ i ].length; a++ ) {
        int b = a + 1;
        if ( b >= projected[ i ].length )
          b = 0;
        projfaces[ a ] = new UVFace( projected[ i ][ a ], projected[ i ][ b ], barycenter );
        projfaces[ a ].stroked = false;
        projfaces[ a ].filled = true;
        projfaces[ a ].setColor( 255, 255, 255, 100 );
      }
      beginShape( TRIANGLES );
      texture( tex );
      for ( int j = 0; j < projfaces.length; j++ )
        projfaces[ j ].printVertices();
      endShape();
    }
    
  }
  
  noFill();
  stroke( 255, 150 );
  for ( int i = 0; i < pt.length; i++ ) {
      planes[ 0 ].drawpt( pt[ i ] );
  }
  
  popMatrix();
  
  fill( 255 );
  text( frameRate, 10, 25 );
  text( "smooth: " + smoothing, 10, 40 );
  text( "max dist: " + maxdist2project, 10, 55 );
  
}

public void keyPressed() {
  
  if ( keyCode == 38 )
    smoothing += 0.05;
    
  else if ( keyCode == 40 )
    smoothing -= 0.05;
    
  else if ( key == 'r' ) {
    for ( int i = 0; i < pt.length; i++ ) {
      pt[ i ].set( random( -250, 250 ), random( -250, 250 ), random( -350, 350 ) );
    }
  }
  
  else
    println( keyCode );
  
}

